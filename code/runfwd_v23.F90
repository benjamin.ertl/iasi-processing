!     Last change:  BLA  14 Oct 2012    4:18 pm
program runfwd_v23

use prffwd23_m
use globfwd23, only : deri,npp,iunit_fwd,outpfad_fwd,fwdtofiledec

implicit none

! Delete previous output
!csv call system('del /q '//trim(outpfad_fwd)//'/*.*')
call system('rm -rf '//trim(outpfad_fwd)//'/*.*')

call initfwd()

!fwdtofiledec = .false. ! suppress RUNXX-output if switched on

call alloc_fwd()

print *,'Calling proffwd...'
call prffwd(0)
print *,'Forward run finished!'

print *,'Writing spectra to file...'
! write spectrum to file
call tofile_spcxils(outpfad_fwd,'SPEC',finspecxils)

! write measured spectrum to file
call tofile_spcxils(outpfad_fwd,'MESS',messxils)
print *,'Done!'

! write derivatives to file
print *,'Writing derivatives to file...'
if (deri%sw_vmr) then
    call tofile_dvmr(outpfad_fwd,'DVMR',dspec_dvmr)
end if
if (deri%sw_T) then
    call tofile_dT(outpfad_fwd,'DTEM',dspec_dT)
end if
if (deri%sw_HITgam) then
    call tofile_dHIT(outpfad_fwd,'DHGA',dspec_dHITgam)
end if
if (deri%sw_HITint) then
    call tofile_dHIT(outpfad_fwd,'DHIN',dspec_dHITint)
end if
if (deri%sw_LOS) then
    call tofile_spcxils(outpfad_fwd,'DLOS',dspec_dLOS)
end if
if (deri%sw_ILSapo) then
    call tofile_spcxils(outpfad_fwd,'DAPO',dspec_dILS(:,1))
end if
if (deri%sw_ILSphas) then
    call tofile_spcxils(outpfad_fwd,'DPHA',dspec_dILS(:,2))
end if
if (deri%sw_shift) then
    call tofile_spcxils(outpfad_fwd,'DSHI',dspec_dshift)
end if
if (deri%sw_ofset) then
    call tofile_spcxils(outpfad_fwd,'DOFF',dspec_dofset)
end if
if (deri%sw_chan) then
    call tofile_dchan(outpfad_fwd,'DCHA',dspec_dchan)
end if
print *,'Done!'

print *,'Deallocating...'
call dealloc_fwd()
print *,'Done!'

! PP: Quittiere Beendigung des Programmes
call ppquittung()

end program runfwd_v23









!====================================================================
!  tofile_dchan: write chan derivative to file
!====================================================================
subroutine tofile_dchan(outpfad,dateiext,dspec_dchan)

use globfwd23 ,only : n_mw,n_chan,mw,wvskal

implicit none
character(len=*),intent(in) :: outpfad,dateiext
real,dimension(mw(n_mw)%rb,2,n_chan),intent(in) :: dspec_dchan

character(2) :: extb
integer :: j,k,l,m

! Speichern des Feldes
do j = 1,n_mw
    write (extb,'(I2.2)') j
    open (10,file = trim(outpfad)//'/'//dateiext//extb//'.DAT',status = 'replace')
    do k = 1,mw(j)%pts
        write (10,'(ES15.9,80(1X,ES12.4))') &
          mw(j)%firstnue + wvskal%dnue * (k - 1) &
            ,((dspec_dchan(mw(j)%lb+k-1,l,m),l = 1,2),m = 1,n_chan)
    end do
    close (10)
end do

end subroutine tofile_dchan



!====================================================================
!  tofile_dHIT: write vmr derivative to file
!====================================================================
subroutine tofile_dHIT(outpfad,dateiext,dspec_dHIT)

use globfwd23 ,only : n_mw,mw,wvskal,deri

implicit none
character(len=*),intent(in) :: outpfad,dateiext
real,dimension(mw(n_mw)%rb,deri%n_vmr),intent(in) :: dspec_dHIT

character(2) :: exta,extb
integer :: i,j,k

! Speichern des Feldes
do i = 1,deri%n_vmr
    write (exta,'(I2.2)') i
    do j = 1,n_mw
        write (extb,'(I2.2)') j
        open (10,file = trim(outpfad)//'/'//dateiext//exta//extb//'.DAT',status = 'replace')
        do k = 1,mw(j)%pts
            write (10,'(ES15.9,2(1X,ES12.4))') &
              mw(j)%firstnue + wvskal%dnue * (k - 1),dspec_dHIT(mw(j)%lb+k-1,i)
        end do
        close (10)
    end do
end do

end subroutine tofile_dHIT



!====================================================================
!  tofile_dT: write T derivative to file
!====================================================================
subroutine tofile_dT(outpfad,dateiext,dspec_dT)

use globfwd23 ,only : n_mw,n_lev,mw,wvskal

implicit none
character(len=*),intent(in) :: outpfad,dateiext
real,dimension(mw(n_mw)%rb,n_lev),intent(in) :: dspec_dT

character(2) :: extb
integer :: j,k,l

! Speichern des Feldes
do j = 1,n_mw
    write (extb,'(I2.2)') j
    open (10,file = trim(outpfad)//'/'//dateiext//extb//'.DAT',status = 'replace')
    do k = 1,mw(j)%pts
        write (10,'(ES15.9,80(1X,ES12.4))') &
          mw(j)%firstnue + wvskal%dnue * (k - 1),(dspec_dT(mw(j)%lb+k-1,l),l = 1,n_lev)
    end do
    close (10)
end do

end subroutine tofile_dT



!====================================================================
!  tofile_dvmr: write vmr derivative to file
!====================================================================
subroutine tofile_dvmr(outpfad,dateiext,dspec_dvmr)

use globfwd23 ,only : n_mw,n_lev,mw,wvskal,deri

implicit none
character(len=*),intent(in) :: outpfad,dateiext
real,dimension(mw(n_mw)%rb,n_lev,deri%n_vmr),intent(in) :: dspec_dvmr

character(2) :: exta,extb
integer :: i,j,k,l

! Speichern des Feldes
do i = 1,deri%n_vmr
    write (exta,'(I2.2)') i
    do j = 1,n_mw
        write (extb,'(I2.2)') j
        open (10,file = trim(outpfad)//'/'//dateiext//exta//extb//'.DAT',status = 'replace')
        do k = 1,mw(j)%pts
            write (10,'(ES15.9,80(1X,ES12.4))') &
              mw(j)%firstnue + wvskal%dnue * (k - 1),(dspec_dvmr(mw(j)%lb+k-1,l,i),l = 1,n_lev)
        end do
        close (10)
    end do
end do

end subroutine tofile_dvmr



!====================================================================
!  tofile_spcxils: write convolved spectrum to file
!====================================================================
subroutine tofile_spcxils(outpfad,dateiext,specxils)

use globfwd23 ,only : n_mw,mw,wvskal

implicit none
character(len=*),intent(in) :: outpfad,dateiext
real,dimension(mw(n_mw)%rb),intent(in) :: specxils

character(2) :: extb
integer :: j,k

! Speichern des Feldes
do j = 1,n_mw
    write (extb,'(I2.2)') j
    open (10,file = trim(outpfad)//'/'//dateiext//extb//'.DAT',status = 'replace')
    do k = 1,mw(j)%pts
        write (10,'(ES15.9,2(1X,ES12.4))') &
          mw(j)%firstnue + wvskal%dnue * (k - 1),specxils(mw(j)%lb+k-1)
    end do
    close (10)
end do

end subroutine tofile_spcxils

