!     Last change:  BLA  28 Mar 2008    3:14 pm
module globsol23

use globfwd23, only: max_n_mw,maxsollines

implicit none

real(8),parameter :: Tsol = 6000.0d0   ! solar Planck T (to generate calibrated ordinate, to determine emission contrast)
real(8),parameter :: omega = 2.6836d-6 ! Solar rotation
real(8),parameter :: tlow = 4000.0d0   ! calculation of Minnaert offset

type sollinedata
    sequence
    integer,dimension(max_n_mw) :: lb,rb
    real(8),dimension(maxsollines) :: nue
    real,dimension(maxsollines) :: strength
    real,dimension(maxsollines) :: gausswidth
    real,dimension(maxsollines) :: sklor
    real,dimension(maxsollines) :: variStrength
    real,dimension(maxsollines) :: variWidth
end type

type (sollinedata) :: sollin

end module globsol23
