!     Last change:  BLA   7 Apr 2008    5:09 pm
module globsdg23

integer,parameter :: nmax_sdg_x = 4000
integer,parameter :: nmax_sdg_nue = 120
integer,parameter :: nmax_sdg_gam0 = 50
integer,parameter :: nmax_sdg_gam2 = 20
integer,parameter :: nmax_sdg_beta = 20
real(8),parameter :: gam2max = 0.5d0  ! relative to gam0
real(8),parameter :: betamax = 4.0d0  ! relative to gam0
real(8),parameter :: dx_sdg = 0.02
real(8),parameter :: pi = 3.141592653589793d0

real,dimension(nmax_sdg_nue,nmax_sdg_gam0,nmax_sdg_gam2,nmax_sdg_beta) :: sdgfeld

contains

!====================================================================
!  sdg_beta: trafo for beta axis
!====================================================================
real(8) function sdg_beta(ibeta,gam0)

implicit none

integer,intent(in) :: ibeta
real(8),intent(in) :: gam0

sdg_beta = betamax * gam0 * real(ibeta,8) / real(nmax_sdg_beta - 1,8)

end function sdg_beta

!====================================================================
!  sdg_beta_inv: inverse trafo for beta axis
!====================================================================
subroutine sdg_beta_inv(beta,gam0,ibeta,betarest)

implicit none

real(8),intent(in) :: beta,gam0
integer,intent(out) :: ibeta
real(8),intent(out) :: betarest

real (8) :: betawert

betawert = real(nmax_sdg_beta - 1,8) * beta / (gam0 * betamax)
ibeta = int(betawert)
betarest = betawert - real(ibeta,8)

if (ibeta .gt. nmax_sdg_beta - 2) then
    !call sdg_warnout('SDG: requested beta requires extrapolation!',0)
    ibeta = nmax_sdg_beta - 2
    betarest = 1.0d0
end if

end subroutine sdg_beta_inv

!====================================================================
!  sdg_gam0: trafo for gam0 axis
!====================================================================
real(8) function sdg_gam0(igam0,gauhwhm)

implicit none

integer,intent(in) :: igam0
real(8),intent(in) :: gauhwhm

sdg_gam0 = gauhwhm * exp(0.25d0 * real(igam0 - 25,8))

end function sdg_gam0

!====================================================================
! sdg_gam0_inv: inverse trafo for gam0 axis
!====================================================================
subroutine sdg_gam0_inv(gam0,gauhwhm,igam0,gam0rest)

implicit none

real(8),intent(in) :: gam0,gauhwhm
integer,intent(out) :: igam0
real(8),intent(out) :: gam0rest

real(8) :: gam0wert

gam0wert = 25.0d0 + 4.0d0 * log(gam0 / gauhwhm)
igam0 = int(gam0wert)
gam0rest = gam0wert - real(igam0,8)

if (igam0 .gt. nmax_sdg_gam0 - 2) then
    call sdg_warnout('SDG: requested gam0 requires extrapolation!',0)
end if

end subroutine sdg_gam0_inv

!====================================================================
!  sdg_gam2: trafo for gam2 axis
!====================================================================
real(8) function sdg_gam2(igam2,gam0)

implicit none

integer,intent(in) :: igam2
real(8),intent(in) :: gam0

sdg_gam2 = gam2max * gam0 * real(igam2,8) / real(nmax_sdg_gam2 - 1,8)

end function sdg_gam2

!====================================================================
!  sdg_gam2_inv: inverse trafo for gam2 axis
!====================================================================
subroutine sdg_gam2_inv(gam2,gam0,igam2,gam2rest)

implicit none

real(8),intent(in) :: gam2,gam0
integer,intent(out) :: igam2
real(8),intent(out) :: gam2rest

real (8) :: gam2wert

gam2wert = real(nmax_sdg_gam2 - 1,8) * gam2 / (gam0 * gam2max)
igam2 = int(gam2wert)
gam2rest = gam2wert - real(igam2,8)

if (igam2 .gt. nmax_sdg_gam2 - 2) then
    !call sdg_warnout('SDG: requested gam2 requires extrapolation!',0)
    igam2 = nmax_sdg_gam2 - 2
    gam2rest = 1.0d0
end if

end subroutine sdg_gam2_inv

!====================================================================
!  sdg_nue: trafo for nue-axis
!====================================================================
real(8) function sdg_nue(inue)

implicit none

integer,intent(in) :: inue

sdg_nue = 0.4d0 * (EXP(0.04d0 * real(inue,8)) - 1.0d0)

end function sdg_nue

!====================================================================
!  sdg_nue_inv: inverse trafo for nue-axis
!====================================================================
subroutine sdg_nue_inv(nuerel,inue,nuerest)

implicit none

real(8),intent(in) :: nuerel
integer,intent(out) :: inue
real(8),intent(out) :: nuerest

real(8) :: nuewert

nuewert = 25.0d0 * log(2.5d0 * abs(nuerel) + 1.0d0)

inue = int(nuewert)
nuerest = nuewert - real(inue,8)

end subroutine sdg_nue_inv

!====================================================================
!  Warnung rausschreiben und Programm evtl. beenden
!====================================================================
subroutine sdg_warnout(text,predec)

implicit none

character(len=*),intent(in) :: text
integer,intent(in) :: predec
integer :: intdec

print *,'Warning:'
print *, trim(text)
print *,'To shutdown program: enter 0, to go on: enter 1.'
read *, intdec
if (intdec .eq. 0 .or. predec .eq. 0) stop

end subroutine sdg_warnout


end module globsdg23
