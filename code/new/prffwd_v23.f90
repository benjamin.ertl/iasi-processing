!     Last change:  BLA  14 Oct 2012    4:38 pm
! Forward code for ground based FTIR remote sensing in the infrared
! Version 1.0, date 06-05-24

module prffwd23_m

use globfwd23
use globlev23

implicit none

! arrays used in forward calculation
real,dimension(:),allocatable,save :: gndbb,gndbbp1K,soltrm,solbb,atmtrm,spec,emiss,epsgnd,ggs,specxils,messxils &
  ,finspecxils,chanfeld,dspecdshift
real,dimension(:,:),allocatable,save :: trm_speci,ils_mw,trmslant_lev,trmslantggs_lev,trmcumslant_lev,emiss_lev
real,dimension(:,:,:),allocatable,save :: ilsparts
real,dimension(:,:,:,:),allocatable,save :: tau_lev
real,dimension(:,:),allocatable,save :: tau_h2ocont

! derivatives
real,dimension(:,:,:),allocatable,save :: dspec_dvmr,dspec_dchan
real,dimension(:,:),allocatable,save :: dspec_dT,dspec_dHITgam,dspec_dHITint,dspec_dILS,dspec_dbase
real,dimension(:),allocatable,save :: dspec_dTgnd,dspec_dLOS,dspec_dsolshi,dspec_dsolint,dspec_dofset,dspec_dshift

! aux arrays for calculation of derivatives
real,dimension(:,:),allocatable :: wrkils_mw
real,dimension(:),allocatable :: wrktau,wrkemiss,wrksumtau,wrkspeca,wrkspecb,wrkspecc &
  ,wrkspecxils,wrkchanfeld,dtaudvmrslant,dtaudTslant,wrkemisslev

private

! public subroutines (accessible by retrieval)
public prffwd,initfwd,alloc_fwd,dealloc_fwd,ppquittung

! public arrays (accessible by retrieval)
public finspecxils,messxils,dspec_dvmr,dspec_dchan,dspec_dT,dspec_dHITgam,dspec_dHITint &
  ,dspec_dILS,dspec_dTgnd,dspec_dLOS,dspec_dofset,dspec_dshift



contains



!====================================================================
!  prffwd: execute forward run
!====================================================================
subroutine prffwd(nfwdrun)

use globfwd23 ,only : max_n_base
use globlines23 ,only : anysdgdec

implicit none

integer,intent(in) :: nfwdrun

logical :: lblbindec,sdgbindec
integer :: i,j,k
real,parameter :: epsapo = 0.01
real,parameter :: epsphas = 0.01
real,parameter :: epschan = 0.01
real,parameter :: epsT = 1.0
real :: faktor
real,dimension(max_n_base) :: knotwert

if (nfwdrun .eq. 0 .or. calcfwddec) then
    ! Read measured spectra, initialize fast ILS calculation, init eps_gnd
    if (nfwdrun .eq. 0) then
        ! read measurement file for each MW
        if (messdec) then
            print *,'Reading measured spectra...       '
            do i = 1,n_mw
                call load_mess(messdatei(i),messkind,mw(i)%pts,mw(i)%firstnue &
                  ,wvskal%dnue,messxils(mw(i)%lb:mw(i)%rb))
            end do
        end if

        ! initialize fast ILS calculation
        print *,'Initialising fast ILS calculation...  '
        call init_ils(ilsparts)
        call tofile_ilsparts(ilsparts)
        print *,'Done!'

        ! initialize eps_gnd
        ! Initialize eps_gnd
        print *,'Initializing eps_gnd...'
        call init_epsgnd()

    end if

    if (nfwdrun .gt. 0 .and. h2ofitdec) then
        print *,'Updating H2O VMR...                     '
        lev%h2o_ppmv(1:n_lev) = lev%vmr_ppmv(1:n_lev,ptrh2o)
    end if

    if (nfwdrun .gt. 0 .and. (deri%sw_T .or. h2ofitdec)) then
        ! Setup hydrostatic atmosphere (calculate altitude levels from p stratification)
        print *,'Regenerating hydrostatic equilibrium...   '
        call hydrostatb(obs%h_m,obs%lat_rad,lev%p_Pa(1:n_lev),lev%T_K(1:n_lev)&
          ,lev%h2o_ppmv(1:n_lev),lev%h_m(1:n_lev),lev%n_cbm(1:n_lev),lev%colair(1:n_lev) &
          ,lev%colair_do(1:n_lev),lev%colair_up(1:n_lev))

        ! Raytracing
        print *,'Raytracing...                           '
        call raytrace(lev%h_m(1:n_lev),lev%p_Pa(1:n_lev),lev%T_K(1:n_lev) &
          ,obs%lat_rad,obs%azi_rad,obs%sza_rad,obs%astrodec,lev%sza_rad(1:n_lev))
    end if

    if (nfwdrun .eq. 0) then
        ! Initialise species, read HITRAN data, initialise SDG calculation
        print *,'Initializing spezies...           '
        call initspecies(specidatei)
        print *,'Reading Voigt table...            '
        call read_voigttable()
        print *,'Reading HITRAN line data...       '
        ! Check whether binary lbl-file is available for current job
        call init_lblkennung(lblbindec)
        if (lblbindec) then
            ! Read lbl data from current binary file
            print *,'Reading from binary linedata file ...'
            call read_hitbin_lbl()
        else
            ! Read lbl data from original HITRAN file
            print *,'Reading from HITRAN ASCII files...'
            call read_hitran_lbl()
            ! Generate binary lbl data for use in future runs
            print *,'Generating binary linedata file...'
            call write_hitbin_lbl()
        end if
        ! check whether binary SDG-file is available
        if (anysdgdec) then
            sdgdatei = 'SDGTABLE.BIN'
            call sdg_checktable(sdgbindec)
            if (sdgbindec) then
                print *,'Reading SDG table from file...'
                call sdg_readtable()
            else
                print *,'Generating SDG table values...'
                call sdg_init()
                print *,'Writing SDG table values to file...'
                call sdg_writetable()
            end if
        end if

        ! Initialisierung des xsc-Feldes
        if (n_tau_xsc .gt. 0) then
            print *,'Reading HITRAN XSC data...'
            call read_hitran_xsc(lev%T_K(1:n_lev),lev%p_Pa(1:n_lev),tau_lev(:,:,:,n_tau_lbl+1:n_tau_lbl+n_tau_xsc))
        end if

        ! Initialisierung des Wasserdampf-Kontinuums        
        do i = 1,n_lev
            if (lev%p_Pa(i) .gt. 1.0d4) then
                call make_h2o_cont(lev%T_K(i),lev%p_Pa(i),lev%h2o_cont(i),tau_h2ocont(:,i))
            else
                tau_h2ocont(:,i) = 0.0            
            end if
        end do

        ! Initialisierung des lbl-Feldes
        if (n_tau_lbl .gt. 0) then
            ! initialise level-dependent lbl pointers and decide which lines in the MW rim can be neglected
            print *,'Initialize pointers for MW and rim lines...'
            call init_lblcalc()
            print *,'Preparing lbl-calculation...'
            do i = 1,n_tau_lbl
                do j = 1,n_lev
                    call init_tau_lbl(i,lev%T_K(j),lev%p_Pa(j),lev%colair(j) &
                      ,lev%sza_rad(j),lev%h2o_ppmv(j),lev%vmr_ref_ppmv(j,i),lev%vmr_ppmv(j,i),lev%wind_scal(j),tau_lev(1,:,j,i))
                    call init_tau_lbl(i,lev%T_K(j) + real(epsT,8),lev%p_Pa(j),lev%colair(j) &
                      ,lev%sza_rad(j),lev%h2o_ppmv(j),lev%vmr_ref_ppmv(j,i),lev%vmr_ppmv(j,i),lev%wind_scal(j),tau_lev(2,:,j,i))
                    print *,'Species, level: ',i,j
                end do
            end do
            tau_lev(2,:,:,1:n_tau_lbl) = (tau_lev(2,:,:,1:n_tau_lbl) - tau_lev(1,:,:,1:n_tau_lbl)) / real(epsT,8)
        end if

        ! Set reference Ts
        lev%T_K_ref(1:n_lev) = lev%T_K(1:n_lev)

    end if

    ! calculate transmission for each species
    ! calculate total atm transmission
    ! Note: trmslant_lev array is temporarily filled with optical densities !!
    print *,'Calculating transmission per species... '
    trmslant_lev = 0.0
    do i = 1,n_tau
        wrktau = 0.0
        do j = 1,n_lev
            faktor = 1.0e-6 * lev%colair(j) * lev%vmr_ppmv(j,i) / cos(lev%sza_rad(j))
            wrktau(:) = wrktau(:) + faktor * (tau_lev(1,:,j,i) + (lev%T_K(j) - lev%T_K_ref(j)) * tau_lev(2,:,j,i))
            trmslant_lev(:,j) = trmslant_lev(:,j) + faktor * (tau_lev(1,:,j,i) + (lev%T_K(j) - lev%T_K_ref(j)) * tau_lev(2,:,j,i))
        end do
        trm_speci(:,i) = exp(-wrktau(:))
    end do

    ! H2O continuum
    do i = 1,n_lev
        if (lev%p_Pa(i) .gt. 1.0d4) then
            trmslant_lev(:,i) = trmslant_lev(:,i) + 1.0d-4 * lev%colair(i) * lev%h2o_cont(i) &
              / cos(lev%sza_rad(i)) * tau_h2ocont(:,i) 
        end if
    end do

    ! Transmission fuer atm Gegenstrahlung (andere Strahlrichtung als fuer die Nadir-Beobachtung)
    faktor = cos(lev%sza_rad(1)) / cos(ggswinkel)
    trmslantggs_lev = exp(-trmslant_lev * faktor)

    ! Calculate level transmissions
    trmslant_lev = exp(-trmslant_lev)

    ! calculate total atm transmission and cumulative trm along slant path
    ! Note: trmslant_lev array is temporarily filled with optical densities !!
    print *,'Calculating atm transmission...'
    trmcumslant_lev(:,n_lev) = trmslant_lev(:,n_lev)
    do i = n_lev - 1,1,-1
        trmcumslant_lev(:,i) = trmcumslant_lev(:,i+1) * trmslant_lev(:,i)
    end do
    atmtrm = trmcumslant_lev(:,1)

    ! calculate atmospheric self-emission
    print *,'Calculating atm self-emission...'

    call make_bbratio(lev%T_K(n_lev),obs%Tgnd_K,wrkspeca)
    emiss_lev(:,n_lev) = wrkspeca * (1.0 - trmslant_lev(:,n_lev))
    emiss = emiss_lev(:,n_lev)
    do i = n_lev-1,1,-1
        call make_bbratio(lev%T_K(i),obs%Tgnd_K,wrkspeca)
        emiss_lev(:,i) = wrkspeca * (1.0 - trmslant_lev(:,i))
        emiss = emiss + emiss_lev(:,i) * trmcumslant_lev(:,i+1)
    end do

    ! calculate Gegenstrahlung ggs
    print *,'Calculating atm ggs...'
    call make_bbratio(lev%T_K(1),obs%Tgnd_K,wrkspeca)
    ggs = wrkspeca * (1.0 - trmslantggs_lev(:,1)) ! ggs enthaelt nach Schleifendurchlauf die gesamte ggs-Emission
    wrkspecb = trmslantggs_lev(:,1)   ! wrkspecb enthaelt die kumulative ggs-Transmission
    do i = 2,n_lev
        call make_bbratio(lev%T_K(i),obs%Tgnd_K,wrkspeca)
        ggs = ggs + wrkspeca * wrkspecb * (1.0 - trmslantggs_lev(:,i))
        wrkspecb = wrkspecb * trmslantggs_lev(:,i)
    end do

    ! calculate BB-curve for Tgnd
    if (nfwdrun .eq. 0 .or. deri%sw_T) then
        call make_bb(obs%Tgnd_K,gndbb)
        if (deri%sw_T) call make_bb(obs%Tgnd_K + 1.0d0,gndbbp1K)
    end if

    ! calculate final fine-grid spectra for each geometry, apply shift
    print *,'Calculating shifted fine-grid spectra...'
    call make_spec(gndbb,emiss,epsgnd,ggs,atmtrm,mw(1:n_mw)%centernue,mw(1:n_mw)%shift,spec,dspecdshift)

    ! calculate ILS for each MW
    if (nfwdrun .eq. 0 .or. deri%sw_ILSapo .or. deri%sw_ILSphas) then
        print *,'Calculating ILS...'
        do i = 1,n_mw
            call make_ils(mw(i)%centernue,instr%apo,instr%phas,ilsparts,ils_mw(:,i))
        end do
        print *,'Done!'
    end if

    ! convolve fine-grid spectra with ILS
    print *,'Convolution with ILS...                  '
    call convolve(spec,ils_mw,specxils)

    ! generate channeling
    if (nfwdrun .eq. 0 .or. deri%sw_chan) then
        if (n_chan .gt. 0) then
            print *,'Generating channel spectrum...           '
            call channeling(instr%chanlambda(1:n_chan),instr%chanamp(1:n_chan,1:n_mw),chanfeld)
        else
            chanfeld = 1.0
        end if
    end if

    ! apply offset + channeling
    do i = 1,n_mw
        finspecxils(mw(i)%lb:mw(i)%rb) = specxils(mw(i)%lb:mw(i)%rb) &
          * chanfeld(mw(i)%lb:mw(i)%rb) + instr%ofset(i)
    end do

    ! add noise to forward calculation
    if (instr%noisdec) call add_noise(instr%noise(1:n_mw),finspecxils)

    ! Write forward arrays to file
    if (fwdtofiledec) then
        print *,'Writing forward output to file ...'
        ! write atm state and SZA_lev to file
        call tofile_raytra(nfwdrun,lev%p_Pa(1:n_lev),lev%colair(1:n_lev) &
          ,lev%T_K(1:n_lev),lev%h_m(1:n_lev),lev%h2o_ppmv(1:n_lev),lev%sza_rad(1:n_lev))
        ! write optical depths and transmission to file
        call tofile_tau(nfwdrun,tau_lev)
        call tofile_trm(nfwdrun,trm_speci,atmtrm)
        ! write ground emission and total atm emission to file
        call tofile_spc(nfwdrun,'_BBG',gndbb)
        call tofile_spc(nfwdrun,'_EMIS',emiss)
        call tofile_spc(nfwdrun,'_EPSG',epsgnd)
        call tofile_spc(nfwdrun,'_GGS',ggs)
        ! write fine-grid spectra to file
        call tofile_spc(nfwdrun,'_SPEC',spec)
        call tofile_spc(nfwdrun,'_DSHI',dspecdshift)
        ! write ILS to file
        call tofile_ils(nfwdrun,ils_mw)
        ! write specxils to file (does not include baseline, channeling, offset)
        call tofile_spcxils(nfwdrun,'_SPCXILS',specxils)
        ! write channeling to file
        call tofile_spcxils(nfwdrun,'_CHAXILS',chanfeld)
        ! write final spectrum to file
        call tofile_spcxils(nfwdrun,'_FINXILS',finspecxils)
        ! write measured spectrum to file
        call tofile_spcxils(nfwdrun,'_MESXILS',messxils)
    end if
end if

! derivative section=================================================

! calculate vmr derivatives
if (deri%sw_vmr) then
    print *,'Calculating VMR derivatives...'
    do k = 1,deri%n_vmr
        wrkemiss = epsgnd
        do j = 1,n_lev
            ! recalculate slant tau in current level
            faktor = 1.0e-6 * lev%colair(j) / cos(lev%sza_rad(j))

            dtaudvmrslant = faktor * (tau_lev(1,:,j,deri%npoint_fwd(k)) &
              + (lev%T_K(j) - lev%T_K_ref(j)) * tau_lev(2,:,j,deri%npoint_fwd(k)))

            call make_bbratio(lev%T_K(j),obs%Tgnd_K,wrkspecb)
            wrkspeca = (wrkspecb - wrkemiss) * dtaudvmrslant * trmcumslant_lev(:,j)

            ! coadd emission from current level to cumulative emission from all levels below
            ! target level
            wrkemiss = wrkemiss * trmslant_lev(:,j) + emiss_lev(:,j)

            ! calculate final VMR-derivative
            call make_specc(gndbb,wrkspeca,mw(1:n_mw)%centernue,mw(1:n_mw)%shift,wrkspecb)
            call convolve(wrkspecb,ils_mw,wrkspecxils)
            dspec_dvmr(:,j,k) = wrkspecxils * chanfeld
        end do
    end do
    print *,'Done!'
end if

! calculate shift derivatives
if (deri%sw_shift) then
    print *,'Calculating shift derivatives...'
    call convolve(dspecdshift,ils_mw,wrkspecxils)
    dspec_dshift = wrkspecxils * chanfeld
    print *,'Done!'
end if

! calculate offset derivative
if (deri%sw_ofset) then
    print *,'Calculating offset derivative...'
    dspec_dofset = 1.0
    print *,'Done!'
end if

! calculate ILS derivatives
if (deri%sw_ILSapo) then
    print *,'Calculating ILS derivatives (apo)...'
    do i = 1,n_mw
        call make_ils(mw(i)%centernue,instr%apo + epsapo &
          ,instr%phas,ilsparts,wrkils_mw(:,i))
    end do
    call convolve(spec(:),wrkils_mw,wrkspecxils)
    dspec_dils(:,1) = (wrkspecxils - specxils) * chanfeld / epsapo
    print *,'Done!'
end if
if (deri%sw_ILSphas) then
    print *,'Calculating ILS derivatives (phase)...'
    do i = 1,n_mw
        call make_ils(mw(i)%centernue,instr%apo,instr%phas + epsphas &
          ,ilsparts,wrkils_mw(:,i))
    end do
    call convolve(spec(:),wrkils_mw,wrkspecxils)
    dspec_dils(:,2) = (wrkspecxils - specxils) * chanfeld / epsphas
    print *,'Done!'
end if

! calculate channeling derivatives
if (deri%sw_chan) then
    print *,'Calculating channeling derivatives...'
    do i = 1,n_chan
        ! cosine / real part
        instr%chanamp(i,:) = instr%chanamp(i,:) + cmplx(epschan,0.0)
        call channeling(instr%chanlambda(1:n_chan),instr%chanamp(1:n_chan,1:n_mw),wrkchanfeld)
        instr%chanamp(i,:) = instr%chanamp(i,:) - cmplx(epschan,0.0)
        dspec_dchan(:,1,i) = specxils * (wrkchanfeld - chanfeld) / epschan
        ! sine / imag part
        instr%chanamp(i,:) = instr%chanamp(i,:) + cmplx(0.0,epschan)
        call channeling(instr%chanlambda(1:n_chan),instr%chanamp(1:n_chan,1:n_mw),wrkchanfeld)
        instr%chanamp(i,:) = instr%chanamp(i,:) - cmplx(0.0,epschan)
        dspec_dchan(:,2,i) = specxils * (wrkchanfeld - chanfeld) / epschan
    end do
    print *,'Done!'
end if

if (deri%sw_T) then
    print *,'Calculating T derivative...'
    wrkemiss = epsgnd
    do j = 1,n_lev

        ! recalculate slant tau in current level
        faktor = 1.0e-6 * lev%colair(j) / cos(lev%sza_rad(j))
        dtaudTslant = 0.0
        do i = 1,n_tau_lbl
            dtaudTslant = dtaudTslant + faktor * lev%vmr_ppmv(j,i) * tau_lev(2,:,j,i)
        end do
        do i = 1,n_tau_xsc
            dtaudTslant = dtaudTslant + faktor * lev%vmr_ppmv(j,n_tau_lbl+i) * tau_lev(2,:,j,n_tau_lbl+i)
        end do

        call make_bbratio(lev%T_K(j),obs%Tgnd_K,wrkspecb)

        ! radiance change due to transmission change
        wrkspeca = (wrkspecb - wrkemiss) * dtaudTslant * trmcumslant_lev(:,j)

        ! radiance change due to increase of Planck function
        call make_bbratio(lev%T_K(j) + 1.0,obs%Tgnd_K,wrkspecc)
        if (j .lt. n_lev) then
            wrkspeca = wrkspeca + (wrkspecc - wrkspecb) * (1.0d0 - trmslant_lev(:,j)) &
              * trmcumslant_lev(:,j+1)
        else
            wrkspeca = wrkspeca + (wrkspecc - wrkspecb) * (1.0d0 - trmslant_lev(:,j))
        end if

        ! coadd emission from current level to cumulative emission from all levels below target level
        wrkemiss = wrkemiss * trmslant_lev(:,j) + emiss_lev(:,j)

        ! calculate final T-derivative
        call make_specc(gndbb,wrkspeca,mw(1:n_mw)%centernue,mw(1:n_mw)%shift,wrkspecb)
        call convolve(wrkspecb,ils_mw,wrkspecxils)
        dspec_dT(:,j) = wrkspecxils * chanfeld

    end do

    ! calculate derivative for change in ground T
    wrkspeca = epsgnd * (gndbbp1K - gndbb) * atmtrm
    call convolve(wrkspeca,ils_mw,wrkspecxils)
    dspec_dTgnd = wrkspecxils * chanfeld

    print *,'Done!'
end if

! Write derivative arrays to file
if (fwdtofiledec) then
    if (deri%sw_vmr) then
        call tofile_dvmr(nfwdrun,'_DVMR',dspec_dvmr)
    end if
    if (deri%sw_T) then
        call tofile_dT(nfwdrun,'_DTEM',dspec_dT)
    end if
    if (deri%sw_ILSapo) then
        call tofile_spcxils(nfwdrun,'_DAPO',dspec_dILS(:,1))
    end if
    if (deri%sw_ILSphas) then
        call tofile_spcxils(nfwdrun,'_DPHA',dspec_dILS(:,2))
    end if
    if (deri%sw_shift) then
        call tofile_spcxils(nfwdrun,'_DSHI',dspec_dshift)
    end if
    if (deri%sw_ofset) then
        call tofile_spcxils(nfwdrun,'_DOFF',dspec_dofset)
    end if
    if (deri%sw_base) then
        call tofile_dbase(nfwdrun,'_DBASE',dspec_dbase)
    end if
    if (deri%sw_chan) then
        call tofile_dchan(nfwdrun,'_DCHA',dspec_dchan)
    end if
end if

end subroutine prffwd




!====================================================================
!  add_noise: add noise to simulated spectrum
!====================================================================
subroutine add_noise(rms,specxils)

use globfwd23

implicit none

real,dimension(n_mw),intent(in) :: rms
real,dimension(mw(n_mw)%rb),intent(inout) :: specxils

integer :: i,j
real,dimension(2) :: rand

if (abs(2.0 * instr%opdmax * wvskal%dnue - 1.0) .gt. 1.0e-3) then
    call warnout('Proper noise gen requires min sampling!',1)
end if

call random_seed
do i = 1,n_mw
    do j = mw(i)%lb,mw(i)%rb
        call random_number(rand)
        specxils(j) = specxils(j) + rms(i) * sqrt(-2.0 * log(rand(1))) * cos(2.0 * pi * rand(2))
    end do
end do

end subroutine add_noise



!====================================================================
!  alloc_fwd: Allocate arrays of size defined during runtime
!====================================================================
subroutine alloc_fwd()

use globfwd23
use globlev23

implicit none

! allocate work array for forward calculation
allocate (gndbb(mw(n_mw)%rbfinext),gndbbp1K(mw(n_mw)%rbfinext),atmtrm(mw(n_mw)%rbfinext) &
  ,spec(mw(n_mw)%rbfinext),emiss(mw(n_mw)%rbfinext),epsgnd(mw(n_mw)%rbfinext),ggs(mw(n_mw)%rbfinext) &
  ,dspecdshift(mw(n_mw)%rbfinext))
allocate (specxils(mw(n_mw)%rb))
allocate (chanfeld(mw(n_mw)%rb))
allocate (tau_lev(2,mw(n_mw)%rbfinext,n_lev,n_tau)) ! at refT, refT + deriT
allocate (tau_h2ocont(mw(n_mw)%rbfinext,n_lev))
allocate (trmslant_lev(mw(n_mw)%rbfinext,n_lev),trmslantggs_lev(mw(n_mw)%rbfinext,n_lev) &
  ,trmcumslant_lev(mw(n_mw)%rbfinext,n_lev),emiss_lev(mw(n_mw)%rbfinext,n_lev))
allocate (trm_speci(mw(n_mw)%rbfinext,n_tau))
allocate (ilsparts(-wvskal%ilsradius:wvskal%ilsradius,0:n_ifg-1,2))
allocate (ils_mw(-wvskal%ilsradius:wvskal%ilsradius,n_mw))

! allocate work arrays for deri calculation
allocate (wrktau(mw(n_mw)%rbfinext),wrksumtau(mw(n_mw)%rbfinext))
allocate (wrkspeca(mw(n_mw)%rbfinext),wrkspecb(mw(n_mw)%rbfinext),wrkspecc(mw(n_mw)%rbfinext))
allocate (wrkspecxils(mw(n_mw)%rb))
allocate (wrkchanfeld(mw(n_mw)%rb))
allocate (wrkils_mw(-wvskal%ilsradius:wvskal%ilsradius,n_mw))
allocate (dtaudvmrslant(mw(n_mw)%rbfinext),dtaudTslant(mw(n_mw)%rbfinext) &
  ,wrkemisslev(mw(n_mw)%rbfinext),wrkemiss(mw(n_mw)%rbfinext))


! allocate forward arrays (accessible from retrieval)
allocate (finspecxils(mw(n_mw)%rb),messxils(mw(n_mw)%rb))

! allocate arrays containing derivatives (accessible from retrieval)
allocate (dspec_dvmr(mw(n_mw)%rb,n_lev,deri%n_vmr))
allocate (dspec_dT(mw(n_mw)%rb,n_lev))
allocate (dspec_dTgnd(mw(n_mw)%rb))
allocate (dspec_dHITgam(mw(n_mw)%rb,deri%n_vmr))
allocate (dspec_dHITint(mw(n_mw)%rb,deri%n_vmr))
allocate (dspec_dLOS(mw(n_mw)%rb))
allocate (dspec_dsolshi(mw(n_mw)%rb))
allocate (dspec_dsolint(mw(n_mw)%rb))
allocate (dspec_dILS(mw(n_mw)%rb,2))
allocate (dspec_dshift(mw(n_mw)%rb))
allocate (dspec_dofset(mw(n_mw)%rb))
allocate (dspec_dbase(mw(n_mw)%rb,deri%maxnbase))
allocate (dspec_dchan(mw(n_mw)%rb,2,max_n_chan))

end subroutine alloc_fwd


!====================================================================
!  baseline: apply baseline parameters on selected MW
!            (include scale due to assumed continuum opacity!)
!====================================================================
subroutine baseline(imw,taucontwert,szaradobs,knot,skalfeld)

use globfwd23 ,only : max_n_base,mw

implicit none

integer,parameter :: n_smooth = 39  ! number of subintervals between knots for construction of baseline
real,parameter :: diff_base = 0.05 ! smoothing for continuum

integer,intent(in) :: imw
real,intent(in) :: taucontwert
real(8),intent(in) :: szaradobs
real,dimension(mw(imw)%n_base),intent(in) :: knot
real,dimension(mw(imw)%pts),intent(out) :: skalfeld

integer :: j,iknot,icont,n_basegrid,n_csampling
real :: trmcont,contgridconst,gridconst,rest,xc,xs
real,dimension(:),allocatable :: contlin,contwrka,contwrkb,continuum

contgridconst = 1.0 / real(n_smooth)
gridconst = 1.0 / real(mw(imw)%pts - 1)

trmcont = exp(-taucontwert / cos(szaradobs))

select case (mw(imw)%n_base)

    case (1) ! only scale
        skalfeld(1:mw(imw)%pts) = trmcont * knot(1)

    case (2) ! scale + slope
        do j = 1,mw(imw)%pts
            rest = gridconst * real(j - 1)
            skalfeld(j) = trmcont * ((1.0 - rest) * knot(1) + rest * knot(2))
        end do


    case default ! general baseline fit, more than two knots involved
        ! Create baseline on temp grid, smooth baseline points
        n_basegrid = 1 + (mw(imw)%n_base - 1) * n_smooth ! determine number of points in baseline grid
        allocate (contlin(1:n_basegrid),contwrka(1:n_basegrid),contwrkb(1:n_basegrid),continuum(1:n_basegrid))

        ! calculate contlin (linear interpolation between knots)
        contlin(1) = knot(1)
        contlin(n_basegrid) = knot(mw(imw)%n_base)
        do j = 2,n_basegrid - 1
            xc = contgridconst * real(j - 1)
            iknot = int(xc) + 1
            rest = xc - real(iknot - 1)
            contlin(j) = (1.0 - rest) * knot(iknot) + rest * knot(iknot+1)
        end do

        ! calculate smoothed continuum (mean of smoothing from left to right and vice versa)
        contwrka(1) = contlin(1)
        do j = 2,n_basegrid
            contwrka(j) = (1.0 - diff_base) * contwrka(j-1) + diff_base * contlin(j)
        end do
        contwrkb(n_basegrid) = contlin(n_basegrid)
        do j = n_basegrid - 1,1,- 1
            contwrkb(j) = (1.0 - diff_base) * contwrkb(j+1) + diff_base * contlin(j)
        end do
        continuum = 0.5 * trmcont * (contwrka + contwrkb)

        ! Linear interpolation to spectral grid of measurement
        do j = 1,mw(imw)%pts
            xs = real(n_smooth) * (0.5 + gridconst * real((mw(imw)%n_base - 2) * (j - 1)))
            icont = int(xs)
            rest = xs - real(icont)
            skalfeld(j) = (1.0 - rest) * continuum(icont+1) + rest * continuum(icont+2)
        end do

        ! Linear interpolation to continuum sampling grid
        n_csampling = 1 + 3 * (mw(imw)%n_base - 1)
        gridconst = 1.0 / real(n_csampling - 1)
        do j = 1,n_csampling
            xs = real(n_smooth) * (0.5 + gridconst * real((mw(imw)%n_base - 2) * (j - 1)))
            icont = int(xs)
            rest = xs - real(icont)
            mw(imw)%base_cont(j) = (1.0 - rest) * continuum(icont+1) + rest * continuum(icont+2)
        end do

        deallocate (contlin,contwrka,contwrkb,continuum)

end select

end subroutine baseline



!====================================================================
!  ckd_25 Berechnung
!====================================================================
subroutine calc_h2ocont_ckd25(v,t,p,vmrh2o,h2ocon)

  use mt_ckdcoe_25_m

  implicit none

  real(8),parameter :: hck=1.4387687d0     ! h*c/k [K/cm-1]
  real(8),intent(in) :: v,t,p,vmrh2o
  real(8),intent(inout) :: h2ocon
  real(8),parameter :: dv1 = -20.0d0,ddv = 10.0d0
  real(8),parameter :: p0=1013.0d0,T0 = 296.0d0,f1=0.25,beta=350.
  integer,parameter  :: n_s = 6
  integer :: i1,jfac
  real(8) :: vi1,r1,rs296,rs260,rf296,sfac,sh2o,cself,rhoave,xkt,tfac,fscal,cfrgn,td
  real(8) :: f0,V0F1,HWSQ1,BETA1,C_1,N_1,C_2,BETA2,N_2,vdelsq1,vdelmsq1,VF1,VmF1,VF2
  real(8),dimension(0:14) :: xfacrev
  real(8),dimension(120) :: xfacrev1
!    Self correction factors for 820-960 cm-1.
      DATA XFACREV(0:14)/ &
          1.003,1.009,1.015,1.023,1.029,1.033, &
          1.037,1.039,1.040,1.046,1.036,1.027, &
          1.01,1.002,1.00/

!     Self correction factors for 2000-3190 cm-1 (mt_ckd_2.5).
      DATA XFACREV1(1:120)/ &
         1.000,1.040,1.080,1.120,1.160,&
         1.200,1.240,1.280,1.318,1.357,&
         1.404,1.453,1.499,1.553,1.608,&
         1.674,1.746,1.818,1.899,1.984,&
         2.078,2.174,2.276,2.385,2.502,&
         2.624,2.747,2.883,3.018,3.170,&
         3.321,3.473,3.635,3.803,3.974,&
         4.144,4.327,4.500,4.703,4.887,&
         5.102,5.286,5.498,5.701,5.935,&
         6.155,6.405,6.633,6.892,7.115,&
         7.397,7.650,7.917,8.177,8.437,&
         8.704,8.953,9.192,9.428,9.644,&
         9.821,9.954,10.11,10.17,10.21,&
         10.26,10.29,10.28,10.26,10.20,&
         10.15,10.16,10.25,10.02,9.965,&
         10.01,9.934,9.847,9.744,9.566,&
         9.436,9.181,8.872,8.547,8.155,&
         7.730,7.261,6.777,6.271,5.807,&
         5.313,4.845,4.444,4.074,3.677,&
         3.362,3.087,2.826,2.615,2.385,&
         2.238,2.148,1.979,1.939,1.773,&
         1.696,1.642,1.569,1.510,1.474,&
         1.425,1.375,1.322,1.272,1.230,&
         1.180,1.130,1.080,1.040,1.000/

!
!!! linear interpolation in wavenumber
!
  i1 = int((v-dv1)/ddv) + 1
  vi1 = dv1+(i1-1)*ddv
  r1 = (v-vi1)/ddv
  rs296 = cslf296(i1)+(cslf296(i1+1)-cslf296(i1)) * r1
  rs260 = cslf260(i1)+(cslf260(i1+1)-cslf260(i1)) * r1
  rf296 = cfrn296(i1)+(cfrn296(i1+1)-cfrn296(i1)) * r1

  RHOAVE = (p/P0)*(T0/t)     


!=======================================================================
!                             SELF

            TFAC = (t-T0)/(260.0d0-T0)
            sh2o = 0.0d0
               IF (rs296.GT.0.) THEN
                  SH2O = rs296*(rs260/rs296)**TFAC
                  SFAC = 1.0d0

                  IF (v .GE. 820. .AND. v .LE. 960.) THEN
                     JFAC = nint((v - 820.)/10.)
                     SFAC = XFACREV(JFAC)
                  ENDIF
!   ***                        
!     Correction to the self continuum     mt_ckd_2.5   Jan 2010
!   ***
                  IF (v .GE. 2000. .AND. v .LE. 3190.) THEN
                     JFAC = nint((v - 1990.)/10.)
                     SFAC = XFACREV1(JFAC)
                  ENDIF

                  sfac = sfac * ( 1.0d0 + ( f1/(1.0d0+(v/beta)**n_s) ) )

                  SH2O = SFAC * SH2O
               ENDIF

               xkt = t/hck
               cself = sh2o * vmrh2o * RHOave * 1.0d-20 * RADFN(v,XKT)

!=======================================================================
!                             FOREIGN
            f0     = 0.06
            V0F1   = 255.67
            HWSQ1  = 240.**2
            BETA1  = 57.83 
            C_1    = -0.42
            N_1    = 8
            C_2    = 0.3
            BETA2  = 630.
            N_2    = 8

               vdelsq1  = (V-V0F1)**2
               vdelmsq1 = (V+V0F1)**2
               VF1  = ((V-V0F1)/beta1)**N_1
               VmF1 = ((V+V0F1)/beta1)**N_1
               VF2  =  (V/beta2)**N_2

               FSCAL = 1. + & 
                     (f0 + C_1*( (HWSQ1/(VDELSQ1 +HWSQ1+VF1))  +   &
                                 (HWSQ1/(VDELmSQ1+HWSQ1+VmF1)) ) ) / &
                                                      (1.+C_2*VF2) 

               cfrgn = rf296 * fscal  * (1.0d0 - vmrh2o) * RHOave * 1.0d-20 * RADFN(v,XKT)         

!=========================================================================
!                             total

               h2ocon = cfrgn + cself

contains

      FUNCTION RADFN (VI,XKT)                                           
        implicit none
        real(8),intent(in):: vi,xkt
        real(8) :: radfn
        real(8) :: xvi,xviokt,EXPVKT
!                                                                       
!      IN THE SMALL XVIOKT REGION 0.5 IS REQUIRED                       
!                                                                       
      XVI = VI                                                          
!                                                                       
      IF (XKT.GT.0.0) THEN                                              
!                                                                       
         XVIOKT = XVI/XKT                                               
!                                                                       
         IF (XVIOKT.LE.0.01) THEN                                       
            RADFN = 0.5d0*XVIOKT*XVI                                      
!                                                                       
         ELSEIF (XVIOKT.LE.10.0) THEN                                   
            EXPVKT = EXP(-XVIOKT)                                       
            RADFN = XVI*(1.0d0-EXPVKT)/(1.0d0+EXPVKT)                         
!                                                                       
         ELSE                                                           
            RADFN = XVI                                                 
         ENDIF                                                          
!                                                                       
      ELSE                                                              
         RADFN = XVI                                                    
      ENDIF                                                             
!                                                                       
                                                                       
      END  function radfn

end subroutine calc_h2ocont_ckd25



!====================================================================
!  channeling: apply channeling to specxils
!====================================================================
subroutine channeling(lambda,amplitude,chanfeld)

use globfwd23 ,only : pi,n_mw,n_chan,mw,wvskal

implicit none

real,dimension(n_chan),intent(in) :: lambda
complex,dimension(n_chan,n_mw),intent(in) :: amplitude
real,dimension(mw(n_mw)%rb),intent(out) :: chanfeld

integer :: imw,i,j
real(8) :: argument

! Channeling auf Spektrum
chanfeld = 1.0
do imw = 1,n_mw
    do i = 1,n_chan
        do j = 1,mw(imw)%pts
            argument = 2.0d0 * pi * (mw(imw)%firstnue + (j - 1) * wvskal%dnue) &
              / lambda(i)
            chanfeld(mw(imw)%lb+j-1) = chanfeld(mw(imw)%lb+j-1) * &
              (1.0 + aimag(amplitude(i,imw)) * sin(argument) &
              + real(amplitude(i,imw)) * cos(argument))
        end do
    end do
end do

end subroutine channeling



!====================================================================
!  checkmess: make spectral grid and MW bounds compatible with measurement
!====================================================================
subroutine check_mess(n_mw,firstnue_input,lastnue_input,messkind &
      ,messdatei,firstnue,lastnue,dnue)

use globfwd23, only : iunit_fwd

implicit none

integer,intent(in) :: n_mw,messkind
real(8),dimension(n_mw),intent(in) :: firstnue_input,lastnue_input
character(len=*),dimension(n_mw),intent(in) :: messdatei
real(8),dimension(n_mw),intent(out) :: firstnue,lastnue
real(8),intent(out) :: dnue

character(2) :: dumchar
character(80) :: sfitheader
logical :: dateidadec
integer :: i,j,nofpts,zaehler
real(8) :: xdummy,ydummy
real(8),dimension(n_mw) :: firstnue_mess,lastnue_mess,dnue_mess

do i = 1,n_mw

    print *,'current file:',messdatei(i)
    inquire (file = messdatei(i),exist = dateidadec)
    if (.not. dateidadec) then
        call warnout('check_mess: spectrum not found!',0)
    end if
    
    select case (messkind)
        case (0) !dpt-Format
            open (iunit_fwd+1,file = messdatei(i),status = 'old',action = 'read')
            read (iunit_fwd+1,*) firstnue_mess(i),ydummy
            zaehler = 1
            do
                read (iunit_fwd+1,*,end = 100) xdummy,ydummy
                zaehler = zaehler + 1
            end do
            100 continue
            close (iunit_fwd+1)

            nofpts = zaehler
            lastnue_mess(i) = xdummy
            dnue_mess(i) = (lastnue_mess(i) - firstnue_mess(i)) / real(nofpts - 1,8)

        case (1) !eigenes binaeres Format
            open (iunit_fwd+1,file = messdatei(i), status = 'old',action = 'read')
            do j = 1,6
                call gonext(iunit_fwd+1,.true.)
            end do
            read (iunit_fwd+1) dumchar
            read (iunit_fwd+1) firstnue_mess(i)
            read (iunit_fwd+1) dnue_mess(i)
            read (iunit_fwd+1) nofpts
            close (iunit_fwd+1)
            lastnue_mess(i) = firstnue_mess(i) + dnue_mess(i) * real(nofpts - 1,8)

        case (2) !SFIT2 binaeres Format
            open (iunit_fwd+1,file = messdatei(i), status = 'old',action = 'read')
            read (iunit_fwd+1) sfitheader
            read (iunit_fwd+1) firstnue_mess(i)
            read (iunit_fwd+1) lastnue_mess(i)
            read (iunit_fwd+1) dnue_mess(i)
            read (iunit_fwd+1) nofpts
            close (iunit_fwd+1)

    end select

    print *,'Measurement: first / last nue, deltanue:' &
      ,firstnue_mess(i),lastnue_mess(i),dnue_mess(i)
end do

dnue = 0.0d0
do i = 1,n_mw
    dnue = dnue + abs(dnue_mess(i))
end do
dnue = dnue / real(n_mw,8)

do i = 1,n_mw

    print *,'Checking consistency of spectra, MW', i

    ! spectral gridpoints must be ordered ascending in wave number
    if (dnue_mess(i) .le. 0.0d0) then
        call warnout('Negative wave number increment in measurement!',0)
    end if

    ! grid spacing must be consistent
    if (abs(dnue_mess(i) - dnue) .gt. 0.1d0 * dnue * dnue &
      / (lastnue_input(i) - firstnue_input(i))) then
       call warnout('Incompatible spectral increment!',0)
    end if

    ! MW must lie fully inside spectral range covered by measurement
    if ((firstnue_input(i) - firstnue_mess(i)) &
      * (firstnue_input(i) - lastnue_mess(i)) .gt. 1.0d-10 &
      .or. (lastnue_input(i) - firstnue_mess(i)) &
      * (lastnue_input(i) - lastnue_mess(i)) .gt. 1.0d-10) then
        call warnout('MW not covered by measurement!',0)
    end if

end do

! Determine MW bounds
do i = 1,n_mw
    print *,'Choosing appropriate spectral bounds, MW', i
    firstnue(i) = firstnue_mess(i) + dnue * nint((firstnue_input(i) - firstnue_mess(i)) / dnue)
    lastnue(i) = firstnue_mess(i) + dnue * nint((lastnue_input(i) - firstnue_mess(i)) / dnue)
end do

end subroutine check_mess



!====================================================================
!  convolve: convolution of fine-grid spectrum with ILS
!====================================================================
subroutine convolve(spec,ils_mw,specxils)

use globfwd23 ,only : n_mw,mw,wvskal

implicit none

real,dimension(mw(n_mw)%rbfinext),intent(in) :: spec
real,dimension(-wvskal%ilsradius:wvskal%ilsradius,n_mw),intent(in) :: ils_mw
real,dimension(mw(n_mw)%rb),intent(out) :: specxils

integer :: i,j,k,ifinefirst,ifine,icount
real(8) :: sumwert

do i = 1,n_mw
    ! nfinefirst ist der finegrid-Index des ersten Punktes im MW
    ifinefirst = mw(i)%lbfinext + wvskal%shiftradius + wvskal%ilsradius
    ! Berechnung fuer alle coarse-grid Punkte durchfuehren, coarsegrid zaehler auf Null setzen
    icount = 0
    do j = mw(i)%lb,mw(i)%rb
        ! Faltung mit der ILS
        ifine = ifinefirst + icount * wvskal%interpol
        sumwert = 0.0d0
        do k = -wvskal%ilsradius,wvskal%ilsradius
            sumwert = sumwert + ils_mw(k,i) * spec(ifine - k)
        end do
        specxils(j) = sumwert * wvskal%dnuefine
        icount = icount + 1
    end do
end do

end subroutine convolve



!====================================================================
!  dealloc_fwd: Allocate arrays of size defined during runtime
!====================================================================
subroutine dealloc_fwd()

use globfwd23
use globlev23

implicit none

deallocate (ilsparts)
deallocate (gndbb,gndbbp1K,atmtrm,spec,emiss,epsgnd,ggs,dspecdshift)
deallocate (specxils)
deallocate (chanfeld)
deallocate (trm_speci)
deallocate (tau_lev)
deallocate (tau_h2ocont)
deallocate (trmslant_lev,trmslantggs_lev,trmcumslant_lev,emiss_lev)
deallocate (wrktau,wrksumtau)
deallocate (wrkspeca,wrkspecb,wrkspecc)
deallocate (wrkspecxils)
deallocate (wrkchanfeld)
deallocate (wrkils_mw)
deallocate (dtaudvmrslant,dtaudTslant,wrkemisslev,wrkemiss)

deallocate (finspecxils,messxils)

! deallocate arrays containing derivatives (if deri switched on)
deallocate (dspec_dvmr)
deallocate (dspec_dT)
deallocate (dspec_dHITgam)
deallocate (dspec_dHITint)
deallocate (dspec_dTgnd)
deallocate (dspec_dLOS)
deallocate (dspec_dILS)
deallocate (dspec_dshift)
deallocate (dspec_dofset)
deallocate (dspec_dchan)

end subroutine dealloc_fwd



!====================================================================
!  gonext: Einlesen bis zum naechsten $ Zeichen
!====================================================================
subroutine gonext(ifile,bindec)

implicit none

integer,intent(in) :: ifile
logical,intent(in) :: bindec

character(1) :: nextchar

nextchar='x'
do while (nextchar /= '$')
    if (bindec) then
        read(ifile) nextchar
    else
        read(ifile,'(A1)') nextchar
    end if

end do

end subroutine gonext



!====================================================================
!  subroutine hydrostat determines pressure from altitude and T
!====================================================================
subroutine hydrostata(lat_obs_rad,h_lev_m,T_lev_K,h2o_lev_ppmv &
  ,p_lev_pa,n_cbm,colair_lev,colair_lev_do,colair_lev_up)

use globfwd23

implicit none

real(8),intent(in) :: lat_obs_rad
real(8),dimension(n_lev),intent(in) :: h_lev_m,T_lev_K,h2o_lev_ppmv
real(8),dimension(n_lev),intent(inout) :: p_lev_pa
real(8),dimension(n_lev),intent(out) :: n_cbm,colair_lev &
  ,colair_lev_do,colair_lev_up

integer,parameter :: nfine = 20  ! fine layering for pressure solver
integer :: i,j
real(8) :: g_eff_gnd,g_eff,mutot_amu,mueff_amu,skalh,ptot,h,T,h2o,h2osat,deltah &
  ,deltaT,deltah2o,sumntot,sumndry,ntot,ndry,ptotmid,slant,wup,wdo,nfracdry,coldryinlayer
real(8),dimension(n_lev) :: h2o_lev_wrk

! gravitational acceleration as function of altitude
! International Gravity Formula 1967, 1967 Geodetic Reference System,
! Helmert's equation or Clairault's formula
! taken from http://en.wikipedia.org/wiki/Acceleration_due_to_gravity
! (altitude dependent part separated)

! Surface g_eff as function of latitude
g_eff_gnd = 9.780327d0 * (1.0d0 + 0.0053024d0 * sin(lat_obs_rad) * sin(lat_obs_rad) &
      - 5.8d-6 * sin(2.0d0 * lat_obs_rad) * sin(2.0d0 * lat_obs_rad))

! restrict H2O VMR below saturation VMR
do i = 1,n_lev
    h2o_lev_wrk(i) = h2o_lev_ppmv(i)
    h2osat = 2.25d15 * (EXP(-5.417d3 / T_lev_K(i))) / p_lev_pa(i) ! approx saturation vmr for H2O [ppmv]
    if (h2o_lev_wrk(i) .lt. 0.0d0) then
        print *,'Warning: Neg H2O VMR in hydrostata! Level:',i
        h2o_lev_wrk(i) = 0.0d0
    else
        if (h2o_lev_wrk(i) .gt. 4.0d0 * h2osat) then
            print *,'Warning: H2O VMR above saturation in hydrostata! Level:',i
            h2o_lev_wrk(i) = 4.0d0 * h2osat
        end if
    end if
end do

colair_lev_do(1) = 0.0d0
do i = 1,n_lev - 1

    ptot = p_lev_pa(i) ! init pressure at bottom of first sublayers

    deltah = (h_lev_m(i+1) - h_lev_m(i)) / real(nfine,8)
    deltaT = (T_lev_K(i+1) - T_lev_K(i)) / real(nfine,8)
    deltah2o = (h2o_lev_wrk(i+1) - h2o_lev_wrk(i)) / real(nfine,8)

    ! determine pressure stratification
    sumntot = 0.0d0
    sumndry = 0.0d0
    mueff_amu = 0.0d0
    wup = 0.0d0
    wdo = 0.0d0
    do j = 1,nfine
        ! determine prescribed and derived values in the middle of sublayer
        h = h_lev_m(i) + (real(j,8) - 0.5d0) * deltah
        T = T_lev_K(i) + (real(j,8) - 0.5d0) * deltaT
        g_eff = g_eff_gnd + dgdh * h
        h2o = h2o_lev_wrk(i) + (real(j,8) - 0.5d0) * deltah2o
        mutot_amu = (mudry + 1.0d-6 * h2o * muh2o) / (1.0d0 + 1.0d-6 * h2o)
        skalh = kboltz * T / (g_eff * mutot_amu * amunit)
        ptotmid = ptot * exp(-0.5d0 * deltah / skalh) ! pressure in middle of sublayer
        ! determine pressure at top of current sublayer
        ptot = ptot * exp(-deltah / skalh)

        ! determine number-density-weighted means
        ntot = ptotmid / (kboltz * T) ! mean total number density in layer
        ndry = ntot / (1.0d0 + 1.0d-6 * h2o)
        sumntot = sumntot + ntot
        sumndry = sumndry + ndry
        mueff_amu = mueff_amu + mutot_amu * ntot
        slant = (real(j,8) - 0.5d0) / real(nfine,8)
        wup = wup + ndry * (1.0d0 - slant)
        wdo = wdo + ndry * (slant)
    end do
    p_lev_pa(i+1) = ptot
    mueff_amu = mueff_amu / sumntot
    nfracdry = sumndry / sumntot
    wup = wup / sumndry
    wdo = wdo / sumndry

    ! determine partial columns *associated with each level*
    ! col_do,col_up,col = col_do + col_up
    coldryinlayer = nfracdry * (p_lev_pa(i) - p_lev_pa(i+1)) &
      / (mueff_amu * amunit * (g_eff_gnd + 0.5d0 * dgdh * (h_lev_m(i+1) + h_lev_m(i))))

    colair_lev_do(i+1) = coldryinlayer * wdo / (wup + wdo)
    colair_lev_up(i) = coldryinlayer * wup / (wup + wdo)
    colair_lev(i) = colair_lev_do(i) + colair_lev_up(i)
end do

colair_lev(n_lev) = colair_lev_do(n_lev)
colair_lev_up(n_lev) = 0.0d0

! Determine total particle number density at each level
do i = 1,n_lev
    n_cbm(i) = p_lev_pa(i) / ((1.0d0 + 1.0d-6 * h2o_lev_wrk(i)) * kboltz * T_lev_K(i))
end do

end subroutine hydrostata



!====================================================================
!  subroutine hydrostat determines altitudes from pressure profile
!====================================================================
subroutine hydrostatb(h_obs_m,lat_obs_rad,p_lev_pa,T_lev_K,h2o_lev_ppmv,h_lev_m &
  ,n_cbm,colair_lev,colair_lev_do,colair_lev_up)

use globfwd23

implicit none

real(8),intent(in) :: h_obs_m,lat_obs_rad
real(8),dimension(n_lev),intent(in) :: p_lev_pa,T_lev_K,h2o_lev_ppmv
real(8),dimension(n_lev),intent(out) :: h_lev_m,n_cbm,colair_lev &
  ,colair_lev_do,colair_lev_up

integer,parameter :: niter = 50  ! iteration for altitude solver
integer,parameter :: nfine = 20  ! fine layering for altitude solver
integer :: i,j,k
real(8) :: g_eff_gnd,g_eff,mutot_amu,mueff_amu,skalh,ptot,h,dhcorr,T,h2o,h2osat,deltah &
  ,deltaT,deltah2o,sumntot,sumndry,ntot,ndry,ptotmid,slant,wup,wdo,nfracdry,coldryinlayer

! gravitational acceleration as function of altitude
! International Gravity Formula 1967, 1967 Geodetic Reference System
! Helmert's equation or Clairault's formula
! taken from http://en.wikipedia.org/wiki/Acceleration_due_to_gravity
! (altitude dependent part separated)

! Surface g_eff as function of latitude
g_eff_gnd = 9.780327d0 * (1.0d0 + 0.0053024d0 * sin(lat_obs_rad) * sin(lat_obs_rad) &
      - 5.8d-6 * sin(2.0d0 * lat_obs_rad) * sin(2.0d0 * lat_obs_rad))

! lowest level coincides with observer
h_lev_m(1) = h_obs_m

do i = 1,n_lev - 1

    ! g_eff lower level, skale heigt lower level
    g_eff = g_eff_gnd + dgdh * h_lev_m(i)
    mutot_amu = (mudry + 1.0d-6 * h2o_lev_ppmv(i) * muh2o) / (1.0d0 + 1.0d-6 * h2o_lev_ppmv(i))
    skalh = kboltz * T_lev_K(i) / (g_eff * mutot_amu * amunit)

    ! estimate altitude of upper level
    h_lev_m(i+1) = h_lev_m(i) - skalh * log(p_lev_pa(i+1) / p_lev_pa(i))

    ! Improve estimation of altitude, use subdivision + recursive scheme
    do j = 1,niter

        ptot = p_lev_pa(i)

        ! nfine sublayers
        deltah = (h_lev_m(i+1) - h_lev_m(i)) / real(nfine,8)
        deltaT = (T_lev_K(i+1) - T_lev_K(i)) / real(nfine,8)
        deltah2o = (h2o_lev_ppmv(i+1) - h2o_lev_ppmv(i)) / real(nfine,8)

        do k = 1,nfine
            h = h_lev_m(i) + (real(k,8) - 0.5d0) * deltah
            T = T_lev_K(i) + (real(k,8) - 0.5d0) * deltaT
            g_eff = g_eff_gnd + dgdh * h
            h2o = h2o_lev_ppmv(i) + (real(k,8) - 0.5d0) * deltah2o
            h2osat = 2.25d15 * (EXP(-5.417d3 / T)) / ptot ! approx saturation vmr for H2O [ppmv]
            if (h2o .lt. 0.0d0) then
                h2o = 0.0d0
            else
                if (h2o .gt. 4.0d0 * h2osat) then
                    h2o = 4.0d0 * h2osat
                end if
            end if
            mutot_amu = (mudry + 1.0d-6 * h2o * muh2o) / (1.0d0 + 1.0d-6 * h2o)
            skalh = kboltz * T / (g_eff * mutot_amu * amunit)
            ptot = ptot * exp(-deltah / skalh)
        end do

        ! Improve upper level altitude
        dhcorr = skalh * log(ptot / p_lev_pa(i+1))
        h_lev_m(i+1) = h_lev_m(i+1) + dhcorr

        ! exit iteration when convergence reached
        if (abs(dhcorr) .lt. 0.001d0) exit

    end do

    ! Warning if no convergence reached
    if (j .eq. niter) then
        call warnout('Sub hydrostat: no convergence!',1)
    end if
    if (skalh .lt. 3.0d3 .or. skalh .gt. 1.2d4) then
        print *,'Retrieved scale height:',skalh
        print *,'Current H2O VMRs: ',h2o_lev_ppmv(i),h2o_lev_ppmv(i+1)
        print *,'Current Ts:       ',T_lev_K(i),T_lev_K(i+1)
        call warnout('Sub hydrostat: implausible scale height!',1)
    end if

end do

! Determine total particle number density at each level
do i = 1,n_lev
    h2o = h2o_lev_ppmv(i)
    h2osat = 2.25d15 * (EXP(-5.417d3 / T_lev_K(i))) / p_lev_pa(i) ! approx saturation vmr for H2O [ppmv]
    if (h2o .lt. 0.0d0) then
        h2o = 0.0d0
    else
        if (h2o .gt. h2osat) then
            h2o = h2osat
        end if
    end if
    n_cbm(i) = p_lev_pa(i) / ((1.0d0 + 1.0d-6 * h2o) * kboltz * T_lev_K(i))
end do

colair_lev_do(1) = 0.0d0
do i = 1,n_lev - 1

    ptot = p_lev_pa(i) ! init pressure at bottom of first sublayers

    deltah = (h_lev_m(i+1) - h_lev_m(i)) / real(nfine,8)
    deltaT = (T_lev_K(i+1) - T_lev_K(i)) / real(nfine,8)
    deltah2o = (h2o_lev_ppmv(i+1) - h2o_lev_ppmv(i)) / real(nfine,8)

    ! determine pressure stratification
    sumntot = 0.0d0
    sumndry = 0.0d0
    mueff_amu = 0.0d0
    wup = 0.0d0
    wdo = 0.0d0
    do j = 1,nfine
        ! determine prescribed and derived values in the middle of sublayer
        h = h_lev_m(i) + (real(j,8) - 0.5d0) * deltah
        T = T_lev_K(i) + (real(j,8) - 0.5d0) * deltaT
        g_eff = g_eff_gnd + dgdh * h
        h2o = h2o_lev_ppmv(i) + (real(j,8) - 0.5d0) * deltah2o
        h2osat = 2.25d15 * (EXP(-5.417d3 / T)) / ptot ! approx saturation vmr for H2O [ppmv]
        if (h2o .lt. 0.0d0) then
            h2o = 0.0d0
        else
            if (h2o .gt. h2osat) then
                h2o = h2osat
            end if
        end if
        mutot_amu = (mudry + 1.0d-6 * h2o * muh2o) / (1.0d0 + 1.0d-6 * h2o)
        skalh = kboltz * T / (g_eff * mutot_amu * amunit)
        ptotmid = ptot * exp(-0.5d0 * deltah / skalh) ! pressure in middle of sublayer
        ! determine pressure at top of current sublayer
        ptot = ptot * exp(-deltah / skalh)

        ! determine number-density-weighted means
        ntot = ptotmid / (kboltz * T) ! mean total number density in layer
        ndry = ntot / (1.0d0 + 1.0d-6 * h2o)
        sumntot = sumntot + ntot
        sumndry = sumndry + ndry
        mueff_amu = mueff_amu + mutot_amu * ntot
        slant = (real(j,8) - 0.5d0) / real(nfine,8)
        wup = wup + ndry * (1.0d0 - slant)
        wdo = wdo + ndry * (slant)
    end do
    mueff_amu = mueff_amu / sumntot
    nfracdry = sumndry / sumntot
    wup = wup / sumndry
    wdo = wdo / sumndry

    ! determine partial columns *associated with each level*
    ! col_do,col_up,col = col_do + col_up
    coldryinlayer = nfracdry * (p_lev_pa(i) - p_lev_pa(i+1)) &
      / (mueff_amu * amunit * (g_eff_gnd + 0.5d0 * dgdh * (h_lev_m(i+1) + h_lev_m(i))))

    colair_lev_do(i+1) = coldryinlayer * wdo / (wup + wdo)
    colair_lev_up(i) = coldryinlayer * wup / (wup + wdo)
    colair_lev(i) = colair_lev_do(i) + colair_lev_up(i)
end do

colair_lev(n_lev) = colair_lev_do(n_lev)
colair_lev_up(n_lev) = 0.0d0

end subroutine hydrostatb



!====================================================================
!  infospecies
!====================================================================
subroutine infospecies(whichspecies,masseamu,qa,qb,qc,qd,qe)

use globfwd23

implicit none
integer,intent(in) :: whichspecies
real(8),intent(out) :: masseamu,qa,qb,qc,qd,qe

logical :: decmarke
integer :: i

decmarke = .false.
do i = 1,n_species
    if (whichspecies .eq. species(i)%identifier) then
        masseamu = species(i)%masseamu
        qa = species(i)%qa
        qb = species(i)%qb
        qc = species(i)%qc
        qd = species(i)%qd
        qe = species(i)%qe
        decmarke = .true.
    end if
end do

if (.not. decmarke) then
    print *,'Requested species: ',whichspecies
    call warnout('Error in infospecies: no species assignment!',0)
end if

end subroutine infospecies



!====================================================================
!  init_epsgnd:  generate eps_gnd values from tabellated input values
!====================================================================
subroutine init_epsgnd()

use globfwd23

implicit none

integer :: i,j,itable
real(8) :: nue,rest

! Teste, ob Tabelle alle benoetigten Spektralbereiche umfasst
do i = 1,n_mw
    if ((mw(i)%firstnuefinext - obs%nue_eps(1)) * (mw(i)%firstnuefinext - obs%nue_eps(n_eps)) .ge. 0.0d0) then
        call warnout('firstnue: epsgnd_table too narrow!',0)
    end if
    if ((mw(i)%lastnuefinext - obs%nue_eps(1)) * (mw(i)%lastnuefinext - obs%nue_eps(n_eps)) .ge. 0.0d0) then
        call warnout('lastnue: epsgnd_table too narrow!',0)
    end if
end do

do i = 1,n_mw
    itable = 1
    do j = 1,mw(i)%ptsfinext
        nue = mw(i)%firstnuefinext + wvskal%dnuefine * real(j - 1,8)
        do while (obs%nue_eps(itable) .lt. nue)
            itable = itable + 1
        end do
        rest = (nue - obs%nue_eps(itable-1)) / (obs%nue_eps(itable) - obs%nue_eps(itable-1))
        epsgnd(mw(i)%lbfinext+j-1) = (1.0 - rest) * obs%epsgnd(itable-1) + rest * obs%epsgnd(itable)
    end do
end do

end subroutine init_epsgnd



!====================================================================
!  initfwd:  Initialize forward model runs
!====================================================================
subroutine initfwd()

use globfwd23
use globlev23

implicit none

integer :: i

print *,'Initializing forward model...     '

! PP: Steuerungskennung fuer Parallelprozessierung lesen
wrkpfad = 'wrk_fwd'
call readppkennung(trim(wrkpfad)//'/npp.inp',inppfad_fwd,inppfad_inv,outpfad_fwd,outpfad_inv,npp)
print *,'Processing handle:',npp
iunit_fwd = iunitoffset_fwd + 2 * (npp - 1)

! Read from input file (also determines altitude levels z(i))
print *,'Reading general input file...     '
call read_input_fwd(trim(inppfad_fwd)//'/PRFFWD.INP')
print *,'Reading general input completed!  '

! Delete files in output directory
print *,'Deleting previous output...       '
call system('del /q '//trim(outpfad_fwd)//'/*.*')
call system('del /q '//trim(outpfad_inv)//'/*.*')

! Read pT file (p(z) and T(z))
print *,'Reading pT input file...          '
call read_pT(ptdatei,'INP_FWD/Tclim.dat',obs%lat_rad,obs%JDdate_d &
  ,n_lev,lev%h_m(1:n_lev),lev%T_K(1:n_lev),lev%p_Pa(1:n_lev))

! read chemical composition
print *,'Reading VMR files...              '
do i = 1,n_tau
    call read_vmr(vmrdatei(i),n_lev,lev%h_m(1:n_lev),lev%vmr_ppmv(1:n_lev,i))
end do

! read reference vmr
print *,'Reading VMR ref files...          '
do i = 1,n_tau
    call read_vmr(vmrrefdatei(i),n_lev,lev%h_m(1:n_lev),lev%vmr_ref_ppmv(1:n_lev,i))
end do

! Either read H2O from separate file or from forward species
if (h2ofwddec) then
    lev%h2o_ppmv(1:n_lev) = lev%vmr_ppmv(1:n_lev,ptrh2o)
else
    print *,'Reading h2o file...               '
    call read_vmr(h2odatei,n_lev,lev%h_m(1:n_lev),lev%h2o_ppmv(1:n_lev))
end if
! H2O for continuum calc
lev%h2o_cont(1:n_lev) = 1.0d-6 * lev%h2o_ppmv(1:n_lev)

! Final adjustment of p(z) and raytracing
! Setup hydrostatic atmosphere (calculate altitude levels from p stratification)
print *,'generating hydrostatic equilibrium...   '
call hydrostata(obs%lat_rad,lev%h_m(1:n_lev),lev%T_K(1:n_lev),lev%h2o_ppmv(1:n_lev) &
  ,lev%p_Pa(1:n_lev),lev%n_cbm(1:n_lev),lev%colair(1:n_lev),lev%colair_do(1:n_lev) &
  ,lev%colair_up(1:n_lev))

! Raytracing
print *,'Raytracing...                           '
call raytrace(lev%h_m(1:n_lev),lev%p_Pa(1:n_lev),lev%T_K(1:n_lev) &
  ,obs%lat_rad,obs%azi_rad,obs%sza_rad,obs%astrodec,lev%sza_rad(1:n_lev))

! Read information on wind speed and turbulence
if (winddec) then
    print *,'Reading wind profile...'
    call read_wind(winddatei,n_lev,lev%h_m(1:n_lev),lev%sza_rad(1:n_lev),obs%azi_rad,lev%wind_scal(1:n_lev))
else
    lev%wind_scal(1:n_lev) = 0.0d0
end if

! Setup of MWs (coarse grid and fine grid)
print *,'Initializing MWs...               '
call init_mws()

! perform fwd calculation (if F, only update of derivatives)
calcfwddec = .true.

print *,'Initialisation forward model finished!'

end subroutine initfwd



!====================================================================
!  init_ils: initialisiert schnelle Berechnung der ILS
!      ilsparts(2,0:n_ifg-1,-wvskal%ilsradius:wvskal%ilsradius))
!      ilsparts(1,...): gerader Anteil
!      ilsparts(2,...): ungerader Anteil
!====================================================================
subroutine init_ils (ilsparts)

use globfwd23, only : pi,n_ifg,instr,wvskal

implicit none

real,dimension(-wvskal%ilsradius:wvskal%ilsradius,0:n_ifg-1,2),intent(out) :: ilsparts

integer :: i,j
real,dimension(0:n_ifg-1) :: modul_abs
real :: nue,xi,xeins,xnorm,opd,pi_x_opd,term

! Berechnung der numerischen Apodisierung
select case (instr%apokind)
    case (1)
        do i = 0,n_ifg - 1
            modul_abs(i) = 1.0
        end do
    case (2)
        do i = 0,n_ifg - 1
            opd = real(i) / real(n_ifg - 1)
            modul_abs(i) = 1.0 - opd
        end do
    case (3)
        do i = 0,n_ifg - 1
            pi_x_opd = pi * real(i) / real(n_ifg - 1)
            modul_abs(i) = 0.53856 + 0.46144 * cos(pi_x_opd)
        end do
    case (4)
        do i = 0,n_ifg - 1
            pi_x_opd = pi * real(i) / real(n_ifg - 1)
            modul_abs(i) = 0.42323 + 0.49755 * cos(pi_x_opd) + 0.07922 *&
              cos(2.0 * pi_x_opd)
        end do
    case (5)
        do i = 0,n_ifg - 1
            pi_x_opd = pi * real(i) / real(n_ifg - 1)
            modul_abs(i) =  0.35875 + 0.48829 * cos(pi_x_opd)&
              + 0.14128 * cos(2 * pi_x_opd) + 0.01168 * cos(3 * pi_x_opd)
        end do
    case (6)
        do i = 0,n_ifg - 1
            opd = real(i) / real(n_ifg - 1)
            term = (1.0d0 - opd * opd)
            modul_abs(i) =  0.384093 - 0.087577 * term + 0.703484 * term * term
      end do
    case (7)
        do i = 0,n_ifg - 1
            opd = real(i,8) / real(n_ifg - 1)
            term = (1.0d0 - opd * opd)
            modul_abs(i) =  0.152442 - 0.136176 * term + 0.983734 * term * term
        end do
    case (8)
        do i = 0,n_ifg - 1
            opd = real(i) / real(n_ifg - 1)
            term = (1.0 - opd * opd) * (1.0 - opd * opd)
            modul_abs(i) =  0.045335 + 0.554883 * term + 0.399782 * term * term
      end do
    case (9)
        do i = 0,n_ifg - 1
            opd = real(i) / real(n_ifg - 1)
            modul_abs(i) =  exp(-3.423 * opd * opd) ! Vorfaktor: ln(2) / (0.45)**2
        end do
end select

xnorm = 1.0d0 / real(n_ifg - 1)
xeins = xnorm * instr%opdmax

! Eintrag der geraden Anteile
do j = -wvskal%ilsradius,wvskal%ilsradius
    nue = wvskal%dnuefine * real(j)
    ilsparts(j,0,1) = sinc_qu(nue,xeins)
end do
do i = 1,n_ifg - 2
    xi = xnorm * instr%opdmax * real(i)
    do j = -wvskal%ilsradius,wvskal%ilsradius
         nue = wvskal%dnuefine * real(j)
         ilsparts(j,i,1) = 2.0 * sinc_qu(nue,xeins) * cos(2.0 * pi * nue * xi)
    end do
end do
do j = -wvskal%ilsradius,wvskal%ilsradius
    nue = wvskal%dnuefine * real(j)
    ilsparts(j,n_ifg-1,1) = sinc_qu(nue,xeins) * cos(2.0 * pi * nue * instr%opdmax)&
     + cosinc_qu(nue,xeins) * sin(2.0 * pi * nue * instr%opdmax)
end do

! Eintrag der ungeraden Anteile
do j = -wvskal%ilsradius,wvskal%ilsradius
    nue = wvskal%dnuefine * real(j)
    ilsparts(j,0,2) = cosinc_qu(nue,xeins)
end do
do i = 1,n_ifg - 2
    xi = xnorm * instr%opdmax * real(i)
    do j = -wvskal%ilsradius,wvskal%ilsradius
         nue = wvskal%dnuefine * real(j)
         ilsparts(j,i,2) = 2.0 * sinc_qu(nue,xeins) * sin(2.0 * pi * nue * xi)
    end do
end do
do j = -wvskal%ilsradius,wvskal%ilsradius
    nue = wvskal%dnuefine * real(j)
    ilsparts(j,n_ifg-1,2) = sinc_qu(nue,xeins) * sin(2.0 * pi * nue * instr%opdmax)&
     - cosinc_qu(nue,xeins) * cos(2.0 * pi * nue * instr%opdmax)
end do

! Anwendung der Apodisierung (numerisch) auf ILS-Zerlegung
do i = 0,n_ifg - 1
    ilsparts(:,i,1) = modul_abs(i) * ilsparts(:,i,1)
    ilsparts(:,i,2) = modul_abs(i) * ilsparts(:,i,2)
end do

contains

! internal function init_ils: cosinc
real function cosinc(nue,xwert)

use globfwd23, only: pi

implicit none

real,intent(in) :: nue,xwert
real(8),parameter :: epswert = 1.0d-20

if (abs(nue * xwert) .gt. epswert) then
    cosinc = 2.0d0 * sin(pi * nue * xwert) * sin(pi * nue * xwert) / (pi * nue)
else
    cosinc = 2.0d0 * pi * nue * xwert * xwert * (1.0d0  - &
      0.3166666666666666d0 * (pi * nue * xwert)**4)
end if

end function cosinc

! internal function init_ils: cosinc
real function cosinc_qu(nue,xwert)

use globfwd23, only: pi

implicit none

real,intent(in) :: nue,xwert
real(8),parameter :: epswert = 1.0d-20

if (abs(nue * xwert) .gt. epswert) then
    cosinc_qu = (2.0d0 * pi * nue * xwert - sin(2.0d0 * pi * nue * xwert))&
      / (2.0d0 * pi * pi * nue * nue * xwert)
else
    cosinc_qu = 0.6666666666666666d0 * pi * nue * xwert * xwert * &
      ( 1.0d0 - 0.2d0 * pi * pi * nue * nue * xwert * xwert)
end if

end function cosinc_qu

! internal function init_ils: sinc
real function sinc(nue,xwert)

use globfwd23, only: pi

implicit none

real,intent(in) :: nue,xwert
real(8),parameter :: epswert = 1.0d-20

if (abs(nue * xwert) .gt. epswert) then
    sinc = sin(2.0d0 * pi * nue * xwert) / (pi * nue)
else
    sinc = xwert * (2.0d0  - &
      1.333333333333333d0 * pi * pi * nue * nue * xwert * xwert)
end if

end function sinc

! internal function init_ils: cosinc
real function sinc_qu(nue,xwert)

use globfwd23, only: pi

implicit none

real,intent(in) :: nue,xwert
real(8),parameter :: epswert = 1.0d-20

if (abs(nue * xwert) .gt. epswert) then
    sinc_qu = sin(pi * nue * xwert) * sin(pi * nue * xwert) &
      / (pi * pi * nue * nue * xwert)
else
    sinc_qu = xwert * (1.0d0 - &
      0.316666666666666d0 * (pi * nue * xwert)**4)
end if

end function sinc_qu

end subroutine init_ils



!====================================================================
!  init_lblcalc: set lower and upper pointer to lines which might require in-depth lbl calculation
!               (these lines are inside or near the extended MW)
!====================================================================
subroutine init_lblcalc()

use globfwd23
use globlines23

implicit none

integer :: ilin,imw,itau,lptr_lbl,rptr_lbl,lptr_rim,rptr_rim,iT
real(8) :: masseamu,lownue,highnue,qa,qb,qc,qd,qe
real(8),dimension(3) :: vnormwert

do itau = 1,n_tau_lbl

    do imw = 1,n_mw

        lownue = mw(imw)%firstnuefinext - mwextd_lbl
        highnue = mw(imw)%lastnuefinext + mwextd_lbl

        if (lines%rb(imw,itau) - lines%lb(imw,itau) .ge. 0) then

            ! rim pointers

            lptr_rim = lines%lb(imw,itau) - 1
            rptr_rim = lines%rb(imw,itau) + 1

            do ilin = lines%lb(imw,itau),lines%rb(imw,itau)
                if (lines%nue(ilin) .lt. lownue) lptr_rim = ilin
            end do

            do ilin = lines%rb(imw,itau),lines%lb(imw,itau),-1
                if (lines%nue(ilin) .gt. highnue) rptr_rim = ilin
            end do

            ! lbl pointers

            lptr_lbl = lptr_rim + 1
            rptr_lbl = rptr_rim - 1

            do ilin = lines%lb(imw,itau),lines%rb(imw,itau)
                if ((lines%nue(ilin) - lownue) * (highnue - lines%nue(ilin)) .gt. 0.0d0) rptr_lbl = ilin
            end do

            do ilin = lines%rb(imw,itau),lines%lb(imw,itau),-1
                if ((lines%nue(ilin) - lownue) * (highnue - lines%nue(ilin)) .gt. 0.0d0) lptr_lbl = ilin
            end do


            lines%lb_lbl(imw,itau) = lptr_lbl
            lines%rb_lbl(imw,itau) = rptr_lbl

            lines%lb_rim(imw,itau) = lptr_rim
            lines%rb_rim(imw,itau) = rptr_rim

        else
            lines%lb_lbl(imw,itau) = lines%lb(imw,itau)
            lines%rb_lbl(imw,itau) = lines%rb(imw,itau)
            lines%lb_rim(imw,itau) = lines%rb(imw,itau)
            lines%rb_rim(imw,itau) = lines%lb(imw,itau)
        end if

    end do

end do

! generate control file
if (fwdtofiledec) then
    open (iunit_fwd,file =  trim(outpfad_fwd) // '/' //'LBLPTR.TST',status = 'replace')
    do itau = 1,n_tau_lbl
        do imw = 1,n_mw
            write(iunit_fwd,'(A12,I4, I4)') 'Spezies  MW ',itau,imw
            write(iunit_fwd,'(A40,3(1X,I7))') 'Full pointer section:' &
              ,lines%lb(imw,itau),lines%rb(imw,itau),lines%rb(imw,itau) - lines%lb(imw,itau) + 1
            write(iunit_fwd,'(A40,3(1X,I7))') 'Lbl pointer section: ' &
              ,lines%lb_lbl(imw,itau),lines%rb_lbl(imw,itau),lines%rb_lbl(imw,itau) - lines%lb_lbl(imw,itau) + 1
            write(iunit_fwd,'(A40,3(1X,I7))') 'Lrim pointer section:' &
              ,lines%lb(imw,itau),lines%lb_rim(imw,itau),lines%lb_rim(imw,itau) - lines%lb(imw,itau) + 1
            write(iunit_fwd,'(A40,3(1X,I7))') 'Rrim pointer section:' &
              ,lines%rb_rim(imw,itau),lines%rb(imw,itau),lines%rb(imw,itau) - lines%rb_rim(imw,itau) + 1
        end do
    end do
    close (iunit_fwd)
end if

! Precalculate gaussian width for T = 296.0d0 and estimate linestrength as function of T
! Precalculate Lorentzian width at ppa = 1 and T = 296.0d0

T_lbl(1) = 180.0d0
T_lbl(2) = 250.0d0
T_lbl(3) = 320.0d0

do itau = 1,n_tau_lbl

    do imw = 1,n_mw

        do ilin = lines%lb(imw,itau),lines%rb(imw,itau)

            ! Gaussian width at reference T (296 K)
            call infospecies(lines%species(ilin),masseamu,qa,qb,qc,qd,qe)
            lines%gauhwhm(ilin) = 0.832555d0 * lines%nue(ilin) / clicht * sqrt(2.0d0 * kboltz * 296.0d0 / (amunit * masseamu))

            do iT = 1,3
                vnormwert(iT) = make_vnorm(lines%species(ilin),lines%nue(ilin) &
                  ,lines%ergniveau(ilin),lines%kappacm(ilin),T_lbl(iT))
            end do

            ! parabolic fit
            lines%vnorm_coeffa(ilin) = vnormwert(2)
            lines%vnorm_coeffb(ilin) = 0.5d0 * (vnormwert(3) - vnormwert(1))
            lines%vnorm_coeffc(ilin) = 0.5d0 * (vnormwert(1) + vnormwert(3))

        end do

    end do

end do

end subroutine init_lblcalc



!====================================================================
!  init_lblkennung:  Construct name for binary lbl-file
!                    check whether binary lbl-file is present
!====================================================================
subroutine init_lblkennung(lblbindec)

use globfwd23

implicit none

logical,intent(out) :: lblbindec
character(150) :: curchar
integer :: i,j,iowert,zaehler
real(8) :: wert,charfloat

write (lblkennung(1:3),'(A3)') 'MW_'
write (lblkennung(4:5),'(I2.2)') n_mw
write (lblkennung(6:8),'(A3)') 'NT_'
write (lblkennung(9:10),'(I2.2)') n_tau_lbl

! calculate first characteristic number for MW bounds
wert = 0.0d0
do i = 1,n_mw
    wert = wert + cos(0.001d0 * real(i,8) * mw(i)%firstnue_input) * cos(mw(i)%firstnue_input) &
      + cos(0.001d0 * real(i,8) * mw(i)%lastnue_input) * cos(mw(i)%lastnue_input)
end do
wert = 0.5d0 * abs(wert) / real(n_mw,8)
write (lblkennung(11:13),'(A3)') 'NS_'
write (lblkennung(14:23),'(I10.10)') int(9.99d8 * wert)

! calculate second characteristic number for MW bounds
wert = 0.0d0
do i = 1,n_mw
    wert = wert + cos(0.01d0 * real(i,8) * mw(i)%firstnue_input) * cos(mw(i)%firstnue_input) &
      + sin(mw(i)%lastnue_input - mw(i)%firstnue_input) * sin(1.0d-4 * mw(i)%lastnue_input)
end do
wert = 0.5d0 * abs(wert) / real(n_mw,8)
write (lblkennung(24:26),'(A3)') 'NS_'
write (lblkennung(27:36),'(I10.10)') int(9.99d8 * wert)

! calculate characteristic number from HITRAN file names
wert = 0.0d0
zaehler = 0
do i = 1,n_tau_lbl
    curchar = trim(hitdatei(i))
    do j = 1,len(trim(curchar))
        charfloat = real(iachar(curchar(j:j)),8)
        wert = wert + cos(0.01d0 * charfloat * real(j,8)) + sin(0.01d0 * charfloat * charfloat * real(i,8))
        zaehler = zaehler + 1
    end do
end do
wert = 0.5d0 * abs(wert) / real (zaehler,8)
write (lblkennung(37:40),'(A3)') 'NA_'
write (lblkennung(41:50),'(I10.10)') int(9.99d8 * wert)

! check presence of required binary linedata file
print *, trim(wrkpfad) // '/' // lblkennung // '.HIT'
open (iunit_fwd,file = trim(wrkpfad) // '/' // lblkennung // '.HIT',iostat = iowert &
  ,status = 'old',position = 'rewind',action = 'read')
if (iowert .eq. 0) then
    lblbindec = .true.
    print *,'Binary linedata file found!'
else
    lblbindec = .false.
    print *,'Binary linedata file missing!'
end if
close(iunit_fwd)

end subroutine init_lblkennung



!====================================================================
!  init_mws: setting up spectral coarse and fine grid
!====================================================================
subroutine init_mws()

use globfwd23
use globlines23, only : mwextd_readlines_rim

implicit none

integer :: i
real(8) :: mwextd      ! wavenumber extension for each MW (ILS convolution and shift)

! Coarse grid vector
print *,'Setting up coarse grid vector...'

do i = 1,n_mw
    mw(i)%pts = 1 + nint((mw(i)%lastnue - mw(i)%firstnue) / wvskal%dnue)
end do

mw(1)%lb = 1
mw(1)%rb = mw(1)%pts
do i = 2,n_mw
    mw(i)%lb = mw(i-1)%rb + 1
    mw(i)%rb = mw(i)%lb + mw(i)%pts - 1
end do
print *,'Done!'

! Erweiterungen Fine grid
print *,'Determining fine grid spacing and ILS + shift extension...'

wvskal%interpol = 1 + int(wvskal%dnue / (4.0d-7 * mw(1)%firstnue))   ! sampling 0.0040cm-1 at 1000cm-1 or finer
wvskal%dnuefine = wvskal%dnue / wvskal%interpol
print *,'Fine grid spacing:              ',wvskal%dnuefine
wvskal%ilsradius = nint(real(instr%ilsacc,8) &
  / (instr%opdmax * wvskal%dnuefine)) + 1                            ! ILS sidelobes (boxcar modulation)
print *,'Extension ILS convolution:      ',wvskal%ilsradius

wvskal%shiftradius = nint(wvskal%maxshift / wvskal%dnuefine) + 2     ! extension for spectral shift
print *,'Extension for shift (0.1 cm-1): ',wvskal%shiftradius

wvskal%nsolstep = 6

print *,'Done!'

print *,'Setting up fine-grid markers...'

! Fine grid boundaries
mwextd = wvskal%dnuefine * (wvskal%ilsradius + wvskal%shiftradius)
do i = 1,n_mw
    mw(i)%ptsfinext = (mw(i)%pts - 1) * wvskal%interpol + 1 + &
      2 * (wvskal%ilsradius + wvskal%shiftradius)
    mw(i)%firstnuefinext = mw(i)%firstnue - mwextd
    mw(i)%lastnuefinext = mw(i)%lastnue + mwextd
end do

mw(1)%lbfinext = 1
mw(1)%rbfinext = mw(1)%ptsfinext
do i = 2,n_mw
    mw(i)%lbfinext = mw(i-1)%rbfinext + 1
    mw(i)%rbfinext = mw(i)%lbfinext + mw(i)%ptsfinext - 1
end do
print *,'Done!'

! Erweiterungen fuer Einlesen der HITRAN lbl-Daten
mwextd_readlines_rim = 20.0d0 + mwextd

end subroutine init_mws



!====================================================================
!  initspecies: Initialize masses and Gamache coeffs
!====================================================================
subroutine initspecies(speciesdatei)

use globfwd23

implicit none

character(len=*),intent(in) :: speciesdatei
integer :: i,iowert
real :: realdum

i = 0
open (iunit_fwd,file = speciesdatei,status = 'old',action = 'read',iostat = iowert)
if (iowert .ne. 0) then
    print *,speciesdatei
    call warnout('Cannot open species file!',0)
end if
call gonext(iunit_fwd,.false.)
do
    i = i + 1
    if (i .gt. maxspecies) then
        print *,'Maximum possible number of species is:',maxspecies
        call warnout('Error in initspecies: Too many Species!  ',0)
    end if
    read (iunit_fwd,*,end = 100) species(i)%identifier,species(i)%masseamu &
      ,realdum,species(i)%qa,species(i)%qb,species(i)%qc,species(i)%qd,species(i)%qe
end do
100 continue
close (iunit_fwd)
n_species = i

end subroutine initspecies



!====================================================================
!  init_tau_lbl: Berechnung der optischen Dicke aus Liniendaten
!====================================================================
subroutine init_tau_lbl(itau,Tkel,ppa,colair,sza_ref_rad,h2o_ppmv,vmr_ref_ppmv,vmr_act_ppmv,windscal,tau)

use globfwd23
use globlines23
use globsdg23, only : betamax,gam2max

implicit none

integer,parameter :: maxgrid = 6 ! depth of grid sequence for lbl calculation
real(8),parameter :: deltanuecut = 25.0d0 ! line is enforced to zero at this distance from core
real(8),parameter :: slopemax = 0.03d0 ! line is enforced to zero at this distance from core


integer,intent(in) :: itau
real(8),intent(in) :: Tkel,ppa,colair,sza_ref_rad,h2o_ppmv,vmr_ref_ppmv,vmr_act_ppmv,windscal
real,dimension(mw(n_mw)%rbfinext),intent(out) :: tau

logical :: decrim
integer :: i,j,k,noflines_lbl,noflines_rim,gridposcenter,gridpos,igrid,imerk,iposfine,iposwrk &
  ,gridposcoarse,istep,iminstep,n_rim,ilin,idtable
real :: rest,ca,cb,cc,LMTwert,drest,fakt
complex(8) :: abbruch,contribcenter,contriba,contribb
real(8) :: dnuewrk,reslinenue,lorhwhm,gauhwhm,voigtnorm,vhwhm,vhwhminv,deltanue,gam2,beta,YLM,actnue,ncolratio
real(8) :: sloperef !,faktorlor_liu,faktorgau_liu,corr_liu
real(8) :: firstnuewrk,slope,tauschwelle,abbruchre,abbruchim,wert
real(8) :: dnuerim,rimintvl,deltanuestripe,deltanuestripequ,deltanueact,nuestripemin &
  ,nuestripemax,gaufaktor,Twert,suminstripe

! character(2) :: extb

! arrays for spectral interpolation
integer,dimension(8) :: maxgridpos
real,dimension(:),allocatable :: tau_rim
real,dimension(:,:),allocatable :: tau_wrk

gaufaktor = sqrt(3.3783784d-3 * Tkel)
! estimate the lowest tau-value to be considered non-negligible
ncolratio = colair / (0.5d0 * ppa / (mudry * amunit))
tauschwelle = 1.0d-5 * tauschwell_adj * ncolratio * cos(sza_ref_rad - 0.0175) &
  / (1.0d-6 * colair * 0.5d0 * (vmr_ref_ppmv + abs(vmr_act_ppmv)))
! user adjusted slope value
sloperef = slope_adj * slopemax

do i = 1,n_mw

    noflines_lbl = lines%rb_lbl(i,itau) - lines%lb_lbl(i,itau) + 1
    noflines_rim = lines%rb(i,itau) - lines%rb_rim(i,itau) + 1 + lines%lb_rim(i,itau) - lines%lb(i,itau) + 1

    ! rim-treatment, if required (omit for low pressure or if no rim lines present)
    if (ppa .gt. 1.0d3 .and. noflines_rim .gt. 0) then
        decrim = .true.
    else
        decrim = .false.
    end if

    if (decrimall .and. decrim) then
        ! adjust a spectral gridwidth of approx 0.05 cm-1
        n_rim = nint((mw(i)%lastnuefinext - mw(i)%firstnuefinext) / 0.05d0)
        dnuerim = (mw(i)%lastnuefinext - mw(i)%firstnuefinext) / real(n_rim - 1,8)
        allocate (tau_rim(n_rim))
        tau_rim = 0.0
    end if

    if (noflines_lbl .gt. 0) then

        ! determine spectral grid
        ! determine spectral increments for coarse grids
        dnuewrk = 0.4d0 * voigthwhm(5.0d-7 * mw(i)%centernue * sqrt(dnue_adj),2.0d-7 * ppa * dnue_adj)

        ! add coarsegridpoints to the left and to the right for interpolation
        ! (extend by one coarsest grid mash to the left and the rigth)
        firstnuewrk = mw(i)%firstnuefinext - 1.01d0 * mwextd_lbl - dnuewrk * 2**(maxgrid-1)
        maxgridpos(maxgrid) = int((mw(i)%lastnuefinext + 1.01d0 * mwextd_lbl + dnuewrk * 2**(maxgrid-1) - firstnuewrk) &
          / (dnuewrk * 2**(maxgrid-1))) + 1

        do j = maxgrid - 1,1,-1
            maxgridpos(j) = 2 * maxgridpos(j+1) - 1
        end do

        allocate (tau_wrk(maxgridpos(1),maxgrid))
        tau_wrk = 0.0
    else
        tau(mw(i)%lbfinext:mw(i)%rbfinext) = 0.0
    end if

    do j = lines%lb_lbl(i,itau),lines%rb_lbl(i,itau)

        ! initialize Voigt line
        reslinenue = (lines%nue(j) + ppa * lines%pshift(j)) * (1.0d0 + windscal) * (1.0d0 + skalspeci(i,itau))
        voigtnorm = make_vnorm(lines%species(j),lines%nue(j),lines%ergniveau(j),lines%kappacm(j),Tkel)
        gauhwhm = lines%gauhwhm(j) * gaufaktor
        lorhwhm = ppa * ((1.0d0 - 1.0d-6 * h2o_ppmv) * lines%lorwidthf(j) &
          + 1.0d-6 * h2o_ppmv * lines%lorwidths(j)) * (296.0d0 / Tkel)**lines%lortdepend(j)
        vhwhm = voigthwhm(gauhwhm,lorhwhm)
        vhwhminv = 1.0d0 / vhwhm
        LMTwert = 0.01666667d0 * (Tkel - 260.0d0) ! LM-Ref Temperaturen: 200/260/320 K
        YLM = ppa * lines%YLM(j) * (1.0 + LMTwert * (lines%LMTK1(j) + LMTwert * lines%LMTK2(j)))
        !!! call make_liuparms(lorhwhm,gauhwhm,vhwhm,voigtnorm,faktorlor_liu,faktorgau_liu,corr_liu)
        call make_voigtluparms(lorhwhm,gauhwhm,idtable,drest,fakt)

        ! Ggf SDG-Shape?
        if (lines%sdgdec(j)) then

            gam2 = lines%gam2(j) * ppa * (296.0d0 / Tkel)**lines%gam2tdepend(j)
            beta = lines%beta(j) * ppa * (296.0d0 / Tkel)**lines%betatdepend(j)
            if (gam2 / lorhwhm .gt. gam2max) then
                print *,'Species:',lines%species(j)
                print *,'Line position:',lines%nue(j)
                print *,'p T:',ppa,Tkel
                call warnout('requested gam2 outside table!',1)
            end if
            if (beta / lorhwhm .gt. betamax) then
                print *,'Species:',lines%species(j)
                print *,'Line position:',lines%nue(j)
                print *,'p T:',ppa,Tkel
                call warnout('requested beta outside table!',1)
            end if

            ! line generation for SDG case
            ! determine nearest point on finest grid
            gridposcenter = nint((reslinenue - firstnuewrk) / dnuewrk) + 1
            deltanue =  firstnuewrk + real(gridposcenter - 1,8) * dnuewrk - reslinenue
            iminstep = int(vhwhm / dnuewrk) + 3

            ! determine contribution
            !!!abbruch = voigtliumod(deltanuecut,vhwhm,faktorlor_liu,faktorgau_liu,corr_liu,YLM)
            abbruch = voigtlookup(deltanuecut,vhwhminv,voigtnorm,YLM,idtable,drest,fakt)
            abbruchre = real(abbruch,8)
            abbruchim = aimag(abbruch)
            !!! contribcenter = voigtliumod(deltanue,vhwhm,faktorlor_liu,faktorgau_liu,corr_liu,YLM) &
            !!!   * (1.0 + SDGkorr(deltanue,gauhwhm,lorhwhm,gam2,beta)) - cmplx(abbruchre,abbruchim * deltanue / deltanuecut)
            contribcenter = voigtlookup(deltanue,vhwhminv,voigtnorm,YLM,idtable,drest,fakt) &
              * (1.0 + SDGkorr(deltanue,gauhwhm,lorhwhm,gam2,beta)) - cmplx(abbruchre,abbruchim * deltanue / deltanuecut)

            ! add contribution from line center
            if (real(contribcenter,8) .gt. tauschwelle) then
                tau_wrk(gridposcenter,1) = tau_wrk(gridposcenter,1) + real(contribcenter,8) + aimag(contribcenter)
            end if

            ! proceed from line center to right side
            igrid = 1
            istep = 0
            gridpos = gridposcenter
            contriba = contribcenter
            do while (abs(contriba) .gt. tauschwelle .and. gridpos .lt. maxgridpos(igrid) - 2)

                ! calculate next value
                gridpos = gridpos + 1
                deltanue = firstnuewrk + real(gridpos - 1,8) * dnuewrk * 2**(igrid-1) - reslinenue
                !!! contribb = voigtliumod(deltanue,vhwhm,faktorlor_liu,faktorgau_liu,corr_liu,YLM) &
                !!!   * (1.0 + SDGkorr(deltanue,gauhwhm,lorhwhm,gam2,beta)) - cmplx(abbruchre,abbruchim * deltanue / deltanuecut)
                contribb = voigtlookup(deltanue,vhwhminv,voigtnorm,YLM,idtable,drest,fakt) &
                  * (1.0 + SDGkorr(deltanue,gauhwhm,lorhwhm,gam2,beta)) - cmplx(abbruchre,abbruchim * deltanue / deltanuecut)

                if (real(contribb) .lt. 0.0d0) exit
                slope = (real(contriba,8) - real(contribb,8)) / (real(contriba,8) + real(contribb,8))

                ! either proceed on current grid or change to coarser grid
                if (istep .gt. iminstep .and. slope .lt. sloperef .and. mod(gridpos-1,2) .eq. 0 &
                  .and. igrid .lt. maxgrid) then

                    tau_wrk(gridpos-1,igrid) = tau_wrk(gridpos-1,igrid) - 0.5 * (real(contribb,8) + aimag(contribb))
                    gridpos = (gridpos - 1) / 2 + 1
                    igrid = igrid + 1
                    tau_wrk(gridpos,igrid) = tau_wrk(gridpos,igrid) + real(contribb,8) + aimag(contribb)
                    contriba = contribb
                    istep = 0

                else

                    contriba = contribb
                    tau_wrk(gridpos,igrid) = tau_wrk(gridpos,igrid) + real(contriba,8) + aimag(contriba)
                    istep = istep + 1

                end if

            end do

            ! proceed from line center to left side
            igrid = 1
            istep = 0
            gridpos = gridposcenter
            contriba = contribcenter
            do while (abs(contriba) .gt. tauschwelle .and. gridpos .gt. 2)

                ! calculate next value
                gridpos = gridpos - 1
                deltanue = firstnuewrk + real(gridpos - 1,8) * dnuewrk * 2**(igrid-1) - reslinenue
                !!! contribb = voigtliumod(deltanue,vhwhm,faktorlor_liu,faktorgau_liu,corr_liu,YLM) &
                !!!   * (1.0 + SDGkorr(deltanue,gauhwhm,lorhwhm,gam2,beta)) - cmplx(abbruchre,abbruchim * deltanue / deltanuecut)
                contribb = voigtlookup(deltanue,vhwhminv,voigtnorm,YLM,idtable,drest,fakt) &
                  * (1.0 + SDGkorr(deltanue,gauhwhm,lorhwhm,gam2,beta)) - cmplx(abbruchre,abbruchim * deltanue / deltanuecut)

                if (real(contribb) .lt. 0.0d0) exit
                slope = (real(contriba,8) - real(contribb,8)) / (real(contriba,8) + real(contribb,8))

                ! either proceed on current grid or change to coarser grid
                if (istep .gt. iminstep .and. slope .lt. sloperef .and. mod(gridpos-1,2) .eq. 0 &
                  .and. igrid .lt. maxgrid) then

                    tau_wrk(gridpos+1,igrid) = tau_wrk(gridpos+1,igrid) - 0.5 * (real(contribb,8) + aimag(contribb))
                    gridpos = (gridpos - 1) / 2 + 1
                    igrid = igrid + 1
                    tau_wrk(gridpos,igrid) = tau_wrk(gridpos,igrid) + real(contribb,8) + aimag(contribb)
                    contriba = contribb
                    istep = 0

                else

                    contriba = contribb
                    tau_wrk(gridpos,igrid) = tau_wrk(gridpos,igrid) + real(contriba,8) + aimag(contriba)
                    istep = istep + 1

                end if

            end do

        else

            ! line generation for Voigt case
            ! determine nearest point on finest grid
            gridposcenter = nint((reslinenue - firstnuewrk) / dnuewrk) + 1
            deltanue =  firstnuewrk + real(gridposcenter - 1,8) * dnuewrk - reslinenue
            iminstep = int(vhwhm / dnuewrk) + 3

            ! determine contribution
            !!! abbruch = voigtliumod(deltanuecut,vhwhm,faktorlor_liu,faktorgau_liu,corr_liu,YLM)
            abbruch = voigtlookup(deltanuecut,vhwhminv,voigtnorm,YLM,idtable,drest,fakt)
            abbruchre = real(abbruch,8)
            abbruchim = aimag(abbruch)
            !!! contribcenter = voigtliumod(deltanue,vhwhm,faktorlor_liu,faktorgau_liu,corr_liu,YLM) &
            !!!   - cmplx(abbruchre,abbruchim * deltanue / deltanuecut)
            contribcenter = voigtlookup(deltanue,vhwhminv,voigtnorm,YLM,idtable,drest,fakt) &
              - cmplx(abbruchre,abbruchim * deltanue / deltanuecut)

            ! add contribution from line center
            if (real(contribcenter,8) .gt. tauschwelle) then
                tau_wrk(gridposcenter,1) = tau_wrk(gridposcenter,1) + real(contribcenter,8) + aimag(contribcenter)
            end if

            ! proceed from line center to right side
            igrid = 1
            istep = 0
            gridpos = gridposcenter
            contriba = contribcenter
            do while (abs(contriba) .gt. tauschwelle .and. gridpos .lt. maxgridpos(igrid) - 2)

                ! calculate next value
                gridpos = gridpos + 1
                deltanue = firstnuewrk + real(gridpos - 1,8) * dnuewrk * 2**(igrid-1) - reslinenue
                !!! contribb = voigtliumod(deltanue,vhwhm,faktorlor_liu,faktorgau_liu,corr_liu,YLM) &
                !!!   - cmplx(abbruchre,abbruchim * deltanue / deltanuecut)
                contribb = voigtlookup(deltanue,vhwhminv,voigtnorm,YLM,idtable,drest,fakt) &
                  - cmplx(abbruchre,abbruchim * deltanue / deltanuecut)

                if (real(contribb) .lt. 0.0d0) exit
                slope = (real(contriba,8) - real(contribb,8)) / (real(contriba,8) + real(contribb,8))

                ! either proceed on current grid or change to coarser grid
                if (istep .gt. iminstep .and. slope .lt. sloperef .and. mod(gridpos-1,2) .eq. 0 &
                  .and. igrid .lt. maxgrid) then

                    tau_wrk(gridpos-1,igrid) = tau_wrk(gridpos-1,igrid) - 0.5 * (real(contribb,8) + aimag(contribb))
                    gridpos = (gridpos - 1) / 2 + 1
                    igrid = igrid + 1
                    tau_wrk(gridpos,igrid) = tau_wrk(gridpos,igrid) + real(contribb,8) + aimag(contribb)
                    contriba = contribb
                    istep = 0

                else

                    contriba = contribb
                    tau_wrk(gridpos,igrid) = tau_wrk(gridpos,igrid) + real(contriba,8) + aimag(contriba)
                    istep = istep + 1

                end if

            end do

            ! proceed from line center to left side
            igrid = 1
            istep = 0
            gridpos = gridposcenter
            contriba = contribcenter
            do while (abs(contriba) .gt. tauschwelle .and. gridpos .gt. 2)

                ! calculate next value
                gridpos = gridpos - 1
                deltanue = firstnuewrk + real(gridpos - 1,8) * dnuewrk * 2**(igrid-1) - reslinenue
                !!! contribb = voigtliumod(deltanue,vhwhm,faktorlor_liu,faktorgau_liu,corr_liu,YLM) &
                !!!   - cmplx(abbruchre,abbruchim * deltanue / deltanuecut)
                contribb = voigtlookup(deltanue,vhwhminv,voigtnorm,YLM,idtable,drest,fakt) &
                  - cmplx(abbruchre,abbruchim * deltanue / deltanuecut)

                if (real(contribb) .lt. 0.0d0) exit
                slope = (real(contriba,8) - real(contribb,8)) / (real(contriba,8) + real(contribb,8))

                ! either proceed on current grid or change to coarser grid
                if (istep .gt. iminstep .and. slope .lt. sloperef .and. mod(gridpos-1,2) .eq. 0 &
                  .and. igrid .lt. maxgrid) then

                    tau_wrk(gridpos+1,igrid) = tau_wrk(gridpos+1,igrid) - 0.5 * (real(contribb,8) + aimag(contribb))
                    gridpos = (gridpos - 1) / 2 + 1
                    igrid = igrid + 1
                    tau_wrk(gridpos,igrid) = tau_wrk(gridpos,igrid) + real(contribb,8) + aimag(contribb)
                    contriba = contribb
                    istep = 0

                else

                    contriba = contribb
                    tau_wrk(gridpos,igrid) = tau_wrk(gridpos,igrid) + real(contriba,8) + aimag(contriba)
                    istep = istep + 1

                end if

            end do

        end if

    end do ! loop over spectral lines

    if (noflines_lbl .gt. 0) then
        !! interpolate towards finest work grid
        do igrid = maxgrid - 1,1,-1
            do gridpos = 1,maxgridpos(igrid)
                gridposcoarse = (gridpos - 1) / 2 + 1
                if (mod(gridpos,2) .eq. 0) then
                    ! even grid position (linear interpolation between two points of coarser grid)
                    tau_wrk(gridpos,igrid) = tau_wrk(gridpos,igrid) &
                      + 0.5 * (tau_wrk(gridposcoarse,igrid+1) + tau_wrk(gridposcoarse+1,igrid+1))
                else
                    ! odd grid position (add contribution from coarser grid)
                    tau_wrk(gridpos,igrid) = tau_wrk(gridpos,igrid) + tau_wrk(gridposcoarse,igrid+1)
                end if
            end do
        end do

        ! interpolate towards finegrid (linear interpolation at altitudes beyond 35 km)
        if (ppa .gt. 5.0d2) then
            ! polynomial interpolation
            imerk = 0
            do iposfine = mw(i)%lbfinext,mw(i)%rbfinext
                actnue = mw(i)%firstnuefinext + wvskal%dnuefine * (iposfine - mw(i)%lbfinext)
                wert = (actnue - firstnuewrk) / dnuewrk
                iposwrk = int(wert)
                rest = real(wert - real(iposwrk,8))
                iposwrk = iposwrk + 1
                ! interpolation coeffs
                if (iposwrk .gt. imerk) then
                    imerk = iposwrk
                    ca = 0.5 * (tau_wrk(iposwrk+1,1) - tau_wrk(iposwrk-1,1))
                    cb = 0.5 * (2.0 * tau_wrk(iposwrk-1,1) - 5.0 * tau_wrk(iposwrk,1) &
                      + 4.0 * tau_wrk(iposwrk+1,1) - tau_wrk(iposwrk+2,1))
                    cc = 0.5 * (-tau_wrk(iposwrk-1,1) + 3.0 * tau_wrk(iposwrk,1) &
                      - 3.0 * tau_wrk(iposwrk+1,1) + tau_wrk(iposwrk+2,1))
                end if
                tau(iposfine) = tau_wrk(iposwrk,1) + rest * (ca + rest * (cb + rest * cc))
            end do
        else
           ! linear interpolation
            do iposfine = mw(i)%lbfinext,mw(i)%rbfinext
                actnue = mw(i)%firstnuefinext + wvskal%dnuefine * (iposfine - mw(i)%lbfinext)
                wert = (actnue - firstnuewrk) / dnuewrk
                iposwrk = int(wert)
                rest = real(wert - real(iposwrk,8))
                iposwrk = iposwrk + 1
                tau(iposfine) = (1.0 - rest) * tau_wrk(iposwrk,1) + rest * tau_wrk(iposwrk+1,1)
            end do
        end if

        !! Testoutput
        !if (ilev .eq. 30 .and. itau .eq. 7 .and. i .eq. 1) then
        !    do igrid = 1,maxgrid
        !        write (extb,'(I2.2)') igrid
        !        open (iunit_fwd,file = trim(outpfad)//'\'//'test'//extb//'.dat',status = 'replace')
        !
        !        do k = 1,maxgridpos(igrid)
        !            write (10,'(ES15.9,80(1X,ES12.4))') firstnuewrk + dnuewrk * 2**(igrid-1) * (k - 1),tau_wrk(k,igrid)
        !        end do
        !
        !        close (iunit_fwd)
        !    end do
        !end if

        deallocate (tau_wrk)

    end if

    ! rim contributions
    if (decrimall .and. decrim) then

        ! smallest spectral interval for rim treatment
        rimintvl = (mw(i)%lastnue + mwextd_readlines_rim - (mw(i)%lastnuefinext + mwextd_lbl)) / real(maxrim * maxrim,8)

        if (rimintvl .lt. 0) then
            call warnout('Negative rimintvl!',0)
        end if

        Twert = (Tkel - T_lbl(2)) / (T_lbl(3) - T_lbl(1))

        ! left rim
        if (lines%lb_rim(i,itau) - lines%lb(i,itau) + 1 .gt. 0) then

            ilin = lines%lb_lbl(i,itau) - 1

            do j = 1,maxrim

                suminstripe = 0.0d0
                nuestripemin = mw(i)%firstnuefinext - mwextd_lbl - rimintvl * j * j
                deltanuestripe = mwextd_lbl + 0.5d0 * rimintvl * (j * j + (j - 1) * (j - 1))

                do while (ilin .gt. lines%lb(i,itau) .and. lines%nue(ilin) .ge. nuestripemin)

                    ! determine contribution from current line
                    deltanue = mw(i)%firstnuefinext - lines%nue(ilin) * (1.0d0 + skalspeci(i,itau))

                    gauhwhm = lines%gauhwhm(ilin) * gaufaktor
                    lorhwhm = ppa * ((1.0d0 - 1.0d-6 * h2o_ppmv) * lines%lorwidthf(ilin) &
                      + 1.0d-6 * h2o_ppmv * lines%lorwidths(ilin)) * (296.0d0 / Tkel)**lines%lortdepend(ilin)

                    vhwhm = voigthwhm(gauhwhm,lorhwhm)
                    vhwhminv = 1.0d0 / vhwhm
                    voigtnorm = lines%vnorm_coeffa(ilin) + Twert * (lines%vnorm_coeffb(ilin) + Twert * lines%vnorm_coeffc(ilin))
                    !!! call make_liuparms(lorhwhm,gauhwhm,vhwhm,voigtnorm,faktorlor_liu,faktorgau_liu,corr_liu)
                    call make_voigtluparms(lorhwhm,gauhwhm,idtable,drest,fakt)


                    ! calculate Voigt at extended MW boarder, use 1/x2 scaling elsewhere
                    !!! suminstripe = suminstripe + voigtliumod(deltanue,vhwhm,faktorlor_liu,faktorgau_liu,corr_liu,0.0d0)
                    suminstripe = suminstripe + voigtlookup(deltanue,vhwhminv,voigtnorm,0.0d0,idtable,drest,fakt)
                    ! wind to next line
                    ilin = ilin - 1

                end do

                ! add current bin contribution to tau_rim if relevant

                if (suminstripe .gt. tauschwelle) then
                    deltanuestripequ = deltanuestripe * deltanuestripe
                    do k = 1,n_rim
                        deltanueact = real(k - 1,8) * dnuerim + deltanuestripe
                        tau_rim(k) = tau_rim(k) + suminstripe * deltanuestripequ / (deltanueact * deltanueact)
                    end do
                end if

            end do

        end if  ! end of left rim treatment (if required)

        ! right rim
        if (lines%rb(i,itau) - lines%rb_rim(i,itau) + 1 .gt. 0) then

            ilin = lines%rb_lbl(i,itau) + 1

            do j = 1,maxrim

                suminstripe = 0.0d0
                nuestripemax = mw(i)%lastnuefinext + mwextd_lbl + rimintvl * j * j
                deltanuestripe = mwextd_lbl + 0.5d0 * rimintvl * (j * j + (j - 1) * (j - 1))

                do while (ilin .lt. lines%rb(i,itau) .and. lines%nue(ilin) .le. nuestripemax)

                    ! determine contribution from current line
                    deltanue = lines%nue(ilin) * (1.0d0 + skalspeci(i,itau)) - mw(i)%lastnuefinext

                    gauhwhm = lines%gauhwhm(ilin) * gaufaktor
                    lorhwhm = ppa * ((1.0d0 - 1.0d-6 * h2o_ppmv) * lines%lorwidthf(ilin) &
                      + 1.0d-6 * h2o_ppmv * lines%lorwidths(ilin)) * (296.0d0 / Tkel)**lines%lortdepend(ilin)

                    vhwhm = voigthwhm(gauhwhm,lorhwhm)
                    vhwhminv = 1.0d0 / vhwhm
                    voigtnorm = lines%vnorm_coeffa(ilin) + Twert * (lines%vnorm_coeffb(ilin) + Twert * lines%vnorm_coeffc(ilin))
                    !!! call make_liuparms(lorhwhm,gauhwhm,vhwhm,voigtnorm,faktorlor_liu,faktorgau_liu,corr_liu)
                    call make_voigtluparms(lorhwhm,gauhwhm,idtable,drest,fakt)

                    ! calculate Voigt at extended MW boarder, use 1/x2 scaling elsewhere
                    !!! suminstripe = suminstripe + voigtliumod(deltanue,vhwhm,faktorlor_liu,faktorgau_liu,corr_liu,0.0d0)
                    suminstripe = suminstripe + voigtlookup(deltanue,vhwhminv,voigtnorm,0.0d0,idtable,drest,fakt)
                    ! wind to next line
                    ilin = ilin + 1

                end do

                ! add current bin contribution to tau_rim if relevant

                if (suminstripe .gt. tauschwelle) then
                    deltanuestripequ = deltanuestripe * deltanuestripe
                    do k = 1,n_rim
                        deltanueact = real(n_rim - k,8) * dnuerim + deltanuestripe
                        tau_rim(k) = tau_rim(k) + suminstripe * deltanuestripequ / (deltanueact * deltanueact)
                    end do
                end if

            end do

        end if  ! end of right rim treatment (if required)

        ! add rim contributions to tau (linear interpolation)
        tau(mw(i)%lbfinext) = tau(mw(i)%lbfinext) + tau_rim(1)
        tau(mw(i)%rbfinext) = tau(mw(i)%rbfinext) + tau_rim(n_rim)
        do iposfine = mw(i)%lbfinext + 1,mw(i)%rbfinext - 1
            wert = (wvskal%dnuefine * (iposfine - mw(i)%lbfinext)) / dnuerim
            iposwrk = int(wert)
            rest = real(wert - real(iposwrk,8))
            iposwrk = iposwrk + 1
            tau(iposfine) = tau(iposfine) + (1.0 - rest) * tau_rim(iposwrk) + rest * tau_rim(iposwrk+1)
        end do

        deallocate (tau_rim)

    end if  ! end of rim treatment (if required)

end do  ! loop over MWs

end subroutine init_tau_lbl



!====================================================================
!  load_mess: load measured spectrum
!====================================================================
subroutine load_mess(messdatei,messkind,ptsinmw,firstnue,dnue,messxils)

use globfwd23, only : iunit_fwd

implicit none

character(len=*),intent(in) :: messdatei
integer,intent(in) :: messkind,ptsinmw
real(8),intent(in) :: firstnue,dnue
real,dimension(ptsinmw),intent(out) :: messxils

character(2) :: dumchar
character(80) :: sfitheader
integer :: i,nfirst,dumint
real :: spydummy
real(8) :: xdummy,ydummy,firstnue_mess,dnue_mess,dumdble

select case (messkind)
    case (0) ! dpt-Format

        open (iunit_fwd,file = messdatei,status = 'old',action = 'read')
        xdummy = 1.0d0
        do while (xdummy - firstnue .lt. -0.01d0 * dnue)
            read (iunit_fwd,*) xdummy,ydummy
        end do
        messxils(1) = ydummy
        do i = 2,ptsinmw
            read (iunit_fwd,*) xdummy,messxils(i)
        end do
        close (iunit_fwd)

    case (1) ! eigenes bin-Format

        open (iunit_fwd,file = messdatei, status = 'old',action = 'read')
        do i = 1,6
            call gonext(iunit_fwd,.true.)
        end do
        read(iunit_fwd) dumchar
        read(iunit_fwd) firstnue_mess
        read(iunit_fwd) dnue_mess
        read(iunit_fwd) dumint
        nfirst = nint((firstnue - firstnue_mess) / dnue_mess) + 1
        do i = 1,nfirst - 1
            read(iunit_fwd) spydummy
        end do
        do i = 1,ptsinmw
            read(iunit_fwd) spydummy
            messxils(i) = spydummy
        end do
        close (iunit_fwd)

    case (2) ! SFIT2-bin-Format

        open (iunit_fwd,file = messdatei, status = 'old',action = 'read')
        read (iunit_fwd) sfitheader
        read (iunit_fwd) firstnue_mess
        read (iunit_fwd) dumdble
        read (iunit_fwd) dnue_mess
        read (iunit_fwd) dumint
        nfirst = nint((firstnue - firstnue_mess) / dnue_mess) + 1
        do i = 1,nfirst - 1
            read(iunit_fwd) spydummy
        end do
        do i = 1,ptsinmw
            read(iunit_fwd) spydummy
            messxils(i) = spydummy
        end do
        close (iunit_fwd)

end select

end subroutine load_mess



!====================================================================
!  make_bb: generate Planck for given T (nW / cm2 sterad cm-1)
!====================================================================
subroutine make_bb(T,bb)

use globfwd23

implicit none

real(8),intent(in) :: T
real,dimension(mw(n_mw)%rbfinext),intent(out) :: bb

integer :: i,j
real(8) :: exp_bb,dexp_bb,nue

do i = 1,n_mw

    exp_bb = exp(PlanckB * mw(i)%firstnuefinext / T)
    dexp_bb = exp(PlanckB * wvskal%dnuefine / T)

    do j = 1,mw(i)%ptsfinext

        nue = mw(i)%firstnuefinext + wvskal%dnuefine * real(j - 1,8)

        bb(mw(i)%lbfinext+j-1) = PlanckA * nue * nue * nue / (exp_bb - 1.0d0)

        exp_bb = exp_bb * dexp_bb

    end do
end do

end subroutine make_bb



!====================================================================
!  make_bbratio: calculates ratio between Planck functions
!====================================================================
subroutine make_bbratio(T,Tref,planckratio)

use globfwd23

implicit none

real(8),intent(in) :: T,Tref
real,dimension(mw(n_mw)%rbfinext),intent(out) :: planckratio

integer :: i,j
real(8) :: exp_bb_atm,exp_bb_gnd,dexp_bb_atm,dexp_bb_gnd

do i = 1,n_mw

    exp_bb_atm = exp(PlanckB * mw(i)%firstnuefinext / T)
    dexp_bb_atm = exp(PlanckB * wvskal%dnuefine / T)

    exp_bb_gnd = exp(PlanckB * mw(i)%firstnuefinext / Tref)
    dexp_bb_gnd = exp(PlanckB * wvskal%dnuefine / Tref)

    do j = 1,mw(i)%ptsfinext

        planckratio(mw(i)%lbfinext+j-1) = (exp_bb_gnd - 1.0d0) / (exp_bb_atm - 1.0d0)

        exp_bb_atm = exp_bb_atm * dexp_bb_atm
        exp_bb_gnd = exp_bb_gnd * dexp_bb_gnd

    end do
end do

end subroutine make_bbratio



!====================================================================
!  make_h2o_cont: calculate H2O continuum, selected level (all spectral points)
!====================================================================
subroutine make_h2o_cont(TKel,ppa,h2o_absmix,tau)

use globfwd23

implicit none

real(8),intent(in) :: Tkel,ppa,h2o_absmix
real,dimension(mw(n_mw)%rbfinext),intent(out) :: tau

integer :: i,j,ifine,ipos,ncalc
real :: rest
real(8) :: xpos,pmbar,actnue
real(8),dimension(:),allocatable :: wrktau

pmbar = 0.01 * ppa

do i = 1,n_mw
    ! calculation of continuum on a ~ 1cm-1 grid
    ncalc = 2 + nint(mw(i)%lastnuefinext - mw(i)%firstnuefinext)
    allocate (wrktau(ncalc))
    wrktau(:) = 0.0d0
    do j = 1,ncalc
        actnue = mw(i)%firstnuefinext + (mw(i)%lastnuefinext - mw(i)%firstnuefinext) &
          * real(j - 1,8) / real(ncalc - 1,8)
        call calc_h2ocont_ckd25(actnue,TKel,pmbar,h2o_absmix,wrktau(j))
    end do
    ! linear interpolation on fine grid
    do ifine = 1,mw(i)%ptsfinext
        xpos = real(ifine * (ncalc - 1),8) / real(mw(i)%ptsfinext + 1,8)
        ipos = int(xpos)
        rest = xpos - real(ipos,8)
        tau(mw(i)%lbfinext + ifine - 1) = (1.0 - rest) * wrktau(ipos) + rest * wrktau(ipos+1)
    end do
    deallocate (wrktau)
end do

end subroutine make_h2o_cont



!====================================================================
!  make_ils: calculate ILS
!====================================================================
subroutine make_ils(centernue,apolin,phaslin,ilsparts,ils)

use globfwd23, only: n_ifg,pi,instr,wvskal

implicit none

real(8),intent(in) :: centernue
real,intent(in) :: apolin,phaslin
real,dimension(-wvskal%ilsradius:wvskal%ilsradius,0:n_ifg-1,2),intent(in) :: ilsparts
real,dimension(-wvskal%ilsradius:wvskal%ilsradius),intent(out) :: ils

integer :: i
real :: modwert,modphas,norm,boxnue,term,omega,coswert
real,dimension(0:n_ifg-1) :: modul_re,modul_im

! apply misalignment
do i = 0,n_ifg - 1
    modwert = instr%ilsparms(i,1) * (1.0d0 + (apolin - 1.0) * real(i) / real(n_ifg-1))
    modphas = phaslin + instr%ilsparms(i,2)
    modul_re(i) = cos(modphas) * modwert
    modul_im(i) = -sin(modphas) * modwert
end do

! apply self-apodisation
boxnue = 0.5 * instr%coneifm_rad * instr%coneifm_rad * centernue
do i = 1,n_ifg - 1
    term = pi * boxnue * instr%opdmax * real(i) / real(n_ifg - 1)
    modwert = sin(term) / term
    modul_re(i) = modul_re(i) * modwert
    modul_im(i) = modul_im(i) * modwert
end do

! calculate ILS (area normalized to unity)
ils = 0.0d0
norm = 1.0d0 / modul_re(0)
do i = 0,n_ifg - 1
    ils(:) = ils(:) + norm * (modul_re(i) * ilsparts(:,i,1) + modul_im(i) * ilsparts(:,i,2))
end do

! ad-hoc window apodisation to reduce sidelobes at convolution limits to zero
omega = 0.5 * pi / real(wvskal%ilsradius)
do i = 1,wvskal%ilsradius
    coswert = cos(real(i) * omega)
    ils(-i) = coswert * ils(-i)
    ils(i) =  coswert * ils(i)
end do

end subroutine make_ils



!!====================================================================
!!  make_liuparms: calculates input parameters for modified Liu Voigt calc
!!====================================================================
!subroutine make_liuparms(lorhwhm,gauhwhm,vhwhm,voigtnorm,faktorlor_liu,faktorgau_liu,corr_liu)
!
!implicit none
!
!real(8),intent(in) :: lorhwhm,gauhwhm,vhwhm,voigtnorm
!
!real(8),intent(out) :: faktorlor_liu,faktorgau_liu,corr_liu
!
!real(8) :: cl_liu,cg_liu,d_liu,dp_liu
!
!! aux quantities required for Liu calculation
!d_liu = (lorhwhm - gauhwhm) / (lorhwhm + gauhwhm)
!cl_liu = 0.6818817d0 + d_liu * (0.6129331d0 - d_liu * (0.1838439d0 + d_liu * 0.1156844d0))
!cg_liu = 0.3246017d0 + d_liu * (-0.6182531d0 + d_liu * (0.1768139d0 + d_liu * 0.1210944d0))
!faktorlor_liu = voigtnorm * 0.318309886d0 * cl_liu / vhwhm
!faktorgau_liu = voigtnorm * 0.469718639d0 * cg_liu / vhwhm
!dp_liu = d_liu + 1.0d0
!corr_liu = exp(1.5d0 + 0.8d0 * dp_liu * dp_liu + 0.18d0 * dp_liu * dp_liu * dp_liu)
!
!
!end subroutine make_liuparms



!====================================================================
!  make_voigtluparms: provides input parameters for tabellated Voigt
!====================================================================
subroutine make_voigtluparms(lorhwhm,gauhwhm,idtable,drest,fakt)

use globlines23,only : maxvgt_d

implicit none

real(8),intent(in) :: lorhwhm,gauhwhm
integer,intent(out) :: idtable
real,intent(out) :: drest,fakt

real(8) :: dwert,rwert

dwert = -8.0d0 * log(0.02d0 * lorhwhm / gauhwhm)
idtable = int(dwert) + 1

if (idtable .lt. 1) then ! unterhalb der Tabelle (p gross)
    idtable = 1
    drest = 0.0
else
    if (idtable .gt. maxvgt_d) then ! oberhalb der Tabelle (p klein)
        idtable = maxvgt_d
        drest = 0.0
    else
        drest = dwert - real(idtable - 1,8)
    end if
end if

rwert = 50.0d0 * exp(-0.125d0 * (real(idtable - 1,8) + drest))
fakt = 3.27d-2 / (1.0d0 + rwert * (0.7651d0 + rwert * (0.51447d0 + rwert * &
  (1.586d-2 + rwert * 4.18d-3))))

end subroutine make_voigtluparms



!====================================================================
!  make_spec: generate final fine-grid spectrum (apply shift)
!====================================================================
subroutine make_spec(gndbb,emiss,epsgnd,ggs,atmtrm,centernue,shift,spec,dspecdshift)

use globfwd23, only : mw,wvskal

implicit none

real,dimension(mw(n_mw)%rbfinext),intent(in) :: gndbb,emiss,epsgnd,ggs,atmtrm
real(8),dimension(n_mw),intent(in) :: centernue,shift
real,dimension(mw(n_mw)%rbfinext),intent(out) :: spec,dspecdshift

integer :: i,j,jleft,jright,jshift
real(8) :: wrkshift
real :: rest,b,c
real,dimension(mw(n_mw)%rbfinext) :: wrkspec

! take self-emission into account
wrkspec = gndbb * ((epsgnd + (1.0 - epsgnd) * ggs) * atmtrm + emiss)

! apply shift for each MW, construct parabolic approx using the three nearest points
do i = 1,n_mw

    ! test whether shift is in allowed range
    if (abs(shift(i) * centernue(i)) .gt. wvskal%maxshift) then
        wrkshift = sign(wvskal%maxshift,shift(i))
        print *,'MW',i
        call warnout('make_spec: shift too big!',1)
    else
        wrkshift = shift(i) * centernue(i)
    end if

    jshift = nint(wrkshift / wvskal%dnuefine)
    rest = wrkshift / wvskal%dnuefine - real(jshift,8)
    if (jshift .eq. 0) then
        jleft = mw(i)%lbfinext + 1
        jright = mw(i)%rbfinext - 1
        spec(mw(i)%lbfinext) = 0.0
        spec(mw(i)%rbfinext) = 0.0
        dspecdshift(mw(i)%lbfinext) = 0.0
        dspecdshift(mw(i)%rbfinext) = 0.0
    else
        if (jshift .gt. 0) then
            jleft = mw(i)%lbfinext + 1 + jshift
            jright = mw(i)%rbfinext - 1
            spec(mw(i)%lbfinext:mw(i)%lbfinext+jshift) = 0.0
            spec(mw(i)%rbfinext) = 0.0
            dspecdshift(mw(i)%lbfinext:mw(i)%lbfinext+jshift) = 0.0
            dspecdshift(mw(i)%rbfinext) = 0.0
        else
            jleft = mw(i)%lbfinext + 1
            jright = mw(i)%rbfinext - 1 + jshift
            spec(mw(i)%lbfinext) = 0.0
            spec(mw(i)%rbfinext+jshift:mw(i)%rbfinext) = 0.0
            dspecdshift(mw(i)%lbfinext) = 0.0
            dspecdshift(mw(i)%rbfinext+jshift:mw(i)%rbfinext) = 0.0
        end if
    end if
    do j = jleft,jright
        b = 0.5 * (wrkspec(j-jshift+1) - wrkspec(j-jshift-1))
        c = 0.5 * (wrkspec(j-jshift+1) + wrkspec(j-jshift-1) - 2.0 * wrkspec(j-jshift))
        spec(j) = wrkspec(j-jshift) - rest * (b - rest * c)
        dspecdshift(j) = -b + 2.0 * rest * c
    end do

    dspecdshift(jleft:jright) = dspecdshift(jleft:jright) * centernue(i) / wvskal%dnuefine

end do

end subroutine make_spec



!====================================================================
!  make_specb: generate final fine-grid spectrum (apply shift)
!              no calculation of dspecdshift
!====================================================================
subroutine make_specb(gndbb,emiss,epsgnd,ggs,atmtrm,centernue,shift,spec)

use globfwd23

implicit none

real,dimension(mw(n_mw)%rbfinext),intent(in) :: gndbb,emiss,epsgnd,ggs,atmtrm
real(8),dimension(n_mw),intent(in) :: centernue,shift
real,dimension(mw(n_mw)%rbfinext),intent(out) :: spec

integer :: i,j,jleft,jright,jshift
real(8) :: wrkshift
real :: rest,b,c
real,dimension(mw(n_mw)%rbfinext) :: wrkspec

! take continuum absorption and atmospheric self-emission into account
wrkspec = gndbb * ((epsgnd + (1.0 - epsgnd) * ggs) * atmtrm + emiss)

! apply shift for each MW, construct parabolic approx using the three nearest points
do i = 1,n_mw

    ! test whether shift is in allowed range
    if (abs(shift(i) * centernue(i)) .gt. wvskal%maxshift) then
        wrkshift = sign(wvskal%maxshift,shift(i))
        print *,'MW',i
        call warnout('make_spec: shift too big!',1)
    else
        wrkshift = shift(i) * centernue(i)
    end if

    jshift = nint(wrkshift / wvskal%dnuefine)
    rest = wrkshift / wvskal%dnuefine - real(jshift,8)
    if (jshift .eq. 0) then
        jleft = mw(i)%lbfinext + 1
        jright = mw(i)%rbfinext - 1
        spec(mw(i)%lbfinext) = 0.0
        spec(mw(i)%rbfinext) = 0.0
    else
        if (jshift .gt. 0) then
            jleft = mw(i)%lbfinext + 1 + jshift
            jright = mw(i)%rbfinext - 1
            spec(mw(i)%lbfinext:mw(i)%lbfinext+jshift) = 0.0
            spec(mw(i)%rbfinext) = 0.0
        else
            jleft = mw(i)%lbfinext + 1
            jright = mw(i)%rbfinext - 1 + jshift
            spec(mw(i)%lbfinext) = 0.0
            spec(mw(i)%rbfinext+jshift:mw(i)%rbfinext) = 0.0
        end if
    end if
    do j = jleft,jright
        b = 0.5 * (wrkspec(j-jshift+1) - wrkspec(j-jshift-1))
        c = 0.5 * (wrkspec(j-jshift+1) + wrkspec(j-jshift-1) - 2.0 * wrkspec(j-jshift))
        spec(j) = wrkspec(j-jshift) - rest * (b - rest * c)
    end do
end do

end subroutine make_specb



!====================================================================
!  make_specc: generate final fine-grid spectrum (apply shift)
!              no calculation of dspecdshift (no atm self-emission)
!              !! NOTE: obs%epsgnd not used here, only scaling to absolute
!                    radiances by applying gndbb is performed !!
!====================================================================
subroutine make_specc(gndbb,atmtrm,centernue,shift,spec)

use globfwd23

implicit none

real,dimension(mw(n_mw)%rbfinext),intent(in) :: gndbb,atmtrm
real(8),dimension(n_mw),intent(in) :: centernue,shift
real,dimension(mw(n_mw)%rbfinext),intent(out) :: spec

integer :: i,j,jleft,jright,jshift
real(8) :: wrkshift
real :: rest,b,c
real,dimension(mw(n_mw)%rbfinext) :: wrkspec

! take continuum absorption and atmospheric self-emission into account
wrkspec = gndbb * atmtrm

! apply shift for each MW, construct parabolic approx using the three nearest points
do i = 1,n_mw

    ! test whether shift is in allowed range
    if (abs(shift(i) * centernue(i)) .gt. wvskal%maxshift) then
        wrkshift = sign(wvskal%maxshift,shift(i))
        print *,'MW',i
        call warnout('make_spec: shift too big!',1)
    else
        wrkshift = shift(i) * centernue(i)
    end if

    jshift = nint(wrkshift / wvskal%dnuefine)
    rest = wrkshift / wvskal%dnuefine - real(jshift,8)
    if (jshift .eq. 0) then
        jleft = mw(i)%lbfinext + 1
        jright = mw(i)%rbfinext - 1
        spec(mw(i)%lbfinext) = 0.0
        spec(mw(i)%rbfinext) = 0.0
    else
        if (jshift .gt. 0) then
            jleft = mw(i)%lbfinext + 1 + jshift
            jright = mw(i)%rbfinext - 1
            spec(mw(i)%lbfinext:mw(i)%lbfinext+jshift) = 0.0
            spec(mw(i)%rbfinext) = 0.0
        else
            jleft = mw(i)%lbfinext + 1
            jright = mw(i)%rbfinext - 1 + jshift
            spec(mw(i)%lbfinext) = 0.0
            spec(mw(i)%rbfinext+jshift:mw(i)%rbfinext) = 0.0
        end if
    end if
    do j = jleft,jright
        b = 0.5 * (wrkspec(j-jshift+1) - wrkspec(j-jshift-1))
        c = 0.5 * (wrkspec(j-jshift+1) + wrkspec(j-jshift-1) - 2.0 * wrkspec(j-jshift))
        spec(j) = wrkspec(j-jshift) - rest * (b - rest * c)
    end do
end do

end subroutine make_specc



!====================================================================
!  function make_vnorm: calculates line strength of Voigt line
!====================================================================
real(8) function make_vnorm(lin_species,lin_nue,lin_ergniveau,lin_kappacm,Tkel)

use globfwd23, only : amunit,clicht,kboltz

implicit none

integer,intent(in) :: lin_species
real,intent(in) :: lin_ergniveau,lin_kappacm
real(8),intent(in) :: lin_nue,Tkel

real(8) :: masseamu,qa,qb,qc,qd,qe,ntzunto,stimuess,qrel,xwert

! Gamache coeffs, mass
call infospecies(lin_species,masseamu,qa,qb,qc,qd,qe)
xwert = 8.3333333d-3 * (Tkel - 296.0d0)
qrel = exp(-xwert * (qa + xwert * (qb + xwert * (qc + xwert * (qd + xwert * qe)))))
ntzunto = qrel * exp(1.43877d0 * lin_ergniveau * (1.0d0 / 296.0d0 - 1.0d0 / Tkel))
stimuess = (1.0d0 - exp(-1.43877d0 * lin_nue / Tkel)) / (1.0d0 - exp(-4.86071d-3 * lin_nue))
make_vnorm = 1.0d-4 * lin_kappacm * ntzunto * stimuess  !per molecule

end function make_vnorm



!====================================================================
!  Beendigung des Programmes quittieren
!====================================================================
subroutine ppquittung()

use globfwd23 , only : npp,iunit_fwd,wrkpfad

implicit none

character (len=1) :: extension

if (npp .gt. 0) then
    extension = char(64 + npp)
    open (iunit_fwd,file = trim(wrkpfad)//'/RUN' // extension // '.FLG',status = 'replace',action = 'write')
else
    open (iunit_fwd,file = trim(wrkpfad)//'/RUN.FLG',status = 'replace',action = 'write')
end if

write (iunit_fwd,'(A14,I3)') 'RUN FINISHED: ',npp
close (iunit_fwd)

end subroutine ppquittung



!====================================================================
!  subroutine raytrace: determines raypath
!====================================================================
subroutine raytrace(h_lev_m,p_lev_pa,T_lev_K,latrad,azirad,sza_rad,astrodec,sza_lev_rad)

use globfwd23

implicit none


real(8),dimension(n_lev),intent(in) :: h_lev_m,p_lev_pa,T_lev_K
real(8),intent(in) :: latrad,azirad,sza_rad
logical,intent(in) :: astrodec
real(8),dimension(n_lev),intent(out) :: sza_lev_rad

integer :: i
real(8) :: axa,bxb,wert,Mradius,Nradius,Reff,refindex,sphereconst,dsza,elev_grad

! determine effective Radius of Earth (as function of geographical latitude and azimuth)
axa = Rearth_equat * Rearth_equat
bxb = Rearth_pol * Rearth_pol
wert = axa * cos(latrad) * cos(latrad) + bxb * sin(latrad) * sin(latrad)
Mradius = (axa * bxb) / sqrt(wert * wert * wert)
Nradius = axa / sqrt(wert)
Reff = 1.0d0 / ((cos(azirad) * cos(azirad)) / Mradius + (sin(azirad) * sin(azirad)) / Nradius)
!Reff = 6.371d6

! astrodec = F input of astronomical angle at observer, T input of apparent refracted angle at observer
if (astrodec) then
    ! set start value at observer to input apparent refracted angle
    sza_lev_rad(1) = sza_rad
else
    ! Use standard refraction formula to estimate apparent angle at observer
    ! taken from Meeus,1994
    ! numerical factor is 1.02 * 283K / 101000 Pa * conversion arcmin to rad
    elev_grad = radtograd * (0.5d0 * pi - sza_rad)
    dsza = 8.3136d-7 / tan(elev_grad + 10.3d0 / (elev_grad + 5.11d0)) * p_lev_pa(1) / T_lev_K(1)
    sza_lev_rad(1) = sza_rad - dsza
end if

refindex = 1.0d0 + (7.7518d-7 + &
  4.5814d-17 * (wvskal%nueraytra - 100.0d0) * (wvskal%nueraytra - 100.0d0)) * p_lev_pa(1) / T_lev_K(1)
sphereconst = refindex * (Reff + h_lev_m(1)) * sin(sza_lev_rad(1))

do i = 2,n_lev
    refindex = 1.0d0 + (7.7518d-7 + &
      4.5814d-17 * (wvskal%nueraytra - 100.0d0) * (wvskal%nueraytra - 100.0d0)) * p_lev_pa(i) / T_lev_K(i)
    sza_lev_rad(i) = asin(sphereconst / (refindex * (Reff + h_lev_m(i))))
end do

end subroutine raytrace



!====================================================================
!  read_hitbin_lbl: reads linedata from temporary binary file
!====================================================================
subroutine read_hitbin_lbl()

use globfwd23
use globlines23

implicit none

character(len=2) :: dumchar
character(len=150) :: hitdatei_test
integer :: i,j,n_test,lpointer,rpointer
real(8) :: lownue,highnue

! read header information in binary lbl-file (and check in parts)
open (iunit_fwd,file = trim(wrkpfad) // '/' // lblkennung // '.HIT',status = 'old',action = 'read')
call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) n_test
if (n_test .ne. n_mw) then
    print *,'Current number of MWs:  ',n_mw
    print *,'Archived number of MWs: ',n_test
    call warnout('Read_hitbin_lbl: number of MWs inconsistent!',0)
end if
do i = 1,n_mw
    read (iunit_fwd,*) lownue, highnue
    if (abs(mw(i)%firstnue_input - lownue) + abs(mw(i)%lastnue_input - highnue) .gt. 0.01d0) then
        print *,'Number of current MW:',i
        print *, 'Current MW bounds:  ',mw(i)%firstnue_input,mw(i)%lastnue_input
        print *, 'Archived MW bounds: ',lownue,highnue
        call warnout('Read_hitbin_lbl: input MW bounds incons!',0)
    end if
end do

call gonext(iunit_fwd,.false.)

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) n_test
if (n_test .ne. n_tau_lbl) then
    print *,'Current number of lbl species:  ',n_tau_lbl
    print *,'Archived number of lbl species: ',n_test
    call warnout('Read_hitbin_lbl: number of MWs inconsistent!',0)
end if

do i = 1,n_tau_lbl
    read (iunit_fwd,'(A150)') hitdatei_test
    if (trim(hitdatei_test) .ne. trim(hitdatei(i))) then
        print *,'Lbl Species: ',i
        call warnout('Read_hitbin_lbl: inconsistent filename!',0)
    end if
    print *,trim(hitdatei(i))
    do j = 1,n_mw
        read (iunit_fwd,*) lpointer,rpointer
        lines%lb(j,i) = lpointer
        lines%rb(j,i) = rpointer
        print *,lpointer,rpointer
    end do
end do
close (iunit_fwd)

! read binary lbl information
open (iunit_fwd,file = trim(wrkpfad) // '/' // lblkennung // '.HIT',status = 'old' &
  ,action = 'read')
! read past binary entry point
do i = 1,4
    call gonext(iunit_fwd,.true.)
end do
read (iunit_fwd) dumchar
anysdgdec = .false.
do i = 1,lines%rb(n_mw,n_tau_lbl)
    read (iunit_fwd) lines%species(i),lines%nue(i),lines%kappacm(i),lines%lorwidthf(i),lines%lorwidths(i) &
      ,lines%ergniveau(i),lines%lortdepend(i),lines%pshift(i),lines%gam2(i),lines%gam2tdepend(i) &
      ,lines%beta(i),lines%betatdepend(i),lines%sdgdec(i),lines%YLM(i),lines%LMTK1(i),lines%LMTK2(i)
    if (lines%sdgdec(i)) anysdgdec = .true.
end do

close (iunit_fwd)

end subroutine read_hitbin_lbl



!====================================================================
!  readhitran: reads linedata for all species in all MWs
!====================================================================
subroutine read_hitran_lbl()

use globfwd23
use globlines23

implicit none

character(len=100) :: zeile
logical :: decreadiso,decshape,decLM,dechit08
integer :: i,j,k,zaehler,hitspecies,iowert
real :: auxgam2,auxgam2tdepend,auxbeta,auxbetatdepend,auxYLM,auxLMTK1,auxLMTK2
real(8) :: lownue_mw_lbl,highnue_mw_lbl,hitnue,hitkappacm,hitdummy &
  ,hitlorwidthf,hitlorwidths,hitergniveau,hitlortdepend,hitpshift

anysdgdec = .false.
zaehler = 0

do i = 1,n_tau_lbl

    print *,'Reading lines from ASCII HITRAN file... species:',i

    ! test whether HITRAN file is accessible, test format
    open (iunit_fwd,file = hitdatei(i),iostat = iowert,status = 'old',action = 'read')
    if (iowert .ne. 0) then
        print *,'Cannot read HITRAN file:'
        print *,hitdatei(i)
        call ppquittung()
        stop
    else
         read (iunit_fwd,'(A100)') zeile
         if (zeile(41:41) .eq. '.') then
             dechit08 = .false.
         else
             if (zeile(42:42) .eq. '.') then
                 print *,'HIT08 format detected!'
                 dechit08 = .true.
             else
                 call warnout('Incompatible HIT format!',0)
             end if
         end if
    end if
    close (iunit_fwd)

    do j = 1,n_mw

        print *,'MW:  ',j
        open (iunit_fwd,file = hitdatei(i),iostat = iowert,status = 'old')

        lines%lb(j,i) = zaehler + 1
        lownue_mw_lbl = mw(j)%firstnue_input - mwextd_readlines_rim
        highnue_mw_lbl = mw(j)%lastnue_input + mwextd_readlines_rim

        ! init aux parms
        decshape = .false.
        decLM = .false.
        auxYLM = 0.0
        auxLMTK1 = 0.0
        auxLMTK2 = 0.0
        auxgam2 = 0.0
        auxgam2tdepend = 0.0
        auxbeta = 0.0
        auxbetatdepend = 0.0

        do ! read through line data file

            if (dechit08) then
                read (iunit_fwd,801,end = 102) hitspecies,hitnue,hitkappacm,hitdummy &
                 ,hitlorwidthf,hitlorwidths,hitergniveau,hitlortdepend,hitpshift
            else
                read (iunit_fwd,800,end = 102) hitspecies,hitnue,hitkappacm,hitdummy &
                 ,hitlorwidthf,hitlorwidths,hitergniveau,hitlortdepend,hitpshift
            end if

            if (hitnue .gt. highnue_mw_lbl) exit

            if ((hitnue - lownue_mw_lbl) * (highnue_mw_lbl - hitnue) .ge. 0.0d0) then


                if ((999 - hitspecies) * (hitspecies - 997) .ge. 0) then

                    ! this entry represents SDG parameters for the next entry which represents a spectral line
                    ! Set aux quantities for SDG calculation
                    if (hitspecies .eq. 997) then  ! 997 line mixing
                        decLM = .true.
                        auxLMTK1 = hitkappacm
                        auxLMTK2 = hitdummy
                        auxYLM = hitergniveau
                    end if
                    if (hitspecies .eq. 998) then  ! 998 SDV
                        decshape = .true.
                        auxgam2 = hitkappacm
                        auxgam2tdepend = hitdummy
                    end if
                    if (hitspecies .eq. 999) then  ! 999 Galatry
                        decshape = .true.
                        auxbeta = hitkappacm
                        auxbetatdepend = hitdummy
                    end if

                else
                    ! this entry represents a physical line

                    ! Selection of isotopologues
                    if (iso_handle(i) .eq. 0) then
                        decreadiso = .true.
                    else
                        decreadiso = .false.
                        if (iso_handle(i) .gt. 0) then
                            do k = 1,iso_handle(i)
                                if ((hitspecies - iso_kennung(k,i)) .eq. 0) decreadiso = .true.
                            end do
                        else
                            if ((hitspecies - iso_kennung(1,i)) .ge. 0) decreadiso = .true.
                        end if
                    end if

                    ! read line if isotopic species selected
                    if (decreadiso) then

                        zaehler = zaehler + 1

                        if (zaehler .gt. maxlines) then
                           print *,'Maximal allowed number of lines ',maxlines
                           print *,'Reading species in MW:',i,j
                           call warnout('readhitran: too many lines!',0)
                        end if

                        lines%sdgdec(zaehler) = .false.
                        lines%species(zaehler) = hitspecies
                        lines%nue(zaehler) =  hitnue
                        lines%kappacm(zaehler) = hitkappacm
                        lines%lorwidthf(zaehler) = 9.87167d-6 * hitlorwidthf
                        lines%ergniveau(zaehler) = hitergniveau
                        lines%lortdepend(zaehler) = hitlortdepend
                        lines%pshift(zaehler) = 9.87167d-6 * hitpshift
                        lines%YLM(zaehler) = 0.0
                        lines%LMTK1(zaehler) = 0.0
                        lines%LMTK2(zaehler) = 0.0

                        ! self-broadening
                        ! For any species apart from H2O (all isos) overwrite self-broadening
                        ! with 1.27x foreign-broadening
                        if ((16 - lines%species(zaehler)) * (lines%species(zaehler) - 11) .ge. 0) then
                            lines%lorwidths(zaehler) = 9.87167d-6 * hitlorwidths
                        else
                            lines%lorwidths(zaehler) = 1.27d0 * 9.87167d-6 * hitlorwidthf
                        end if

                        ! aux parms for line available?
                        if (decshape .or. decLM) then
                            if (decshape) then
                                lines%gam2(zaehler) = 9.87167d-6 * auxgam2
                                lines%gam2tdepend(zaehler) = auxgam2tdepend
                                lines%beta(zaehler) = 9.87167d-6 * auxbeta
                                lines%betatdepend(zaehler) = auxbetatdepend
                                lines%sdgdec(zaehler) = .true.
                                anysdgdec = .true.
                            end if
                            if (decLM) then
                                lines%YLM(zaehler) = 9.87167d-6 * auxYLM
                                lines%LMTK1(zaehler) = auxLMTK1
                                lines%LMTK2(zaehler) = auxLMTK2
                            end if
                        end if

                        ! reset aux parameters
                        decshape = .false.
                        decLM = .false.
                        auxYLM = 0.0
                        auxLMTK1 = 0.0
                        auxLMTK2 = 0.0
                        auxgam2 = 0.0
                        auxgam2tdepend = 0.0
                        auxbeta = 0.0
                        auxbetatdepend = 0.0

                    end if

                end if ! real line or non-Voigt parameters line

            end if  ! if inside spectral bounds

        end do ! read through linedata file

        102 continue

        close (iunit_fwd)

        lines%rb(j,i) = zaehler

        print *,'Line pointer and lines for species and MW: ',i,j
        print *, lines%lb(j,i),lines%rb(j,i),lines%rb(j,i) - lines%lb(j,i) + 1

    end do ! MWs

end do ! species

print*,'...reading lines from HITRAN file completed!'

800 format(I3,F12.6,E10.3,E10.3,F5.4,F5.4,F10.4,F4.2,F8.6)
801 format(I3,F12.6,E10.3,E10.3,F6.5,F4.3,F10.4,F4.2,F8.6)  ! HIT 08 format


end subroutine read_hitran_lbl



!====================================================================
!  read_hitran_xsc: reads cross section data in HITRAN format
!====================================================================
subroutine read_hitran_xsc(TKelref,pParef,tau_lev_xsc)

use globfwd23

implicit none

real(8),dimension(n_lev),intent(in) :: TKelref,pParef
real,dimension(2,mw(n_mw)%rbfinext,n_lev,n_tau_xsc),intent(inout) :: tau_lev_xsc

logical :: classerrdec
integer :: i,j,k,l,m,iowert,nlines,npt,n_xscset,nclass,nactgrid,minpos &
  ,minposT,minposp,nzaehler
real :: deltaT,deltap,faktorT,faktorp
real(8) :: pTorr,actnue,rest,distwert,distT,distp,mindist,mindistT &
  ,mindistp,xscwert,sump,sumT,testp,testT
type meas_section
    sequence
    integer :: ngrid,lb,rb,iclass
    real(8) :: w1,w2,T,pPa,dw
    logical :: classdec
end type meas_section
type ptclass
    sequence
    integer :: member,kindpT
    real(8) :: w1,w2,T,pPa
end type ptclass
type (meas_section),dimension(:),allocatable :: meas
type (ptclass),dimension(:),allocatable :: class
real,dimension(:),allocatable :: xsc_meas
real,dimension(:,:),allocatable :: xsc_class
real,dimension(:,:),allocatable :: dtaudp_lev ! dimension: mw(n_mw)%rbfinext,n_lev

allocate (dtaudp_lev(mw(n_mw)%rbfinext,n_lev))

do i = 1,n_tau_xsc

    print *,'Reading lines from HITRAN xsc file... species:',i

    open (iunit_fwd,file = hitdatei(n_tau_lbl+i),iostat = iowert,status = 'old',action = 'read')
    if (iowert .ne. 0) then
        print *,'Cannot read HITRAN xsc file:'
        print *,hitdatei(i)
        call ppquittung()
        stop
    end if

    ! determine total number of xsc sections collected in file
    n_xscset = 0
    do
        read(iunit_fwd,'(40X,I7,53X)',end = 103) npt

        n_xscset = n_xscset + 1

        ! find number of lines in xsc file, go ahead by number of lines
        if (mod(npt,10) == 0) then
           nlines = npt/10
        else
           nlines = ceiling(npt/10.)
        end if
        do k = 1,nlines
           read(iunit_fwd,*)
        end do
    end do
    103 continue
    rewind(iunit_fwd)

    print *,'Number of xsc blocks in file:',n_xscset

    ! read description for each block
    allocate(meas(n_xscset),class(n_xscset))
    do j = 1,n_xscset
        read(iunit_fwd,'(20X,2(F10.4),I7,F7.2,F6.2,40X)') meas(j)%w1,meas(j)%w2,meas(j)%ngrid,meas(j)%T,pTorr
        meas(j)%pPa = 133.322d0 * pTorr
        meas(j)%dw = (meas(j)%w2 - meas(j)%w1) / real((meas(j)%ngrid - 1),8)

        if (mod(meas(j)%ngrid,10) .eq. 0) then
           nlines = meas(j)%ngrid/10
        else
           nlines = ceiling(meas(j)%ngrid/10.)
        end if
        do k = 1,nlines
           read(iunit_fwd,*)
        end do
    end do
    rewind(iunit_fwd)

    ! setup pointer structure for spectral sections in xsc file
    meas(1)%lb = 1
    meas(1)%rb = meas(1)%ngrid
    do j = 2,n_xscset
        meas(j)%lb = meas(j-1)%rb + 1
        meas(j)%rb = meas(j)%lb + meas(j)%ngrid - 1
    end do

    ! classification of spectral sections found (collect spectral sections which refer to same pT)
    nclass = 0
    meas(1:n_xscset)%classdec = .false.
    do j = 1,n_xscset
        if (.not. meas(j)%classdec) then
              nclass = nclass + 1
              meas(j)%classdec = .true.
              meas(j)%iclass = nclass
              class(nclass)%member = 1
              testT = meas(j)%T
              testp = meas(j)%pPa
              class(nclass)%w1 = meas(j)%w1
              class(nclass)%w2 = meas(j)%w2
        end if
        do k = j + 1,n_xscset
            if (.not. meas(k)%classdec .and. meas(k)%w1 .gt. class(nclass)%w2) then
                if (abs(meas(k)%T - testT) .lt. 2.0d0 .and. &
                  abs(meas(k)%pPa - testp) / abs(meas(k)%pPa + testp + 1.0d3) &
                    .lt. 0.01d0) then
                    meas(k)%classdec = .true.
                    meas(k)%iclass = nclass
                    class(nclass)%member = class(nclass)%member + 1
                end if
            end if
        end do
    end do

    ! Use average pT for class pT
    do j = 1,nclass
        sump = 0.0d0
        sumT = 0.0d0
        nzaehler = 0
        do k = 1,n_xscset
            if (meas(k)%iclass .eq. j) then
                nzaehler = nzaehler + 1
                sump = sump + meas(k)%pPa
                sumT = sumT + meas(k)%T
            end if
        end do
        class(j)%pPa = sump / real(nzaehler,8)
        class(j)%T = sumT / real(nzaehler,8)
    end do

    print *,'number of p/T classes found:',nclass

    ! log-output
    write(*,*) 'xsc species:',i
    write(*,*) 'xsc blocks found:'
    do j = 1,n_xscset
       write(*,'(2f10.4,i10,2f10.4,i4)') meas(j)%w1,meas(j)%w2,meas(j)%ngrid &
         ,0.01d0 * meas(j)%pPa,meas(j)%T,meas(j)%iclass
    end do
    write(*,*)

    if (nclass .eq. 2) then
        call warnout('XSC interpol does not support nclass = 2',0)
    end if


    classerrdec = .false.
    do j = 1,nclass
        print *,'class / members: ',j,class(j)%member
        if (class(j)%member .ne. class(1)%member) then
            classerrdec = .true.
        end if
    end do
    if (classerrdec) call warnout('XSC interpol requires identical classes!',0)

    ! Merging spectral sections and interpolating to fine grid in all MWs
    ! take spectral correction factor into account in each MW
    ! (note: in contrast to lbl treatment passive trafo has to be applied!)
    allocate (xsc_class(mw(n_mw)%rbfinext,nclass))
    xsc_class = 0.0
    do k = 1,n_xscset
        ! allocate and read current xsc data
        allocate (xsc_meas(meas(k)%ngrid))
        print *,'Reading current xsc section...',k
        read (iunit_fwd,*)
        read (iunit_fwd,'(10(E10.3))') (xsc_meas(j),j = 1,meas(k)%ngrid)
        ! transforming to m2 column
        xsc_meas = 1.0e-4 * xsc_meas
        do j = 1,nclass
            if (meas(k)%iclass .eq. j) then
                do l = 1,n_mw
                    if (.not.(meas(k)%w2 .lt. mw(l)%firstnuefinext * (1.0d0 - skalspeci(l,n_tau_lbl+i)) &
                      .or. meas(k)%w1 .gt. mw(l)%lastnuefinext * (1.0d0 - skalspeci(l,n_tau_lbl+i)))) then
                        do m = 1,mw(l)%ptsfinext
                            actnue = (mw(l)%firstnuefinext + real(m-1,8) * wvskal%dnuefine) &
                              * (1.0d0 - skalspeci(l,n_tau_lbl+i))
                            if ((actnue - meas(k)%w1) * (actnue - meas(k)%w2) .lt. 0.0d0) then
                                ! linear spectral interpolation to requested position
                                nactgrid = int((actnue - meas(k)%w1) / meas(k)%dw)
                                rest = (actnue - meas(k)%w1) / meas(k)%dw - real(nactgrid,8)
                                xscwert = (1.0d0 - rest) * xsc_meas(nactgrid+1) &
                                  + rest * xsc_meas(nactgrid+2)
                                xsc_class(mw(l)%lbfinext+m-1,j) = xscwert
                            end if
                        end do
                    end if
                end do
            end if
        end do
        ! deallocate current xsc data
        deallocate (xsc_meas)
    end do
    close (iunit_fwd)
    ! If only one class available insert xsc_class to tau_lev_xsc and exit subroutine
    if (nclass .eq. 1) then
        do j = 1,n_lev
            tau_lev_xsc(1,:,j,i) = xsc_class(:,1)
        end do
        tau_lev_xsc(2,:,:,i) = 0.0 !deriT
        return
    end if

    ! General interpolation:
    !   Determine nearest class
    !   Determine class with same p nearest in T
    !   Determine class with same T nearest in p
    !   Determine both p and T derivative and tau_lev_xsc
    do j = 1,n_lev
        ! find nearest class
        minpos = 1
        call givedist(class(1)%T,class(1)%pPa,TKelref(j),pParef(j),mindist,distT,distp)
        do k = 2,nclass
            call givedist(class(k)%T,class(k)%pPa,TKelref(j),pParef(j),distwert,distT,distp)
            if (distwert .lt. mindist) then
                minpos = k
                mindist = distwert
            end if
        end do
        class(:)%kindpT = 2
        class(minpos)%kindpT = 0

        ! find nearest classes which are neighbours in p or T
        minposT = 0
        minposp = 0
        call givedist(5.0d3,1.0d6,0.0d0,0.0d0,distwert,mindistT,mindistp)
        do k = 1,nclass
            call givedist(class(k)%T,class(k)%pPa,class(minpos)%T,class(minpos)%pPa,distwert,distT,distp)
            ! classify remaining classes but nearest in p or T neighbours
            if (class(k)%kindpT .eq. 2) then
                if (distT .le. distp) then
                    class(k)%kindpT = -1 ! allowed for use in dp calculation
                else
                    class(k)%kindpT = 1  ! allowed for use in dT calculation
                end if
            end if
            if (distwert .lt. mindistT .and. class(k)%kindpT .eq. 1) then
                minposT = k
                mindistT = distwert
            end if
            if (distwert .lt. mindistp .and. class(k)%kindpT .eq. -1) then
                minposp = k
                mindistp = distwert
            end if
        end do

        deltaT = real(TKelref(j) - class(minpos)%T)
        deltap = real(pParef(j) - class(minpos)%pPa)
        ! warning if no partners both along p and T found
        ! determine deriT
        if (minposT .eq. 0) then
            tau_lev_xsc(2,:,j,i) = 0.0
            faktorT = 0.0
        else
            if (abs(class(minposT)%T - class(minpos)%T) .lt. 0.1d0) then
                call warnout('Calculation of dxsc/dT impossible!',0)
            end if
            tau_lev_xsc(2,:,j,i) = (xsc_class(:,minposT) - xsc_class(:,minpos)) &
              / (class(minposT)%T - class(minpos)%T)
            if (abs(deltaT) .lt. abs(class(minposT)%T - class(minpos)%T)) then
                faktorT = 1.0
            else
                faktorT = 1.0 + log(abs((class(minposT)%T - class(minpos)%T) / deltaT))
            end if
        end if
        ! determine derip
        if (minposp .eq. 0) then
            dtaudp_lev(:,j) = 0.0
            faktorp = 0.0
        else
            if (abs(class(minposp)%Ppa - class(minpos)%Ppa) .lt. 1.0d0) then
                call warnout('Calculation of dxsc/dp impossible!',0)
            end if
            dtaudp_lev(:,j) = (xsc_class(:,minposp) - xsc_class(:,minpos)) &
              / (class(minposp)%pPa - class(minpos)%pPa)
            if (abs(deltap) .lt. abs(class(minposp)%Ppa - class(minpos)%pPa)) then
                faktorp = 1.0
            else
                faktorp = 1.0 + log(abs((class(minposp)%Ppa - class(minpos)%pPa) / deltap))
            end if
        end if
        ! determine xsc at pT Ref, reject negative xsc values
        ! linear interpolation within region spanned by xsc measurements
        ! damped extrapolation outside
        do k = 1,n_mw
            do l = mw(k)%lbfinext,mw(k)%rbfinext
                tau_lev_xsc(1,l,j,i) = xsc_class(l,minpos) &
                  + faktorT * deltaT * tau_lev_xsc(2,l,j,i) &
                  + faktorp * deltap * dtaudp_lev(l,j)
                if (tau_lev_xsc(1,l,j,i) .lt. 0.0 ) tau_lev_xsc(1,l,j,i) = 0.0
            end do
        end do
    end do
    ! deallocate species-specific arrays
    deallocate (meas,class,xsc_class)
end do

deallocate (dtaudp_lev)

print*,'...reading lines from HITRAN xsc file completed!'

contains

! internal function distance
subroutine givedist(T1,p1,T2,p2,distance,distT,distp)

implicit none

real(8),intent(in) :: T1,p1,T2,p2
real(8),intent(out) ::  distance,distp,distT

distp = abs(p1 - p2) / (p1 + p2 + 0.1d0)
distT = abs(T1 - T2) / (T1 + T2)
distance = distp * distp + distT * distT

end subroutine givedist

end subroutine read_hitran_xsc





!====================================================================
!  read_input_fwd: read forward model input file
!====================================================================
subroutine read_input_fwd(infile)

use globfwd23
use globlev23
use globlines23, only : tauschwell_adj,slope_adj,dnue_adj

implicit none

character(len=*),intent(in) :: infile

integer :: i,j,iowert
complex(8):: mwbds

open (iunit_fwd,file = infile,iostat = iowert,status = 'old',action = 'read')
if (iowert .ne. 0) then
    print *,'Cannot read input file:    '
    print *,infile
    call ppquittung()
    stop
end if

! Output and work path
!call gonext(iunit_fwd,.false.)
!read (iunit_fwd,'(A)') outpfad
!read (iunit_fwd,'(A)') wrkpfad
!print *,'Output path:                    ',outpfad
!print *,'Work   path:                    ',wrkpfad

! Observer location
call gonext(iunit_fwd,.false.)
!read (iunit_fwd,*) obs%h_m
read (iunit_fwd,*) obs%lat_rad
read (iunit_fwd,*) obs%lon_rad
!print *,'Observer altitude:              ',obs%h_m
print *,'Observer latitude:              ',obs%lat_rad
print *,'Observer longitude:             ',obs%lon_rad

! Geometries
call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) instr%semiextfov_rad
read (iunit_fwd,*) obs%astrodec
read (iunit_fwd,*) obs%JDdate_d,obs%sza_rad,obs%azi_rad
print *,'External FOV/2:                 ',instr%semiextfov_rad
print *,'Use apparent SZA:               ',obs%astrodec
print *,'JD:                             ',obs%JDdate_d
print *,'SZA:                            ',obs%sza_rad
print *,'Azimuth:                        ',obs%azi_rad

! MWs
call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) n_mw
do i = 1,n_mw
    read (iunit_fwd,*) mwbds
    mw(i)%firstnue_input = real(mwbds,8)
    mw(i)%lastnue_input = aimag(mwbds)
end do
print *,'MW bounds (input):              '
do i = 1,n_mw
    print *,mw(i)%firstnue_input,mw(i)%lastnue_input
    if (mw(i)%lastnue_input - mw(i)%firstnue_input .lt. 0.0d0) then
        call warnout('MW bounds not descending!',0)
    end if
end do

! Update MW bounds and increment
call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) messdec
print *,'Update MWs from measurement:    '
if (messdec) then
    read (iunit_fwd,*) messkind
    print *,'Measured spectra:               '
    do i = 1,n_mw
        read (iunit_fwd,*) messdatei(i)
        print *,messdatei(i)
    end do
    call check_mess(n_mw,mw(1:n_mw)%firstnue_input,mw(1:n_mw)%lastnue_input,messkind &
      ,messdatei(1:n_mw),mw(1:n_mw)%firstnue,mw(1:n_mw)%lastnue,wvskal%dnue)
else
    read (iunit_fwd,*) wvskal%dnue
    print *,'Spectral increment:             ',wvskal%dnue
    do i = 1,n_mw
        mw(i)%firstnue = wvskal%dnue * nint(mw(i)%firstnue_input / wvskal%dnue)
        mw(i)%lastnue = wvskal%dnue * nint(mw(i)%lastnue_input / wvskal%dnue)
    end do
end if
print *,'MW limits (adjusted):           '
do i = 1,n_mw
    mw(i)%centernue = 0.5d0 * (mw(i)%firstnue + mw(i)%lastnue)
    print *,mw(i)%firstnue,mw(i)%lastnue
end do

! determine mean nue for raytracing
wvskal%nueraytra = 0.0d0
do i = 1,n_mw
    wvskal%nueraytra = wvskal%nueraytra + mw(i)%centernue
end do
wvskal%nueraytra = wvskal%nueraytra / real(n_mw,8)

! model atmosphere general
call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) n_lev
print *,'Number of levels:               ',n_lev

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) (lev%h_m(i),i = 1,n_lev)
lev%h_m = 1.0d3 * lev%h_m ! convert km to m
obs%h_m = lev%h_m(1)      ! lowest level is observer altitude
print *,'Level altitudes [m]:'
write (*,'(5(ES11.4))') (lev%h_m(i),i = 1,n_lev)

call gonext(iunit_fwd,.false.)
read (iunit_fwd,'(A)') ptdatei
print *,'pT-file:                        ',ptdatei

! model atmosphere: H2O
call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) h2ofwddec
if (h2ofwddec) then
    read (iunit_fwd,*) ptrh2o
    print *,'forward pointer H2O:            ',ptrh2o
else
    read (iunit_fwd,'(A)') h2odatei
    print *,'h2o-file:                       '
    print *,h2odatei
end if

! define trace species
call gonext(iunit_fwd,.false.)
read (iunit_fwd,'(A)') specidatei
read (iunit_fwd,*) n_tau_lbl
read (iunit_fwd,*) n_tau_xsc
print *,'Number of LBL species:              ',n_tau_lbl
print *,'Number of xsc species:              ',n_tau_xsc
n_tau = n_tau_lbl + n_tau_xsc
if (n_tau .gt. maxtau) then
    print *,'Max number of species:',maxtau
    call warnout('Too many species!',0)
end if

! Select line-by-line species
call gonext(iunit_fwd,.false.)
print *,'HITRAN lbl files:                   '
do i = 1,n_tau_lbl
    read (iunit_fwd,'(A)') hitdatei(i)
    read (iunit_fwd,*) iso_handle(i)
    ! if handle = 0, all isos are collected, no reading here
    ! if handle >= 1, select specified number of isos
    if (iso_handle(i) .ge. 1) then
        do j = 1,iso_handle(i)
            read (iunit_fwd,*) iso_kennung(j,i)
        end do
    end if
    ! if handle = -1, all isos beyond the mark are collected
    if (iso_handle(i) .eq. -1) then
        read (iunit_fwd,*) iso_kennung(1,i)
    end if
end do
do i = 1,n_tau_lbl
    print *,hitdatei(i)
end do

! define computational accuracy lbl-species
call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) tauschwell_adj
if (tauschwell_adj .gt. 1.0d0) tauschwell_adj = 1.0d0
if (tauschwell_adj .lt. 0.0d0) tauschwell_adj = 0.0d0
read (iunit_fwd,*) slope_adj
if (slope_adj .gt. 1.0d0) slope_adj = 1.0d0
if (slope_adj .lt. 0.0d0) slope_adj = 0.0d0
read (iunit_fwd,*) dnue_adj
if (dnue_adj .gt. 1.0d0) dnue_adj = 1.0d0
if (dnue_adj .lt. 0.1d0) dnue_adj = 0.1d0

print *,'tauschwell_adj:',tauschwell_adj
print *,'slope_adj:',slope_adj
print *,'dnue_adj:',dnue_adj

read (iunit_fwd,*) decrimall
if (.not. decrimall) print *,'Neglecting all rim contributions!'

! Select xsc species
call gonext(iunit_fwd,.false.)
print *,'HITRAN xsc files:                   '
do i = n_tau_lbl+1,n_tau_lbl+n_tau_xsc
    read (iunit_fwd,'(A)') hitdatei(i)
    print *,hitdatei(i)
end do

! spectral scale correction for each species in each MW
call gonext(iunit_fwd,.false.)
print *,'Spectral scale corr per species and MW:'
do i = 1,n_tau
    read (iunit_fwd,*) (skalspeci(j,i),j=1,n_mw)
    print *,(skalspeci(j,i),j=1,n_mw)
end do

! model atmosphere composition
call gonext(iunit_fwd,.false.)
print *,'VMR files:                      '
do i = 1,n_tau
    read (iunit_fwd,'(A)') vmrdatei(i)
    print *,vmrdatei(i)
end do

! reference vmr files
call gonext(iunit_fwd,.false.)
print *,'VMR ref files:                  '
do i = 1,n_tau
    read (iunit_fwd,'(A)') vmrrefdatei(i)
    print *,vmrrefdatei(i)
end do

! Nadir version: ground T and eps / standard version: solar spectrum
call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) obs%Tgnd_K
print *,'T gnd:',obs%Tgnd_K
read (iunit_fwd,*) n_eps
if (n_eps .gt. max_n_eps) then
    print *,'requested n_eps: ',n_eps
    print *,'max_n_eps:       ',max_n_eps
    call warnout('Requested n_eps too big!',0)
end if
do i = 1,n_eps
    read (iunit_fwd,*) obs%nue_eps(i),obs%epsgnd(i)
    print *,obs%nue_eps(i),obs%epsgnd(i)
end do

! continuum transmission towards zenith, emission
call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) (mw(j)%tau_cont,j = 1,n_mw)
read (iunit_fwd,*) emissdec
print *,'zenith continuum tau:',(mw(j)%tau_cont,j = 1,n_mw)
print *,'add atm emission:    ',emissdec

! wind information
call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) winddec
if (winddec) then
    read (iunit_fwd,'(A)') winddatei
    print *,'File containing wind information:',winddatei
end if

! Instrument specification
call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) instr%opdmax
read (iunit_fwd,*) instr%apokind
read (iunit_fwd,*) instr%ilsacc
read (iunit_fwd,*) instr%coneifm_rad
print *,'Instrument OPDmax:              ',instr%opdmax
print *,'Apodisation:                    ',instr%apokind
print *,'Radius ILS convolution:         ',instr%ilsacc
print *,'Instrument internal FOV/2:      ',instr%coneifm_rad

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) instr%apo,instr%phas
print *,'2 parm apo + phase:             '
print *,instr%apo,instr%phas

call gonext(iunit_fwd,.false.)
print *,'Extended ILS parms:             '
do i = 0,n_ifg - 1
    read (iunit_fwd,*) instr%ilsparms(i,1),instr%ilsparms(i,2)
    print *,instr%ilsparms(i,1),instr%ilsparms(i,2)
end do

! Add noise on forward calculation?
call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) instr%noisdec
if (instr%noisdec) then
    read (iunit_fwd,*) (instr%noise(i),i = 1,n_mw)
end if
print *,'Add noise to calc spectrum:     ',instr%noisdec
if (instr%noisdec) then
    print *,'Noise level to be added per MW: '
    do i = 1,n_mw
        print *,instr%noise(i)
    end do
end if

! ordinate offset, shift and baseline parms
call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) (instr%ofset(j),j = 1,n_mw)
print *,'radiance offset:     ',(instr%ofset(j),j = 1,n_mw)

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) (mw(j)%shift,j = 1,n_mw)
print *,'spectral scale:      ',(mw(j)%shift,j = 1,n_mw)
wvskal%maxshift = 0.0d0
do i = 1,n_mw
    if (wvskal%maxshift .lt. abs(mw(i)%shift) * mw(i)%centernue) &
      wvskal%maxshift = abs(mw(i)%shift) * mw(i)%centernue
end do
wvskal%maxshift = wvskal%maxshift + 5.0d0 / (instr%opdmax + 0.1d0)


call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) (mw(j)%n_base,j = 1,n_mw)
do i = 1,n_mw
    if (mw(i)%n_base .lt. 1 .or. mw(i)%n_base .gt. max_n_base) then
        print *,'MW + requested baseline points:',i,mw(i)%n_base
        call warnout('Invalid number of baseline points!',0)
    end if
end do
deri%maxnbase = maxval(mw(1:n_mw)%n_base) ! largest number of baseline points in any MW

call gonext(iunit_fwd,.false.)
do i = 1,n_mw
    read (iunit_fwd,*) (mw(i)%base_knot(j),j = 1,mw(i)%n_base)
end do

! Channeling
call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) n_chan
if (n_chan .gt. max_n_chan) then
    n_chan = max_n_chan
    print *,'Max allowed number of chan frequencies:',max_n_chan
    call warnout('Number of chan frequencies adjusted!',1)
end if
do i = 1,n_chan
    read (iunit_fwd,*) instr%chanlambda(i)
    print *,'Chan frequencies:     ',instr%chanlambda(i)
end do

call gonext(iunit_fwd,.false.)
if (n_chan .gt. 0) then
    do i = 1,n_mw
        read (iunit_fwd,*) (instr%chanamp(j,i),j = 1,n_chan)
        print *,'Chan amplitudes:      ',(instr%chanamp(j,i),j = 1,n_chan)
    end do
end if

! Decide whether fwd results are written to file
call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) fwdtofiledec

! Derivatives
call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) deri%sw_vmr
read (iunit_fwd,*) deri%sw_HITgam
read (iunit_fwd,*) deri%sw_HITint
read (iunit_fwd,*) deri%n_vmr
print *,'derivative vmr:           ',deri%sw_vmr
print *,'derivative HITgam:        ',deri%sw_HITgam
print *,'derivative HITint:        ',deri%sw_HITint
print *,'number of species:        ',deri%n_vmr
call gonext(iunit_fwd,.false.)
h2ofitdec = .false.
do i = 1,deri%n_vmr
    read (iunit_fwd,*) deri%npoint_fwd(i)
    print *,'vmr fwd pointer:          ',deri%npoint_fwd(i)
    if (deri%npoint_fwd(i) .gt. n_tau) then
        print *,'Requested line pointer:',deri%npoint_fwd(i)
        print *,'Total number of VMR species:',n_tau
        call warnout('Incorrect VMR-deri pointer!',0)
    end if
    if (deri%npoint_fwd(i) .eq. ptrh2o) then
        h2ofitdec = .true.
    end if
end do

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) deri%sw_T
print *,'derivative T:             ',deri%sw_T

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) deri%sw_LOS
print *,'derivative LOS:           ',deri%sw_LOS

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) deri%sw_solshi
print *,'derivative sol scale:     ',deri%sw_solshi

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) deri%sw_ILSapo
read (iunit_fwd,*) deri%sw_ILSphas
print *,'derivative ILS (apo):     ',deri%sw_ILSapo
print *,'derivative ILS (phase):   ',deri%sw_ILSphas

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) deri%sw_shift
print *,'derivative shift:         ',deri%sw_shift

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) deri%sw_ofset
print *,'derivative offset:        ',deri%sw_ofset

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) deri%sw_base
print *,'derivative baseline:      ',deri%sw_base

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) deri%sw_chan
print *,'derivative channeling:    ',deri%sw_chan

close (iunit_fwd)

end subroutine read_input_fwd



!====================================================================
!  PP-Prozessierung: Handle lesen
!====================================================================
subroutine readppkennung(nppdatei,inppfad_fwd,inppfad_inv,outpfad_fwd,outpfad_inv,npp)

use globfwd23, only : iunitoffset_fwd

implicit none

character(len=*),intent(in) :: nppdatei
character(len=150),intent(out) :: inppfad_fwd,inppfad_inv,outpfad_fwd,outpfad_inv
integer,intent(out) :: npp

logical :: dateidadec
character(len=1) :: extension

inquire (file = trim(nppdatei),exist = dateidadec)
if (dateidadec) then
    open (iunitoffset_fwd,file = trim(nppdatei),status = 'old',action = 'read')
    call gonext(iunitoffset_fwd,.false.)
    read (iunitoffset_fwd,*) npp
    close (iunitoffset_fwd)
    if (npp .lt. 1 .or. npp .gt. 8) then
        print *,npp
        call warnout('Valid npp range: 1 ... 8',0)
    end if

    extension = char(64 + npp)
    inppfad_fwd = 'INP_FWD/RUN' // extension
    inppfad_inv = 'INP_INV/RUN' // extension
    outpfad_fwd = 'OUT_FWD/RUN' // extension
    outpfad_inv = 'OUT_INV/RUN' // extension

else

    npp = 0
    inppfad_fwd = 'INP_FWD'
    inppfad_inv = 'INP_INV'
    outpfad_fwd = 'OUT_FWD'
    outpfad_inv = 'OUT_INV'

end if



end subroutine readppkennung



!====================================================================
!  read_pT: read and interpolate T(z) profile to specified altitude grid
!====================================================================
subroutine read_pT(ptdatei,pTclimdatei,latrad,JDwert,n_lev,h_lev_m,T_K,p_Pa)

use globfwd23, only : iunit_fwd

implicit none

character(len=*),intent(in) :: pTdatei,pTclimdatei
integer,intent(in) :: n_lev
real(8),intent(in) :: latrad,JDwert
real(8),dimension(n_lev),intent(in) :: h_lev_m
real(8),dimension(n_lev),intent(out) :: T_K,p_Pa

character(len=13) :: dumchar
integer,parameter :: n_lev_clim = 71
integer,parameter :: npT_max = 120
integer :: i,inear,iowert,npT,imonth,ilat,ilev
real(8) :: lnp,jahr,month,gewicht,dumreal,latwert,twert,hclimschwell,dh
real(8),dimension(17) :: tfeld
real(8),dimension(npT_max) :: hinp_m,Tinp_K,pinp_Pa
real(8),dimension(n_lev_clim) :: hclim_m,Tclim_K,sumgewicht
real(8),dimension(n_lev) :: Twrka,Twrkb

! construct climatological T(h) profile (zero for year count: 1.1.2000,new year)
jahr = 2.737907d-3 * (JDwert - 2451544.5d0)
month = 12.0d0 * (jahr - int(jahr))

open (iunit_fwd,file = pTclimdatei,iostat = iowert,status = 'old',action = 'read')
if (iowert .ne. 0) then
    print *,'Cannot read clim pT file:'
    print *,pTclimdatei
    call ppquittung()
    stop
else
    call gonext(iunit_fwd,.false.)
    read (iunit_fwd,*) hclimschwell
    hclimschwell = 1.0d3 * hclimschwell
    call gonext(iunit_fwd,.false.)
    do ilev = n_lev_clim,1,-1
        read (iunit_fwd,401) dumchar,hclim_m(ilev)
    end do
    hclim_m = 1.0d3 * hclim_m
end if
close (iunit_fwd)

open (iunit_fwd,file = ptclimdatei,iostat = iowert,status = 'old',action = 'read')

call gonext(iunit_fwd,.false.)

Tclim_K(:) = 0.0d0
sumgewicht(:) = 0.0d0

! SH (do not read equatorial entry twice)
do imonth = 0,11
    call gonext(iunit_fwd,.false.)
    twert = imonth + 0.5
    do ilev = n_lev_clim,1,-1
        read (iunit_fwd,401) dumchar,dumreal,dumchar,(tfeld(ilat),ilat = 1,16)
        do ilat = 1,16
            latwert = 0.08726d0 * (ilat - 17)
            gewicht = (0.5d0 + 0.5d0 * cos(0.523598775d0 * (month - twert)))**10 &
              * exp(-2.0d0 * 11.459d0 * abs(latrad - latwert))
            Tclim_K(ilev) = Tclim_K(ilev) + gewicht * tfeld(ilat)
            sumgewicht(ilev) = sumgewicht(ilev) + gewicht
        end do
    end do
end do

! NH
do imonth = 0,11
    call gonext(iunit_fwd,.false.)
    twert = imonth + 0.5
    do ilev = n_lev_clim,1,-1
        read (iunit_fwd,401) dumchar,dumreal,dumchar,(tfeld(ilat),ilat = 1,17)
        do ilat = 1,17
            latwert = 0.08726d0 * (ilat - 1)
            gewicht = (0.5d0 + 0.5d0 * cos(0.523598775d0 * (month - twert)))**10 &
              * exp(-2.0d0 * 11.459d0 * abs(latrad - latwert))
            Tclim_K(ilev) = Tclim_K(ilev) + gewicht * tfeld(ilat)
            sumgewicht(ilev) = sumgewicht(ilev) + gewicht
        end do
    end do
end do

Tclim_K = Tclim_K / sumgewicht

close (iunit_fwd)

! process daily pT file (standard part of file)
open (iunit_fwd,file = ptdatei,iostat = iowert,status = 'old')
if (iowert .ne. 0) then
    print *,'Cannot read pT file:'
    print *,pTdatei
    call ppquittung()
    stop
end if

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) npT
if (npT .gt. npT_max) then
    print *,'Max levels for p/T input:',npT_max
    call warnout('Read_pT: Too many levels in p/T file!',0)
end if

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) (hinp_m(i),i = 1,npT)
hinp_m(1:npT) = 1.0d3 * hinp_m(1:npT) ! km to m

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) (pinp_Pa(i),i = 1,npT)
pinp_Pa(1:npT) = 1.0d2 * pinp_Pa(1:npT) ! hPa to Pa

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) (Tinp_K(i),i = 1,npT)

close (iunit_fwd)

! interpolation / extrapolation of reference clim T to model levels
do i = 1,n_lev
    ! downward extrapolation
    if (h_lev_m(i) .lt. hclim_m(1)) then
        Twrka(i) = Tclim_K(1) + 6.5d-3 * (hclim_m(1) - h_lev_m(i))
    else
        if (h_lev_m(i) .gt. hclim_m(n_lev_clim)) then
            ! upward extrapolation
            Twrka(i) = Tclim_K(n_lev_clim)
        else
            ! interpolation
            inear = 1
            do while (hclim_m(inear+1) .lt. h_lev_m(i) .and. inear .lt. n_lev_clim)
                inear = inear + 1
            end do
            Twrka(i) = Tclim_K(inear) + (Tclim_K(inear+1) - Tclim_K(inear)) * &
              (h_lev_m(i) - hclim_m(inear)) / (hclim_m(inear+1) - hclim_m(inear))
        end if
    end if
end do

! interpolation / extrapolation of daily T to model levels
do i = 1,n_lev
    ! downward extrapolation
    if (h_lev_m(i) .lt. hinp_m(1)) then
        Twrkb(i) = Tinp_K(1) + (Tinp_K(2) - Tinp_K(1)) * &
              (h_lev_m(i) - hinp_m(1)) / (hinp_m(2) - hinp_m(1))
    else
        if (h_lev_m(i) .gt. hinp_m(npT)) then
            ! upward extrapolation
            Twrkb(i) = Tinp_K(npT)
        else
            ! interpolation
            inear = 1
            do while (hinp_m(inear+1) .lt. h_lev_m(i) .and. inear .lt. npT)
                inear = inear + 1
            end do
            Twrkb(i) = Tinp_K(inear) + (Tinp_K(inear+1) - Tinp_K(inear)) * &
              (h_lev_m(i) - hinp_m(inear)) / (hinp_m(inear+1) - hinp_m(inear))
            lnp = log(pinp_Pa(inear)) + log(pinp_Pa(inear+1) / pinp_Pa(inear)) * &
              (h_lev_m(i) - hinp_m(inear)) / (hinp_m(inear+1) - hinp_m(inear))
            p_Pa(i) = exp(lnp)
        end if
    end if
end do

! combine daily T and Tclim
do i = 1,n_lev
    dh = h_lev_m(i) - hclimschwell
    if (dh .gt. 0.0d0) then
        gewicht = 1.0d0 / (1.0d0 + 1.0d-7 * dh * dh)
        T_K(i) = gewicht * Twrkb(i) + (1.0d0 - gewicht) * Twrka(i)
    else
        T_K(i) = Twrkb(i)
    end if
end do

! interpolate for pressure at station level
if (h_lev_m(1) .lt. hinp_m(1)) then
    ! downward extrapolation
    print *,'Warning: ground p + T extrapolated!!'
    lnp = log(pinp_Pa(1)) + log(pinp_Pa(2) / pinp_Pa(1)) * &
      (h_lev_m(1) - hinp_m(1)) / (hinp_m(2) - hinp_m(1))
    p_Pa(1) = exp(lnp)
else
    ! interpolation
    inear = 1
    do while (hinp_m(inear+1) .lt. h_lev_m(1) .and. inear .lt. npT)
        inear = inear + 1
    end do
    lnp = log(pinp_Pa(inear)) + log(pinp_Pa(inear+1) / pinp_Pa(inear)) * &
    (h_lev_m(1) - hinp_m(inear)) / (hinp_m(inear+1) - hinp_m(inear))
     p_Pa(1) = exp(lnp)
end if

! construct approx hydrostatic atmosphere
do i = 2,n_lev
    dh = 14.6889d0 * (T_K(i-1) + T_K(i)) * (1.0d0 + 1.57d-7 * (h_lev_m(i-1) + h_lev_m(i)))
    p_Pa(i) = p_Pa(i-1) * exp(-(h_lev_m(i) - h_lev_m(1)) / dh)
end do

401 format(A13,F6.1,A1,17(F6.1))

end subroutine read_pT



!====================================================================
!  read_vmr: read vmr-Files
!====================================================================
subroutine read_vmr(vmrdatei,n_lev,h_lev_m,vmr_lev)

use globfwd23, only : iunit_fwd

implicit none

character(len=*),intent(in) :: vmrdatei


integer,intent(in) :: n_lev
real(8),dimension(n_lev),intent(in) :: h_lev_m
real(8),dimension(n_lev),intent(out) :: vmr_lev

integer,parameter :: nvmr_max = 120
logical :: flagdown
integer :: i,iowert,nvmr,inear
real(8),dimension(nvmr_max) :: hinp_m,vmrinp

open (iunit_fwd,file = vmrdatei,iostat = iowert,status = 'old',action = 'read')
if (iowert .ne. 0) then
    print *,'Cannot read vmr file:'
    print *,vmrdatei
    call ppquittung()
    stop
end if

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) nvmr
if (nvmr .gt. nvmr_max) then
    print *,'Max levels for vmr input:',nvmr_max
    call warnout('Read_vmr: Too many levels in vmr file!',0)
end if

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) (hinp_m(i),i = 1,nvmr)
hinp_m(1:nvmr) = 1.0d3 * hinp_m(1:nvmr) ! km to m

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) (vmrinp(i),i=1,nvmr)
close (iunit_fwd)

flagdown = .false.

! interpolation / extrapolation of input VMR to model levels
do i = 1,n_lev
    ! downward extrapolation
    if (h_lev_m(i) .lt. hinp_m(1)) then
        vmr_lev(i) = vmrinp(1)
        if (hinp_m(1) - h_lev_m(i) .gt. 1.0d2) then
            flagdown = .true.
        end if
    else
        if (h_lev_m(i) .gt. hinp_m(nvmr)) then
            ! upward extrapolation
            vmr_lev(i) = vmrinp(nvmr)
        else
            ! interpolation
            inear = 1
            do while (hinp_m(inear+1) .lt. h_lev_m(i) .and. inear .lt. nvmr)
                inear = inear + 1
            end do
            vmr_lev(i) =vmrinp(inear) + (vmrinp(inear+1) - vmrinp(inear)) * &
              (h_lev_m(i) - hinp_m(inear)) / (hinp_m(inear+1) - hinp_m(inear))
        end if
    end if
end do

if (flagdown) then
    print *,vmrdatei
    print *,'Warning: Extrapolation in VMR at lower bound!'
end if

end subroutine read_vmr


!====================================================================
!  read_voigttable: read and initialize tabellated voigt
!====================================================================
subroutine read_voigttable()

use globlines23,only : maxvgt_d,maxvgt_nue,voigttable

implicit none

integer :: iowert,i,j
complex :: cwert
real,dimension(:),allocatable :: werteinzeile

allocate (werteinzeile(maxvgt_d))

! read tabellated values from file
open (iunit_fwd,file = 'INP_FWD/voigttable.dat',iostat = iowert,status = 'old',action = 'read')

if (iowert .ne. 0) then
    print *,'Cannot read voigt table!'
    call ppquittung()
    stop
end if

call gonext(iunit_fwd,.false.)
do i = 1,maxvgt_nue
    read(iunit_fwd,*)(werteinzeile(j),j = 1,maxvgt_d)
    do j = 1,maxvgt_d
        voigttable(i,j) = cmplx(werteinzeile(j),0.0)
    end do
end do

close (iunit_fwd)

deallocate (werteinzeile)

! imaginary part holds derivatives as fct of d-index
do i = 1,maxvgt_nue
    do j = 1,maxvgt_d - 1
        cwert = cmplx(real(voigttable(i,j)),real(voigttable(i,j+1)) - real(voigttable(i,j)))
        voigttable(i,j) = cwert
    end do
end do

end subroutine read_voigttable



!====================================================================
!  read_wind: read information on wind speed and shear
!====================================================================
subroutine read_wind(winddatei,n_lev,h_lev_m,sza_lev_rad,azi_rad,wind_scal_lev)

use globfwd23, only : iunit_fwd

implicit none

character(len=*),intent(in) :: winddatei
integer,intent(in) :: n_lev
real(8),dimension(n_lev),intent(in) :: h_lev_m,sza_lev_rad
real(8),intent(in) :: azi_rad
real(8),dimension(n_lev),intent(out) :: wind_scal_lev

integer,parameter :: nwind_max = 120
logical :: flagup,flagdown
integer :: i,j,iowert,nwind,inear
real(8),parameter :: kwclicht = 3.335641d-9
real(8) :: dist,distmin,sinazi,cosazi,werta,wertb
real(8),dimension(nwind_max) :: hinp_m,u_inp,v_inp

sinazi = sin(azi_rad)
cosazi = cos(azi_rad)

open (iunit_fwd,file = winddatei,iostat = iowert,status = 'old',action = 'read')
if (iowert .ne. 0) then
    print *,'Cannot read wind file:'
    print *,winddatei
    call ppquittung()
    stop
end if

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) nwind
if (nwind .gt. nwind_max) then
    print *,'Max levels for wind input:',nwind_max
    call warnout('Read_vmr: Too many levels in vmr file!',0)
end if

call gonext(iunit_fwd,.false.)
read (iunit_fwd,*) (hinp_m(i),i = 1,nwind)
hinp_m(1:nwind) = 1.0d3 * hinp_m(1:nwind) ! km to m

call gonext(iunit_fwd,.false.)
do i = 1,nwind
    read (iunit_fwd,*) u_inp(i),v_inp(i) ! inputs are velocities, units m/s
end do

close (iunit_fwd)

do i = 1,n_lev
    ! find nearest input level
    distmin = abs(hinp_m(1) - h_lev_m(i))
    inear = 1
    do j = 2,nwind
        dist = abs(hinp_m(j) - h_lev_m(i))
        if (dist .lt. distmin) then
           distmin = dist
           inear = j
        end if
    end do
    ! is nearest input level below or above current model level?
    if (hinp_m(inear) .le. h_lev_m(i)) then
        ! upward extrapolation or interpolation
        if (inear .eq. nwind) then
            wind_scal_lev(i) = sin(sza_lev_rad(i)) * (u_inp(nwind) * sinazi + v_inp(nwind) * cosazi) * kwclicht
            if (h_lev_m(i) - hinp_m(nwind) .gt. 5.0d3) then
                flagup = .true.
            end if
        else
            werta = sin(sza_lev_rad(i)) * (u_inp(inear) * sinazi + v_inp(inear) * cosazi) * kwclicht
            wertb = sin(sza_lev_rad(i)) * (u_inp(inear+1) * sinazi + v_inp(inear+1) * cosazi) * kwclicht
            wind_scal_lev(i) = werta + (wertb - werta) * (h_lev_m(i) - hinp_m(inear)) / (hinp_m(inear+1) - hinp_m(inear))
        end if
    else
        ! downward extrapolation or interpolation
        if (inear .eq. 1) then
            wind_scal_lev(i) = sin(sza_lev_rad(i)) * (u_inp(1) * sinazi + v_inp(1) * cosazi) * kwclicht
            if (hinp_m(1) - h_lev_m(i) .gt. 1.0d2) then
                flagdown = .true.
            end if
        else
            werta = sin(sza_lev_rad(i)) * (u_inp(inear-1) * sinazi + v_inp(inear-1) * cosazi) * kwclicht
            wertb = sin(sza_lev_rad(i)) * (u_inp(inear) * sinazi + v_inp(inear) * cosazi) * kwclicht
            wind_scal_lev(i) = werta + (wertb - werta) * (h_lev_m(i) - hinp_m(inear-1)) / (hinp_m(inear) - hinp_m(inear-1))
        end if
    end if
end do

if (flagdown) then
    print *,winddatei
    print *,'Warning: Extrapolation of wind data at lower bound!'
end if

if (flagup) then
    print *,winddatei
    print *,'Warning: Extrapolation of wind data at upper bound!'
end if

end subroutine read_wind



!====================================================================
!  Function SDG: calculates SDG correction from tabellated values
!====================================================================
real function SDGkorr(nue,gauhwhm,gam0,gam2,beta)

use globsdg23

implicit none

real(8),intent(in) :: nue,gauhwhm,gam0,gam2,beta

integer :: inue,igam0,igam2,ibeta,i,j,k,l
real(8) :: nuerest,gam0rest,gam2rest,betarest,nuerel &
  ,sumwert,faktorgam0,wi,wj,wk,wl


! determine position in table
nuerel = (abs(nue) + 1.0d-15) / voigthwhm(gauhwhm,gam0)

call sdg_nue_inv(nuerel,inue,nuerest)
call sdg_gam0_inv(gam0,gauhwhm,igam0,gam0rest)
call sdg_gam2_inv(gam2,gam0,igam2,gam2rest)
call sdg_beta_inv(beta,gam0,ibeta,betarest)

if (inue .gt. nmax_sdg_nue - 2) then
    SDGkorr = 0.0d0
else
    ! decide between inter or extrapolation along nue and / or gam0
    if (igam0 .lt. 0) then
        igam0 = 0
        gam0rest = 0.0d0
        faktorgam0 = gam0 / sdg_gam0(0,gauhwhm)
    else
        faktorgam0 = 1.0d0
    end if
    ! linear interpolation in table
    sumwert = 0.0d0
    do i = 0,1 ! beta
        do j = 0,1 ! gam2
            do k = 0,1 ! gam0
                do l = 0,1 ! nue
                    wi = (1 - i) * (1.0d0 - betarest) + i * betarest
                    wj = (1 - j) * (1.0d0 - gam2rest) + j * gam2rest
                    wk = (1 - k) * (1.0d0 - gam0rest) + k * gam0rest
                    wl = (1 - l) * (1.0d0 - nuerest) + l * nuerest
                    sumwert = sumwert + wi * wj * wk * wl * sdgfeld(inue+1+l,igam0+1+k,igam2+1+j,ibeta+1+i)
                end do
            end do
        end do
    end do
    SDGkorr = sumwert * faktorgam0
end if

end function SDGkorr



!====================================================================
!  sdg_checktable: determines whether SDG binary table is available
!====================================================================
subroutine sdg_checktable(sdgbindec)

use globfwd23, only : wrkpfad,sdgdatei,iunit_fwd

implicit none

logical,intent(out) :: sdgbindec

integer :: iowert

! check presence of required binary sdg file
print *, trim(wrkpfad) // '/' //  trim(sdgdatei)
open (iunit_fwd,file = trim(wrkpfad) // '/' // trim(sdgdatei),iostat = iowert &
  ,status = 'old',position = 'rewind',action = 'read')
if (iowert .eq. 0) then
    sdgbindec = .true.
    print *,'Binary SDG file found!'
else
    sdgbindec = .false.
    print *,'Binary SDG file missing!'
end if
close(iunit_fwd)

end subroutine sdg_checktable



!====================================================================
!  sdg_init: initializes array for SDG calculation
!====================================================================
subroutine sdg_init()

use globsdg23

implicit none

integer :: ibeta,igam2,igam0,ix,inue
real(8) :: gam0,gauhwhm,hwhmwert,kdopp,gam2,beta,x,terma,termb &
  ,subterma,subtermb,sumwert
real(8),dimension(:),allocatable :: xfeld,nuefeld

allocate (xfeld(nmax_sdg_x),nuefeld(nmax_sdg_nue))

do ibeta = 0,nmax_sdg_beta - 1
    do igam2 = 0,nmax_sdg_gam2 - 1
        do igam0 = 0,nmax_sdg_gam0 - 1
            print *,ibeta,igam2,igam0

            ! All tabellated line shapes are scaled to empirical Voigt HWHM = 1
            gauhwhm = 1.0d0
            gam0 = sdg_gam0(igam0,gauhwhm)
            hwhmwert = voigthwhm(gauhwhm,gam0)
            gauhwhm = gauhwhm / hwhmwert
            gam0 = gam0 / hwhmwert
            kdopp = 0.25d0 * gauhwhm * gauhwhm / log(2.0d0)

            ! Calculate FT correction if required
            ! if gam2 and beta 0 -> no FT correction added to Voigt
            if (igam2 .gt. 0 .or. ibeta .gt. 0) then
                gam2 = sdg_gam2(igam2,gam0)
                beta = sdg_beta(ibeta,gam0)

                ! calculate FT of difference SDG - Voigt reference
                do ix = 0,nmax_sdg_x - 1
                    x = dx_sdg * real(ix,8)

                    ! SDG (final result: SDG = terma * termb)
                    terma = exp(-(gam0 - 1.5d0 * gam2) * x) &
                      / sqrt((1.0d0 + gam2 * x) * (1.0d0 + gam2 * x) * (1.0d0 + gam2 * x))
                    if (beta * x .gt. 1.0d-4) then
                        subterma = 2.0 * (beta * x - 1.0 + exp(-beta * x))
                        subtermb = gam2 * x * (1.0 - exp(-beta * x)) * (1.0 - exp(-beta * x)) / (1.0 + gam2 * x)
                        termb = exp(-kdopp * (subterma - subtermb) / (beta * beta))
                    else
                        subterma = 1.0 - beta * x / 3.0
                        subtermb = gam2 * x * (1.0 - beta * x) / (1.0 + gam2 * x)
                        termb = exp(-kdopp * x * x * (subterma - subtermb))
                    end if

                    ! subtract FT of reference Voigt + write into array
                    xfeld(ix+1) = terma * termb - exp(-gam0 * x) * exp(-kdopp * x * x)
                 end do

                 ! Generate FT of difference SDG - Voigt on nonequidistant nue-grid
                 do inue = 0,nmax_sdg_nue - 1
                     sumwert = 0.0d0
                     ! sum up all contributions along x
                     ! Note: xfeld(0) = 0 by construction, therefore omitted
                     do ix = 1,nmax_sdg_x - 1
                         x = dx_sdg * real(ix,8)
                         sumwert = sumwert + xfeld(ix+1) * cos(sdg_nue(inue) * x)
                     end do
                     nuefeld(inue+1) = sumwert * dx_sdg / pi
                 end do
            else
                nuefeld(:) = 0.0d0
            end if

            ! final tabellated value
            do inue = 0,nmax_sdg_nue - 1
                sdgfeld(inue+1,igam0+1,igam2+1,ibeta+1) = nuefeld(inue+1) &
                / VoigtRef(sdg_nue(inue),1.0d0,gauhwhm,gam0)
            end do

            ! Enforce zero correction at rim of table
            do inue = 0,nmax_sdg_nue - 1
                sdgfeld(inue+1,igam0+1,igam2+1,ibeta+1) = sdgfeld(inue+1,igam0+1,igam2+1,ibeta+1) &
                  - sdgfeld(nmax_sdg_nue,igam0+1,igam2+1,ibeta+1) * sdg_nue(inue) / sdg_nue(nmax_sdg_nue - 1)
            end do
        end do
    end do
end do

deallocate (xfeld,nuefeld)

contains

real(8) function VoigtRef(xwert,voigtnorm,ingauhwhm,inlorhwhm)

use globsdg23, only: pi

implicit none

real(8),intent(in) :: xwert,voigtnorm,ingauhwhm,inlorhwhm

integer :: j
real(8) :: bgin,werta,sumwert,bg,ag,bgf,gausswert

bgin = ingauhwhm / sqrt(log(2.0d0))
werta = 4.0d0 * inlorhwhm * inlorhwhm

sumwert = 0.0d0
do j = 120,1,-1
    bg = 0.1d0 * exp(0.4d0 * real(j,8))
    ag = 0.225676d0 * exp(-0.25d0 / (bg * bg)) / bg
    bgf = sqrt(bg * bg * werta + bgin * bgin)
    gausswert = ag * exp(-xwert * xwert / (bgf * bgf)) / (bgf * sqrt(pi))
    sumwert = sumwert + gausswert
end do

VoigtRef = voigtnorm * sumwert

end function VoigtRef

end subroutine sdg_init



!====================================================================
!  sdg_readtable: Read tabellated SDG correction
!====================================================================
subroutine sdg_readtable()

use globfwd23, only : wrkpfad,sdgdatei,iunit_fwd
use globsdg23

implicit none

character(len=2) :: dumchar
integer :: i,j,k,l,iwert
real(8) :: wert

open (iunit_fwd,file = trim(wrkpfad) // '/' // trim(sdgdatei),status = 'old',action = 'read')

! check array dimensions
call gonext(iunit_fwd,.false.)
read(iunit_fwd,*) iwert
if (iwert .ne. nmax_sdg_nue) then
    call warnout('SDG-table: nmax-nue incompatible!',0)
end if
read(iunit_fwd,*) iwert
if (iwert .ne. nmax_sdg_gam0) then
    call warnout('SDG-table: nmax-gam0 incompatible!',0)
end if
read(iunit_fwd,*) iwert
if (iwert .ne. nmax_sdg_gam2) then
    call warnout('SDG-table: nmax-gam2 incompatible!',0)
end if
read(iunit_fwd,*) iwert
if (iwert .ne. nmax_sdg_beta) then
    call warnout('SDG-table: nmax-beta incompatible!',0)
end if

! check gridpoint positions
call gonext(iunit_fwd,.false.)
do i = 0,nmax_sdg_nue - 1
    read(iunit_fwd,*) wert
    if (abs(wert - sdg_nue(i)) / (sdg_nue(i) + 1.0d-4) .gt. 1.0d-3) then
        call warnout('SDG-table: nue incompatible!',0)
    end if
end do
call gonext(iunit_fwd,.false.)
do i = 0,nmax_sdg_gam0 - 1
    read(iunit_fwd,*) wert
    if (abs(wert - sdg_gam0(i,1.0d0)) / sdg_gam0(i,1.0d0) .gt. 1.0d-3) then
        call warnout('SDG-table: gam0 incompatible!',0)
    end if
end do
call gonext(iunit_fwd,.false.)
do i = 0,nmax_sdg_gam2 - 1
    read(iunit_fwd,*) wert
    if (abs(wert - sdg_gam2(i,1.0d0)) / sdg_gam2(i,1.0d0) .gt. 1.0d-3) then
        call warnout('SDG-table: gam2 incompatible!',0)
    end if
end do
call gonext(iunit_fwd,.false.)
do i = 0,nmax_sdg_beta - 1
    read(iunit_fwd,*) wert
    if (abs(wert - sdg_beta(i,1.0d0)) / sdg_beta(i,1.0d0) .gt. 1.0d-3) then
        call warnout('SDG-table: beta incompatible!',0)
    end if
end do

close (iunit_fwd)

! read binary table content
open (iunit_fwd,file = trim(wrkpfad) // '/' // trim(sdgdatei), status = 'old',action = 'read')

do i = 1,6
    call gonext(iunit_fwd,.true.)
end do
read(iunit_fwd) dumchar

do i = 0,nmax_sdg_beta - 1
    do j = 0,nmax_sdg_gam2 - 1
        do k = 0,nmax_sdg_gam0 - 1
            do l = 0,nmax_sdg_nue - 1
                read(iunit_fwd) sdgfeld(l+1,k+1,j+1,i+1)
            end do
        end do
    end do
end do

close (iunit_fwd)

end subroutine sdg_readtable



!====================================================================
!  sdg_writetable: write sdg-table values to file
!====================================================================
subroutine sdg_writetable()

use globfwd23, only : wrkpfad,sdgdatei,iunit_fwd
use globsdg23

implicit none

character(len=2) :: eol,exta,extb
character(len=16) :: numchar
integer :: i,j,k,l

! Output in ASCII for checking of table
do i = 0,nmax_sdg_gam2 - 1
    write (exta,'(I2.2)') i
    do j = 0,nmax_sdg_beta - 1
        write (extb,'(I2.2)') j
        open (iunit_fwd,file = trim(wrkpfad)//'/SDG'//exta//extb//'.DAT',status = 'replace')
        do k = 0,nmax_sdg_nue - 1
            write (iunit_fwd,'(ES12.4,100(1X,ES12.4))') sdg_nue(k),(sdgfeld(k+1,l+1,j+1,i+1),l = 0,nmax_sdg_gam0 - 1)
        end do
        close (iunit_fwd)
    end do
end do


! end of line 0D+0A ASCI: 13 + 10
eol = char(13) // char(10)

open (iunit_fwd,file = trim(wrkpfad) // '/' // trim(sdgdatei), status = 'replace')

! header: characterisation of tabellated regions
! number of gridpoints
write (iunit_fwd) 'Number of gridpoints',eol
write (iunit_fwd) 'along nue',eol
write (iunit_fwd) 'along gam0',eol
write (iunit_fwd) 'along gam2',eol
write (iunit_fwd) 'along beta',eol
write (iunit_fwd) '$',eol
numchar = '              '
write (numchar,'(I6)') nmax_sdg_nue
write (iunit_fwd) trim(numchar),eol
numchar = '              '
write (numchar,'(I6)') nmax_sdg_gam0
write (iunit_fwd) trim(numchar),eol
numchar = '              '
write (numchar,'(I6)') nmax_sdg_gam2
write (iunit_fwd) trim(numchar),eol
numchar = '              '
write (numchar,'(I6)') nmax_sdg_beta
write (iunit_fwd) trim(numchar),eol,eol

! values nuerel
write (iunit_fwd) '$',eol
do i = 0,nmax_sdg_nue - 1
    numchar = '              '
    write (numchar,'(ES10.3)') sdg_nue(i)
    write (iunit_fwd) trim(numchar),eol
end do
write (iunit_fwd) eol

! values gam0 / gauhwhm
write (iunit_fwd) '$',eol
do i = 0,nmax_sdg_gam0 - 1
    numchar = '              '
    write (numchar,'(ES10.3)') sdg_gam0(i,1.0d0)
    write (iunit_fwd) trim(numchar),eol
end do
write (iunit_fwd) eol

! values gam2 / gam0
write (iunit_fwd) '$',eol
do i = 0,nmax_sdg_gam2 - 1
    numchar = '              '
    write (numchar,'(ES10.3)') sdg_gam2(i,1.0d0)
    write (iunit_fwd) trim(numchar),eol
end do
write (iunit_fwd) eol

! values beta / gam0
write (iunit_fwd) '$',eol
do i = 0,nmax_sdg_beta - 1
    numchar = '              '
    write (numchar,'(ES10.3)') sdg_beta(i,1.0d0)
    write (iunit_fwd) trim(numchar),eol
end do
write (iunit_fwd) eol

! binary entries
write (iunit_fwd) '$',eol
do i = 0,nmax_sdg_beta - 1
    do j = 0,nmax_sdg_gam2 - 1
        do k = 0,nmax_sdg_gam0 - 1
            do l = 0,nmax_sdg_nue - 1
                write (iunit_fwd) sdgfeld(l+1,k+1,j+1,i+1)
            end do
        end do
    end do
end do

close (iunit_fwd)

end subroutine sdg_writetable



!====================================================================
!  tofile_dbase: write continuum baseline derivative to file
!====================================================================
subroutine tofile_dbase(nfwdrun,dateiext,dspec_dbase)

use globfwd23,only : outpfad_fwd,n_mw,n_lev,mw,wvskal,deri,iunit_fwd

implicit none

integer,intent(in) :: nfwdrun
character(len=*),intent(in) :: dateiext
real,dimension(mw(n_mw)%rb,deri%maxnbase),intent(in) :: dspec_dbase

character(2) :: chrun,exta
integer :: i,k,l

write (chrun,'(I2.2)') nfwdrun
do i = 1,n_mw
    write (exta,'(I2.2)') i
    open (iunit_fwd,file = trim(outpfad_fwd)//'/RUN'//chrun//dateiext//exta//'.DAT',status = 'replace')
    do k = 1,mw(i)%pts
        write (iunit_fwd,'(ES15.9,100(1X,ES12.4))') &
          mw(i)%firstnue + wvskal%dnue * (k - 1),(dspec_dbase(mw(i)%lb+k-1,l),l = 1,mw(i)%n_base)
    end do
    close (iunit_fwd)
end do

end subroutine tofile_dbase



!====================================================================
!  tofile_dchan: write chan derivative to file
!====================================================================
subroutine tofile_dchan(nfwdrun,dateiext,dspec_dchan)

use globfwd23,only : outpfad_fwd,n_mw,n_chan,mw,wvskal,iunit_fwd

implicit none

integer,intent(in) :: nfwdrun
character(len=*),intent(in) :: dateiext
real,dimension(mw(n_mw)%rb,2,n_chan),intent(in) :: dspec_dchan

character(2) :: chrun,extb
integer :: j,k,l,m

! Speichern des Feldes
write (chrun,'(I2.2)') nfwdrun
do j = 1,n_mw
    write (extb,'(I2.2)') j
    open (iunit_fwd,file = trim(outpfad_fwd)//'/RUN'//chrun//dateiext//extb//'.DAT',status = 'replace')
    do k = 1,mw(j)%pts
        write (iunit_fwd,'(ES15.9,80(1X,ES12.4))') &
          mw(j)%firstnue + wvskal%dnue * (k - 1) &
            ,((dspec_dchan(mw(j)%lb+k-1,l,m),l = 1,2),m = 1,n_chan)
    end do
    close (iunit_fwd)
end do

end subroutine tofile_dchan



!====================================================================
!  tofile_dHIT: write vmr derivative to file
!====================================================================
subroutine tofile_dHIT(nfwdrun,dateiext,dspec_dHIT)

use globfwd23,only : outpfad_fwd,n_mw,mw,wvskal,deri,iunit_fwd

implicit none

integer,intent(in) :: nfwdrun
character(len=*),intent(in) :: dateiext
real,dimension(mw(n_mw)%rb,deri%n_vmr),intent(in) :: dspec_dHIT

character(2) :: chrun,exta,extb
integer :: i,j,k

! Speichern des Feldes
write (chrun,'(I2.2)') nfwdrun
do i = 1,deri%n_vmr
    write (exta,'(I2.2)') i
    do j = 1,n_mw
        write (extb,'(I2.2)') j
        open (iunit_fwd,file = trim(outpfad_fwd)//'/RUN'//chrun//dateiext//exta &
          //extb//'.DAT',status = 'replace')
        do k = 1,mw(j)%pts
            write (iunit_fwd,'(ES15.9,2(1X,ES12.4))') &
              mw(j)%firstnue + wvskal%dnue * (k - 1),dspec_dHIT(mw(j)%lb+k-1,i)
        end do
        close (iunit_fwd)
    end do
end do

end subroutine tofile_dHIT



!====================================================================
!  tofile_dT: write T derivative to file
!====================================================================
subroutine tofile_dT(nfwdrun,dateiext,dspec_dT)

use globfwd23,only : outpfad_fwd,n_mw,n_lev,mw,wvskal,iunit_fwd

implicit none

integer,intent(in) :: nfwdrun
character(len=*),intent(in) :: dateiext
real,dimension(mw(n_mw)%rb,n_lev),intent(in) :: dspec_dT

character(2) :: chrun,extb
integer :: j,k,l

! Speichern des Feldes
write (chrun,'(I2.2)') nfwdrun
do j = 1,n_mw
    write (extb,'(I2.2)') j
    open (iunit_fwd,file = trim(outpfad_fwd)//'/RUN'//chrun//dateiext//extb &
      //'.DAT',status = 'replace')
    do k = 1,mw(j)%pts
        write (iunit_fwd,'(ES15.9,80(1X,ES12.4))') &
          mw(j)%firstnue + wvskal%dnue * (k - 1),(dspec_dT(mw(j)%lb+k-1,l),l = 1,n_lev)
    end do
    close (iunit_fwd)
end do

end subroutine tofile_dT



!====================================================================
!  tofile_dvmr: write vmr derivative to file
!====================================================================
subroutine tofile_dvmr(nfwdrun,dateiext,dspec_dvmr)

use globfwd23,only : outpfad_fwd,n_mw,n_lev,mw,wvskal,deri,iunit_fwd

implicit none

integer,intent(in) :: nfwdrun
character(len=*),intent(in) :: dateiext
real,dimension(mw(n_mw)%rb,n_lev,deri%n_vmr),intent(in) :: dspec_dvmr

character(2) :: chrun,exta,extb
integer :: i,j,k,l

! Speichern des Feldes
write (chrun,'(I2.2)') nfwdrun
do i = 1,deri%n_vmr
    write (exta,'(I2.2)') i
    do j = 1,n_mw
        write (extb,'(I2.2)') j
        open (iunit_fwd,file = trim(outpfad_fwd)//'/RUN'//chrun//dateiext//exta &
          //extb//'.DAT',status = 'replace')
        do k = 1,mw(j)%pts
            write (iunit_fwd,'(ES15.9,80(1X,ES12.4))') &
              mw(j)%firstnue + wvskal%dnue * (k - 1),(dspec_dvmr(mw(j)%lb+k-1,l,i),l = 1,n_lev)
        end do
        close (iunit_fwd)
    end do
end do

end subroutine tofile_dvmr



!====================================================================
!  tofile_ils: write ILS for each MW to file
!====================================================================
subroutine tofile_ils(nfwdrun,ils_mw)

use globfwd23, only : outpfad_fwd,wvskal,iunit_fwd

implicit none

integer,intent(in) :: nfwdrun
real,dimension(-wvskal%ilsradius:wvskal%ilsradius,n_mw),intent(in) :: ils_mw

character(2) :: chrun
integer :: i,j

! Speichern des Feldes
write (chrun,'(I2.2)') nfwdrun
open (iunit_fwd,file = trim(outpfad_fwd)//'/RUN'//chrun//'_ILS_MW.DAT',status = 'replace')
do i = -wvskal%ilsradius,wvskal%ilsradius
    write (iunit_fwd,'(ES15.8,30(1X,ES12.4))') &
      wvskal%dnuefine * real(i,8),(ils_mw(i,j),j=1,n_mw)
end do
close (iunit_fwd)

end subroutine tofile_ils



!====================================================================
!  tofile_ilsparts: Rausschreiben der ILS-Zerlegung
!====================================================================
subroutine tofile_ilsparts(ilsparts)

use globfwd23, only : outpfad_fwd,n_ifg,wvskal,iunit_fwd

implicit none

real,dimension(-wvskal%ilsradius:wvskal%ilsradius,0:n_ifg-1,2),intent(in) :: ilsparts
integer :: i,j

! Gerade Anteile
open (iunit_fwd,file = trim(outpfad_fwd)//'/ILSPARTG.DAT',status = 'replace')
do i = -wvskal%ilsradius,wvskal%ilsradius
    write(iunit_fwd,'(ES10.3,10000(1X,ES10.3))') (ilsparts(i,j,1),j = 0,n_ifg - 1)
end do
close (iunit_fwd)

! Ungerade Anteile
open (iunit_fwd,file = trim(outpfad_fwd)//'/ILSPARTU.DAT',status = 'replace')
do i = -wvskal%ilsradius,wvskal%ilsradius
    write(iunit_fwd,'(ES10.3,10000(1X,ES10.3))') (ilsparts(i,j,2),j = 0,n_ifg - 1)
end do
close (iunit_fwd)

end subroutine tofile_ilsparts



!====================================================================
!  tofile_raytra: write p,T,h,sza to file
!====================================================================
subroutine tofile_raytra(nfwdrun,p_lev_pa,lev_colair,T_lev_K,h_lev_m,h2o_lev_ppmv,sza_lev_rad)

use globfwd23, only : outpfad_fwd,n_lev,iunit_fwd

implicit none

integer,intent(in) :: nfwdrun
real(8),dimension(n_lev),intent(in) :: p_lev_pa,lev_colair,T_lev_K,h_lev_m &
  ,h2o_lev_ppmv,sza_lev_rad

character(2) :: chrun
integer :: i

write (chrun,'(I2.2)') nfwdrun
open (iunit_fwd,file = trim(outpfad_fwd)//'/RUN'//chrun//'_RAYTRACE.DAT',status = 'replace')
write (iunit_fwd,'(A)') 'Level index    Altitude(m)    Temperature(K)  ' &
  // '   Pressure(Pa)    vert. air column    H2O[ppmv]    SZA_lev(rad)'
do i = 1,n_lev
    write (iunit_fwd,'(I4,10(6X,ES12.5))') i,h_lev_m(i),T_lev_K(i),p_lev_pa(i) &
      ,lev_colair(i),h2o_lev_ppmv(i),sza_lev_rad(i)
end do
close (iunit_fwd)

end subroutine tofile_raytra



!====================================================================
!  tofile_spc: write solar trm array to file
!====================================================================
subroutine tofile_spc(nfwdrun,dateiext,spec)

use globfwd23, only : outpfad_fwd,n_mw,mw,wvskal,iunit_fwd

implicit none

integer,intent(in) :: nfwdrun
character(len=*),intent(in) :: dateiext
real,dimension(mw(n_mw)%rbfinext),intent(in) :: spec

character(2) :: chrun,extb
integer :: j,k

! Speichern des Feldes
write (chrun,'(I2.2)') nfwdrun
do j = 1,n_mw
    write (extb,'(I2.2)') j
    open (iunit_fwd,file = trim(outpfad_fwd)//'/RUN'//chrun// &
      dateiext//extb//'.DAT',status = 'replace')
    do k = 1,mw(j)%ptsfinext
        write (iunit_fwd,'(ES15.9,2(1X,ES12.4))') &
          mw(j)%firstnuefinext + wvskal%dnuefine * (k - 1),spec(mw(j)%lbfinext+k-1)
    end do
    close (iunit_fwd)
end do

end subroutine tofile_spc



!====================================================================
!  tofile_spcxils: write convolved spectrum to file
!====================================================================
subroutine tofile_spcxils(nfwdrun,dateiext,specxils)

use globfwd23, only : outpfad_fwd,n_mw,mw,wvskal,iunit_fwd

implicit none

integer,intent(in) :: nfwdrun
character(len=*),intent(in) :: dateiext
real,dimension(mw(n_mw)%rb),intent(in) :: specxils

character(2) :: chrun,extb
integer :: j,k

! Speichern des Feldes
write (chrun,'(I2.2)') nfwdrun
do j = 1,n_mw
    write (extb,'(I2.2)') j
    open (iunit_fwd,file = trim(outpfad_fwd)//'/RUN'//chrun//dateiext//extb//'.DAT',status = 'replace')
    do k = 1,mw(j)%pts
        write (iunit_fwd,'(ES15.9,2(1X,ES14.6))') &
          mw(j)%firstnue + wvskal%dnue * (k - 1),specxils(mw(j)%lb+k-1)
    end do
    close (iunit_fwd)
end do


end subroutine tofile_spcxils



!====================================================================
!  tofile_tau: write tau array to file
!====================================================================
subroutine tofile_tau(nfwdrun,tau_lev)

use globfwd23, only : outpfad_fwd,n_lev,n_tau,n_mw,mw,wvskal,iunit_fwd

implicit none

integer,intent(in) :: nfwdrun
real,dimension(2,mw(n_mw)%rbfinext,n_lev,n_tau),intent(in) :: tau_lev

character(2) :: chrun,exta,extc
integer :: i,j,k,l

! Speichern des Feldes
write (chrun,'(I2.2)') nfwdrun
do i = 1,n_tau
    write (exta,'(I2.2)') i
    do k = 1,n_mw
        write (extc,'(I2.2)') k
        open (iunit_fwd,file = trim(outpfad_fwd)//'/RUN'//chrun// &
          '_TAU'//exta//extc//'.DAT',status = 'replace')
        do l = 1,mw(k)%ptsfinext
            write (iunit_fwd,'(ES15.9,100(1X,ES12.4))') &
              mw(k)%firstnuefinext + wvskal%dnuefine * (l - 1),(tau_lev(1,mw(k)%lbfinext+l-1,j,i),j=1,n_lev)
        end do
        close (iunit_fwd)
    end do
end do

end subroutine tofile_tau



!====================================================================
!  tofile_trm: write trm array to file
!====================================================================
subroutine tofile_trm(nfwdrun,trm_speci,atmtrm)

use globfwd23

implicit none

integer,intent(in) :: nfwdrun
real,dimension(mw(n_mw)%rbfinext,n_tau),intent(in) :: trm_speci
real,dimension(mw(n_mw)%rbfinext),intent(in) :: atmtrm

character(2) :: chrun,extb
integer :: j,k,l

! save total atm transmission and transmission per species
write (chrun,'(I2.2)') nfwdrun
do k = 1,n_mw
    write (extb,'(I2.2)') k
    open (iunit_fwd,file = trim(outpfad_fwd)//'/RUN'//chrun//'_TRM'//extb//'.DAT',status = 'replace')
    do l = 1,mw(k)%ptsfinext
        write (iunit_fwd,'(ES15.9,40(1X,ES12.4))') &
          mw(k)%firstnuefinext + wvskal%dnuefine * (l - 1) &
          ,atmtrm(mw(k)%lbfinext+l-1),(trm_speci(mw(k)%lbfinext+l-1,j),j=1,n_tau)
    end do
    close (iunit_fwd)
end do

end subroutine tofile_trm



!====================================================================
!  Berechnung der empirischen HWHM einer Voigtkurve
!====================================================================
real(8) function voigthwhm(gauhwhm,lorhwhm)

implicit none

real(8),intent(in) :: gauhwhm,lorhwhm

voigthwhm = 0.5346d0 * lorhwhm + sqrt(0.2166d0 * lorhwhm * lorhwhm + gauhwhm * gauhwhm)

end function voigthwhm



!!====================================================================
!!  Berechnung eines Voigtwertes (mit modifizierter Liu-Routine)
!!====================================================================
!complex(8) function voigtliumod(deltanue,vhwhm,faktorlor_liu,faktorgau_liu,corr_liu,YLM)
!
!implicit none
!
!real(8),intent(in) :: deltanue,vhwhm,faktorlor_liu,faktorgau_liu,corr_liu,YLM
!
!real(8) :: deltaxrel,deltaxrelqu,argument,korrektur,voigtre
!
!deltaxrel = deltanue / vhwhm
!deltaxrelqu = deltaxrel * deltaxrel
!argument = 4.0d0 * (ABS(deltaxrel) - 4.5d0)
!if (argument .gt. 100.0d0) then
!    korrektur = corr_liu / (1.0d0 + corr_liu)
!else
!    korrektur = (corr_liu + 1.0d0 / (1.0d0 + EXP(argument))) / (1.0d0 + corr_liu)
!end if
!voigtre = korrektur * (faktorlor_liu / (deltaxrelqu + 1.0d0) &
!  + faktorgau_liu * EXP(-0.69314718d0 * deltaxrelqu))
!
!voigtliumod = cmplx(voigtre,YLM * deltaxrel * voigtre)
!
!end function voigtliumod



!====================================================================
!  Berechnung eines Voigtwertes (tabelliert)
!====================================================================
complex(8) function voigtlookup(deltanue,vhwhminv,voigtnorm,YLM,idtable,drest,fakt)

use globlines23,only : maxvgt_d,maxvgt_nue,voigttable

implicit none

real(8),intent(in) :: deltanue,vhwhminv,voigtnorm,YLM
integer,intent(in) :: idtable
real,intent(in) :: drest,fakt

integer :: ixtable
real :: xtable,xrest,x,xquad,voigtre
complex :: cwert

! Bestimme Position entlang spektraler Abszisse
xtable = abs(20.0d0 * deltanue * vhwhminv)
ixtable = int(xtable) + 1
xrest = xtable - real(ixtable - 1)
if (ixtable .gt. maxvgt_nue - 1) then
   ! extrapolation
   x = deltanue * vhwhminv
   xquad = x * x
   voigtre = (real(voigttable(maxvgt_nue,idtable)) + drest * aimag(voigttable(maxvgt_nue,idtable))) &
     * ( 101.0 / (xquad + 1.0) + fakt * 10000.0 / (xquad * xquad)) / (fakt + 1.0)
else
   ! interpolation
   cwert = (1.0 - xrest) * voigttable(ixtable,idtable) + xrest * voigttable(ixtable+1,idtable)
   voigtre = real(cwert) + drest * aimag(cwert)
end if

voigtlookup = vhwhminv * voigtnorm * cmplx(voigtre,YLM * deltanue * vhwhminv * voigtre)

end function voigtlookup



!====================================================================
!  Warnung rausschreiben und Programm evtl. beenden
!====================================================================
subroutine warnout(text,predec)

implicit none

character(len=*),intent(in) :: text
integer,intent(in) :: predec
integer :: intdec

print *,'Warning:'
print *, trim(text)
print *,'To shutdown program: enter 0, to go on: enter 1.'
read *, intdec
if (intdec .eq. 0 .or. predec .eq. 0) then
    call ppquittung()
    stop
end if

end subroutine warnout



!====================================================================
!  write_hitbin_lbl: write lbl-infos to binary file
!====================================================================
subroutine write_hitbin_lbl()

use globfwd23
use globlines23

implicit none

!character(len=1) :: extension
character(len=2) :: eol
character(len=16) :: numchar,voidchar
integer :: i,j

! end of line 0D+0A ASCI: 13 + 10
eol = char(13) // char(10)
voidchar = ''

open (iunit_fwd,file = trim(wrkpfad) // '/' // lblkennung // '.HIT', status = 'replace', form='unformatted')

! write ASCII-readable header info
! number of MWs and wave number bounds
write (iunit_fwd) 'Binary line data file for PROFFWD'
write (iunit_fwd) eol,eol
write (iunit_fwd) 'number of MWs',eol
write (iunit_fwd) 'For each MW: input spectral boundaries',eol
write (iunit_fwd) '$',eol
numchar = voidchar
write (numchar,'(I3)') n_mw
write (iunit_fwd) trim(numchar),eol
do i = 1,n_mw
    numchar = voidchar
    write (numchar,'(F9.3)') mw(i)%firstnue_input
    write (iunit_fwd) trim(numchar),'  '
    numchar = voidchar
    write (numchar,'(F9.3)') mw(i)%lastnue_input
    write (iunit_fwd) trim(numchar),eol
end do
write (iunit_fwd) eol,eol

write (iunit_fwd) 'number of MWs',eol
write (iunit_fwd) 'For each MW: extd spectral bounds for reading lines',eol
write (iunit_fwd) '$',eol
numchar = voidchar
write (numchar,'(I3)') n_mw
write (iunit_fwd) trim(numchar),eol
do i = 1,n_mw
    numchar = voidchar
    write (numchar,'(F9.3)') mw(i)%firstnuefinext - mwextd_readlines_rim
    write (iunit_fwd) trim(numchar),'  '
    numchar = voidchar
    write (numchar,'(F9.3)') mw(i)%lastnuefinext + mwextd_readlines_rim
    write (iunit_fwd) trim(numchar),eol
end do
write (iunit_fwd) eol,eol

! number of species and HITRAN line data files, line pointer for each species in each MW
write (iunit_fwd) 'number of lbl-species',eol
write (iunit_fwd) 'For each MW: line pointer boundaries',eol
write (iunit_fwd) '$',eol
numchar = voidchar
write (numchar,'(I3)') n_tau_lbl
write (iunit_fwd) trim(numchar),eol
do i = 1,n_tau_lbl
    write (iunit_fwd) trim(hitdatei(i)),eol
    do j = 1,n_mw
        numchar = voidchar
        write (numchar,'(I7)') lines%lb(j,i)
        write (iunit_fwd) trim(numchar),'  '
        numchar = voidchar
        write (numchar,'(I7)') lines%rb(j,i)
        write (iunit_fwd) trim(numchar),eol
    end do
end do
write (iunit_fwd) eol,eol

! entry point to binary file section
write (iunit_fwd) 'Entry point to binary section',eol
write (iunit_fwd) '$',eol
do i = 1,lines%rb(n_mw,n_tau_lbl)
    write (iunit_fwd) lines%species(i),lines%nue(i),lines%kappacm(i),lines%lorwidthf(i),lines%lorwidths(i) &
      ,lines%ergniveau(i),lines%lortdepend(i),lines%pshift(i),lines%gam2(i),lines%gam2tdepend(i) &
      ,lines%beta(i),lines%betatdepend(i),lines%sdgdec(i),lines%YLM(i),lines%LMTK1(i),lines%LMTK2(i)
end do

close (iunit_fwd)

end subroutine write_hitbin_lbl




end module prffwd23_m
