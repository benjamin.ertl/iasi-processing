!     Last change:  BLA  20 Dec 2012    0:05 am

program proffi96

use globlev23
use globfwd23 , only : messdec,hitdatei,deri,calcfwddec,instr,n_chan,npp,inppfad_inv,outpfad_inv,npp
use prffwd23_m
use prof96_m

implicit none

character (len=100),dimension(nmaxtarget) :: kovardatei
logical :: convergdec,dateidadec
integer :: kind_of_action,maxcycles,i,cycles,lcurv,maxlcurv,lspeci,nfincycle
real(8),dimension(:),allocatable :: dcoldvmr
real(8),dimension(:,:),allocatable :: jakobi,kern_raw,kern_ful,gain_raw &
  ,gain_ful,straf,totinvkov,noiskov
real(8),dimension(:,:,:),allocatable :: regfacs
real(8),dimension(:),allocatable :: startparms,regparms,actparms,newparms
real(8),dimension(:),allocatable :: messpec,calcspec,linspec,refweightspec,weightspec
real(8) :: actvari,linvari,stab,lskala,lskalb

! Erste Vorwaertsrechnung
print *,'First run of forward model...'
call initfwd()
call alloc_fwd()
call prffwd(0)
print *,'...forward run finished!'

! Parallel-Batching
iunit_inv = iunitoffset_inv + 2 * (npp - 1)

! Eingabedatei PROFFIT lesen, Eingabe fuer Fehlerrechnung vorhanden, Eingabe fuer Interkorrelationen vorhanden
print *,'Reading general input file retrieval...'
call readinput (trim(inppfad_inv) // '/PROFFIT9.INP',kind_of_action,maxcycles,lspeci,maxlcurv,stab &
  ,lskala,lskalb,kovardatei)

! Delete previous output
call kill_files(outpfad_inv)

! Optionale Inputdateien fuer varifix, ddw-Option, species intercorrelation, error calculation
inquire (file = trim(inppfad_inv) // '/RMS_FIX9.INP',exist = dateidadec)
if (dateidadec) then
    rmsfixdec = .true.
    print*,'Fixed RMS option activated!'
    ! RMS-Wert einlesen und quadrieren
    open (iunit_inv,file = trim(inppfad_inv) // '/RMS_FIX9.INP',status = 'old',action = 'read')
        call gonext(iunit_inv,.false.)
        read (iunit_inv,*) rms_fix
    close (iunit_inv)
else
    rmsfixdec = .false.
end if

inquire (file = trim(inppfad_inv) // '/RMS_DDW9.INP',exist = dateidadec)
if (dateidadec) then
    ddwdec = .true.
    print*,'Dynamic deweighting option activated!'
    ! RMS-Wert einlesen und quadrieren
    open (iunit_inv,file = trim(inppfad_inv) // '/RMS_DDW9.INP',status = 'old',action = 'read')
        call gonext(iunit_inv,.false.)
        read (iunit_inv,*) rms_ddw
    close (iunit_inv)
else
    ddwdec = .false.
end if

inquire (file = trim(inppfad_inv) // '/INTERCV9.INP',exist = dateidadec)
if (dateidadec) then
    globoedec = .true.
    print*,'PROFFIT assumes inter-species correlations!'
    if (maxlcurv .ne. 1) then
        maxlcurv = 1
        call warnout('L-curve + inter-species correlation not supported!')
    end if
else
    globoedec = .false.
end if

! Allgemeiner Konsistenztest
print *,'Performimg consistency check to fwd calc settings...'
call consistent

! Dimensionen spektraler Vektoren sowie voller und reduzierter Parametervektoren festlegen
! Falls Fehlerrechnung: nerrparms festlegen
call initvects

! Gaskennungen von KOPRA einlesen
do i = 1,nvmrspeci
    gaskennung(i) = hitdatei(deri%npoint_fwd(i))
end do
if (tfitdec) gaskennung(ntargetspeci) = 'Temperature'

! Allocierung der Felder, Spektren, Parameter und Hilfsfelder
allocate(messpec(npos_spec(nofmws)),calcspec(npos_spec(nofmws)),linspec(npos_spec(nofmws)) &
  ,refweightspec(npos_spec(nofmws)),weightspec(npos_spec(nofmws)))

! Allocierung von Parametervektoren, ggf. Jakobi fuer Fehlerrechnung allocieren
allocate(jakobi(npos_spec(nofmws),maxfulparms))
allocate(gain_raw(maxredparms,npos_spec(nofmws)),gain_ful(maxfulparms,npos_spec(nofmws)))
allocate(kern_raw(maxredparms,maxredparms),kern_ful(maxfulparms,maxfulparms) &
  ,totinvkov(maxredparms,maxredparms),noiskov(maxfulparms,maxfulparms))
allocate(regfacs(2,ntargetspeci,noflayers))
allocate(startparms(maxfulparms),regparms(maxfulparms),actparms(maxfulparms) &
  ,newparms(maxfulparms))
allocate(dcoldvmr(noflayers))
allocate(straf(2,ntargetspeci))

messpec = 0.0d0
calcspec = 0.0d0
linspec = 0.0d0
weightspec = 0.0d0
refweightspec = 0.0d0
jakobi = 0.0d0
kern_raw = 0.0d0
kern_ful = 0.0d0
gain_raw = 0.0d0
gain_ful = 0.0d0
totinvkov = 0.0d0
regfacs = 0.0d0
startparms = 0.0d0
regparms = 0.0d0
actparms = 0.0d0
newparms = 0.0d0
noiskov = 0.0d0
straf = 0.0d0

! Ggf berechnetes Spektrum einlesen
if (messdec) then
    messpec = messxils
    ! Mittleres Signal in allem MWs berechnen (f�r Fehlerrechnung: Umskalierung rel. Offset)
    meanmess_allmws = 0.0d0
    do i = 1,npos_spec(nofmws)
        meanmess_allmws = meanmess_allmws + messpec(i)
    end do
    meanmess_allmws = meanmess_allmws / real(npos_spec(nofmws),8)
end if

! Reg-parameter, Reg-faktoren, coldecs einlesen und initialisieren
call loadregparms(trim(inppfad_inv) // '/REGPARM9.INP',trim(inppfad_inv) &
  // '/REGFACS9.INP',trim(inppfad_inv) // '/REGPDF9.INP',regparms,regfacs)
print *,'regparms + regfacs have been loaded.'

! Initialisierung der inversen Kovarianzmatrix
call initkovars(trim(inppfad_inv),'/INTERCV9.INP',kovardatei,regparms,regfacs,totinvkov)
print *,'covariance matrices are initialized.'

! Initialisierung der Gewichtung der spektralen Stuetzstellen (ohne dynamischen Anteil)
call initweights(refweightspec)
weightspec = refweightspec

! Skalierung der Regularisierungsprofile entsprechend der Angaben in reg(3,i)
do i = 1,ntargetspeci
    regparms(mpos_ful(i):npos_ful(i)) = reg(3,i) * regparms(mpos_ful(i):npos_ful(i))
end do

select case (kind_of_action)

    case (1)
        ! Reine KOPRA-Vorwaertsrechnung (ist bereits durchgef�hrt), Ergebnisse rausschreiben
        calcspec = finspecxils
        call write_spec(outpfad_inv,calcspec)
        if (messdec) call write_specs(outpfad_inv,messpec,calcspec)
!csv        if (messdec) call write_specs(outpfad_inv,messpec,calcspec,weightspec)
        print *,'Results of fwd calculation written to file.'

    case (2)

        ! Retrieval

        ! Besetzung von Parametern, Jakobimatrix und Spektrum gemaess
        ! Ergebnissen der 1. Vorwaertsrechnung
        call get_parms(startparms)
        actparms = startparms
        call get_jakobi(jakobi,calcspec)

        ! Berechnung des aktuellen rms-Wertes
        call stat1d_var(messpec,calcspec,weightspec,npos_spec(nofmws),actvari)
        print *,'rms: ',sqrt(actvari)

        call write_acts(outpfad_inv,messpec,calcspec,linspec,actparms &
          ,actvari,actvari,straf,0)


        ! Schleife (Berechnung L-Kurve)
        do lcurv = 1,maxlcurv
            ! Schleife (linearisierte Inversion)
            do cycles = 1,maxcycles

                ! linearisierte Inversion
                print *,'inverting...',cycles
                call invers(messpec,calcspec,refweightspec,totinvkov,regparms &
                  ,startparms,actparms,actvari,stab,jakobi,newparms,weightspec,linspec)

                ! Ggf Shift-Schritt begrenzen, oder Abbruch (mit oder ohne Meldung)
                if (shiftfitdec) call check_shift(actparms,newparms)

                call parms_to_screen(actparms,newparms)

                call put_parms(newparms)


                ! verbesserte Vorwaertsrechnung
                print *,'new forward calculation...'
                call prffwd(cycles)

                ! Jakobimatrix und Spektrum aktualisieren
                call get_jakobi(jakobi,calcspec)

                ! Varianz bestimmen
                call stat1d_var(messpec,calcspec,weightspec,npos_spec(nofmws),actvari)
                call stat1d_var(messpec,linspec,weightspec,npos_spec(nofmws),linvari)
                call calc_straf(newparms,regparms,totinvkov,actvari,straf)
                print *,'rms: ',sqrt(actvari),sqrt(linvari)
                call write_acts(outpfad_inv,messpec,calcspec,linspec,newparms &
                  ,actvari,linvari,straf,cycles)

                ! Naechste Iteration oder Abbruch, wenn Konvergenzkriterium erf�llt
                ! Neue Parameter uebernehmen
                if (convergdec(jakobi,messpec,weightspec,actparms,newparms)) then
                    print *,'convergence reached!'
                    nfincycle = cycles
                    actparms = newparms
                    exit
                else
                    actparms = newparms
                end if
            end do

            ! Regularisierung vermindern
            if (maxlcurv .gt. 1 .and. lcurv .lt. maxlcurv) then
                reg(1,lspeci) = lskala * reg(1,lspeci)
                reg(2,lspeci) = lskalb * reg(2,lspeci)

                ! Initialisierung der inversen Kovarianzmatrix
                call initkovars(trim(inppfad_inv),'/INTERCV9.INP',kovardatei,regparms,regfacs,totinvkov)
                print *,'covariance matrices are re-initialized.'

            end if
            call write_lcurv(outpfad_inv,actvari,straf(1,lspeci))

        end do

        ! Bestimmung des Teilchenzahlprofiles
        dcoldvmr(1:noflayers) = 1.0d-6 * lev%colair(1:noflayers)

        ! Averaging kernels + Gainmatrix bestimmen und rausschreiben
        print*,'Calculating AKs and noise error bars...'
        call vertres(jakobi,weightspec,regparms,startparms,actparms,totinvkov,actvari,kern_raw,kern_ful,gain_raw,gain_ful)
        print *,'Writing AKs (+ gain matrix) to file...'
        call write_kernels(outpfad_inv,kern_raw,kern_ful)
        if (gainoutdec) then
            call write_gain(outpfad_inv,gain_raw,gain_ful)
        end if

        ! Rauschkovarianz : Konstruktion der vollen maxfulparms * maxfulparms Kov-Matrix
        noiskov = actvari * matmul(gain_ful,transpose(gain_ful))

        ! Fitqualitaet in jedem MW
        do i = 1,nofmws
            call stat1d_var(messpec(mpos_spec(i):npos_spec(i)),calcspec(mpos_spec(i):npos_spec(i)) &
              ,weightspec(mpos_spec(i):npos_spec(i)),ptsinmw(i),vari_mw(i))
        end do

        ! Ergebnisse rausschreiben
        if (jakoutdec) then
            call write_jakobi(outpfad_inv,jakobi,weightspec,regparms,startparms,actparms,totinvkov,actvari,stab)
            call write_kovar(outpfad_inv,totinvkov)
        end if
        call write_parms(outpfad_inv,lev%h_m(1:noflayers),lev%colair_do,lev%colair_up,lev%T_K(1:noflayers) &
          ,lev%p_pa(1:noflayers),lev%n_cbm(1:noflayers),startparms,regparms,actparms,noiskov,nfincycle,maxcycles)
        call write_specs(outpfad_inv,messpec,calcspec,weightspec)

end select

! Deallocation der Felder in Vorwaertsrechnung
print *,'Deallocating arrays in forward model...'
call dealloc_fwd()

! Deallokation der Felder
print *,'Deallocating arrays in retrieval...'
deallocate(straf)
deallocate(dcoldvmr)
deallocate(regfacs)
deallocate(startparms,regparms,actparms,newparms)
deallocate(totinvkov,noiskov)
deallocate(kern_raw,kern_ful)
deallocate(gain_raw,gain_ful)
deallocate(jakobi)
deallocate(messpec,calcspec,linspec,refweightspec,weightspec)

! PP: Quittiere Beendigung des Programmes
call ppquittung()

end program proffi96




!====================================================================
!  addiert die Inhomogenitaet zum Loesungsvektor der Inversion
!  (vec und regparms sind reduzierte Vektoren)
!====================================================================
subroutine addinhom(redparmsin,redregparms,redstabparms,bdiag,totinvkov,actvari,stab,redparmsout)

use prof96_m

implicit none

real(8),dimension(maxredparms),intent(in) :: redparmsin,redregparms,redstabparms,bdiag
real(8),dimension(maxredparms,maxredparms),intent(in) :: totinvkov
real(8),intent(in) :: actvari,stab

real(8),dimension(maxredparms),intent(out) :: redparmsout

integer i

real(8) :: variwert

! Bestimmung der fuer Reg. anzuwendenden Varianz
if (rmsfixdec) then
    variwert = rms_fix * rms_fix
else
    variwert = actvari
end if

! Mit der Regularisierung verbundene Inhomogenitaet
redparmsout = redparmsin + matmul(variwert * totinvkov,redregparms)

! Mit der L-M Stabilisierung verbundene Inhomogenitaet
do i = 1,maxredparms
    redparmsout(i) = redparmsout(i) + variwert * stab * bdiag(i) * redstabparms(i)
end do

end subroutine addinhom



!====================================================================
!  Berechnung der Strafterme
!====================================================================
subroutine calc_straf(fulparms,fulregparms,totinvkov,actvari,straf)

use prof96_m

implicit none

real(8),dimension(maxfulparms),intent(in) :: fulparms,fulregparms
real(8),dimension(maxredparms,maxredparms),intent(in) :: totinvkov
real(8),intent(in) :: actvari
real(8),dimension(2,ntargetspeci),intent(out) :: straf

integer :: mpos,npos,i
real(8) :: variwert
real(8),dimension(maxredparms) :: redparms,redregparms,wrkredparms

call trafo_tored_parms(fulregparms,fulparms,redparms)
call trafo_tored_parms(fulregparms,fulregparms,redregparms)

! Bestimmung der fuer Reg. anzuwendenden Varianz
if (rmsfixdec) then
    variwert = rms_fix * rms_fix
else
    variwert = actvari
end if

! Im Falle globaler Kovarianz nur Gesamtstrafterm, sonst pro Spezies
straf = 0.0d0
wrkredparms = redparms - redregparms
if (globoedec) then
    straf(1,1) = variwert * dot_product(wrkredparms,matmul(totinvkov,wrkredparms))
else
    do i = 1,ntargetspeci
        if (method(i) .ne. 0) then
            mpos = mpos_red(i)
            npos = npos_red(i)
            straf(1,i) = variwert * dot_product(wrkredparms(mpos:npos) &
              ,matmul(totinvkov(mpos:npos,mpos:npos),wrkredparms(mpos:npos)))
        end if
    end do
end if

end subroutine calc_straf



!====================================================================
!  Shift exception handling
!====================================================================
subroutine check_shift(actfulparms,newfulparms)

use prffwd23_m, only : ppquittung
use globfwd23
use prof96_m

implicit none

real(8),dimension(maxfulparms),intent(in) :: actfulparms
real(8),dimension(maxfulparms),intent(inout) :: newfulparms

logical :: testdec
integer :: ioffset, imw
real(8) :: threshold


! determine offset position for shift values in state vector
! (ipos is position of last non-shift entry in full state vector)

! VMR (and T)
if (tfitdec) then
    ioffset = ntargetspeci * noflayers + 1 !VMRs, T und Tgnd
else
    ioffset = nvmrspeci * noflayers !VMRs
end if

! ILS parms
if (ilsmodfitdec) ioffset = ioffset + 1
if (ilsphasfitdec) ioffset = ioffset + 1

! channeling
if (chanfitdec) then
    ioffset = ioffset + nofmws * nofchans
end if

if (limitshiftdec) then
    ! check whether shift change between new and previous solution larger than threshold
    do imw = 1,nofmws
        threshold = 0.5d0 * limitshift / ( instr%opdmax * mw(imw)%centernue)
        if (abs(newfulparms(ioffset+imw) - actfulparms(ioffset+imw)) .gt. threshold) then
            newfulparms(ioffset+imw) = actfulparms(ioffset+imw) &
            + sign(1.0d0,newfulparms(ioffset+imw) - actfulparms(ioffset+imw)) * threshold
        end if
    end do
end if

! check whether any shift value out of allowed range (stop with message or just break execution)
testdec = .false.
do imw = 1,nofmws
    if (abs(newfulparms(ioffset + imw) * mw(imw)%centernue) .gt. wvskal%maxshift) then
        testdec = .true.
        print *,'new shift violates boundary in MW: ',imw
    end if
end do

if (testdec) then
    call warnout('shift too big!')
end if

end subroutine check_shift



!====================================================================
!  Konsistenztest
!====================================================================
subroutine consistent ()

use globfwd23 , only : deri,n_chan,n_mw
use prof96_m

implicit none

integer :: i,flag

flag = -1

if (nvmrspeci .ge. 1 .and. .not. deri%sw_vmr) then
    print*,'VMR deriv: inconsistent settings!'
    flag = 0
end if
if (tfitdec .and. .not.(deri%sw_T)) then
    print*,'T deriv: inconsistent settings!'
    flag = 1
end if
if (shiftfitdec .and. .not. deri%sw_shift) then
    print*,'Shift deriv: inconsistent settings!'
    flag = 4
end if
if (chanfitdec .and. .not. deri%sw_chan) then
    print*,'Channeling deriv: inconsistent settings!'
    flag = 5
end if
if (ilsmodfitdec .and. .not. deri%sw_ILSapo) then
    print *,'ILS mod deriv: inconsistent settings!'
    flag = 6
end if
if (ilsphasfitdec .and. .not. deri%sw_ILSphas) then
    print *,'ILS phas deriv: inconsistent settings!'
    flag = 7
end if

if (tfitdec) then
    if (method(ntargetspeci) .eq. 0) then
         print*,'T-profile: scaling fit not allowed!'
         flag = 8
    end if
end if

if (nvmrspeci .ge. 1) then
    if (nvmrspeci .ne. deri%n_vmr) then
        print *,'nvmrspeci inconsistent!'
        flag = 9
    end if
    do i = 1,nvmrspeci
        if (method(i) .eq. 0 .and. logdec(i)) then
            print*,'Log + scaling combi not allowed!'
            flag = 10
        end if
    end do
end if

if (nofmws .ne. n_mw) then
    print*,'Number of MWs inconsistent!'
    flag = 11
end if

! Ergebnis des Konsistenztests auswerten
if (flag .ne. -1) then
    print *,'fwd-calculation / retrieval - settings:',flag
    call warnout('Consistency check failed! ')
end if

end subroutine consistent



!====================================================================
!  Pruefen, ob Konvergenz erreicht
!====================================================================
function convergdec(jakobi,messpec,weightspec,actparms,newparms)

use prof96_m

implicit none

real(8),dimension(npos_spec(nofmws),maxfulparms),intent(in) :: jakobi
real(8),dimension(npos_spec(nofmws)),intent(in) :: messpec,weightspec
real(8),dimension(maxfulparms),intent(in) :: actparms,newparms

integer :: i,j
logical :: convergdec
real(8),dimension(npos_spec(nofmws)) :: tot_deri
real(8) :: mean_in_mw,ratio,newratio

! Absolute Aenderung des Spektrums durch Aenderung der Parameter
tot_deri = 0.0d0
do i = 1,maxfulparms
    do j = 1,npos_spec(nofmws)
        tot_deri(j) = tot_deri(j) + abs(jakobi(j,i)&
          * (actparms(i) - newparms(i))) * weightspec(i)
    end do
end do

ratio = 0.0d0
! Maximale relative Aenderung suchen (bez. MW in jedem MW)
do i = 1,nofmws
    call stat1d_mean(messpec(mpos_spec(i):npos_spec(i)) &
      ,weightspec(mpos_spec(i):npos_spec(i)),ptsinmw(i),mean_in_mw)
    do j = 1,ptsinmw(i)
        newratio = tot_deri(mpos_spec(i)+j-1) / mean_in_mw
        if (newratio .gt. ratio) ratio = newratio
    end do
end do

if (ratio .lt. conv_scal) then
    convergdec = .true.
else
    convergdec = .false.
end if

end function convergdec




!====================================================================
! EigenVDecomp       - Bestimmt EWs + EVs fuer symm. pos. semidef. Matrizen
!                      Ausgabe: Anzahl signifikanter EWs, EWs, zugehoerige EVs
!                      (spaltenweise in Matrix)
!====================================================================

subroutine EigenVDecomp(matrix,nmax,nevals,Ew,Ev)

implicit none

integer,intent(in) :: nmax
real(8),dimension(nmax,nmax),intent(in) :: matrix
integer,intent(out) :: nevals
real(8),dimension(nmax),intent(out) :: Ew
real(8),dimension(nmax,nmax),intent(out) :: Ev

logical :: iterdec
integer :: i,j,k,l,imax,nzaehler
real(8),dimension(nmax) :: testvec,newvec
real(8),dimension(nmax,nmax) :: wrkmat
real(8) :: wert,norm,maxnorm,chkwert,chkwertneu,abbruch

! Eigenwerte auf Null setzen
Ew = 0.0d0
! Eigenvektoren auf Einheitsmatrix setzen
Ev = 0.0d0
do i = 1,nmax
    Ev(i,i) = 1.0d0
end do


! Check der Eingabematrix ==================================
! Diagonale positiv? Hinreichend symmetrisch?
do i = 1,nmax
    if (matrix(i,i) .lt. 0.0d0) THEN
      call warnout('Abbruch in EigenVDecomp: Matrix nicht pos. semidefinit!')
      return
    end if
    do j = 1,nmax
      if (abs(matrix(i,j) - matrix(j,i)) .gt. &
        1.0d-8 * sqrt(matrix(i,i) * matrix(i,i) + matrix(j,j) * matrix(j,j))) THEN
          call warnout('Abbruch in EigenVDecomp: Matrix nicht symmetrisch!')
          return
      end if
    end do
end do

! Eingabematrix kopieren (und symmetrisieren)
wrkmat = 0.5d0 * (matrix + transpose(matrix))

! Alle EVs abarbeiten================================================
nevals = nmax
do i = 1,nmax
    ! Wenn bereits EV gefunden: Matrix bearbeiten (Hotelling's deflation)
    if (i .gt. 1) then
        ! Hotelling
        do j = 1,nmax
            wrkmat(j,j) = wrkmat(j,j) - Ew(i-1) * Ev(j,i-1) * Ev(j,i-1)
            do k = j + 1,nmax
                wrkmat(j,k) = wrkmat(j,k) - Ew(i-1) * Ev(j,i-1) * Ev(k,i-1)
                wrkmat(k,j) = wrkmat(j,k)
            end do
        end do
        ! Orthogonalisierung der verbleibenden EVs
        do j = i,nmax
            do k = j - 1,1,-1
                ! Skalarprodukt
                wert = dot_product(Ev(:,j),Ev(:,k))
                ! Kollinearen Anteil abziehen
                do l = 1,nmax
                    Ev(l,j) = Ev(l,j) - wert * Ev(l,k)
                end do
            end do
            ! Vektor j erneut normieren
            norm = sqrt(dot_product(Ev(:,j),Ev(:,j)))
            Ev = Ev / norm
        end do
    end if
    ! Rekordhalter unter verbliebenen Vektoren suchen
    maxnorm = 0.0d0
    do j = i,nmax
        ! Testvektor besetzen
        testvec = Ev(:,j)
        ! Matrixmultiplikation
        norm = 0.0d0
        do k = 1,nmax
            wert = 0.0d0
            do l = 1,nmax
                wert = wert + wrkmat(k,l) * testvec(l)
            end do
            norm = norm + wert * wert
        end do
        if (norm .gt. maxnorm) then
            imax = j
            maxnorm = norm
        end if
    end do
    ! Nachfolgend groessten EV iterieren
    ! Testvektor besetzen, EVs umordnen: Rekordhalter auf Platz i
    testvec = Ev(:,imax)
    Ev(:,imax) = Ev(:,i)
    Ev(:,i) = testvec(:)
    ! Wiederholte Matrixmultiplikation und Orthogonalisierung
    iterdec = .true.
    nzaehler = 0
    chkwert = real(2 * nmax,8)
    do while (iterdec)
        nzaehler = nzaehler + 1
        ! Matrixmultiplikation
        newvec = matmul(wrkmat,testvec)
        ! Norm bestimmen
        norm = sqrt(dot_product(newvec,newvec))
        ! Neuen Vektor normieren und in testvec kopieren, zuvor Abbruchkriterium testen
        chkwertneu = 0.0d0
        do j = 1,nmax
            chkwertneu = chkwertneu + abs(testvec(j) - newvec(j) / norm)
        end do
        testvec = newvec / norm

        ! Abbruch fuer EV Nr. i erreicht?
        if (chkwertneu .lt. 1.0d-10 .and. chkwertneu .ge. chkwert) then
            iterdec = .false.
        else
            chkwert = chkwertneu
        end if
        ! Beenden, falls keine Konvergenz
        if (nzaehler .gt. 50000) then
            print *,'EV',i
            call warnout ('Abbruch in EigenVDecomp: Keine Konvergenz!')
            return
        end if
    end do

    ! Ersten Eigenwert uebernehmen oder ggf aktuellen EW uebernehmen
    if (i .eq. 1) then
        Ew(1) = norm
        Ev(:,1) = testvec
    else
        ! Kleinster signifikanter EW gefunden?
        ! Ueberpruefe Orthogonalitaet zu bisher aufgefundenen EWs
        abbruch = 0.0d0
        do j = 1,i - 1 ! alle bisher aufgefundenen EVs
            wert = 0.0d0
            wert = dot_product(Ev(:,j),testvec)
            abbruch = abbruch + wert * wert
        end do
        if (abbruch .lt. 1.0d-12) then
            Ew(i) = norm
            Ev(:,i) = testvec
        else
            nevals = i - 1
            ! Schleife (Index i ueber EVs) verlassen
            exit
        end if
    end if

  end do

end subroutine EigenVDecomp



!====================================================================
!  Jakobimatrix und synthetisches Spektrum bereitstellen
!====================================================================
subroutine get_jakobi(jakobi,calcspec)

use globfwd23 , only : mw
use prffwd23_m
use prof96_m

implicit none

real(8),dimension(npos_spec(nofmws),maxfulparms),intent(out) :: jakobi
real(8),dimension(npos_spec(nofmws)),intent(out) :: calcspec

logical :: anydeczero
integer :: i,imw,ispeci,iparms,mpos


!********** Spektrum eintragen
calcspec = finspecxils

!********** Jakobimatrix eintragen
jakobi = 0.0d0

! Ableitungen nach vmr f�r alle Species
do ispeci = 1,nvmrspeci
    ! Teste, ob Teile der Jakbi fuer aktuelle Spezies null zu setzen sind
    anydeczero = .false.
    do imw = 1,nofmws
        if (deczerojakobi(imw,ispeci)) anydeczero = .true.
    end do
    if (anydeczero) then
        do iparms = 1,noflayers
            do imw = 1,nofmws
                if (deczerojakobi(imw,ispeci)) then
                    jakobi(mpos_spec(imw):npos_spec(imw),mpos_ful(ispeci)+iparms-1) &
                      = 0.0d0
                else
                    jakobi(mpos_spec(imw):npos_spec(imw),mpos_ful(ispeci)+iparms-1) &
                      = dspec_dvmr(mpos_spec(imw):npos_spec(imw),iparms,ispeci)
                end if
            end do
        end do
    else
        do iparms = 1,noflayers
            jakobi(1:npos_spec(nofmws),mpos_ful(ispeci)+iparms-1) &
              = dspec_dvmr(1:npos_spec(nofmws),iparms,ispeci)
        end do
    end if
end do

mpos = npos_ful(nvmrspeci)

! ggf. Ableitungen nach T
if (tfitdec) then
    do iparms = 1,noflayers
        jakobi(1:npos_spec(nofmws),mpos_ful(ntargetspeci)+iparms-1) = dspec_dT(1:npos_spec(nofmws),iparms)
    end do
    jakobi(1:npos_spec(nofmws),npos_ful(ntargetspeci)+1) = dspec_dTgnd(1:npos_spec(nofmws))
    mpos = mpos + noflayers + 1
end if

! ggf. ILS mod  / phas
if (ilsmodfitdec) then
    mpos = mpos + 1
    jakobi(1:npos_spec(nofmws),mpos) = dspec_dILS(1:npos_spec(nofmws),1)
end if
if (ilsphasfitdec) then
    mpos = mpos + 1
    jakobi(1:npos_spec(nofmws),mpos) = dspec_dILS(1:npos_spec(nofmws),2)
end if

! ggf. Ableitungen Channeling eintragen
if (chanfitdec) then
    do imw = 1,nofmws
        do i = 1,nofchans
            mpos = mpos + 1
            ! cos-Anteil
            jakobi(mpos_spec(imw):npos_spec(imw),mpos) = dspec_dchan(mpos_spec(imw):npos_spec(imw),1,i)
            mpos = mpos + 1
            ! sin-Anteil
            jakobi(mpos_spec(imw):npos_spec(imw),mpos) = dspec_dchan(mpos_spec(imw):npos_spec(imw),2,i)
        end do
    end do
end if

! ggf. Ableitungen nach Shift
if (shiftfitdec) then
    do imw = 1,nofmws
        mpos = mpos + 1
        jakobi(mpos_spec(imw):npos_spec(imw),mpos) = dspec_dshift(mpos_spec(imw):npos_spec(imw))
    end do
end if

end subroutine get_jakobi



!====================================================================
!  Einlesen der Parameter von KOPRA
!====================================================================
subroutine get_parms(actparms)

use globfwd23
use globlev23
use prof96_m

implicit none

real(8),dimension(maxfulparms),intent(out) :: actparms

integer :: i,imw,ispeci,mpos

! vmr
do ispeci = 1,nvmrspeci
    actparms(mpos_ful(ispeci):npos_ful(ispeci)) = &
        lev%vmr_ppmv(1:noflayers,deri%npoint_fwd(ispeci))
end do
! ggf. T
if (tfitdec) then
    actparms(mpos_ful(ntargetspeci):npos_ful(ntargetspeci)) = lev%T_K(1:noflayers)
end if

mpos = npos_ful(ntargetspeci)

! ggf. Tgnd
if (tfitdec) then
   mpos = mpos + 1
   actparms(mpos) = obs%Tgnd_K
end if

! ggf. ILS mod / phase
if (ilsmodfitdec) then
    mpos = mpos + 1
    actparms(mpos) = instr%apo  !/ ilsunit
end if
if (ilsphasfitdec) then
    mpos = mpos + 1
    actparms(mpos) = instr%phas  !/ ilsunit
end if

! ggf. Channeling
if (chanfitdec) then
    do imw = 1,nofmws
        do i = 1,nofchans
            mpos = mpos + 1
            actparms(mpos) = real(instr%chanamp(i,imw),8) !/ chanunit
            mpos = mpos + 1
            actparms(mpos) = aimag(instr%chanamp(i,imw)) !/ chanunit
        end do
    end do
end if

! ggf. Shift
if (shiftfitdec) then
    do imw = 1,nofmws
        mpos = mpos + 1
        actparms(mpos) = mw(imw)%shift !/ shiftunit
    end do
end if

end subroutine get_parms



!====================================================================
!  gonext: Einlesen bis zum naechsten $ Zeichen
!====================================================================
subroutine gonext(ifile,bindec)

implicit none

integer,intent(in) :: ifile
logical,intent(in) :: bindec

character(1) :: nextchar

nextchar='x'
do while (nextchar /= '$')
    if (bindec) then
        read(ifile) nextchar
    else
        read(ifile,'(A1)') nextchar
    end if

end do

end subroutine gonext



!====================================================================
!  initkovar: Initialisiert Kovarianzmatrizen
!====================================================================
subroutine initkovars(inppfad_inv,intercvdatei,kovardatei,regparms,regfacs,totinvkov)

use globfwd23 , only : mw
use prof96_m

implicit none

character(len=*),intent(in) :: inppfad_inv,intercvdatei
character(len=*),dimension(nmaxtarget),intent(in) :: kovardatei
real(8),dimension(maxfulparms),intent(in) :: regparms
real(8),dimension(2,ntargetspeci,noflayers),intent(in) :: regfacs

real(8),dimension(maxredparms,maxredparms),intent(out) :: totinvkov

character(len=100) :: datei
logical :: dateidadec,kovinvdec
integer :: i,j,k,l,maxinter,iinter,mpos
real(8) :: reginter
real(8),dimension(noflayers,noflayers) :: kovarrest
real(8),dimension(noflayers) :: kovardiag
real(8),dimension(maxfulparms) :: wrkparms
real(8),dimension(noflayers,noflayers) :: wrkkovarin,wrkkovar
real(8),dimension(2*noflayers,2*noflayers) :: intercov

totinvkov = 0.0d0

! Initialisierung der Kov.-matrizen je nach Methode
do i = 1,ntargetspeci
    select case (method(i))

        case (1) ! TP

            ! Regularisierung gegen Steigung und Werte
            totinvkov(mpos_red(i),mpos_red(i)) = reg(2,i) * regfacs(2,i,1) + reg(1,i) * regfacs(1,i,1)
            totinvkov(mpos_red(i),mpos_red(i)+1) = - reg(2,i) * regfacs(2,i,1)
            do j = 2,noflayers-1
                  totinvkov(mpos_red(i)+j-1,mpos_red(i)+j-1) = &
                    reg(2,i) * (regfacs(2,i,j-1) + regfacs(2,i,j)) + reg(1,i) * regfacs(1,i,j)
                  totinvkov(mpos_red(i)+j-1,mpos_red(i)+j-2) = - reg(2,i) * regfacs(2,i,j-1)
                   totinvkov(mpos_red(i)+j-1,mpos_red(i)+j) = - reg(2,i) * regfacs(2,i,j)
            end do
            totinvkov(mpos_red(i)+noflayers-1,mpos_red(i)+noflayers-1) = &
              reg(2,i) * regfacs(2,i,noflayers-1) + reg(1,i) * regfacs(1,i,noflayers)
            totinvkov(mpos_red(i)+noflayers-1,mpos_red(i)+noflayers-2) =  - reg(2,i) * regfacs(2,i,noflayers-1)

        case (2) ! OE

            ! Kovarianzdatei einlesen
            inquire (file = inppfad_inv // '/' // kovardatei(i),exist = dateidadec)
            if (.not. dateidadec) then
                call warnout('initkovars: covariance i,i file not found!')
            end if

            open (iunit_inv,file = inppfad_inv // '/' // kovardatei(i),status = 'old',action = 'read')
            call gonext(iunit_inv,.false.)
            read (iunit_inv,*) kovinvdec
            do j = 1,noflayers
                read (iunit_inv,*) (wrkkovarin(j,k),k = 1,noflayers)
            end do
            close (iunit_inv)

            ! Inversion der Matrix, wenn nicht bereits in Eingabefile invertiert
            if (kovinvdec) then
                print *,'inverting i,i covariance matrix...',i
                call matinvers(wrkkovarin,wrkkovar,noflayers)
            else
                wrkkovar = wrkkovarin
            end if

            ! Beruecksichtigung der Reg-Faktoren

            ! Aufteilung diagonaler Anteil und Glaettungsanteil
            ! Bestimmung diagonaler Anteil
            do j = 1,noflayers
                kovardiag(j) = 0.0d0
                do k = 1,noflayers
                    kovardiag(j) = kovardiag(j) + wrkkovar(j,k)
                end do
            end do
            ! Bestimmung Glaettungsanteil
            kovarrest(:,:) = wrkkovar
            do j = 1,noflayers
                kovarrest(j,j) = kovarrest(j,j) - kovardiag(j)
            end do

            ! Eintrag in Gesamt-Kovmatrix
            totinvkov(mpos_red(i):npos_red(i),mpos_red(i):npos_red(i)) = reg(2,i) * kovarrest
            do j = 1,noflayers
                totinvkov(mpos_red(i)+j-1,mpos_red(i)+j-1) = totinvkov(mpos_red(i)+j-1,mpos_red(i)+j-1) &
                  + reg(1,i) * kovardiag(j)
            end do


    end select
end do

! Ggf Einlesen der Interkorrelationen (und Konsistenz der Eingabeparameter)
if (globoedec) then
    open (iunit_inv,file = inppfad_inv // '/' // intercvdatei,status = 'old',action = 'read')
    do i = 1,ntargetspeci
        call gonext(iunit_inv,.false.)
        read (iunit_inv,*) maxinter
        do j = 1,maxinter
            read (iunit_inv,*) iinter, datei
            inquire (file = inppfad_inv // '/' // datei,exist = dateidadec)
            if (.not. dateidadec) then
                call warnout('initkovars: covariance i,j file not found!')
            end if
            ! Konsistenzchecks
            if (iinter .le. i .or. iinter .gt. nvmrspeci) then
                call warnout('initkovars: invalid intercorr indices!')
            end if
            if (method(iinter) .eq. 0 .or. method(i) .eq. 0) then
                call warnout('initkovars: intercorr requires OE/TP in both indices!')
            end if
            if (logdec(iinter) .and. .not. logdec(i) .or. logdec(i) .and. .not. logdec(iinter)) then
                call warnout('initkovars: Mixing of log/lin method in intercorr!')
            end if

            open (iunit_inv+1,file = inppfad_inv // '/' // datei,status = 'old',action = 'read')
                call gonext(iunit_inv+1,.false.)
                read (iunit_inv+1,*) reginter
                call gonext(iunit_inv+1,.false.)
                do k = 1,2 * noflayers
                    read (iunit_inv+1,*) (intercov(k,l),l = 1,2 * noflayers)
                end do
            close(iunit_inv+1)

            ! Zuaddieren zu totinvkov, Regfaktor beachten

            ! Element i,i
            do k = 1,noflayers
                do l = 1,noflayers
                    totinvkov(mpos_red(i)+k-1,mpos_red(i)+l-1) = totinvkov(mpos_red(i)+k-1,mpos_red(i)+l-1) &
                      + reginter * intercov(k,l)
                end do
            end do
            ! Element iinter,iinter
            do k = 1,noflayers
                do l = 1,noflayers
                    totinvkov(mpos_red(iinter)+k-1,mpos_red(iinter)+l-1) = &
                      totinvkov(mpos_red(iinter)+k-1,mpos_red(iinter)+l-1) &
                      + reginter * intercov(noflayers+k,noflayers+l)
                end do
            end do

            ! Elemente i,iinter
            do k = 1,noflayers
                do l = 1,noflayers
                    totinvkov(mpos_red(i)+k-1,mpos_red(iinter)+l-1) = &
                      totinvkov(mpos_red(i)+k-1,mpos_red(iinter)+l-1) &
                      + reginter * intercov(k,noflayers+l)
                end do
            end do

            ! Elemente iinter,i
            do k = 1,noflayers
                do l = 1,noflayers
                    totinvkov(mpos_red(iinter)+k-1,mpos_red(i)+l-1) = &
                      totinvkov(mpos_red(iinter)+k-1,mpos_red(i)+l-1) &
                      + reginter * intercov(noflayers+k,l)
                end do
            end do

        end do
    end do
    close (iunit_inv)
end if

! Uebergang von relativen auf absolute Kovarianzen (nicht fuer T)
if (tfitdec) then
    wrkparms(1:npos_ful(nvmrspeci)) = regparms(1:npos_ful(nvmrspeci))
    wrkparms(mpos_ful(ntargetspeci):npos_ful(ntargetspeci)) = 1.0d0
else
    wrkparms = regparms
end if

do i = 1,ntargetspeci
    if (.not. logdec(i)) then ! nur, wenn kein log-Retrieval
        do j = 1,ntargetspeci
            if (method(i) .ne. 0 .and. method(j) .ne. 0) then ! nur, wenn keine Skalierung
                do k = 1,noflayers
                    do l = 1,noflayers
                        totinvkov(mpos_red(i)+k-1,mpos_red(j)+l-1) = &
                          totinvkov(mpos_red(i)+k-1,mpos_red(j)+l-1) / &
                          (wrkparms(mpos_ful(i)+k-1) * wrkparms(mpos_ful(j)+l-1))
                    end do
                end do
            end if
        end do
    end if
end do

! Zeiger auf erste Position hinter Abschnitt, der zu Spezieseintrag korrespondiert, fahren (VMR oder T-Wert)
if (tfitdec) then
    mpos = npos_red(ntargetspeci) + 2 ! beachte Tgnd
else
    mpos = npos_red(nvmrspeci) + 1
end if

! Channeling (keine Regularisierung)
if (chanfitdec) then
    mpos = mpos + 2 * nofmws * nofchans
end if

! Regularisierung von shift und solar shift
if (shiftfitdec) then
    totinvkov(mpos:mpos+nofmws-1,mpos:mpos+nofmws-1) = -regshift
    do j = mpos,mpos + nofmws - 1
        totinvkov(j,j) = real(nofmws - 1,8) * regshift
    end do
    mpos = mpos + nofmws
end if

end subroutine initkovars



!====================================================================
! Initialisierung der Dimensionen aller Vektoren
!====================================================================
subroutine initvects()

use globfwd23
use prof96_m

implicit none

integer :: i


! Spektrengrenzen und Spektrengrid festlegen
dwvnr = wvskal%dnue
do i = 1,nofmws
    ptsinmw(i) = mw(i)%pts
    firstwvnr(i) = mw(i)%firstnue
end do

! Positionen im spektralen Vektor festlegen
call set_pos_inlvec

! Parameter festlegen und Konsistenztest
noflayers = n_lev
if (noflayers .gt. nmaxlayers) call warnout('Zahl der Schichten zu gross!')

if (tfitdec) then
    maxfulparms = ntargetspeci * noflayers + 1 !VMRs, T und Tgnd
else
    maxfulparms = nvmrspeci * noflayers !VMRs
end if

! ILS
if (ilsmodfitdec) maxfulparms = maxfulparms + 1
if (ilsphasfitdec) maxfulparms = maxfulparms + 1

! channling
if (chanfitdec) maxfulparms = maxfulparms + 2 * nofmws * nofchans

! shift
if (shiftfitdec) maxfulparms = maxfulparms + nofmws

! Anzahl reduzierter Parameter bestimmen
maxredparms = 0

! VMRs
do i = 1,nvmrspeci
    if (method(i) .eq. 0) then
        maxredparms = maxredparms + 1
    else
        maxredparms = maxredparms + noflayers
    end if
end do

! T
if (tfitdec) then
    maxredparms = maxredparms + noflayers + 1 ! Tatm und Tgnd
end if

! ILS mod/phas
if (ilsmodfitdec) maxredparms = maxredparms + 1
if (ilsphasfitdec) maxredparms = maxredparms + 1

! channeling
if (chanfitdec) maxredparms = maxredparms + 2 * nofmws * nofchans

! shift
if (shiftfitdec) maxredparms = maxredparms + nofmws

! Falls errormode: Anzahl der Fehlerkomponenten festlegen

! Ueberblick Berechnung der einzelnen Fehlerbeitraege:
! Offset  (Annahme: Fehlerwert in allen MWs gleich, 1 Eintrag)
! Channeling (Annahme: Fehlerwert in allen MWs gleich, 2 * nofmws * nmaxchan Eintraege)
! ILSmod  (Annahme: Fehlerwert in allen MWs gleich, 1 Eintrag)
! ILSphas (Annahme: Fehlerwert in allen MWs gleich, 1 Eintrag)
! LOS     (Annahme: Fehlerwert in allen MWs gleich, 1 Eintrag)
! Solare Linien Intensitaet (Annahme: Fehlerwert in allen MWs gleich, ein Eintrag)
! Temperatur (jedes T-Pattern ein Eintrag)
! Berechnung Fehlerbeitraege Spektroskopie / Stoergase: HITint (jede vmr-Spezies ein Eintrag)
! Berechnung Fehlerbeitraege Spektroskopie / Stoergase: HITgam (jede vmr-Spezies ein Eintrag)

! Positionen im vollen und reduzierten Retrievalvektor bestimmen
call set_pos_infvec

end subroutine initvects



!====================================================================
! Initialisierung der Gewichte fuer spektrale Stuetzstellen
!====================================================================
subroutine initweights(weight)

use prof96_m

implicit none

real(8),dimension(npos_spec(nofmws)),intent(out) :: weight

integer :: i,imw,idww
real(8) :: steep,nue,wert

weight = 1.0d0

do imw = 1,nofmws
    do idww = 1,ndww(imw)
        do i = 1,ptsinmw(imw)
            nue = firstwvnr(imw) + dwvnr * real(i - 1,8)
            steep = 1.0d0 / (abs(vw_steep(idww,imw)) + 1.0d-3)
            wert =  1.0 / (exp(steep * (nue - real(vw_bnds(idww,imw),8))) + 1.0d0) &
              + 1.0 / (exp(steep * (aimag(vw_bnds(idww,imw)) - nue)) + 1.0d0)
            weight(mpos_spec(imw)+i-1) = weight(mpos_spec(imw)+i-1) * wert
        end do
    end do
end do



end subroutine initweights



!====================================================================
! linearisierte Profilinversion (TP ueber Abweichung Startprofil)
!====================================================================
subroutine invers(mess,calc,refweight,totinvkov,regparms,startparms &
  ,actparms,actvari,stab,einjakobi,newparms,weight,new)

use prof96_m

implicit none

real(8),dimension(npos_spec(nofmws)),intent(in) :: mess,calc,refweight
real(8),dimension(maxredparms,maxredparms),intent(in) :: totinvkov
real(8),dimension(maxfulparms),intent(in) :: regparms,startparms,actparms
real(8),intent(in) :: actvari,stab
real(8),dimension(npos_spec(nofmws),maxfulparms),intent(in) :: einjakobi
real(8),dimension(maxfulparms),intent(out) :: newparms
real(8),dimension(npos_spec(nofmws)),intent(out) :: weight,new

logical :: declinear
integer :: i,j,k,linzaehler
real(8),dimension(npos_spec(nofmws),maxredparms) :: jak
real(8),dimension(npos_spec(nofmws)) :: intercalc
real(8),dimension(maxfulparms) :: interparms
real(8),dimension(maxredparms) :: redregparms,redstabparms,rednewparms &
  ,redinterparms,redwrkparmsa,redwrkparmsb,redfakparms,bdiag
real(8),dimension(maxredparms,maxredparms) :: b,breg,binv
real(8),dimension(npos_spec(nofmws)) :: af
real(8) :: faktor,sumw,sumwr,diff
real(16) :: wert

! Ggf. Bestimmung des dynamischen Gewichtsanteiles
if (ddwdec) then
    do i = 1,npos_spec(nofmws)
        diff = abs(mess(i) - calc(i))
        if (diff .gt. rms_ddw) then
            weight(i) = refweight(i) * rms_ddw / diff
        end if
        sumw = sumw + weight(i)
        sumwr = sumwr + refweight(i)
    end do
    weight = weight * sumwr / sumw
else
    weight = refweight
end if

! Reduzierten Parametervektor f�r Regularisierungsprofil erzeugen
! ggf. Trafo auf log. Skala
call trafo_tored_parms(regparms,regparms,redregparms)
call trafo_tored_parms(regparms,actparms,redstabparms)

interparms = actparms
intercalc = calc

declinear = .false.
linzaehler = 0

do while (.not. declinear)
    ! Transformation auf log. Basis und Reduzierung der Jakobimatrix und des
    ! aktuellen Parametervektoren
    call trafo_jakobi(einjakobi,regparms,startparms,interparms,jak)
    call trafo_tored_parms(regparms,interparms,redinterparms)

    ! Berechnung der Matrix AT* DWW * A, Diagonalelemente fuer L-M Stab.
    do i = 1,maxredparms
        do j = 1,maxredparms
            wert = 0.0d0
            do k = 1,npos_spec(nofmws)
                wert = wert + jak(k,i) * weight(k) * jak(k,j)
            end do
            b(i,j) = wert
        end do
    end do

    do i = 1,maxredparms
        bdiag(i) = b(i,i)
    end do

    ! Regularisierung der Matrix gemaess Vorgabe, Stabilisierung
    ! ATA -> (ATA+reg*BTB+stab*BTB)
    call regmatrix(b,totinvkov,actvari,stab,breg)

    ! Matrix invertieren
    ! (ATA+reg*BTB+stab*BTB) -> (ATA+reg*BTB+stab*BTB)**-1
    call matinvers(breg,binv,maxredparms)

    ! Bildung des Vektors AT* DWW * (DeltaL+ A * f): homogener Anteil
    ! redwrkparmsa = matmul(jak,mess - intercalc + af)
    af = matmul(jak,redinterparms)
    do i = 1,maxredparms
        wert = 0.0d0
        do k = 1,npos_spec(nofmws)
            wert = wert + jak(k,i) * weight(k) * (mess(k) - intercalc(k) + af(k))
        end do
        redwrkparmsa(i) = wert
    end do

    ! Inhomogenen Loesungsanteil zuaddieren, dadurch Bildung von
    ! AT*(DeltaL+Af)+reg*BTB*freg+stab*fstab
    call addinhom(redwrkparmsa,redregparms,redstabparms,bdiag,totinvkov,actvari,stab,redwrkparmsb)

    ! Multiplikation der Matrix (ATA+reg*BTB+stab*BTB)-1 mit Vektor
    ! AT*(DeltaL+Af)+reg*BTB*freg+stab*fstab dadurch Bildung des neuen
    ! reduzierten Parametervektors
    rednewparms = matmul(binv,redwrkparmsb)

    ! Skalierung der neuen L�sung hinunter auf linearen Bereich
    ! (Keine Beschr�nkung bei Saeulenfit oder linearer Skalierung)
    redfakparms = 1.0d0

    do i = 1,nvmrspeci
        if (logdec(i)) then
             ! Im log-Fall max 50% Aenderung der VMRs zulassen
             ! Im Fall der allgemeinen PDF, lineares Limit: Aenderung um max 0.4 * f_apriori / regpdf
            redfakparms(mpos_red(i):npos_red(i)) = &
              2.5d0 * abs(rednewparms(mpos_red(i):npos_red(i)) - redinterparms(mpos_red(i):npos_red(i)))
        end if
    end do

    linzaehler = linzaehler + 1
    faktor = maxval(redfakparms)
    if (faktor .gt. 1.0001d0 .and. linzaehler .le. 10) then
        rednewparms = redinterparms + (rednewparms - redinterparms) / faktor
    else
        declinear = .true.
    end if

    ! Ruecktrafo auf lineare vmr-Skala und Wiederherstellung des vollen Parametervektors
    call trafo_toful_parms(rednewparms,regparms,startparms,interparms,newparms)

    ! Zwischenwerte der Parameter f�r naechste Iteration setzen
    interparms = newparms
    intercalc = calc + matmul(einjakobi,interparms - actparms)

end do

! Berechnung des neuen Spektrums entsprechend der linearisierten Vorwaertsrechnung
new = intercalc

end subroutine invers



!====================================================================
!  L�schen bestehender Ergebnisdateien
!====================================================================
subroutine kill_files(outpfad)

use prof96_m

implicit none

character(len=*),intent(in) :: outpfad

print *,'Deleting files from previous runs...'
print *,''
call system('rm -rf '//trim(outpfad)//'/*.*')


end subroutine kill_files



!====================================================================
!  loadregparms: liest Regularisierungsprofil ein
!====================================================================
subroutine loadregparms(regparmdatei,regfacdatei,regpdfdatei,regparms,regfacs)

use prof96_m

implicit none

character(len=*),intent(in) :: regparmdatei,regfacdatei,regpdfdatei
real(8),dimension(maxfulparms),intent(out) :: regparms
real(8),dimension(2,ntargetspeci,noflayers),intent(out) :: regfacs

logical :: dateidadec
integer :: i,j

! Einlesen der Regparms
regparms = 0.0d0
inquire (file = regparmdatei,exist = dateidadec)
if (.not. dateidadec) then
    call warnout('loadregparms: file not found!')
end if

open (iunit_inv,file = regparmdatei,status = 'old',action = 'read')

do i = 1,ntargetspeci
    call gonext(iunit_inv,.false.)
    read (iunit_inv,*) (regparms(j),j = mpos_ful(i),npos_ful(i))
end do

close (iunit_inv)

! Testen, dass keine negativen oder Nullwerte in regparms (nur erlaubt im Skalierungsfall)
do i = 1,ntargetspeci
    if (method(i) .ne. 0) then
        do j = 1,noflayers
            if (regparms(mpos_ful(i)+j-1) .lt. 1.0d-30) then
                print *, 'Spezies:',i
                print *, 'Layer:',j
                call warnout('loadregparms: a-priori zero or negative!')
            end if
        end do
    end if
end do

! Einlesen der Reg.faktoren
inquire (file = regfacdatei,exist = dateidadec)
if (.not. dateidadec) then
    call warnout('initkovars: regfac-file not found!')
end if

coldecs = .false.
regfacs = 0.0d0
open (iunit_inv,file = regfacdatei,status = 'old',action = 'read')
do i = 1,ntargetspeci
    call gonext(iunit_inv,.false.)
    do j = 1,noflayers
        read (iunit_inv,*) regfacs(1,i,j),regfacs(2,i,j),coldecs(j,i)
    end do
end do
close (iunit_inv)

! Teste, ob optionale regpdf-Datei vorhanden
inquire (file = regpdfdatei,exist = dateidadec)
if (dateidadec) then

    print*,'PROFFIT assumes generalized PDF!'
    fulpdfdecs(:,:) = .false.

    ! Einlesen
    open (iunit_inv,file = regpdfdatei,status = 'old',action = 'read')
    do i = 1,ntargetspeci
        call gonext(iunit_inv,.false.)
        do j = 1,noflayers
            read (iunit_inv,*) regpdf(j,i)
            if (regpdf(j,i) .lt. 0.0d0) regpdf(j,i) = 0.0d0
            if (regpdf(j,i) .gt. 1.0d0) regpdf(j,i) = 1.0d0
            if (regpdf(j,i) .gt. 1.0d-8) then
                fulpdfdecs(j,i) = .true.
            else
                fulpdfdecs(j,i) = .false.
            end if
        end do
    end do

else
    regpdf = 0.0d0
end if

end subroutine loadregparms



!====================================================================
!  matinvers: invertiert eine symmetrische nmax x nmax Matrix
!====================================================================
subroutine matinvers(matein,mataus,nmax)

implicit none

integer,intent(in) :: nmax
real(8),dimension(nmax,nmax),intent(in) :: matein
real(8),dimension(nmax,nmax),intent(out) :: mataus

logical :: warnung
logical,dimension(nmax):: done
integer :: i,j,k,l,jj,kk,jk,lk,nrank,nvmax,icount
real(8) :: check
real(16) :: vkk,vjk,pivot
real(16),dimension(:),allocatable :: v

! Eindimensionale Darstellung der symmetrischen Eingabematrix in v
nvmax = (nmax * nmax + nmax) / 2
allocate (v(nvmax))

! Werte in v eintragen und diag eintragen
icount = 0
do i = 1,nmax ! Zeilenindex
    do j = 1,i ! Spaltenindex
        icount = icount + 1
        v(icount) = matein(j,i)
    end do
end do

! Berechnung der Inversen (nach Blobel / Lohrmann, S.67)
! reset flags
done(1:nmax) = .false.

! loop
nrank = 0
do i = 1,nmax

    ! search for pivot and test for linearity and zero matrix
    k = 0
    jj = 0
    vkk = 0.0d0

    ! groesstes verbliebenes Diagonalelement aufsuchen
    do j = 1,nmax
        jj = jj + j
        if (.not. done(j)) then
            if (k .eq. 0) k = j
            if (abs(v(jj)) .ge. vkk) then
                vkk = abs(v(jj))
                k = j
            end if
        end if
    end do
    done(k) = .true.

    ! kk is previous diagonal element
    kk = (k * k - k) / 2

    ! prepare exchange step
    nrank = nrank + 1
    pivot = 1.0d0 / v(kk+k)
    v(kk+k) = - pivot
    jj = 0

    ! exchange step
    jk = kk
    do j = 1,nmax
        if (j .le. k) jk = jk + 1
        if (j .gt. k) jk = jk + j - 1
        if (j .ne. k) then
            vjk = v(jk)
            v(jk) = pivot * vjk
            ! index of pivot row / column
            lk = kk
            do l = 1,j
                if (l .le. k) lk = lk + 1
                if (l .gt. k) lk = lk + l - 1
                if (l .ne. k) then
                    v(jj+l) = v(jj+l) - v(lk) * vjk
                end if
            end do
        end if
        jj = jj + j
    end do

end do

! Werte aus v austragen (Vorzeichenwechsel!)
jj = 0
do j = 1,nmax ! Zeilenindex
    jj = jj + j
    mataus(j,j) = -v(jj)
    do i = j+1,nmax ! Spaltenindex
        icount = j + i * (i - 1) / 2
        mataus(j,i) = -v(icount)
        mataus(i,j) = -v(icount)
    end do
end do

deallocate (v)

!Test: ist die Inverse gefunden?
warnung = .false.
do i = 1,nmax
    do j = 1,nmax
        check = 0.0d0
        do k = 1,nmax
            check = check + matein(i,k) * mataus(k,j)
        end do
        if (i .eq. j) then
            if (abs(check - 1.0d0) .gt. 1.0d-4) warnung = .true.
        else
            if (abs(check) .gt. 1.0d-4) warnung = .true.
        end if
    end do
end do
if (warnung) then
    print *,'subroutine matinvers: inverted matrix incorrect!'
end if

end subroutine matinvers





!====================================================================
!  Parameter auf Schirm schreiben
!====================================================================
subroutine parms_to_screen(actparms,newparms)

use prof96_m

implicit none

real(8),dimension(maxfulparms),intent(in) :: actparms,newparms

integer :: i,j

do i = 1,ntargetspeci
    write (*,'(A80)') gaskennung(i)
    write(*,'(ES9.1,10(ES8.1))') &
      (actparms(j),j = mpos_ful(i),npos_ful(i),noflayers/8)
    write(*,'(ES9.1,10(ES8.1))') &
      (newparms(j),j = mpos_ful(i),npos_ful(i),noflayers/8)
end do
print *,'other fitparms:'
write(*,'(ES9.1,30(ES8.1))') (actparms(i),i = npos_ful(ntargetspeci)+1,maxfulparms)
write(*,'(ES9.1,30(ES8.1))') (newparms(i),i = npos_ful(ntargetspeci)+1,maxfulparms)

end subroutine parms_to_screen



!====================================================================
!  Parameter in KOPRA setzen
!====================================================================
subroutine put_parms(actparms)

use globfwd23
use globlev23
use prof96_m

implicit none

real(8),dimension(maxfulparms),intent(in) :: actparms

integer :: i,imw,ispeci,mpos

! vmr-Profile f�r alle Species
do ispeci = 1,nvmrspeci
    lev%vmr_ppmv(1:noflayers,deri%npoint_fwd(ispeci)) = actparms(mpos_ful(ispeci):npos_ful(ispeci))
end do

! ggf. T-Profil
if (tfitdec) then
    lev%T_K(1:noflayers) = actparms(mpos_ful(ntargetspeci):npos_ful(ntargetspeci))
    obs%Tgnd_K = actparms(npos_ful(ntargetspeci)+1)
    mpos = npos_ful(ntargetspeci) + 1
else
    mpos = npos_ful(nvmrspeci)
end if

! mpos = npos_ful(ntargetspeci)

! ggf. ILS apo / phas
if (ilsmodfitdec) then
    mpos = mpos + 1
    instr%apo = actparms(mpos)
end if
if (ilsphasfitdec) then
    mpos = mpos + 1
    instr%phas = actparms(mpos)
end if

! ggf. Channeling
if (chanfitdec) then
    do imw = 1,nofmws
        do i = 1,nofchans
            mpos = mpos + 1
            instr%chanamp(i,imw) = cmplx(actparms(mpos),actparms(mpos+1),kind = 8)
            mpos = mpos + 1
        end do
    end do
end if

! ggf. Shift
if (shiftfitdec) then
    do imw = 1,nofmws
        mpos = mpos + 1
        mw(imw)%shift = actparms(mpos)
    end do
end if

end subroutine put_parms



!====================================================================
!  Einlesen der Steuerungsparameter
!====================================================================
subroutine readinput(inpdatei,kind_of_action,maxcycles,lspeci,maxlcurv &
  ,stab,lskala,lskalb,kovardatei)

use globfwd23 , only : n_chan,messdec
use prof96_m

implicit none

character(len=*),intent(in) :: inpdatei
integer,intent(out) :: kind_of_action,maxcycles,lspeci,maxlcurv
real(8),intent(out) :: stab,lskala,lskalb
character(len=100),dimension(nmaxtarget),intent(out) :: kovardatei

logical :: dateidadec
integer :: i,j,nzerojak,izeromw,izerospeci
real(8),parameter :: stabconst = 1.0d-15

inquire (file = inpdatei,exist = dateidadec)
if (.not. dateidadec) then
    call warnout('readinput: file not found!')
end if

open (iunit_inv,file = trim(inpdatei),status = 'old',action = 'read')
call gonext(iunit_inv,.false.)
! Vorwaertsrechnung oder Retrieval?
read(iunit_inv,*) kind_of_action
if (kind_of_action .eq. 2 .and. .not. messdec) call warnout('Retrieval requires measurement!')

! Anzahl zu fittender Spezies, Fit der Temperatur j/n, Anzahl MWs
call gonext(iunit_inv,.false.)
read(iunit_inv,*) nvmrspeci

read(iunit_inv,*) tfitdec
if (tfitdec) then
    ntargetspeci = nvmrspeci + 1
else
    ntargetspeci = nvmrspeci
end if
if (ntargetspeci .gt. nmaxtarget) call warnout('Zielspecies: Anzahl zu gross! ')

! Angaben zu MWs, Ausgabepfad festlegen
call gonext(iunit_inv,.false.)
read(iunit_inv,*) nofmws
if (nofmws .gt. nmaxmw) call warnout('MWs: Anzahl zu gro�! ')

! Angaben zum Fitverfahren
call gonext(iunit_inv,.false.)
do i = 1,ntargetspeci
    read(iunit_inv,*) method(i),logdec(i),reg(1,i),reg(2,i),reg(3,i),reg(4,i)
    read(iunit_inv,*) kovardatei(i)
    if (logdec(i)) then
        if (reg(3,i) .lt. 1.0d-30) call warnout('Wert von R3 unzul�ssig! ')
    end if
end do

! Zero parts of Jacobean?
call gonext(iunit_inv,.false.)
deczerojakobi(:,:) = .false.
read(iunit_inv,*) nzerojak
if (nzerojak .eq. 0) print *,'No zeroing in Jacobean!'
do i = 1,nzerojak
    read(iunit_inv,*) izeromw,izerospeci
    print *,'Zeroing Jacobean (imw,ispeci): ',izeromw,izerospeci
    if ((izeromw - 1) * (nofmws - izeromw) .lt. 0) then
        call warnout('value izeromw invalid! ')
    end if
    if ((izerospeci - 1) * (nvmrspeci - izerospeci) .lt. 0) then
        call warnout('value izerospeci invalid! ')
    end if
    deczerojakobi(izeromw,izerospeci) = .true.
end do

! ILS-Fit Modulation / Phase?
call gonext(iunit_inv,.false.)
read(iunit_inv,*) ilsmodfitdec
read(iunit_inv,*) ilsphasfitdec

! Angaben zum Channeling
call gonext(iunit_inv,.false.)
read(iunit_inv,*) chanfitdec
nofchans = n_chan

! Angaben zum Shift
call gonext(iunit_inv,.false.)
read(iunit_inv,*) shiftfitdec
if (shiftfitdec) then
    read(iunit_inv,*) stopshiftdec
    read(iunit_inv,*) limitshiftdec
    read(iunit_inv,*) limitshift
    read(iunit_inv,*) regshift
end if

! Angaben zum Deweighting
call gonext(iunit_inv,.false.)
do i = 1,nofmws
    read(iunit_inv,*) ndww(i)
    if (ndww(i) .gt. ndwwmax) then
        print *,'MW  ndww  ndwwmax',i,ndww(i),ndwwmax
        call warnout('Too many dww windows! ')
    end if
    do j = 1,ndww(i)
        read(iunit_inv,*) vw_bnds(j,i),vw_steep(j,i)
    end do
end do

! Angaben zur Konvergenz
call gonext(iunit_inv,.false.)
read(iunit_inv,*) maxcycles
read(iunit_inv,*) conv_scal
read(iunit_inv,*) stab
stab = stabconst * stab

! Angaben zur Berechnung der L-Kurve
call gonext(iunit_inv,.false.)
read(iunit_inv,*) lspeci
read(iunit_inv,*) maxlcurv
read(iunit_inv,*) lskala
read(iunit_inv,*) lskalb
if (lspeci .gt. ntargetspeci) call warnout('L-curve: Illegal species selected!')

! Ausgabe/Berechnung verschiedener Groessen
call gonext(iunit_inv,.false.)
read(iunit_inv,*) jakoutdec
read(iunit_inv,*) gainoutdec
close (iunit_inv)

end subroutine readinput



!====================================================================
!  Regularisierung und Stabilisierung der zu invertierenden Matrix
!====================================================================
subroutine regmatrix(b,totinvkov,actvari,stab,breg)

use prof96_m

implicit none

real(8),dimension(maxredparms,maxredparms),intent(in) :: b
real(8),dimension(maxredparms,maxredparms),intent(in) :: totinvkov
real(8),intent(in) :: actvari,stab
real(8),dimension(maxredparms,maxredparms),intent(out) :: breg

integer :: i
real(8) :: variwert

! Bestimmung der fuer Reg. anzuwendenden Varianz
if (rmsfixdec) then
    variwert = rms_fix * rms_fix
else
    variwert = actvari
end if

! Regularisierung anwenden
breg = b + variwert * totinvkov

! Stabilisierung nach L-M
do i = 1,maxredparms
    breg(i,i) = breg(i,i) + stab * variwert * b(i,i)
end do

end subroutine regmatrix



!====================================================================
!  Setup der Positionen im vollen und reduzierten Retrievalvektor
!====================================================================
subroutine set_pos_infvec

use prof96_m

implicit none

integer :: i

! Positionen im reduzierten Retrievalvektor (fuer jede Species)
mpos_red(1) = 1
if (method(1) .eq. 0) then
    npos_red(1) = 1
else
    npos_red(1) = noflayers
end if

do i = 2,ntargetspeci
    mpos_red(i) = npos_red(i-1) + 1
    if (method(i) .eq. 0) then
        npos_red(i) = mpos_red(i)
    else
        npos_red(i) = mpos_red(i) + noflayers - 1
    end if
end do

! Positionen im vollen Retrievalvektor (fuer jede Species)
mpos_ful(1) = 1
npos_ful(1) = noflayers
do i = 2,ntargetspeci
    mpos_ful(i) = npos_ful(i-1) + 1
    npos_ful(i) = mpos_ful(i) + noflayers - 1
end do

end subroutine set_pos_infvec



!====================================================================
!  Setup der Positionen im Spektralvektor
!====================================================================
subroutine set_pos_inlvec

use globfwd23
use prof96_m

implicit none

integer :: i

mpos_spec(1) = 1
npos_spec(1) = mw(1)%pts
do i = 2,nofmws
    mpos_spec(i) = npos_spec(i-1) + 1
    npos_spec(i) = mpos_spec(i) + mw(i)%pts - 1
end do

end subroutine set_pos_inlvec



!====================================================================
!  Statistik eines Datensatzes: Mittelwert eines Vektors berechnen
!====================================================================
subroutine stat1d_mean(vektor,weight,ncycles,mean)

implicit none

integer,intent(in) :: ncycles
real(8),dimension(ncycles),intent(in) :: vektor,weight
real(8),intent(out) :: mean

integer :: i
real(8) :: normwert

! Berechnung des Mittelwertes
mean = 0.0d0
normwert = 0.0d0
do i = 1,ncycles
    mean = mean + vektor(i) * weight(i)
    normwert = normwert + weight(i)
end do
mean = mean / normwert

end subroutine stat1d_mean



!====================================================================
!  Statistik eines Datensatzes: Streuung 1d
!====================================================================
subroutine stat1d_var(vektora,vektorb,weight,ncycles,variwert)

implicit none

integer,intent(in) :: ncycles
real(8),dimension(ncycles),intent(in) :: vektora,vektorb,weight
real(8),intent(out) :: variwert

integer :: i
real(8) :: normwert

! Berechnung der Varianz
variwert = 0.0d0
normwert = 0.0d0
do i = 1,ncycles
    variwert = variwert + (vektora(i) - vektorb(i)) * (vektora(i) - vektorb(i)) * weight(i)
    normwert = normwert + weight(i)
end do
variwert = variwert / normwert

end subroutine stat1d_var



!====================================================================
!  Statistik eines 2d Datensatzes
!====================================================================
subroutine stat2d_var(matrix,nmax,ncycles,covari)

implicit none

integer,intent(in) :: nmax,ncycles
real(8),dimension(nmax,ncycles),intent(in) :: matrix
real(8),dimension(nmax,nmax),intent(out) :: covari

integer :: i,j,k
real(8),dimension(nmax) :: mean

! Berechnung des Mittelwertes
mean = 0.0d0
do i = 1,nmax
    do j = 1,ncycles
        mean(i) = mean(i) + matrix(i,j)
    end do
mean(i) = mean(i) / real(ncycles,8)
end do

! Berechnung der Varianz
covari = 0.0d0
do i = 1,nmax
    do j = 1,nmax
        do k = 1,ncycles
            covari(i,j) = covari(i,j) + (matrix(i,k) - mean(i)) * (matrix(j,k) - mean(j))
        end do
        covari(i,j) = covari(i,j) / real((ncycles - 1),8)
    end do
end do

end subroutine stat2d_var



!====================================================================
!  Transformation der Jakobimatrix auf log. Skala und Reduzierung
!  (Beachte: actparms,stratparms m�ssen als volle Vektoren auf linearer
!   vmr-Skala vorliegen!)
!====================================================================
subroutine trafo_jakobi(einjakobi,regparms,startparms,actparms,ausjakobi)

use globfwd23 , only : mw
use prof96_m

implicit none

real(8),dimension(npos_spec(nofmws),maxfulparms),intent(in) :: einjakobi
real(8),dimension(maxfulparms),intent(in) :: regparms,startparms,actparms
real(8),dimension(npos_spec(nofmws),maxredparms),intent(out) :: ausjakobi

integer :: i,j,imw,ioffset

ausjakobi = 0.0d0
do i = 1,ntargetspeci
    if (method(i) .eq. 0) then
        do j = 1,noflayers
            if (coldecs(j,i)) then
                ausjakobi(:,mpos_red(i)) = ausjakobi(:,mpos_red(i)) &
                  + startparms(mpos_ful(i)+j-1) * einjakobi(:,mpos_ful(i)+j-1)
            end if
        end do
    else
        if (logdec(i)) then
            do j = 1,noflayers
                if (actparms(mpos_ful(i)+j-1) .gt. regparms(mpos_ful(i)+j-1)) then
                    ausjakobi(:,mpos_red(i)+j-1) = einjakobi(:,mpos_ful(i)+j-1) &
                      / ((1.0d0 - regpdf(j,i)) / actparms(mpos_ful(i)+j-1) &
                      + regpdf(j,i) / regparms(mpos_ful(i)+j-1))
                else
                    ausjakobi(:,mpos_red(i)+j-1) = einjakobi(:,mpos_ful(i)+j-1) &
                      * actparms(mpos_ful(i)+j-1)
                end if
            end do
        else
            do j = 1,noflayers
                ausjakobi(:,mpos_red(i)+j-1) = einjakobi(:,mpos_ful(i)+j-1)
            end do
        end if
    end if
end do

ioffset = 0
if (tfitdec) then ! Extra-Ableitung fuer Tgnd
    ioffset = ioffset + 1
    ausjakobi(:,npos_red(ntargetspeci)+ioffset) = einjakobi(:,npos_ful(ntargetspeci)+ioffset)
end if

! ILS mod / phas
if (ilsmodfitdec) then
    ioffset = ioffset + 1
    ausjakobi(:,npos_red(ntargetspeci)+ioffset) = ilsunit * einjakobi(:,npos_ful(ntargetspeci)+ioffset)
end if
if (ilsphasfitdec) then
    ioffset = ioffset + 1
    ausjakobi(:,npos_red(ntargetspeci)+ioffset) = ilsunit * einjakobi(:,npos_ful(ntargetspeci)+ioffset)
end if

! ggf. Channeling
if (chanfitdec) then
    do imw = 1,nofmws
        do i = 1,nofchans
            ioffset = ioffset + 1
            ausjakobi(:,npos_red(ntargetspeci)+ioffset) = chanunit * einjakobi(:,npos_ful(ntargetspeci)+ioffset)
            ioffset = ioffset + 1
            ausjakobi(:,npos_red(ntargetspeci)+ioffset) = chanunit * einjakobi(:,npos_ful(ntargetspeci)+ioffset)
        end do
    end do
end if

! ggf. Shift
if (shiftfitdec) then
    do imw = 1,nofmws
        ioffset = ioffset + 1
        ausjakobi(:,npos_red(ntargetspeci)+ioffset) = shiftunit * einjakobi(:,npos_ful(ntargetspeci)+ioffset)
    end do
end if

end subroutine trafo_jakobi



!====================================================================
!  Transformation auf lin. Skala und Expansion eines Profils
!====================================================================
subroutine trafo_toful_parms(rednewparms,regparms,startparms,actparms,newparms)

use globfwd23 , only : mw
use prof96_m

implicit none

real(8),dimension(maxredparms),intent(in) :: rednewparms
real(8),dimension(maxfulparms),intent(in) :: regparms,startparms,actparms
real(8),dimension(maxfulparms),intent(out) :: newparms

integer :: i,j,imw,ioffset

do i = 1,ntargetspeci
    if (method(i) .eq. 0) then
        do j = 1,noflayers
            if (coldecs(j,i)) then
                newparms(mpos_ful(i)+j-1) = actparms(mpos_ful(i)+j-1)&
                  + rednewparms(mpos_red(i)) * startparms(mpos_ful(i)+j-1)
            else
                newparms(mpos_ful(i)+j-1) = startparms(mpos_ful(i)+j-1)
            end if
        end do
    else
        if (logdec(i)) then
            do j = 1,noflayers
                if (rednewparms(mpos_red(i)+j-1) .lt. 1.0d0) then
                    newparms(mpos_ful(i)+j-1) = regparms(mpos_ful(i)+j-1) * exp(rednewparms(mpos_red(i)+j-1))
                else
                    if (regpdf(j,i) .lt. pdflimit) then
                        newparms(mpos_ful(i)+j-1) = regparms(mpos_ful(i)+j-1) * exp(rednewparms(mpos_red(i)+j-1))
                    else
                        newparms(mpos_ful(i)+j-1) = findf(rednewparms(mpos_red(i)+j-1),regparms(mpos_ful(i)+j-1),regpdf(j,i))
                    end if
                end if

            end do
        else
            newparms(mpos_ful(i):npos_ful(i)) = rednewparms(mpos_red(i):npos_red(i))
        end if
    end if
end do

ioffset = 0

!  ggf Tgnd
if (tfitdec) then
    ioffset = ioffset + 1
    newparms(npos_ful(ntargetspeci)+ioffset) = rednewparms(npos_red(ntargetspeci)+ioffset)
end if

! ggf. ILS mod / phas
if (ilsmodfitdec) then
    ioffset = ioffset + 1
    newparms(npos_ful(ntargetspeci)+ioffset) = ilsunit * rednewparms(npos_red(ntargetspeci)+ioffset)
end if
if (ilsphasfitdec) then
    ioffset = ioffset + 1
    newparms(npos_ful(ntargetspeci)+ioffset) = ilsunit * rednewparms(npos_red(ntargetspeci)+ioffset)
end if

! ggf. Channeling
if (chanfitdec) then
    do imw = 1,nofmws
        do i = 1,nofchans
            ioffset = ioffset + 1
            newparms(npos_ful(ntargetspeci)+ioffset) = chanunit * rednewparms(npos_red(ntargetspeci)+ioffset)
            ioffset = ioffset + 1
            newparms(npos_ful(ntargetspeci)+ioffset) = chanunit * rednewparms(npos_red(ntargetspeci)+ioffset)
        end do
    end do
end if

! ggf. Shift
if (shiftfitdec) then
    do imw = 1,nofmws
        ioffset = ioffset + 1
        newparms(npos_ful(ntargetspeci)+ioffset) = shiftunit * rednewparms(npos_red(ntargetspeci)+ioffset)
    end do
end if


contains

! internal function findf (Ruecktrafo zur verallgemeinerten Logtrafo)
real function findf(ftilde,fap,m)

implicit none

real(8),intent(in) :: ftilde,fap,m

real (8) :: fact,slope,x

! Startwerte setzen
x = 1.0d0
fact = 0.0d0

do while (abs(ftilde - fact) .gt. 1.0d-7)
    fact = (1.0d0 - m) * log(x) + m * (x - 1.0d0)
    slope = (1.0d0 - m) / x + m
    x = x + (ftilde - fact) / sqrt(slope * slope * (1.0d0 - m) / (x * slope + ftilde - fact) + m * slope)
end do

findf = x * fap

end function findf

end subroutine trafo_toful_parms



!====================================================================
!  Transformation auf log. Skala und Reduktion eines Profils
!====================================================================
subroutine trafo_tored_parms(regparms,fulparms,redparms)

use globfwd23 , only : mw
use prof96_m

implicit none

real(8), parameter :: lowlimit = 1.0d-10
real(8), parameter :: highlimit = 1.0d10

real(8),dimension(maxfulparms),intent(in) :: regparms,fulparms
real(8),dimension(maxredparms),intent(out) :: redparms

integer :: i,j,imw,ioffset

redparms = 0.0d0
do i = 1,ntargetspeci
    if (method(i) .eq. 0) then
        redparms(mpos_red(i)) = 0.0d0
    else
        if (logdec(i)) then
            do j = 1,noflayers
                if (fulparms(mpos_ful(i)+j-1) .lt. lowlimit * regparms(mpos_ful(i)+j-1)) then
                    print *,fulparms(mpos_ful(i)+j-1)
                    print *,'spezies  level: ',i,j
                    call warnout('Warning: Underflow in trafo_tored_parms! ')
                end if
                if (fulparms(mpos_ful(i)+j-1) .gt. highlimit * regparms(mpos_ful(i)+j-1)) then
                    print *,fulparms(mpos_ful(i)+j-1)
                    print *,'spezies  level: ',i,j
                    call warnout('Warning: Overflow in trafo_tored_parms! ')
                end if
                if (fulparms(mpos_ful(i)+j-1) .gt. regparms(mpos_ful(i)+j-1)) then
                    redparms(mpos_red(i)+j-1) = (1.0d0 - regpdf(j,i)) &
                      * log(fulparms(mpos_ful(i)+j-1) / regparms(mpos_ful(i)+j-1)) &
                      + regpdf(j,i) * (fulparms(mpos_ful(i)+j-1) / regparms(mpos_ful(i)+j-1) - 1.0d0)
                else
                    redparms(mpos_red(i)+j-1) = log(fulparms(mpos_ful(i)+j-1) / regparms(mpos_ful(i)+j-1))
                end if
            end do
        else
            redparms(mpos_red(i):npos_red(i)) = fulparms(mpos_ful(i):npos_ful(i))
        end if
    end if
end do

ioffset = 0
! ggf. Tgnd
if (tfitdec) then
    ioffset = ioffset + 1
    redparms(npos_red(ntargetspeci)+ioffset) = fulparms(npos_ful(ntargetspeci)+ioffset)
end if

! ggf. ILS mod / phas
if (ilsmodfitdec) then
    ioffset = ioffset + 1
    redparms(npos_red(ntargetspeci)+ioffset) = fulparms(npos_ful(ntargetspeci)+ioffset) / ilsunit
end if
if (ilsphasfitdec) then
    ioffset = ioffset + 1
    redparms(npos_red(ntargetspeci)+ioffset) = fulparms(npos_ful(ntargetspeci)+ioffset) / ilsunit
end if

! ggf. Channeling
if (chanfitdec) then
    do imw = 1,nofmws
        do i = 1,nofchans
            ioffset = ioffset + 1
            redparms(npos_red(ntargetspeci)+ioffset) = fulparms(npos_ful(ntargetspeci)+ioffset) / chanunit
            ioffset = ioffset + 1
            redparms(npos_red(ntargetspeci)+ioffset) = fulparms(npos_ful(ntargetspeci)+ioffset) / chanunit
        end do
    end do
end if
! ggf. Shift
if (shiftfitdec) then
    do imw = 1,nofmws
        ioffset = ioffset + 1
        redparms(npos_red(ntargetspeci)+ioffset) = fulparms(npos_ful(ntargetspeci)+ioffset) / shiftunit
    end do
end if

end subroutine trafo_tored_parms



!====================================================================
!  Bestimmung der Hoehenaufloesung (averaging kernel matrix)
!  und der Gainmatrix
!====================================================================
subroutine vertres(jakobi,weight,regparms,startparms,actparms,totinvkov,actvari,kern_raw,kern_ful,gain_raw,gain_ful)

use globfwd23 , only : mw
use prof96_m

implicit none

real(8),dimension(npos_spec(nofmws),maxfulparms),intent(in) :: jakobi
real(8),dimension(npos_spec(nofmws)),intent(in) :: weight
real(8),dimension(maxfulparms),intent(in) :: regparms,startparms,actparms
real(8),dimension(maxredparms,maxredparms),intent(in) :: totinvkov
real(8),intent(in) :: actvari
real(8),dimension(maxredparms,maxredparms),intent(out) :: kern_raw
real(8),dimension(maxfulparms,maxfulparms),intent(out) :: kern_ful
real(8),dimension(maxredparms,npos_spec(nofmws)),intent(out) :: gain_raw
real(8),dimension(maxfulparms,npos_spec(nofmws)),intent(out) :: gain_ful

integer :: i,j,k,imw,iful,ired
real(8),dimension(npos_spec(nofmws),maxredparms) :: jak
real(8),dimension(maxredparms,maxredparms) :: b,breg,binv
real(8),dimension(maxfulparms,maxredparms) :: trafomat
real(16) :: wert

! Transformation auf log. Basis und Reduzierung der Jakobimatrix und der Parametervektoren
call trafo_jakobi(jakobi,regparms,startparms,actparms,jak)

! Berechnung der Matrix jakobi*jakobit
do i = 1,maxredparms
    do j = 1,maxredparms
        wert = 0.0d0
        do k = 1,npos_spec(nofmws)
            wert = wert + jak(k,i) * weight(k) * jak(k,j)
        end do
        b(i,j) = wert
    end do
end do

! Regularisierung und Stabilisierung der Matrix gemaess Vorgabe
! ATA -> (ATA + reg * BTB)
call regmatrix(b,totinvkov,actvari,0.0d0,breg)

! Matrix invertieren
call matinvers(breg,binv,maxredparms)
gain_raw = matmul(binv,transpose(jak))
kern_raw = matmul(binv,b)

! Konstruktion der Trafomatrix zur Herstellung der vollen Gainmatrix
trafomat = 0.0d0
ired = 0
iful = 0
do i = 1,ntargetspeci
    if (method(i) .eq. 0) then
        ired = ired + 1
        do j = 1,noflayers
            iful = iful + 1
            if (coldecs(j,i)) then
                trafomat(iful,ired) = startparms(iful)
            end if
        end do
    else
        if (logdec(i)) then
            do j = 1,noflayers
                iful = iful + 1
                ired = ired + 1
                if (actparms(iful) .gt. regparms(iful)) then
                    trafomat(iful,ired) = 1.0d0 / ((1.0d0 - regpdf(j,i)) / actparms(iful) + regpdf(j,i) / regparms(iful))
                else
                    trafomat(iful,ired) = actparms(iful)
                end if
            end do
        else
            do j = 1,noflayers
                iful = iful + 1
                ired = ired + 1
                trafomat(iful,ired) = 1.0d0
            end do
        end if
    end if
end do

! ggf Tgnd
if (tfitdec) then
    iful = iful + 1
    ired = ired + 1
    trafomat(iful,ired) = 1.0d0
end if

! ggf. Channeling
if (chanfitdec) then
    do imw = 1,nofmws
        do i = 1,nofchans
            iful = iful + 1
            ired = ired + 1
            trafomat(iful,ired) = chanunit
            iful = iful + 1
            ired = ired + 1
            trafomat(iful,ired) = chanunit
        end do
    end do
end if

! ggf. Shift
if (shiftfitdec) then
    do imw = 1,nofmws
        iful = iful + 1
        ired = ired + 1
        trafomat(iful,ired) = shiftunit
    end do
end if

! Anwendung
! G(full,lin) = M * G(red,log)
! R = G(full,lin) * Jakobi
gain_ful = matmul(trafomat,gain_raw)
kern_ful = matmul(gain_ful,jakobi)

end subroutine vertres



!====================================================================
!  Warnung rausschreiben und Programm evtl. beenden
!====================================================================
subroutine warnout(text)

use prffwd23_m, only : ppquittung
use prof96_m, only : stopshiftdec

implicit none

character(len=*),intent(in) :: text
integer :: intdec

print *,'Warnung:'
print *, trim(text)

if (stopshiftdec) then
    ! ohne Nutzerantwort beenden
    call ppquittung()
    stop
else
    ! Nutzer entscheidet ueber Abbruch
    print *,'Abbruch mit 0, weiter mit 1:'
    read *, intdec
    if (intdec .eq. 0) then
        call ppquittung()
        stop
    end if
end if

end subroutine warnout



!====================================================================
!  Ergebnisse der aktuellen Iteration rausschreiben (Profile,Spektren,rms)
!====================================================================
subroutine write_acts(outpfad,messpec,calcspec,linspec,actparms,actvari &
  ,linvari,straf,cycles)

use prof96_m

implicit none

character(len=*),intent(in) :: outpfad
integer,intent(in) :: cycles
real(8),dimension(npos_spec(nofmws)),intent(in) :: messpec,calcspec,linspec
real(8),dimension(maxfulparms),intent(in) :: actparms
real(8),intent(in) :: actvari,linvari
real(8),dimension(2,ntargetspeci),intent(in) :: straf

integer :: i

open (iunit_inv,file = trim(outpfad)//'/ACTRMS.DAT',status = 'unknown',position = 'append',action = 'write')
write(iunit_inv,'(I4,100(2X,ES13.6))') cycles,sqrt(actvari),sqrt(linvari) &
  ,(straf(1,i),i = 1,ntargetspeci)
close (iunit_inv)

open (iunit_inv,file = trim(outpfad)//'/ACTSPECS.DAT',status = 'unknown',position = 'append',action = 'write')
write (iunit_inv,*) cycles
do i = 1,npos_spec(nofmws)
    write (iunit_inv,'(I4,3(2X,ES10.3))') i,messpec(i),calcspec(i),linspec(i)
end do
close (iunit_inv)

open (iunit_inv,file = trim(outpfad)//'/ACTPARMS.DAT',status = 'unknown',position = 'append',action = 'write')
write (iunit_inv,'(500(2X,ES10.3))') (actparms(i),i = 1,maxfulparms)
close (iunit_inv)

end subroutine write_acts



!====================================================================
!  Ergebnisse der Inversion rausschreiben (Gainmatrix)
!====================================================================
subroutine write_gain(outpfad,gain_raw,gain_ful)

use prof96_m

implicit none

character(len=*),intent(in) :: outpfad
real(8),dimension(maxredparms,npos_spec(nofmws)),intent(in) :: gain_raw
real(8),dimension(maxfulparms,npos_spec(nofmws)),intent(in) :: gain_ful

integer :: i,j

! Rausschreiben der Gainmatrix
open (iunit_inv,file = trim(outpfad)//'/GAIN_RAW.DAT',status = 'replace',action = 'write')
do i = 1,npos_spec(nofmws)
    write(iunit_inv,'(ES10.3,2000(1X,ES10.3))') (gain_raw(j,i),j = 1,maxredparms)
end do
close (iunit_inv)
open (iunit_inv,file = trim(outpfad)//'/GAIN_FUL.DAT',status = 'replace',action = 'write')
do i = 1,npos_spec(nofmws)
    write(iunit_inv,'(ES10.3,2000(1X,ES10.3))') (gain_ful(j,i),j = 1,maxfulparms)
end do
close (iunit_inv)

end subroutine write_gain



!====================================================================
!  Ergebnisse der Inversion rausschreiben (Jakobimatrix,JTJ)
!====================================================================
subroutine write_jakobi(outpfad,jakobi,weight,regparms,startparms,actparms &
  ,totinvkov,actvari,stab)

use prof96_m

implicit none

character(len=*),intent(in) :: outpfad
real(8),dimension(npos_spec(nofmws),maxfulparms),intent(in) :: jakobi
real(8),dimension(npos_spec(nofmws)),intent(in) :: weight
real(8),dimension(maxfulparms),intent(in) :: regparms,startparms,actparms
real(8),dimension(maxredparms,maxredparms),intent(in) :: totinvkov
real(8),intent(in) :: actvari,stab

integer :: i,j,k
real(8),dimension(npos_spec(nofmws),maxredparms) :: jak
real(8),dimension(maxredparms,maxredparms) :: jtj,jtj_reg
real(8) :: wert

! Volle Jakobimatrix rausschreiben
open (iunit_inv,file = trim(outpfad)//'/JAKOBI.DAT',status = 'replace',action = 'write')
do i = 1,npos_spec(nofmws)
    write(iunit_inv,'(ES10.3,2000(1X,ES10.3))') (jakobi(i,j),j = 1,maxfulparms)
end do
close (iunit_inv)

! Reduzierte Jakobimatrix rausschreiben
call trafo_jakobi(jakobi,regparms,startparms,actparms,jak)
open (iunit_inv,file = trim(outpfad)//'/JAK.DAT',status = 'replace',action = 'write')
do i = 1,npos_spec(nofmws)
    write(iunit_inv,'(ES10.3,2000(1X,ES10.3))') (jak(i,j),j = 1,maxredparms)
end do
close (iunit_inv)

! AT * DWW * A rausschreiben
do i = 1,maxredparms
    do j = 1,maxredparms
        wert = 0.0d0
        do k = 1,npos_spec(nofmws)
           wert = wert + jak(k,i) * weight(k) * jak(k,j)
        end do
        jtj(i,j) = wert
    end do
end do

open (iunit_inv,file = trim(outpfad)//'/JTJ.DAT',status = 'replace',action = 'write')
do i = 1,maxredparms
    write(iunit_inv,'(ES10.3,2000(1X,ES10.3))') (jtj(i,j),j = 1,maxredparms)
end do
close (iunit_inv)

! BTB rausschreiben
jtj = 0.0d0
call regmatrix(jtj,totinvkov,actvari,stab,jtj_reg)
open (iunit_inv,file = trim(outpfad)//'/BTB.DAT',status = 'replace',action = 'write')
do i = 1,maxredparms
    write(iunit_inv,'(ES10.3,2000(1X,ES10.3))') (jtj_reg(i,j),j = 1,maxredparms)
end do
close (iunit_inv)

end subroutine write_jakobi



!====================================================================
!  Ergebnisse der Inversion rausschreiben (Kernels)
!====================================================================
subroutine write_kernels(outpfad,kern_raw,kern_ful)

use prof96_m

implicit none

character(len=*),intent(in) :: outpfad
real(8),dimension(maxredparms,maxredparms),intent(in) :: kern_raw
real(8),dimension(maxfulparms,maxfulparms),intent(in) :: kern_ful

integer :: i,j
real(8) :: fdofs

! Rausschreiben der Aufloesungsmatrix
open (iunit_inv,file = trim(outpfad)//'/KERN_RAW.DAT',status = 'replace',action = 'write')
do i = 1,maxredparms
    write(iunit_inv,'(ES10.3,2000(1X,ES10.3))') (kern_raw(i,j),j = 1,maxredparms)
end do
close (iunit_inv)
open (iunit_inv,file = trim(outpfad)//'/KERN_FUL.DAT',status = 'replace',action = 'write')
do i = 1,maxfulparms
    write(iunit_inv,'(ES10.3,2000(1X,ES10.3))') (kern_ful(i,j),j = 1,maxfulparms)
end do
close (iunit_inv)

! DOFs rausschreiben
open (iunit_inv,file = trim(outpfad)//'/DOFS.DAT',status = 'replace',action = 'write')
do i = 1,ntargetspeci
    write (iunit_inv,'(A8,I3)') 'Spezies:',i
    if (method(i) .eq. 0) then
        write (iunit_inv,'(ES10.3)') kern_raw(mpos_red(i),mpos_red(i))
    else
        fdofs = 0.0d0
        do j = 1,noflayers
            fdofs = fdofs + kern_raw(mpos_red(i)+j-1,mpos_red(i)+j-1)
        end do
        write (iunit_inv,'(ES10.3)') fdofs
    end if
end do
fdofs = 0.0d0
do i = npos_red(ntargetspeci)+1,maxredparms
    fdofs = fdofs + kern_raw(i,i)
end do
write (iunit_inv,'(A20)') 'Andere Fitparameter:'
write (iunit_inv,'(ES10.3)') fdofs
close (iunit_inv)

end subroutine write_kernels



!====================================================================
!  Inverse Kovarianzmatrix rausschreiben
!====================================================================
subroutine write_kovar(outpfad,totinvkov)

use prof96_m

implicit none

character(len=*),intent(in) :: outpfad
real(8),dimension(maxredparms,maxredparms),intent(in) :: totinvkov

integer :: i,j,k

if (globoedec) then
    open (iunit_inv,file = trim(outpfad)//'/KOVAR.DAT',status = 'replace',action = 'write')
    do j = 1,npos_red(ntargetspeci)
        write(iunit_inv,'(ES10.3,200(1X,ES10.3))') (totinvkov(j,k),k = 1,npos_red(ntargetspeci))
    end do
    close (iunit_inv)
else
    do i = 1,ntargetspeci
        if (method(i) .ne. 0) then
            open (iunit_inv,file = trim(outpfad)//'/KOVAR.DAT',status = 'replace',action = 'write')
            write (iunit_inv,*) i
            do j = 1,noflayers
                write(iunit_inv,'(ES10.3,200(1X,ES10.3))') (totinvkov(mpos_red(i)+j-1,mpos_red(i)+k-1),k = 1,noflayers)
            end do
            close (iunit_inv)
        end if
    end do
end if


end subroutine write_kovar



!====================================================================
!  L-Kurve rausschreiben
!====================================================================
subroutine write_lcurv(outpfad,vari,strafterm)

use prof96_m, only : iunit_inv

implicit none

character(len=*),intent(in) :: outpfad
real(8),intent(in) :: vari,strafterm

open (iunit_inv,file = trim(outpfad)//'/LCURV.DAT',status = 'replace',action = 'write')
write (iunit_inv,'(ES10.3,2(2X,ES10.3))') vari,strafterm
close (iunit_inv)

end subroutine write_lcurv



!====================================================================
!  Ergebnisse der Inversion rausschreiben (vmr-Profile)
!====================================================================
subroutine write_parms(outpfad,levels,colair_do,colair_up,tprof,pprof &
  ,nprof,startparms,regparms,actparms,noiskov,nfincycle,maxcycles)

use globfwd23 , only : instr,mw
use prof96_m

implicit none

character(len=*),intent(in) :: outpfad
real(8),dimension(noflayers),intent(in) :: levels,colair_up,colair_do &
  ,tprof,pprof,nprof
real(8),dimension(maxfulparms),intent(in) :: startparms,regparms,actparms
real(8),dimension(maxfulparms,maxfulparms),intent(in) :: noiskov
integer,intent(in) :: nfincycle,maxcycles

character(2) :: exta
integer :: i,j,imw,actpos_ful
real(8) :: sumcol
real(8),dimension(noflayers-1) :: partcol

! Ausgabe im Origin-Format
open (iunit_inv,file = trim(outpfad)//'/PROFS.DAT',status = 'replace',action = 'write')
! Ausgabe der Gasprofile (letztes Profil evtl. T-Profil)
do i = 1,ntargetspeci
    write(iunit_inv,'(A80)') gaskennung(i)
    do j = 1,noflayers
        actpos_ful = mpos_ful(i) + j - 1
        write(iunit_inv,'(ES11.4,5(2X,ES11.4))') levels(j),regparms(actpos_ful),startparms(actpos_ful) &
          ,actparms(actpos_ful),sqrt(noiskov(actpos_ful,actpos_ful)),1.0d-6 * nprof(j) * actparms(actpos_ful)
    end do
end do
close (iunit_inv)

! Ausgabe der Teilsaeulen (log. Interpolation von nprof, lin. Interpolation von vmr)
open (iunit_inv,file = trim(outpfad)//'/PARTCOL.DAT',status = 'replace',action = 'write')
do i = 1,ntargetspeci
    actpos_ful = mpos_ful(i)
    do j = 1,noflayers - 1
        partcol(j) = 1.0d-6 * (actparms(actpos_ful+j-1) * colair_up(j) &
          + actparms(actpos_ful+j) * colair_do(j+1))
    end do
    ! Ergebnisse fuer Gas i rausschreiben
    write (iunit_inv,'(A80)') gaskennung(i)
    sumcol = 0.0d0
    do j = 1,noflayers - 1
        sumcol = sumcol + partcol(j)
        write(iunit_inv,'(ES11.4,3(2X,ES11.4))') levels(j),levels(j+1),partcol(j),sumcol
    end do
end do
close (iunit_inv)

! Ausgabe von Hilfsgroessen
open (iunit_inv,file = trim(outpfad)//'/AUXPARMS.DAT',status = 'replace',action = 'write')


! Number of iteration and max number of iterations
write (iunit_inv,'(A)') 'Actual and max number of iterations:'
write (iunit_inv,'(A)') '$'
write (iunit_inv,'(I4)') nfincycle
write (iunit_inv,'(I4)') maxcycles
write (iunit_inv,'(A)') ''
write (iunit_inv,'(A)') ''
write (iunit_inv,'(A)') ''

! Number of MWs
write (iunit_inv,'(A)') 'Number of MWs'
write (iunit_inv,'(A)') '$'
write (iunit_inv,'(I4)') nofmws
write (iunit_inv,'(A)') ''
write (iunit_inv,'(A)') ''
write (iunit_inv,'(A)') ''

! Mean value in measurement and RMS per MW (windowed)
write (iunit_inv,'(A)') 'Mean value and RMS per MW (windowed)'
write (iunit_inv,'(A)') '$'
do imw = 1,nofmws
    write(iunit_inv,'(2X,ES10.3,2X,ES10.3)') meanmess_mw(imw),sqrt(vari_mw(imw))
end do
write (iunit_inv,'(A)') ''
write (iunit_inv,'(A)') ''
write (iunit_inv,'(A)') ''

! Which parameters are fitted?
write (iunit_inv,'(A)') 'Requested fit parameters:'
write (iunit_inv,'(A)') 'Tgnd'
write (iunit_inv,'(A)') 'ILS linear modulation amp'
write (iunit_inv,'(A)') 'ILS constant phase'
write (iunit_inv,'(A)') 'channeling'
write (iunit_inv,'(A)') 'spectral abscissa scale'
write (iunit_inv,'(A)') '$'
write (iunit_inv,'(L1)') tfitdec
write (iunit_inv,'(L1)') ilsmodfitdec
write (iunit_inv,'(L1)') ilsphasfitdec
write (iunit_inv,'(L1)') chanfitdec
write (iunit_inv,'(L1)') shiftfitdec
write (iunit_inv,'(A)') ''
write (iunit_inv,'(A)') ''
write (iunit_inv,'(A)') ''

actpos_ful = npos_ful(ntargetspeci) + 1

! Tgnd
write (iunit_inv,'(A)') 'Tgnd:'
write (iunit_inv,'(A)') '$'
if (tfitdec) then
    write(iunit_inv,'(ES10.3)') actparms(actpos_ful)
    actpos_ful = actpos_ful + 1
end if
write (iunit_inv,'(A)') ''
write (iunit_inv,'(A)') ''
write (iunit_inv,'(A)') ''

! ILS Parameters
write (iunit_inv,'(A)') 'ILS parameters:'
write (iunit_inv,'(A)') '$'
if (ilsmodfitdec) then
    write(iunit_inv,'(ES10.3)') actparms(actpos_ful)
    actpos_ful = actpos_ful + 1
end if
if (ilsphasfitdec) then
    write(iunit_inv,'(ES10.3)') actparms(actpos_ful)
    actpos_ful = actpos_ful + 1
end if
write (iunit_inv,'(A)') ''
write (iunit_inv,'(A)') ''
write (iunit_inv,'(A)') ''

! ggf. Channeling
write (iunit_inv,'(A)') 'Channeling:'
write (iunit_inv,'(A)') 'nof channeling frequencies'
write (iunit_inv,'(A)') 'f1...fN (along row)'
write (iunit_inv,'(A)') 'Acos1 Asin1 ... AcosN AsinN (for each MW)'
write (iunit_inv,'(A)') '$'
if (chanfitdec) then
    write (iunit_inv,'(I3)') nofchans
    do i = 1,nofchans
        write (iunit_inv,'(ES10.3)') instr%chanlambda(i)
    end do
    do imw = 1,nofmws
       write (iunit_inv,'(2X,ES10.3,200(2X,ES10.3))') (actparms(actpos_ful+i-1),i = 1,2 * nofchans)
       actpos_ful = actpos_ful + 2 * nofchans
    end do
end if
write (iunit_inv,'(A)') ''
write (iunit_inv,'(A)') ''
write (iunit_inv,'(A)') ''

! ggf. Shift
write (iunit_inv,'(A)') 'Spectral scale per MW:'
write (iunit_inv,'(A)') '$'
if (shiftfitdec) then
    do imw = 1,nofmws
        write(iunit_inv,'(ES10.3)') actparms(actpos_ful)
        actpos_ful = actpos_ful + 1
    end do
end if
write (iunit_inv,'(A)') ''
write (iunit_inv,'(A)') ''
write (iunit_inv,'(A)') ''

close (iunit_inv)

! Ausgabe der einzelnen vmr-Profile
do i = 1,nvmrspeci
    write (exta,'(I2.2)') i

    open (iunit_inv,file = trim(outpfad)//'/GAS'//exta//'.PRF',status = 'replace',action = 'write')

    write(iunit_inv,'(A80)') gaskennung(i)
    write(iunit_inv,'(A1)') ' '
    write(iunit_inv,'(A1)') '$'
    write(iunit_inv,'(I3)') noflayers

    write(iunit_inv,'(A1)') ' '
    write(iunit_inv,'(A1)') ' '
    write(iunit_inv,'(A13)') 'altitude [km]'
    write(iunit_inv,'(A1)') '$'
    write(iunit_inv,'(5(1X,F8.3))') (1.0d-3 * levels(j),j = 1,noflayers)

    write(iunit_inv,'(A1)') ' '
    write(iunit_inv,'(A1)') ' '
    write(iunit_inv,'(A10)') 'vmr [ppmv]'
    write(iunit_inv,'(A1)') '$'
    write(iunit_inv,'(5(1X,ES11.4))') (actparms(mpos_ful(i)+j-1),j = 1,noflayers)

    close (iunit_inv)

end do

! Blockfile mit pT
if (tfitdec) then
    open (iunit_inv,file = trim(outpfad)//'/PT_OUT.PRF',status ='replace',action = 'write')

    write(iunit_inv,'(A1)') '$'
    write(iunit_inv,'(I3)') noflayers

    write(iunit_inv,'(A1)') ' '
    write(iunit_inv,'(A1)') ' '
    write(iunit_inv,'(A13)') 'altitude [km]'
    write(iunit_inv,'(A1)') '$'
    write(iunit_inv,'(5(1X,F7.3))') (1.0d-3 * levels(j),j = 1,noflayers)

    write(iunit_inv,'(A1)') ' '
    write(iunit_inv,'(A1)') ' '
    write(iunit_inv,'(A14)') 'pressure [hPa]'
    write(iunit_inv,'(A1)') '$'
    write(iunit_inv,'(5(1X,ES11.4))') (1.0d-2 * pprof(i),i = 1,noflayers)

    write(iunit_inv,'(A1)') ' '
    write(iunit_inv,'(A1)') ' '
    write(iunit_inv,'(A15)') 'temperature [K]'
    write(iunit_inv,'(A1)') '$'
    write(iunit_inv,'(5(1X,F6.1))') (tprof(i),i = 1,noflayers)

    close (iunit_inv)
end if

! Table File mit pT
open (iunit_inv,file = trim(outpfad)//'/PT_OUT.DAT',status ='replace',action = 'write')
do i = 1,noflayers
    write (iunit_inv,'(1X,ES10.3,1X,ES11.4,1X,ES10.3)') levels(i),pprof(i),tprof(i)
end do
close (iunit_inv)

end subroutine write_parms



!====================================================================
!  Ergebnisse rausschreiben (Spektrum, einspaltig)
!====================================================================
subroutine write_spec(outpfad,spec)

use prof96_m

implicit none

character(len=*),intent(in) :: outpfad
real(8),dimension(npos_spec(nofmws)),intent(in) :: spec

character(1) :: extension
integer :: i,imw,mpos

do imw = 1,nofmws
    extension = char(64+imw)
    mpos = mpos_spec(imw)
    open (iunit_inv,file = trim(outpfad)//'/CALCSP'//extension//'.DAT',status = 'replace',action = 'write')
    do i = 1,ptsinmw(imw)
        write (iunit_inv,'(ES13.7,3(2X,ES12.5))') firstwvnr(imw) + dwvnr * (i - 1) &
          ,spec(mpos+i-1)
    end do
    close (iunit_inv)
end do

end subroutine write_spec



!====================================================================
!  Ergebnisse der Inversion rausschreiben (Spektren)
!====================================================================
subroutine write_specs(outpfad,messpec,calcspec,weightspec)

use prof96_m

implicit none

character(len=*),intent(in) :: outpfad
real(8),dimension(npos_spec(nofmws)),intent(in) :: messpec,calcspec,weightspec

character(1) :: extension
integer :: i,imw,mpos

do imw = 1,nofmws
    extension = char(64+imw)
    mpos = mpos_spec(imw)
    open (iunit_inv,file = trim(outpfad)//'/INVSPEC'//extension//'.DAT',status = 'replace',action = 'write')
    do i = 1,ptsinmw(imw)
        write (iunit_inv,'(ES13.7,3(2X,ES12.5))') firstwvnr(imw) + dwvnr * (i - 1) &
          ,messpec(mpos+i-1),calcspec(mpos+i-1),messpec(mpos+i-1) - calcspec(mpos+i-1)
    end do
    close (iunit_inv)
    if (ddwdec .or. ndww(imw) .gt. 0) then
        open (iunit_inv,file = trim(outpfad)//'/WEIGHTS'//extension//'.DAT',status = 'replace',action = 'write')
        do i = 1,ptsinmw(imw)
            write (iunit_inv,'(ES13.7,1(2X,ES12.5))') firstwvnr(imw) + dwvnr * (i - 1),weightspec(mpos+i-1)
        end do
        close (iunit_inv)
    end if
end do

end subroutine write_specs



!====================================================================
!  Ergebnisse der Inversion rausschreiben (Kovarianzmatrix Parameterraum)
!====================================================================
subroutine write_vari(outpfad,varip_raw,varip_lin)

use prof96_m

implicit none

character(len=*),intent(in) :: outpfad
real(8),dimension(maxredparms,maxredparms),intent(in) :: varip_raw,varip_lin

integer :: i,j,k

open (iunit_inv,file = trim(outpfad)//'/FVAR_RAW.DAT',status = 'replace',action = 'write')
open (iunit_inv+1,file = trim(outpfad)//'/FVAR_LIN.DAT',status = 'replace',action = 'write')
do i = 1,maxredparms
    write(iunit_inv,'(ES10.3,2000(1X,ES10.3))') (varip_raw(i,j),j = 1,maxredparms)
    write(iunit_inv+1,'(ES10.3,2000(1X,ES10.3))') (varip_lin(i,j),j = 1,maxredparms)
end do
close (iunit_inv+1)
close (iunit_inv)

! Rausschreiben der Bloecke auf der Diagonalen
open (iunit_inv,file = trim(outpfad)//'/FVARI.DAT',status = 'replace',action = 'write')
do i = 1,ntargetspeci
    if (method(i) .ne. 0) then
        write(iunit_inv,*) i,i
        do j = 1,noflayers
            write(iunit_inv,'(ES10.3,200(1X,ES10.3))') &
              (varip_raw(mpos_red(i)+j-1,mpos_red(i)+k-1),k = 1,noflayers)
        end do
    end if
end do
close (iunit_inv)

end subroutine write_vari
