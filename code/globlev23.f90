!     Last change:  BLA  22 May 2010    6:31 pm
module globlev23

use globfwd23, only: maxlev,maxtau

implicit none

type levels
    sequence
    real(8),dimension(maxlev) :: T_K,p_Pa,T_K_ref
    real(8),dimension(maxlev) :: h2o_ppmv,h_m
    real(8),dimension(maxlev) :: n_cbm,colair,colair_do,colair_up,sza_rad
    real(8),dimension(maxlev) :: wind_scal ! factor for scaling of spectral abscissa due to winds
    real(8),dimension(maxlev,maxtau) :: vmr_ppmv
    real(8),dimension(maxlev,maxtau) :: vmr_ref_ppmv
end type

type (levels) :: lev

end module globlev23
