!     Last change:  BLA  11 Oct 2012    6:40 pm
module globfwd23

implicit none

! Fixed parameters
integer,parameter :: max_n_mw = 20        ! max number of MWs handled by the forward code at a time
integer,parameter :: maxspecies = 150     ! max number of species which are supported
integer,parameter :: maxlev = 50          ! max number of levels
integer,parameter :: maxtau = 20          ! max number of species handled by the forward code at a time
integer,parameter :: maxiso = 10          ! max number of isotopologues of a HITRAN species handeable
integer,parameter :: maxlines = 2000000   ! max number of lines of any species in all MWs
integer,parameter :: maxsollines = 20000  ! max number of solarlines in all MW
integer,parameter :: n_ifg = 20           ! Sampling of modulation in IFG for ILS description
integer,parameter :: max_n_chan = 4       ! max number of channeling frequencies
integer,parameter :: max_n_base = 100     ! max number of baseline knot points
integer,parameter :: max_n_eps = 20       ! max number of eps values
integer,parameter :: iunitoffset_fwd = 10 ! Steuerung Parallel-Batching


! Quantities which are set during runtime
! n_lev: Number of model levels
! n_mw: number of microwindows
! n_tau: number of species for optical thikness calculation
! n_species: number of line-by-line species which are recognized
character(150),dimension(maxtau) :: hitdatei,vmrdatei,vmrrefdatei
character(150),dimension(max_n_mw) :: messdatei
character(150) :: ptdatei,h2odatei,specidatei,soldatei,winddatei
character(150) :: inppfad_fwd,inppfad_inv,outpfad_fwd,outpfad_inv,wrkpfad
character(50) :: lblkennung,sdgdatei

logical :: messdec,h2ofwddec,h2ofitdec,fwdtofiledec,calcfwddec,emissdec,winddec,decrimall,tgcdec
integer :: n_lev,n_mw,n_tau_lbl,n_tau_xsc,n_tau,n_species,n_chan,n_eps,messkind,ptrh2o,iunit_fwd,npp
integer,dimension(maxtau) :: iso_handle
integer,dimension(maxiso,maxtau) :: iso_kennung
real(8),dimension(max_n_mw,maxtau) :: skalspeci  ! spectral scale correction for each species

! SI and further constants
real(8),parameter :: pi = 3.141592653589793d0
real(8),parameter :: amunit = 1.6605d-27
real(8),parameter :: kboltz = 1.3807e-23
real(8),parameter :: clicht = 299792458.0d0
real(8),parameter :: Rearth_pol = 6.35675d6
real(8),parameter :: Rearth_equat = 6.37814d6

real(8),parameter :: radtograd = 57.2957795130823d0
real(8),parameter :: mudry = 28.97d0
real(8),parameter :: muh2o = 18.02d0
real(8),parameter :: ggswinkel = 0.7854d0  ! Winkel fuer Gegenstrahlung, hier auf 45 Grad gesetzt
real(8),parameter :: dgdh = - 3.086d-6 ! grav. accel reduces with altitude (per meter)
! Konstanten zur Berechnung der Planckfunktion in nW / cm2 sterad cm-1
! B(nue)= PlanckA * nue**3 / (exp(PlanckB * nue / TKel) - 1.0d0)
real(8),parameter :: PlanckA = 1.191043d-3  
real(8),parameter :: PlanckB = 1.438776

! Datentypen
type instrument
    sequence
    logical :: caldec,noisdec
    integer :: apokind,ilsacc
    real :: opdmax,coneifm_rad,semiextfov_rad
    real :: apo,phas
    real,dimension(0:n_ifg-1,2) :: ilsparms
    real,dimension(max_n_chan) :: chanlambda
    complex,dimension(max_n_chan,max_n_mw) :: chanamp
    real,dimension(max_n_mw) :: ofset,noise
end type

type microwindow
    sequence
    integer :: pts,lb,rb
    integer :: ptsfinext,lbfinext,rbfinext
    integer :: ptsisaext,lbisaext,rbisaext
    integer :: n_base            ! number of baseline coeffs per MW
    real(8) :: firstnue_input,lastnue_input
    real(8) :: firstnue,lastnue,centernue,firstnuefinext,lastnuefinext
    real(8) :: solskalkorr,shift
    real :: tau_cont
    real,dimension(max_n_base) :: base_knot  ! parameters to describe empirical baseline
    real,dimension(1 + 3 * (max_n_base - 1)) :: solsk ! sampling solar BB for continuum evaluation
    real,dimension(1 + 3 * (max_n_base - 1)) :: base_cont ! points sampled on smoothed baseline
end type

type observer
    sequence
    logical :: astrodec
    real(8),dimension(max_n_eps) :: nue_eps
    real(8),dimension(max_n_eps) :: epsgnd
    real(8) :: h_m,Tgnd_K,lat_rad,lon_rad
    real(8) :: sza_rad,azi_rad,JDdate_d,solskalephem
end type

type speciesinfos
    sequence
    integer :: identifier
    real(8) :: masseamu,qa,qb,qc,qd,qe
end type

type wvnrskal
    sequence
    integer :: ilsradius,totradius,interpol,isa,ils
    integer :: nsolstep
    real(8) :: maxshift,dnue,dnueils,dnuefine,dnuerim
    real(8) :: nueraytra
end type

type derivative
    sequence
    logical :: sw_vmr,sw_HITgam,sw_HITint,sw_T &
      ,sw_LOS,sw_solshi,sw_solint,sw_ILSapo,sw_ILSphas,sw_shift,sw_ofset,sw_chan,sw_base
    integer :: n_vmr,maxnbase !maxnbase: largest number of baseline gridpoints which occurs in any MW
    integer,dimension(maxtau) :: npoint_fwd
end type

! Allocate types
type (instrument) instr
type (microwindow) mw(max_n_mw)
type (observer) obs
type (speciesinfos) species(maxspecies)
type (wvnrskal) wvskal
type (derivative) deri

end module globfwd23
