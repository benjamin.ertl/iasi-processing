!     Last change:  BLA  30 Nov 2011    5:16 pm
!====================================================================
!
!  globale Variablen
!
!====================================================================

module prof96_m

use globfwd23 , only : maxtau,maxlev,max_n_chan,max_n_mw

implicit none

! Most parameters adopted from settings in forward model

integer,parameter :: nmaxtarget = maxtau + 1
integer,parameter :: nmaxlayers = maxlev
integer,parameter :: nmaxchan = max_n_chan
integer,parameter :: nmaxmw = max_n_mw
integer,parameter :: ndwwmax = 40
integer,parameter :: iunitoffset_inv = 40

! Beruecksichtigung bei Konstruktion der reduzierten Jakobimatrix
real(8),parameter :: shiftunit = 1.0d-5
real(8),parameter :: baseunit = 0.001d0
real(8),parameter :: ilsunit = 0.01d0
real(8),parameter :: chanunit = 0.01d0
real(8),parameter :: pdflimit = 1.0d-8

character(150) :: pppfad
character(150),dimension(nmaxtarget) :: gaskennung
logical,dimension(nmaxmw,nmaxtarget) :: deczerojakobi

logical :: tfitdec,chanfitdec,basefitdec,shiftfitdec,solfitdec,ilsmodfitdec &
  ,ilsphasfitdec,jakoutdec,rmsfixdec,gainoutdec,errmodedec,globoedec,ddwdec &
  ,limitshiftdec,stopshiftdec,errsmoothdec
logical,dimension(nmaxtarget) :: logdec
integer :: ntargetspeci,nvmrspeci,maxfulparms,maxredparms,nerrparms,nerrgroups &
  ,noflayers,nofmws,nofchans,nofchans_err,nevalsT,ierrspeci,nerrcols,nbasetot,iunit_inv
logical,dimension(nmaxlayers,nmaxtarget) :: coldecs,fulpdfdecs
integer,dimension(nmaxtarget) :: mpos_red,npos_red,mpos_ful,npos_ful,method,nintercorr
integer,dimension(nmaxtarget,nmaxtarget) :: intercorr
integer,dimension(nmaxmw) :: mpos_spec,npos_spec,ptsinmw,ndww
real(8) :: dwvnr,conv_scal,rms_fix,regsol,regshift,rms_ddw,meanmess_allmws,limitshift
real(8),dimension(nmaxmw) :: firstwvnr,regbase,vari_mw,meanmess_mw
real(8),dimension(ndwwmax,nmaxmw) :: vw_steep
real(8),dimension(nmaxlayers,nmaxtarget) :: regpdf
complex(8),dimension(ndwwmax,nmaxmw) :: vw_bnds
real(8),dimension(4,nmaxtarget) :: reg

end module prof96_m
