!     Last change:  BLA   3 Feb 2009    5:21 pm
module globrim23

use globfwd23, only: maxrim,max_n_mw,maxtau,maxlev

implicit none

real,parameter :: tauschwell_rim = 1.0e-5  ! min tau at MW bounds to trigger use of tau_rim
real(8),parameter :: mwextd_contrib_rim = 30.0d0 ! extension of MW sets outer limit of lines contributing to taurim [cm-1]

logical :: allrimdec ! only lbl-calculation, no rim handling, no precalculation of tau_rim
logical,dimension(maxtau,max_n_mw) :: decrim ! does a lbl-species contribute to tau_rim?
integer :: n_prim,n_Trim,n_lev_rim
integer,dimension(maxlev) :: levptr_prim,levptr_Trim
real,dimension(maxrim) :: p_rim,T_rim
real(8) :: mwextd_calc_rim ! extension of MW bounds for taurim table [cm-1]


end module globrim23
