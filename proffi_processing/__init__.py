import os

from configparser import ConfigParser, ExtendedInterpolation


def _setup_configuration():
    conf = ConfigParser(interpolation=ExtendedInterpolation())
    if os.environ.get("DATA"):
        conf['Common'] = {"data_dir": os.environ.get("DATA")}
    else:
        conf['Common'] = {"data_dir": ''}

    return conf

config = _setup_configuration()
