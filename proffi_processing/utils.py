import os
import glob
import shutil
import logging

import pandas as pd
import numpy as np
import netCDF4
# import paramiko

import xarray as xr

from datetime import datetime
from functools import lru_cache

from proffi_processing import config

logger = logging.getLogger('proffi-processing')


@lru_cache(maxsize=128)
def load_iremis_data_opt(date, iremis_dir):
    d = datetime.strptime(str(int(date)), "%Y%m%d")
    basename = "global_emis_inf10_monthFilled_MYD11C3.A"

    filename = basename + str(d.year) + str((d.month - 1) * 3).zfill(2) + '*'

    logger.debug("look up {}".format(os.path.join(iremis_dir, filename)))

    f = glob.glob(os.path.join(iremis_dir, filename))
    if not f:
        logger.warning("no IREMIS data")
        return None

    logger.debug("read IREMIS data netcdf {}".format(f[0]))
    return xr.open_dataset(f[0])


def load_iremis_data(date, iremis=dict()):
    if iremis:
        d1 = datetime.strptime(str(int(iremis['date'])), "%Y%m%d")
        d2 = datetime.strptime(str(int(date)), "%Y%m%d")

        if d1.month != d2.month:
            # load new emissivity file
            filename = config.get('iremis', 'basename_netcdf') + str(d2.year)\
                + str((d2.month - 1) * 3).zfill(2) + '*'

            logger.debug("look up {}".format(os.path.join(
                config.get('iremis', 'data_directory'), filename)))

            f = glob.glob(os.path.join(config.get('iremis', 'data_directory'),
                          filename))

            if not f:
                logger.warning("no IREMIS data")
                iremis['emissivity'] = None
                iremis['file'] = None
                return iremis

            logger.debug("read changed IREMIS data netcdf {}".format(f[0]))
            nc_emi = netCDF4.Dataset(f[0])
            emissivity = dict()
            emissivity['wavenumber'] = nc_emi['wavenumber'][...][::-1]
            emissivity['emis_flag'] = nc_emi['emis_flag'][...]
            for i in range(1, 11):
                emissivity['emis' + str(i)] = nc_emi['emis' + str(i)][...]
            nc_emi.close()

            iremis['emissivity'] = emissivity
            iremis['file'] = f[0]
            iremis['date'] = date
        else:
            logger.debug("reuse IREMIS data netcdf {}".format(iremis['file']))
        return iremis
    else:
        logger.debug("read IREMIS location netcdf {}".format(
            config.get('iremis', 'location_netcdf')))

        f = config.get('iremis', 'location_netcdf')
        if not f or not os.path.exists(f):
            logger.warning("no IREMIS data")
            return {"location_lat": None, "location_lon": None,
                    "date": date, "emissivity": None, "file": None}

        # nc_location = netCDF4.Dataset(f)
        location_lat = np.linspace(89.975, -89.975, num=3600, dtype=np.float32)
        # nc_location['lat'][0]
        location_lon = np.linspace(-179.975, 179.975, num=7200,
                                   dtype=np.float32)
        # nc_location['lon'][:, 0]
        # nc_location.close()

        d = datetime.strptime(str(int(date)), "%Y%m%d")
        filename = config.get('iremis', 'basename_netcdf') + str(d.year)\
            + str((d.month - 1) * 3).zfill(2) + '*'

        logger.debug("look up {}".format(os.path.join(
            config.get('iremis', 'data_directory'), filename)))

        f = glob.glob(os.path.join(config.get('iremis', 'data_directory'),
                      filename))

        if not f:
            logger.warning("no IREMIS data")
            return {"location_lat": location_lat, "location_lon": location_lon,
                    "date": date, "emissivity": None, "file": None}

        logger.debug("read IREMIS data netcdf {}".format(f[0]))
        nc_emi = netCDF4.Dataset(f[0])
        emissivity = dict()
        emissivity['wavenumber'] = nc_emi['wavenumber'][...][::-1]
        emissivity['emis_flag'] = nc_emi['emis_flag'][...]
        for i in range(1, 11):
            emissivity['emis' + str(i)] = nc_emi['emis' + str(i)][...]
        nc_emi.close()

        return {"location_lat": location_lat, "location_lon": location_lon,
                "date": date, "emissivity": emissivity, "file": f[0]}


def group_netcdf_input(nc_files, obs_time=0.075):
    # nc_files = list(os.scandir(path_to_nc_files))
    nc_files = glob.glob(nc_files)
    files = [(os.path.basename(x), os.path.abspath(x), os.stat(x).st_size)
             for x in nc_files]

    df = pd.DataFrame(files, columns=['name', 'path', 'size'])

    obs = []
    for f in nc_files:
        nc = netCDF4.Dataset(f)
        obs.append(len(nc.dimensions['event']))
        nc.close()

    df['#obs'] = obs
    df['time'] = np.ceil(df['#obs'] * obs_time / 60.)
    df_grouped = df.groupby('time')

    return df_grouped


def list_src_dest(nc_files, dest_dir, rel=True):
    output = ''
    df_grouped = group_netcdf_input(nc_files)
    for k in df_grouped.groups.keys():
        g = df_grouped.get_group(k)
        for _, row in g.iterrows():
            if rel:
                output += "{} {}\n".format(os.path.relpath(row['path']),
                                           os.path.join(dest_dir, str(int(k)),
                                                        row['name']))
            else:
                output += "{} {}\n".format(row['path'], os.path.join(dest_dir,
                                           str(int(k)), row['name']))

    return output


def copy_grouped_netcdf_input(df_grouped, dest_dir):
    for k in df_grouped.groups.keys():
        g = df_grouped.get_group(k)
        logger.debug("mkdir {}".format(os.path.join(dest_dir, str(int(k)))))
        os.mkdir(os.path.join(dest_dir, str(int(k))))
        for _, row in g.iterrows():
            logger.debug("copy {}".format(os.path.join(dest_dir, str(int(k)),
                         row['name'])))
            shutil.copy(row['path'], os.path.join(dest_dir, str(int(k))))


# def upload_grouped_netcdf_input(df_grouped, sftp_server, sftp_user,
#                                 sftp_key, sftp_path, sftp_port=22):
#
#     t = paramiko.Transport((sftp_server, 22))
#     key = paramiko.RSAKey.from_private_key_file(sftp_key)
#     t.connect(username=sftp_user, pkey=key)
#     sftp = paramiko.SFTPClient.from_transport(t)
#
#     for k in df_grouped.groups.keys():
#         g = df_grouped.get_group(k)
#         logger.debug("mkdir {}".format(os.path.join(sftp_path, str(int(k)))))
#         sftp.mkdir(os.path.join(sftp_path, str(int(k))))
#         for _, row in g.iterrows():
#             logger.debug("upload {}".format(
#                 os.path.join(sftp_path, str(int(k)), row['name'])))
#             sftp.put(row['path'], os.path.join(
#                 sftp_path, str(int(k)), row['name']))
#
#     return df_grouped


def create_result_netcdf(nc):
    nc.proffi_retrieval_version = 'Version 3.2c (2019)'

    nc.createDimension("event")
    nc.createDimension("atmospheric_grid_levels", 29)
    nc.createDimension("atmospheric_species", 2)
    nc.createDimension("emissivity_specgrid", 20)
    nc.createDimension("spectral_bins", 841)
    nc.createDimension("fit_quality_dim", 5)

    nc_processor_major_version = nc.createVariable(
        "L2_processor_major_version", "u1", ("event",))
    nc_processor_major_version.description =\
        ("L2 (EUMETSAT netcdf) processor major version")

    nc_product_minor_version = nc.createVariable(
        "L2_product_minor_version", "u1", ("event",))
    nc_product_minor_version.description =\
        ("L2 (EUMETSAT netcdf) product minor version")

    # along_track
    nc_along_track = nc.createVariable("along_track", "u2", ("event",))
    nc_along_track.description = "along track satellite observation index"
    # across_track
    nc_across_track = nc.createVariable("across_track", "u1", ("event",))
    nc_across_track.description = "across track satellite observation index"
    # srf_flag
    nc_srf_flag = nc.createVariable("srf_flag", "u1", ("event",))
    nc_srf_flag.description = ("0:water, 1:land low, 2:land high, "
                               "3:land water low, 4:land water high, "
                               "5:sea ice")
    # cld_cov
    nc_cld_cov = nc.createVariable("cld_cov", "f4", ("event",))
    nc_cld_cov.description = "L2 cloud coverage [%]"
    # cld_ph
    nc_cld_ph = nc.createVariable("cld_ph", "u1", ("event",))
    nc_cld_ph.description = "L2 cloud phase (1 = liquid, 2 = ice, 3 = mixed)"
    # cld_tp
    nc_cld_tp = nc.createVariable("cld_tp", "f4", ("event",))
    nc_cld_tp.description = "L2 cloud top pressure [Pa]"
    # cld_tt
    nc_cld_tt = nc.createVariable("cld_tt", "f4", ("event",))
    nc_cld_tt.description = "L2 cloud top temperature [K]"
    # Date
    nc_date = nc.createVariable("Date", "u4", ("event",))
    nc_date.description = "UT Date [YYYYMMDD]"
    # IASI_spectrum_freq
    nc_iasi_spectrum_freq = nc.createVariable("IASI_spectrum_freq", "f4",
                                              ("spectral_bins",))
    nc_iasi_spectrum_freq.description = "frequency of spectra [cm-1]"
    # IASI_spectrum_meas
    nc_iasi_spectrum_meas = nc.createVariable("IASI_spectrum_meas", "f4",
                                              ("event", "spectral_bins",))
    nc_iasi_spectrum_meas.description =\
        "measured IASI spectra [nW/(sr*cm^2*cm-1)]"
    # IASI_spectrum_simu
    nc_iasi_spectrum_simu = nc.createVariable("IASI_spectrum_simu", "f4",
                                              ("event", "spectral_bins",))
    nc_iasi_spectrum_simu.description =\
        "simulated IASI spectra [nW/(sr*cm^2*cm-1)]"
    # LT_hours
    nc_lt_hours = nc.createVariable("LT_hours", "f4", ("event",))
    nc_lt_hours.description = "decimal hours of local time"
    # Time
    nc_time = nc.createVariable("Time", "u4", ("event",))
    nc_time.description = "UT Time [hhmmss]"
    # UT_hours
    nc_ut_hours = nc.createVariable("UT_hours", "f4", ("event",))
    nc_ut_hours.description = "UT decimal hours"
    # atm_H2O_EUML2
    nc_atm_h2o_euml2 = nc.createVariable("atm_H2O_EUML2", "f4",
                                         ("event", "atmospheric_grid_levels",),)
    nc_atm_h2o_euml2.description =\
        ("EUMETSAT L2 H2O profiles ({ln[H2O]} "
         "[natural logarithms of mixing ratios in ppmv])")
    # tropopause_altitude
    nc_trop_altitude = nc.createVariable("tropopause_altitude", "f4",
                                         ("event",),)
    nc_trop_altitude.description =\
        ("Climatological tropopause altitude (period Jan./1990 - Dec./2014) "
         "obtained from CESM1 (WACCM) with specified dynamics (MERRA) "
         "[m a.s.l.]")
    # atm_altitude
    nc_atm_altitude = nc.createVariable("atm_altitude", "f4",
                                        ("event", "atmospheric_grid_levels",),)
    nc_atm_altitude.description = "atmospheric altitude levels [m a.s.l.]"
    # atm_nol
    nc_atm_nol = nc.createVariable("atm_nol", "u1", ("event",))
    nc_atm_nol.description = "number of actual atmospheric grid levels"
    # atm_pressure
    nc_atm_pressure = nc.createVariable("atm_pressure", "f4",
                                        ("event", "atmospheric_grid_levels",),)
    nc_atm_pressure.description = "atmospheric pressure levels [Pa]"
    # fit_quality
    nc_fit_quality = nc.createVariable("fit_quality", "f4", ("event",
                                       "fit_quality_dim",),)
    nc_fit_quality.description =\
        ("variable 1 (residual-to-signal_max): RMS of spectral fit residual / "
         "maximum spectral value [unitless]\n"
         "variable 2 (residual): RMS of spectral fit residual "
         "[nW/(sr*cm^2*cm-1)]\n"
         "variable 3 (transmisivity residual): RMS of spectral fit residual "
         "of atmospheric transmissivity [unitless]\n"
         "variable 4 (systematic transmisivity residual): same as variable 3, "
         "but for the +/-2cm-1 running mean of the residual [unitless]\n"
         "variable 5 (random transmissivity residual): same as variable 3, "
         "but for the residual that remains after removing the +/-2cm-1 "
         "running mean [unitless]")
    # instrument
    nc_instrument = nc.createVariable("instrument", "u1", ("event",))
    nc_instrument.description = "1: IASI-A; 2: IASI-B; 3: IASI-C"
    # iter
    nc_iter = nc.createVariable("iter", "u1", ("event",), fill_value=0)
    nc_iter.description =\
        "number of iterations needed for the retrieval process"
    # lat
    nc_lat = nc.createVariable("lat", "f4", ("event",))
    nc_lat.description =\
        ("geographical latitude [deg], where north is "
         "positive and south negative")
    # lon
    nc_lon = nc.createVariable("lon", "f4", ("event",))
    nc_lon.description =\
        ("geographical longitude [deg], where east is "
         "positive and west negative")
    # sat_angle
    nc_sat_angle = nc.createVariable("sat_angle", "f4", ("event",))
    nc_sat_angle.description = "satellite instrument viewing angle [deg]"
    # sfc_emi
    nc_sfc_emi = nc.createVariable("sfc_emi", "f4",
                                   ("event", "emissivity_specgrid",),)
    nc_sfc_emi.description = "surface emisivity"
    # sfc_emi_wn
    nc_sfc_emi_wn = nc.createVariable("sfc_emi_wn", "f4",
                                      ("emissivity_specgrid",),)
    nc_sfc_emi_wn.description =\
        ("wavenumber scale for surface emissivity data [cm-1]")

    nc_sfc_emi_flag = nc.createVariable("sfc_emi_flag", "i1", ("event",))
    nc_sfc_emi_flag.description =\
        ("Flag_-2 = -2 = Cloud\n"
         "Flag_-1 = -1 = MASUDA\n"
         "Flag_0 = 0 = IREMIS no MOD11 data\n"
         "Flag_1 = 1 = IREMIS baseline fit method was applied\n"
         "Flag_2 = 2 = IREMIS averaged from the 2 adjacent months\n"
         "Flag_3 = 3 = IREMIS annual average\n"
         "Flag_4 = 4 = IREMIS average over the annual average over all emis "
         "with latitude < -80\n"
         "Flag_5 = 5 = ASTER spectral library version 2.0 sea ice emissivity\n"
         "Flag_6 = 6 = EUMETSAT IASI L2 emissivity")

    # flags
    nc_flg_itconv = nc.createVariable("flg_itconv", "u1", ("event",))
    nc_flg_itconv.description =\
        ("0 OEM not attempted\n"
         "1 OEM aborted because first guess residuals too high\n"
         "2 The minimisation did not converge, sounding rejected\n"
         "3 The minimisation did not converge, sounding accepted\n"
         "4 The minimisation converged but sounding rejected\n"
         "5 The minimisation converged, sounding accepted")

    nc_flg_cdlfrm = nc.createVariable("flg_cdlfrm", "u1", ("event",))
    nc_flg_cdlfrm.description =\
        ("0 (all Bits set to 0) No cloud products retrieved\n"
         "Bit 1 = 1 Height assignment performed with NWP forecasts\n"
         "Bit 2 = 1 Height assignment performed with statistical first"
         " guess retrieval\n"
         "Bit 3 = 1 Cloud products retrieved with the CO 2 -slicing\n"
         "Bit 4 = 1 Cloud products retrieved with the χ 2 method\n"
         "Bits 5-8 Spare")

    nc_flg_numit = nc.createVariable("flg_numit", "u1", ("event",))
    nc_flg_numit.description = ("Number of iterations in the OEM")

    nc_flg_dustcld = nc.createVariable("flg_dustcld", "u1", ("event",))
    nc_flg_dustcld.description =\
        ("Indicates presence of dust clouds in the IFOV")

    nc_flg_retcheck = nc.createVariable("flg_retcheck", "u2", ("event",))
    nc_flg_retcheck.description =\
        ("Check that geophysical parameters from "
         "the OEM are within bounds")

    nc_flg_resid = nc.createVariable("flg_resid", "u1", ("event",))
    nc_flg_resid.description =\
        ("0:Residual check was not passed-sounding "
         "rejected, 1:Resisual check passed-sounding accepted")

    nc_flg_iasicld = nc.createVariable("flg_iasicld", "u2", ("event",))
    nc_flg_iasicld.description = ("Results of cloud test")

    nc_flg_cldnes = nc.createVariable("flg_cldnes", "u1", ("event",))
    nc_flg_cldnes.description =\
        ("1 The IASI IFOV is clear\n"
         "2 The IASI IFOV is processed as cloud-free but small cloud "
         "contamination possible\n"
         "3 The IASI IFOV is partially covered by clouds\n"
         "4 High or full cloud coverage")

    nc_flg_initia = nc.createVariable("flg_initia", "u1", ("event",))
    nc_flg_initia.description =\
        "Indicates the measurements used in the first guess retrieval"

    # state_GHGatm
    nc_state_ghgatm = nc.createVariable("state_GHGatm", "f4",
                                        ("event", "atmospheric_species",
                                         "atmospheric_grid_levels",),)
    nc_state_ghgatm.description =\
        ("retrieved profiles of atmospheric "
         "greenhouse gas species (state vector {ln[N2O],ln[CH4]} "
         "[natural logarithms of mixing ratios in ppmv])")
    # state_GHGatm_a
    nc_state_ghgatm_a = nc.createVariable("state_GHGatm_a", "f4",
                                          ("event", "atmospheric_species",
                                           "atmospheric_grid_levels",),)
    nc_state_ghgatm_a.description =\
        ("apriori profiles of atmospheric "
         "greenhouse gas species (state vector {ln[N2O],ln[CH4]} "
         "[natural logarithms of mixing ratios in ppmv])")
    # state_GHGatm_n
    nc_state_ghgatm_n = nc.createVariable("state_GHGatm_n", "f4",
                                          ("event", "atmospheric_species",
                                           "atmospheric_species",
                                           "atmospheric_grid_levels",
                                           "atmospheric_grid_levels",),)
    nc_state_ghgatm_n.description =\
        ("state vector noise matrix for greenhouse gases")
    # state_GHGatm_avk
    nc_state_ghgatm_avk = nc.createVariable("state_GHGatm_avk", "f4",
                                            ("event", "atmospheric_species",
                                             "atmospheric_species",
                                             "atmospheric_grid_levels",
                                             "atmospheric_grid_levels",),)
    nc_state_ghgatm_avk.description =\
        ("state vector {ln[N2O],ln[CH4]} "
         "averaging kernel matrix "
         "[for natural logarithmus of mixing ratios in ppmv]")
    # state_Tatm2GHGatm_xavk
    nc_state_tatm2ghgatm_xavk =\
        nc.createVariable("state_Tatm2GHGatm_xavk", "f4",
                          ("event", "atmospheric_species",
                           "atmospheric_grid_levels",
                           "atmospheric_grid_levels",),)
    nc_state_tatm2ghgatm_xavk.description =\
        ("cross averaging kernel matrix "
         "for natural logarithmus of temperature and greenhouse gases")
    # state_Tskin2GHGatm_xavk
    nc_state_tskin2ghgatm_xavk =\
        nc.createVariable("state_Tskin2GHGatm_xavk", "f4",
                          ("event", "atmospheric_species",
                           "atmospheric_grid_levels",),)
    nc_state_tskin2ghgatm_xavk.description =\
        ("cross averaging kernel matrix "
         "for skin temperature and greenhouse gas species")
    # state_HNO3atm
    nc_state_hno3atm =\
        nc.createVariable("state_HNO3atm", "f4",
                          ("event", "atmospheric_grid_levels",),)
    nc_state_hno3atm.description =\
        ("retrieved profiles of atmospheric "
         "greenhouse gas species (state vector {[HNO3]} "
         "[mixing ratios in ppmv])")
    # state_HNO3atm_a
    nc_state_hno3atm_a =\
        nc.createVariable("state_HNO3atm_a", "f4",
                          ("event", "atmospheric_grid_levels",),)
    nc_state_hno3atm_a.description =\
        ("apriori profiles of atmospheric "
         "greenhouse gas species (state vector {[HNO3]} "
         "[mixing ratios in ppmv])")
    # state_HNO3atm_n
    nc_state_hno3atm_n = nc.createVariable("state_HNO3atm_n", "f4",
                                           ("event", "atmospheric_grid_levels",
                                            "atmospheric_grid_levels",),)
    nc_state_hno3atm_n.description = ("state vector noise matrix for HNO3")
    # state_HNO3atm_avk
    nc_state_hno3atm_avk =\
        nc.createVariable("state_HNO3atm_avk", "f4",
                          ("event", "atmospheric_grid_levels",
                           "atmospheric_grid_levels",),)
    nc_state_hno3atm_avk.description =\
        ("state vector {[HNO3]} averaging "
         "kernel matrix [for mixing ratios in ppmv]")
    # state_Tatm2HNO3atm_xavk
    nc_state_tatm2hno3atm_xavk =\
        nc.createVariable("state_Tatm2HNO3atm_xavk", "f4",
                          ("event", "atmospheric_grid_levels",
                           "atmospheric_grid_levels",),)
    nc_state_tatm2hno3atm_xavk.description =\
        ("cross averaging kernel matrix "
         "for natural logarithmus of temperature and HNO3")
    # state_Tskin2HNO3atm_xavk
    nc_state_tskin2hno3atm_xavk =\
        nc.createVariable("state_Tskin2HNO3atm_xavk",
                          "f4", ("event", "atmospheric_grid_levels",),)
    nc_state_tskin2hno3atm_xavk.description =\
        ("cross averaging kernel matrix "
         "for skin temperature and HNO3")
    # state_Tatm
    nc_state_tatm = nc.createVariable("state_Tatm", "f4",
                                      ("event", "atmospheric_grid_levels",),)
    nc_state_tatm.description =\
        "retrieved profiles of atmospheric temperature [K]"
    # state_Tatm_a
    nc_state_tatm_a = nc.createVariable("state_Tatm_a", "f4",
                                        ("event", "atmospheric_grid_levels",),)
    nc_state_tatm_a.description =\
        "apriori profiles of atmospheric temperature [K]"
    # state_Tatm_n
    nc_state_tatm_n = nc.createVariable("state_Tatm_n", "f4",
                                        ("event", "atmospheric_grid_levels",
                                         "atmospheric_grid_levels",),)
    nc_state_tatm_n.description =\
        ("state vector noise matrix for atmospheric temperature")
    # state_Tskin
    nc_state_tskin = nc.createVariable("state_Tskin", "f4", ("event",))
    nc_state_tskin.description = "retrieved surface skin temperature [K]"
    # state_Tskin_a
    nc_state_tskin_a = nc.createVariable("state_Tskin_a", "f4", ("event",))
    nc_state_tskin_a.description = "apriori surface skin temperature [K]"
    # state_Tskin_n
    nc_state_tskin_n = nc.createVariable("state_Tskin_n", "f4", ("event",))
    nc_state_tskin_n.description = "noise for surface skin temperature"
    # state_WVatm
    nc_state_wvatm = nc.createVariable("state_WVatm", "f4",
                                       ("event", "atmospheric_species",
                                        "atmospheric_grid_levels",),)
    nc_state_wvatm.description =\
        ("retrieved profiles of atmospheric "
         "water vapour species (state vector {ln[H2O],ln[HDO]} "
         "[natural logarithms of mixing ratios in ppmv])")
    # state_WVatm_a
    nc_state_wvatm_a = nc.createVariable("state_WVatm_a", "f4",
                                         ("event", "atmospheric_species",
                                          "atmospheric_grid_levels",),)
    nc_state_wvatm_a.description =\
        ("apriori profiles of atmospheric "
         "water vapour species (state vector {ln[H2O],ln[HDO]} "
         "[natural logarithms of mixing ratios in ppmv])")
    # state_WVatm_n
    nc_state_wvatm_n =\
        nc.createVariable("state_WVatm_n", "f4",
                          ("event", "atmospheric_species",
                           "atmospheric_species", "atmospheric_grid_levels",
                           "atmospheric_grid_levels",),)
    nc_state_wvatm_n.description =\
        ("state vector noise matrix "
         "for atmospheric water vapour species")
    # state_WVatm_avk
    nc_state_wvatm_avk =\
        nc.createVariable("state_WVatm_avk", "f4",
                          ("event", "atmospheric_species",
                           "atmospheric_species", "atmospheric_grid_levels",
                           "atmospheric_grid_levels",),)
    nc_state_wvatm_avk.description =\
        ("state vector {ln[H2O],ln[HDO]} averaging kernel matrix "
         "[for natural logarithms of mixing ratios in ppmv]")
    # state_Tatm2WVatm_xavk
    nc_state_tatm2wvatm_xavk =\
        nc.createVariable("state_Tatm2WVatm_xavk", "f4",
                          ("event", "atmospheric_species",
                           "atmospheric_grid_levels",
                           "atmospheric_grid_levels",),)
    nc_state_tatm2wvatm_xavk.description =\
        ("cross averaging kernel matrix "
         "for natural logarithmus of temperature and water vapor")
    # state_Tskin2WVatm_xavk
    nc_state_tskin2wvatm_xavk =\
        nc.createVariable("state_Tskin2WVatm_xavk", "f4",
                          ("event", "atmospheric_species",
                           "atmospheric_grid_levels",),)
    nc_state_tskin2wvatm_xavk.description =\
        ("cross averaging kernel matrix "
         "for skin temperature and water vapor")
    # state_Tatm_avk
    nc_state_tatm_avk =\
        nc.createVariable("state_Tatm_avk", "f4",
                          ("event", "atmospheric_grid_levels",
                           "atmospheric_grid_levels",),)
    nc_state_tatm_avk.description =\
        ("averaging kernel matrix for atmospheric temperature")
    # state_Tskin2Tatm_xavk
    nc_state_tskin2tatm_xavk =\
        nc.createVariable("state_Tskin2Tatm_xavk", "f4",
                          ("event", "atmospheric_grid_levels",),)
    nc_state_tskin2tatm_xavk.description =\
        ("cross averaging kernel matrix "
         "for skin temperature and atmospheric temperature")


def create_L1CL2_netcdf(nc):
    nc.createDimension("event")
    nc.createDimension("spectral_bins", 8700)
    nc.createDimension("L2_atmospheric_pressure_levels", 101)
    nc.createDimension("emissivity_specgrid", 20)
    nc.createDimension("L2_emissivity_specgrid", 12)
    nc.createDimension("num_L2_Tskin", 2)
    nc.createDimension("num_emi_model", 3)

    nc_l2_processor_major_version = nc.createVariable(
        "L2_processor_major_version", "u1", ("event",))
    nc_l2_processor_major_version.description =\
        ("L2 (EUMETSAT netcdf) processor major version")

    nc_l2_product_minor_version = nc.createVariable(
        "L2_product_minor_version", "u1", ("event",))
    nc_l2_product_minor_version.description =\
        ("L2 (EUMETSAT netcdf) product minor version")

    nc_date = nc.createVariable("Date", "u4", ("event",))
    nc_date.description = "UT Date [YYYYMMDD]"

    nc_time = nc.createVariable("Time", "u4", ("event",))
    nc_time.description = "UT Time [hhmmss]"

    nc_ut_hours = nc.createVariable("UT_hours", "f4", ("event",))
    nc_ut_hours.description = "UT decimal hours"

    nc_lt_hours = nc.createVariable("LT_hours", "f4", ("event",))
    nc_lt_hours.description = "decimal hours of local time"

    nc_lon = nc.createVariable("lon", "f4", ("event",))
    nc_lon.description =\
        ("geographical longitude [deg], where east is "
         "positive and west negative")

    nc_lat = nc.createVariable("lat", "f4", ("event",))
    nc_lat.description =\
        ("geographical latitude [deg], where north is "
         "positive and south negative")

    nc_srf_flag = nc.createVariable("srf_flag", "u1", ("event",))
    nc_srf_flag.description =\
        ("0:water, 1:land low, 2:land high, "
         "3:land water low, 4:land water high, 5:sea ice")

    nc_instrument = nc.createVariable("instrument", "u1", ("event",))
    nc_instrument.description = "1: IASI-A; 2: IASI-B; 3: IASI-C"

    nc_sat_angle = nc.createVariable("sat_angle", "f4", ("event",))
    nc_sat_angle.description = "satellite instrument viewing angle [deg]"

    nc_along_track = nc.createVariable("along_track", "u2", ("event",))
    nc_along_track.description = "along track satellite observation index"

    nc_across_track = nc.createVariable("across_track", "u1", ("event",))
    nc_across_track.description = "across track satellite observation index"

    nc_l1c_freq = nc.createVariable("L1C_freq", "f4", ("spectral_bins",))
    nc_l1c_freq.description = "frequency of spectra [cm-1]"

    nc_l1c_radiances = nc.createVariable("L1C_radiances", "f4", ("event",
                                         "spectral_bins",))
    nc_l1c_radiances.description = "measured IASI spectra [nW/(sr*cm^2*cm-1)]"

    nc_l2_atm_pressure = nc.createVariable("L2_atm_pressure", "f4",
                                           ("L2_atmospheric_pressure_levels",),)
    nc_l2_atm_pressure.description =\
        ("EUMETSAT L2 atmospheric pressure grid [hPa]")

    nc_l2_atm_t = nc.createVariable("L2_atm_T", "f4", ("event",
                                    "L2_atmospheric_pressure_levels",),)
    nc_l2_atm_t.description = "EUMETSAT L2 atmospheric temperature output [K]"

    nc_l2_atm_hum = nc.createVariable("L2_atm_hum", "f4", ("event",
                                      "L2_atmospheric_pressure_levels",),)
    nc_l2_atm_hum.description =\
        ("EUMETSAT L2 atmospheric specific humidity output [g/kg]")

    nc_l2_atm_altitude = nc.createVariable("L2_atm_altitude", "f4", ("event",
                                           "L2_atmospheric_pressure_levels",),)
    nc_l2_atm_altitude.description =\
        ("EUMETSAT L2 atmospheric altitude grid "
         "[km above sea level] calculated by assuming hydrostatic equilibrium")

    nc_sfc_altitude = nc.createVariable("sfc_altitude", "f4", ("event",))
    nc_sfc_altitude.description = "surface altitude [m above sea level]"

    nc_sfc_emi_wn = nc.createVariable("sfc_emi_wn", "f4",
                                      ("num_emi_model",
                                       "emissivity_specgrid",),)
    nc_sfc_emi_wn.description =\
        ("wavenumber scale for surface emissivity "
         "data [cm-1] (vector {IREMIS,ASTER,MASUDA})")

    nc_sfc_emi = nc.createVariable("sfc_emi", "f4", ("event",
                                                     "emissivity_specgrid",),)
    nc_sfc_emi.description = "surface emisivity"

    nc_sfc_emi_flag = nc.createVariable("sfc_emi_flag", "i1", ("event",))
    nc_sfc_emi_flag.description =\
        ("Flag_-2 = -2 = Cloud\n"
         "Flag_-1 = -1 = MASUDA\n"
         "Flag_0 = 0 = IREMIS no MOD11 data\n"
         "Flag_1 = 1 = IREMIS baseline fit method was applied\n"
         "Flag_2 = 2 = IREMIS averaged from the 2 adjacent months\n"
         "Flag_3 = 3 = IREMIS annual average\n"
         "Flag_4 = 4 = IREMIS average over the annual average over all emis "
         "with latitude < -80\n"
         "Flag_5 = 5 = ASTER spectral library version 2.0 sea ice emissivity")

    nc_flg_itconv = nc.createVariable("flg_itconv", "u1", ("event",))
    nc_flg_itconv.description =\
        ("0 OEM not attempted\n"
         "1 OEM aborted because first guess residuals too high\n"
         "2 The minimisation did not converge, sounding rejected\n"
         "3 The minimisation did not converge, sounding accepted\n"
         "4 The minimisation converged but sounding rejected\n"
         "5 The minimisation converged, sounding accepted")

    nc_flg_numit = nc.createVariable("flg_numit", "u1", ("event",))
    nc_flg_numit.description = ("Number of iterations in the OEM")

    nc_flg_dustcld = nc.createVariable("flg_dustcld", "u1", ("event",))
    nc_flg_dustcld.description = ("Indicates presence of dust clouds in the "
                                  "IFOV")

    nc_flg_retcheck = nc.createVariable("flg_retcheck", "u2", ("event",))
    nc_flg_retcheck.description = ("Check that geophysical parameters from "
                                   "the OEM are within bounds")

    nc_flg_resid = nc.createVariable("flg_resid", "u1", ("event",))
    nc_flg_resid.description =\
        ("0:Residual check was not passed-sounding "
         "rejected, 1:Resisual check passed-sounding accepted")

    nc_flg_iasicld = nc.createVariable("flg_iasicld", "u2", ("event",))
    nc_flg_iasicld.description = ("Results of cloud test")

    nc_flg_cldnes = nc.createVariable("flg_cldnes", "u1", ("event",))
    nc_flg_cldnes.description =\
        ("1 The IASI IFOV is clear\n"
         "2 The IASI IFOV is processed as cloud-free but small cloud "
         "contamination possible\n"
         "3 The IASI IFOV is partially covered by clouds\n"
         "4 High or full cloud coverage")

    nc_flg_initia = nc.createVariable("flg_initia", "u1", ("event",))
    nc_flg_initia.description =\
        "Indicates the measurements used in the first guess retrieval"

    # cld_tp
    nc_cld_tp = nc.createVariable("cld_tp", "f4", ("event",))
    nc_cld_tp.description = "L2 cloud top pressure [Pa]"
    # cld_tt
    nc_cld_tt = nc.createVariable("cld_tt", "f4", ("event",))
    nc_cld_tt.description = "L2 cloud top temperature [K]"
    # cld_cov
    nc_cld_cov = nc.createVariable("cld_cov", "f4", ("event",))
    nc_cld_cov.description = "L2 cloud coverage [%]"
    # cld_ph
    nc_cld_ph = nc.createVariable("cld_ph", "u1", ("event",))
    nc_cld_ph.description = "L2 cloud phase (1 = liquid, 2 = ice, 3 = mixed)"

    nc_l2_sfc_pressure = nc.createVariable("L2_sfc_pressure", "f4", ("event",),)
    nc_l2_sfc_pressure.description = "L2 surface pressure [Pa]"

    nc_l2_sfc_tskin = nc.createVariable("L2_sfc_Tskin", "f4", ("event",
                                        "num_L2_Tskin",),)
    nc_l2_sfc_tskin.description = ("L2 (EUMETSAT netcdf) surface skin "
                                   "temperature [K]")

    nc_l2_sfc_emi_wn = nc.createVariable("L2_sfc_emi_wn", "f4",
                                         ("L2_emissivity_specgrid",))
    nc_l2_sfc_emi_wn.description = ("wavenumber scale for surface "
                                    "emissivity data [cm-1] for L2 (EUMETSAT)")

    nc_l2_sfc_emi = nc.createVariable("L2_sfc_emi", "f4", ("event",
                                      "L2_emissivity_specgrid",),)
    nc_l2_sfc_emi.description = "L2 (EUMETSAT netcdf) surface emisivity"

    nc_flg_cdlfrm = nc.createVariable("flg_cdlfrm", "u1", ("event",))
    nc_flg_cdlfrm.description =\
        ("0 (all Bits set to 0) No cloud products retrieved\n"
         "Bit 1 = 1 Height assignment performed with NWP forecasts\n"
         "Bit 2 = 1 Height assignment performed with statistical first"
         " guess retrieval\n"
         "Bit 3 = 1 Cloud products retrieved with the CO 2 -slicing\n"
         "Bit 4 = 1 Cloud products retrieved with the χ 2 method\n"
         "Bits 5-8 Spare")


def _split_by_chunks(netcdf, create_netcdf, size=12000):
    _in = netCDF4.Dataset(netcdf)
    total = _in.dimensions['event'].size

    for i in range(0, total, size):
        _out = netCDF4.Dataset(netcdf[:-3] + '_part{}.nc'.format(
            str(i//size)), 'w')
        create_netcdf(_out)

        for variable in _in.variables:
            if 'event' in _in.variables[variable].dimensions:
                _out.variables[variable][:] = _in.variables[variable][i:i+size]
            else:
                _out.variables[variable][:] = _in.variables[variable][:]

        _out.close()

    _in.close()


def _split_by_date(netcdf, create_netcdf, by_year=False, by_month=False):
    _in = netCDF4.Dataset(netcdf)

    dates = np.unique(_in['Date'][...])
    if isinstance(dates, np.ma.core.MaskedArray):
        dates = np.ma.masked_less(dates, 0)
        dates = dates.compressed()

    months = np.unique(np.floor(dates / 100) * 100)
    years = np.unique(np.floor(months / 10000) * 10000)

    if by_year:
        dates = years
    elif by_month:
        dates = months

    for date in dates:
        part = str(int(date))
        if by_year:
            part = part[:-4]
        elif by_month:
            part = part[:-2]

        _out = netCDF4.Dataset(netcdf[:-3] + '_{}.nc'.format(part), 'w')
        create_netcdf(_out)

        if by_year:
            _filter = ((np.floor(_in['Date'][...] / 10000) * 10000) == date)

        elif by_month:
            _filter = ((np.floor(_in['Date'][...] / 100) * 100) == date)

        else:
            _filter = (_in['Date'][...] == date)

        if isinstance(_filter, np.ma.core.MaskedArray):
            _filter = np.ma.masked_less(_filter, 0)
            _filter = _filter.filled(False)

        for v in _in.variables:
            if 'event' in _in[v].dimensions:
                _out[v][...] = _in[v][_filter]
            else:
                _out[v][...] = _in[v][...]

        _out.close()

    _in.close()


def _split_at(netcdf, create_netcdf, position):
    _in = netCDF4.Dataset(netcdf)

    _out0 = netCDF4.Dataset(netcdf[:-3] + '_{}.nc'.format(0), 'w')
    _out1 = netCDF4.Dataset(netcdf[:-3] + '_{}.nc'.format(1), 'w')
    create_netcdf(_out0)
    create_netcdf(_out1)

    for v in _in.variables:
        if 'event' in _in[v].dimensions:
            _out0[v][...] = _in[v][:position]
            _out1[v][...] = _in[v][position:]
        else:
            _out0[v][...] = _in[v][...]
            _out1[v][...] = _in[v][...]

    _out0.close()
    _out1.close()
    _in.close()


def split_results_netcdf_at(netcdf, position):
    _split_at(netcdf, create_result_netcdf, position)


def split_combined_iasi_netcdf_at(netcdf, position):
    _split_at(netcdf, create_L1CL2_netcdf, position)


def split_combined_iasi_netcdf(netcdf, size=12000):
    _split_by_chunks(netcdf, create_L1CL2_netcdf, size=size)


def split_results_netcdf(netcdf, size=12000):
    _split_by_chunks(netcdf, create_result_netcdf, size=size)


def split_combined_iasi_netcdf_date(netcdf, by_year=False, by_month=False):
    _split_by_date(netcdf, create_L1CL2_netcdf, by_year=by_year,
                   by_month=by_month)


def split_results_netcdf_date(netcdf, by_year=False, by_month=False):
    _split_by_date(netcdf, create_result_netcdf, by_year=by_year,
                   by_month=by_month)


def convert_combined_iasi_netcdf(netcdf, suffix="converted"):
    from proffi_processing.preprocessing import _get_sea_surface_emissivity
    from proffi_processing.preprocessing import _get_surface_emissivity
    from proffi_processing.preprocessing import _generate_altitude_profile

    _in = netCDF4.Dataset(netcdf)

    _out = netCDF4.Dataset(netcdf[:-3] + '_{}.nc'.format(suffix), 'w')
    create_L1CL2_netcdf(_out)

    for v in _in.variables:
        try:
            if 'event' in _in[v].dimensions:
                if v == 'flg_cldfrm':
                    _out['flg_cdlfrm'][...] = _in[v][...]
                elif v == 'L2EUM_sfc_emi':
                    emi = np.ma.masked_greater(_in[v][...].data, 1.0)
                    _out['L2_sfc_emi'][...] = emi[:, :12]
                elif v == 'L2EUM_sfc_emi_wn':
                    continue
                elif v == 'L2_sfc_Tskin':
                    _out[v][..., 0] = _in[v][...]
                elif v == 'L2_atm_T':
                    _out[v][..., :90] = _in[v][...]
                elif v == 'L2_atm_hum':
                    _out[v][..., :90] = _in[v][...]
                elif v == 'L2_sfc_emi':
                    continue
                elif v == 'L2EUM_sfc_emi_num':
                    continue
                elif v == 'L2_atm_nol':
                    continue
                elif v == 'oldpcs_flag':
                    continue
                else:
                    _out[v][...] = _in[v][...]
            else:
                if v == 'L2_atm_pressure':
                    _out[v][:90] = _in[v][...]
                elif v == 'L2_sfc_emi_wn':
                    continue
                else:
                    _out[v][...] = _in[v][...]
        except Exception as e:
            logger.debug("ERROR at ", v)

    _out['L2_sfc_emi_wn'][:12] = _in['L2EUM_sfc_emi_wn'][0, :12]

    iremis = dict()
    for i, date in enumerate(_out['Date'][...]):
        if _out['srf_flag'][i] == 0:
            logger.debug("use MASUDA emissivity for event ", i)
            sfc_emi, sfc_emi_wn, sfc_emi_flag = \
                _get_sea_surface_emissivity(_out['sat_angle'][i])
            _out['sfc_emi'][i] = sfc_emi
            _out['sfc_emi_wn'][1] = sfc_emi_wn
            _out['sfc_emi_flag'][i] = sfc_emi_flag
        else:
            logger.debug("use IREMIS emissivity for event ", i)
            # iremis = load_iremis_data(date, iremis, config)
            iremis = load_iremis_data_opt(date, config.get('iremis',
                                          'data_directory'))
            logger.debug("LRU {}".format(load_iremis_data_opt.cache_info()))
            sfc_emi, sfc_emi_wn, sfc_emi_flag = \
                _get_surface_emissivity(date, _out['lat'][i], _out['lon'][i],
                                        iremis=iremis)
            _out['sfc_emi'][i, :len(sfc_emi)] = sfc_emi
            _out['sfc_emi_wn'][0, :len(sfc_emi_wn)] = sfc_emi_wn
            _out['sfc_emi_flag'][i] = sfc_emi_flag

        d = datetime.strptime(str(int(date)), "%Y%m%d")
        if d < datetime(2010, 9, 14):
            _out['L2_processor_major_version'][i] = 4
        elif d < datetime(2014, 9, 30):
            _out['L2_processor_major_version'][i] = 5
        else:
            _out['L2_processor_major_version'][i] = 6

        srf_pres = _out['L2_sfc_pressure'][i]
        srf_height = _out['sfc_altitude'][i]

        df, df_inter =\
            _generate_altitude_profile(_out['lat'][i],
                                       _out['lon'][i], srf_pres, srf_height,
                                       _out['L2_atm_pressure'][...] * 1e2,
                                       _out['L2_atm_T'][i],
                                       _out['L2_atm_hum'][i] / 1e3)

        _out['L2_atm_altitude'][i, :len(df['alt'].values)] =\
            df['alt'].values / 1000
        _out['L2_product_minor_version'][i] = np.nan
        _out['flg_cldnes'][i] = np.nan
        _out['flg_dustcld'][i] = np.nan
        _out['flg_numit'][i] = np.nan
        _out['flg_retcheck'][i] = np.nan
        _out['flg_cdlfrm'][i] = np.nan
        _out['flg_iasicld'][i] = np.nan
        _out['flg_itconv'][i] = np.nan
        _out['flg_resid'][i] = np.nan

    _out.close()
    _in.close()
