import os
import gc
import sys
import time
import traceback
import psutil
import logging
import subprocess
import shutil
import numpy as np

import netCDF4

from scandir import scandir
from operator import is_not
from functools import partial

from concurrent.futures import ProcessPoolExecutor

from proffi_processing import config
from .postprocessing import _get_proffi_result, _write_result_to_netcdf
from .utils import create_result_netcdf

logger = logging.getLogger('proffi-processing')


def _local_process(working_directory, executable):
    logger.debug("start processing {}".format(working_directory))
    logger.debug("CPUs: {}".format(psutil.cpu_percent(percpu=True)))
    logger.debug("RAM used: {} GB".format(psutil.virtual_memory().used / 1e9))
    stdout_path = os.path.join(working_directory, '{}_out'.format(executable))
    stderr_path = os.path.join(working_directory, '{}_err'.format(executable))
    while True:
        with open(stdout_path, 'w') as stdout, open(stderr_path, 'w') as stderr:
            try:
                p = subprocess.Popen('./{}'.format(executable),
                                     cwd=working_directory, stdout=stdout,
                                     stderr=stderr)
                p.wait()
            except OSError as err:
                logger.warning("OSError %s, waiting...", err)
                time.sleep(10)
                continue
            break
    logger.debug("finished processing {}".format(working_directory))
    del p

    #noise = np.genfromtxt(os.path.abspath(config.get('iasi', 'noise')))
    result = _get_proffi_result(working_directory)

    shutil.rmtree(working_directory, ignore_errors=True)
    #del noise

    gc.collect()
    return result


def process_locally(path, processes=None):
    executable = config.get('processing', 'local_exe')

    # num_processes = min(processes, psutil.cpu_count())
    if processes is None:
        logger.debug("using {} processes".format(os.cpu_count()))
    else:
        logger.debug("using {} processes".format(processes))

    with ProcessPoolExecutor(max_workers=processes) as e:
        jobs = [e.submit(_local_process, wd.path, executable)
                for wd in scandir(path) if wd.is_dir()]

        results = []
        for i, job in enumerate(jobs):
            try:
                result = job.result()
                results.append(result)
            except Exception as e:
                print("ERROR for result {} in {}".format(i, path))
                print("-"*60)
                traceback.print_exc(file=sys.stdout)
                print("-"*60)

    results = list(filter(partial(is_not, None), results))
    if not results:
        return

    results = sorted(results, key=lambda d: d['event'])
    # num_events = int(results[-1]['event']) + 1

    s = path + '.nc'
    s = os.path.join(os.path.dirname(s), os.path.basename(s).strip("L1C+L2_"))

    logger.debug("writing {} results to {}".format(str(len(results)), s))

    nc = netCDF4.Dataset(s, 'w', format="NETCDF4")
    create_result_netcdf(nc)

    for result in results:
        _write_result_to_netcdf(result, nc)

    nc.close()
