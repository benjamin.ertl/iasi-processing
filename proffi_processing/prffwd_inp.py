import os
import numpy as np

default_altitudes = np.array([
  -0.41000, 0.00000, 0.39000, 0.83000, 1.30000, 1.82000,
  2.37000, 2.95000, 3.57000, 4.22000, 4.90000, 5.62000, 6.38000,
  7.18000, 8.01000, 8.88000, 9.78000, 10.92000, 12.00000, 13.66000,
  15.96000, 18.31000, 22.10000, 26.22000, 30.69000, 36.34000, 42.37000,
  48.55000, 55.60000
])

hno3_atm_a = np.array([
  8.093e-05, 6.988e-05, 5.937e-05, 5.969e-05, 6.174e-05,
  6.845e-05, 6.908e-05, 6.998e-05, 8.814e-05, 1.061e-04,
  1.081e-04, 1.124e-04, 1.061e-04, 9.979e-05, 1.140e-04,
  1.138e-04, 1.038e-04, 1.406e-04, 1.864e-04, 3.306e-04,
  5.530e-04, 1.016e-03, 4.488e-03, 4.912e-03, 2.835e-03,
  7.778e-04, 1.039e-04, 6.014e-06, 7.104e-07
])

n2o_atm_a = np.array([
  3.286e-01, 3.286e-01, 3.286e-01, 3.287e-01, 3.288e-01,
  3.290e-01, 3.292e-01, 3.294e-01, 3.296e-01, 3.296e-01,
  3.296e-01, 3.296e-01, 3.299e-01, 3.301e-01, 3.301e-01,
  3.299e-01, 3.293e-01, 3.285e-01, 3.278e-01, 3.255e-01,
  3.186e-01, 2.891e-01, 2.148e-01, 1.658e-01, 1.106e-01,
  3.960e-02, 1.339e-02, 6.724e-03, 1.908e-03
])

ch4_atm_a = np.array([
  1.783e+00, 1.781e+00, 1.779e+00, 1.776e+00, 1.771e+00,
  1.771e+00, 1.773e+00, 1.774e+00, 1.775e+00, 1.775e+00,
  1.776e+00, 1.776e+00, 1.778e+00, 1.779e+00, 1.779e+00,
  1.778e+00, 1.777e+00, 1.774e+00, 1.767e+00, 1.769e+00,
  1.721e+00, 1.604e+00, 1.354e+00, 1.218e+00, 1.083e+00,
  7.880e-01, 5.645e-01, 4.356e-01, 3.047e-01
])

co2_atm_a = np.array([
  4.073e+02, 4.074e+02, 4.075e+02, 4.076e+02, 4.074e+02,
  4.076e+02, 4.079e+02, 4.081e+02, 4.083e+02, 4.084e+02,
  4.084e+02, 4.084e+02, 4.086e+02, 4.088e+02, 4.088e+02,
  4.088e+02, 4.086e+02, 4.085e+02, 4.083e+02, 4.079e+02,
  4.073e+02, 4.054e+02, 4.020e+02, 4.008e+02, 4.007e+02,
  4.007e+02, 4.009e+02, 4.009e+02, 4.009e+02
])


def hitran_lbl_default(config):
    path_to_hitran_h2o = os.path.abspath(config.get('hitran', 'h2o'))
    path_to_hitran_co2 = os.path.abspath(config.get('hitran', 'co2'))
    path_to_hitran_hdo = os.path.abspath(config.get('hitran', 'hdo'))
    path_to_hitran_n2o = os.path.abspath(config.get('hitran', 'n2o'))
    path_to_hitran_ch4 = os.path.abspath(config.get('hitran', 'ch4'))
    path_to_hitran_hno3 = os.path.abspath(config.get('hitran', 'hno3'))
    return """{hitran_h2o}
3
011
012
013
{hitran_hdo}
1
014
{hitran_co2}
0
{hitran_n2o}
0
{hitran_ch4}
0
{hitran_hno3}
0""".format(hitran_h2o=path_to_hitran_h2o, hitran_hdo=path_to_hitran_hdo,
            hitran_co2=path_to_hitran_co2, hitran_n2o=path_to_hitran_n2o,
            hitran_ch4=path_to_hitran_ch4, hitran_hno3=path_to_hitran_hno3)


def prffwd_inp(
        lat: 'latitude of observation in rad' = 0.0000,
        lon: 'longitude of observation in rad' = 0.0000,
        semi_fov: 'semi field of view in rad' = 0.00239,
        apparent_angle: 'astronomical(F) or refracted angle(T)' = '.true.',
        julian_date_time: 'time of observation in jdt' = 2451544.5,
        solar_zenith_angle: 'solar zenith angle in rad' = 0.0,
        azimuth_angle: 'azimuth angle in rad' = 0.0,
        number_of_microwindows: 'number of microwindows' = 1,
        microwindows: 'bounds per microwindow (row)' = '(1190.0, 1400.0)',
        mw_bounds_compatible: 'mw compatible' = '.true.',
        spectrum: 'inc or format and file' = '0\n\'INP_FWD/dpt/spectrum.dpt\'',
        number_of_atm_levels: 'number of atmospheric levels' = 28,
        atm_levels: 'atmospheric levels in km' =
              ("0.0 0.388 0.83 1.3 1.82\n2.37 2.95 3.57 4.22 4.9\n"
               "5.62 6.38 7.18 8.01 8.88\n9.78 10.92 12.0 13.66 15.96\n"
               "18.31 22.1 26.22 30.69 36.34\n42.37 48.55 55.6"),
        path_to_pt_prf: 'path to temp press file' = 'INP_FWD/prf/pt.prf',
        h2o_among_selected_species: 'H2O among selected species' = '.false.',
        h2o: 'species number or path to H2O file' = 'INP_FWD/prf/H2O.prf',
        lbl_species_info_file: 'lbl species info file' = 'INP_FWD/species.inf',
        number_of_lbl_species: 'number of lbl species' = 6,
        number_of_xsc_species: 'number of xsc species' = 0,
        hitran_lbl: 'HITRAN lbl file specifications' =
              ("/hitran/linedata/01_hit12_400to1800_s10piHDO.par\n"
               "3\n011\n012\n013\n"
               "/hitran/linedata/01_hit12_400to1800_s10piHDO.par\n"
               "1\n014\n"
               "/hitran/linedata/02_hit08_f53.par\n"
               "0\n"
               "/hitran/linedata/04_hit08.par\n"
               "0\n"
               "/hitran/linedata/06_hit08.par\n"
               "0\n"
               "/hitran/linedata/12_hit08.par\n"
               "0\n"),
        clipping_of_line_contribution: 'clipping of line contribution' = 1.0,
        internal_lbl_grid_resolution: 'internal lbl grid resolution' = 1.0,
        radiative_transfer_grid_resolution: 'grid resolution' = 1.0,
        include_rim_contributions: 'include rim contributions' = '.true.',
        hitran_xsc: 'HITRAN xsc file specifications' = '',
        trace_species: 'spectral scale vs position given in HITRAN' =
             '0.0d0\n0.0d0\n0.0d0\n0.0d0\n0.0d0\n0.0d0',
        prf_files: 'paths to prf files' =
              ("INP_FWD/prf/H2O.prf\nINP_FWD/prf/H2O.prf\n"
               "INP_FWD/prf/CO2.prf\nINP_FWD/prf/N2O.prf\n"
               "INP_FWD/prf/CH4.prf\nINP_FWD/prf/HNO3.prf\n"),
        prf_ref_files: 'paths to prf reference files' =
              ("INP_FWD/prf/H2O.prf\nINP_FWD/prf/H2O.prf\n"
               "INP_FWD/prf/CO2.prf\nINP_FWD/prf/N2O.prf\n"
               "INP_FWD/prf/CH4.prf\nINP_FWD/prf/HNO3.prf\n"),
        ground_temperature: 'ground temperature in K' = 283.15,
        eps_table_dimension: 'dimension of eps table' = 4,
        eps_table: 'emissivity wavenumber and  eps value (row)' =
             "1136.4 0.985\n1190.5 0.984\n1250.0 0.983\n2439.0 0.978",
        continuum_opt_thickness: 'continuum optical thickness' = 0.0,
        atm_self_emission: 'with atmospheric self-emission' = '.true.',
        doppler_shift: 'take doppler shift into account' = '.false.',
        doppler_wind_file: 'path to wind information file' = '',
        opd_max: 'OPDmax in cm' = 2.0,
        apodisation: 'apodiation, 9 = IASI Gaussian' = 9,
        convolution_accuracy: 'ILS accuracy for convolution' = 4,
        internal_semiFOV: 'internal semi field of view in rad' = 1.0e-6,
        apolin: 'ILS apolin' = 1.0,
        phaslin: 'ILS phaslin' = 0.0,
        apo_phase: 'ILS apo and phase (row)' =
                ("1.0000 0.00000\n1.0000 0.00000\n1.0000 0.00000\n"
                 "1.0000 0.00000\n1.0000 0.00000\n1.0000 0.00000\n"
                 "1.0000 0.00000\n1.0000 0.00000\n1.0000 0.00000\n"
                 "1.0000 0.00000\n1.0000 0.00000\n1.0000 0.00000\n"
                 "1.0000 0.00000\n1.0000 0.00000\n1.0000 0.00000\n"
                 "1.0000 0.00000\n1.0000 0.00000\n1.0000 0.00000\n"
                 "1.0000 0.00000\n1.0000 0.00000\n"),
        with_noise: 'add noise to fwd run' = '.false.',
        noise: 'noise level per mw (row)' = '',
        radiance_offset_per_mw: 'radiance offset per mw (row)' = '0.0',
        shift_per_mw: 'shift per mw (row)' = '0.0',
        empirical_continuum_points_per_mw: 'points per mw (row)' = '1',
        continuum_values: 'continuum values' = '1.0',
        number_of_channeling_frequencies: 'channeling frequencies' = 0,
        amplitude_per_frequency: 'amplitude per frequency (row)' ='',
        write_internal_files: 'write internal fwd run files' = '.false.',
        calculate_vmr_derivaties: 'calculate vmr derivaties' = '.true.',
        calculate_p_scale_derivative: 'p scale derivatie' = '.false.',
        calculate_intensity_derivative: 'intens derivative' = '.false.',
        number_of_species: 'number of species' = 6,
        pointer_to_vmr_forward_calculation: 'fwd run selection' =
                "1\n2\n3\n4\n5\n6\n",
        calculate_t_derivatives: 't derivaties' = '.true.',
        calculate_los_derivative: 'LOS derivative' = '.false.',
        calculate_solar_scale_derivative: 'solar scale derivative' = '.false.',
        calculate_ils_derivatives: 'ILS derivatives' = '.false.\n.false.',
        calculate_shift_derivatives: 'shift derivatives' = '.true.',
        calculate_offset_derivative: 'offset derivative' = '.false.',
        calculate_baseline_derivatives: 'baseline derivative' = '.true.',
        calculate_channeling_derivatives: 'channeling derivative' = '.false.'):
    return """Input file forward model

Location of observer:
latitude  [rad]
longitude [rad] (0 Greenwich, pos is towards East!)
$
{lat:.4f}
{lon:.4f}

Observation geometries:
external semiFOV [rad]
astronomical(F) or refracted apparent angle(T)
time of observation (JD, days and fraction)
JD  SZA   AZIMUTH
$
{semi_fov}
{apparent_angle}
{julian_date_time} {solar_zenith_angle:.4f} {azimuth_angle:.4f}

Microwindows:
Number of microwindows
mw bounds
mw bounds
...
$
{number_of_microwindows}
{microwindows}

Spectral abscissa and microwindows:
Should MW bounds made compatible with measurement(T/F)?
If (F): specify spectral increment
(MW bounds readjusted to match grid nue = i * dnue by spectral increment)
If (T):
specify spectrum format (0: dpt / 1: PROFFIT binary / 2: SFIT2 binary)
specify measurement to be used for each MW
$
{mw_bounds_compatible}
{spectrum}

Model atmosphere general:
number of levels
$
{number_of_atm_levels}

level altitudes [km] (lowermost level defines observer altitude)
(for interpolation of vmr input profiles, lowest level defines
 observer altitude)
$
{atm_levels}

file containing T(z)
$
{path_to_pt_prf}

Model atmosphere: H20 (affects hydrostatic balance + self broadening of H2O)
Is H2O among selected species (T), otherwise read H2O from separate file (F)?
If (T):
Specify species number (internal ordering number from below)
If (F):
Specify separate h2o-file
$
{h2o_among_selected_species}
{h2o}

Trace species:
lbl species info file
number of lbl species
number of xsc species
$
{lbl_species_info_file}
{number_of_lbl_species}
{number_of_xsc_species}

For each lbl-file:
HITRAN lbl files
isotopic handle[, isotopes,...]
iso handle: 0 all isos
            1..N number of isos + specify isos
           -1 all the rest, specify starting iso
$
{hitran_lbl}

accuracy of lbl calculation
clipping of line contribution
(standard: 1.0, allowed range: 0.0 (no clipping)... 1.0)
internal lbl-grid grid resolution
(standard: 1.0, allowed range: 0.0 (whole calculation on finest grid)... 1.0)
radiative transfer grid resolution
(standard: 1.0, allowed range: 0.1 ... 1.0)
include rim contributions from lines outside lbl-range(T/F)
$
{clipping_of_line_contribution}
{internal_lbl_grid_resolution}
{radiative_transfer_grid_resolution}
{include_rim_contributions}

HITRAN xsc files
...
$
{hitran_xsc}

Trace species:
spectral scale vs position given in HITRAN, specify (scale - 1.0)
(MW index increases along row, species index increases along column)
$
{trace_species}


Model atmosphere composition:
vmr file
vmr file
...
$
{prf_files}

Model atmosphere reference (determines accuracy of optical
thickness calculation):
vmr file
vmr file
...
$
{prf_ref_files}

NADIR version:
ground T
dimension of eps-table
wave numbers, eps values
$
{ground_temperature}
{eps_table_dimension}
{eps_table}

Continuum opt thickness (towards zenith) and atm self-emission
Take atm self-emission into account (T/F)?
$
{continuum_opt_thickness}
{atm_self_emission}

Should Doppler shift due to wind shear be taken into account (T/F)?
If T:
Give name of file containing wind information
$
{doppler_shift}
{doppler_wind_file}

ILS specification:
OPDmax [cm]
apodisation
        (1: boxcar
         2: triag
         3: Hamming
         4/5: Blackmann-Harris 3-term/4-term
         6/7/8: Norton-Beer weak/medium/strong
         9: IASI Gaussian)
ILS accuracy for convolution (standard: use 100 for boxcar)
internal semiFOV [rad]
$
{opd_max}
{apodisation}
{convolution_accuracy}
{internal_semiFOV}

ILS specification: 2 parameter apolin, phaslin
$
{apolin} {phaslin}

ILS specification: 20 parameter apo, phase
$
{apo_phase}

Should noise be added to forward calculation?
(If T: give noise level per MW (row) in following row)
$
{with_noise}
{noise}

Spectral ordinate: radiance offset per mw (row)
$
{radiance_offset_per_mw}

Spectral abscissa: shift per mw (row)
$
{shift_per_mw}

Number of empirical continuum points in each MW
$
{empirical_continuum_points_per_mw}

Continuum values (row: gridpoint in MW, column: MW index)
$
{continuum_values}

Channeling: Number of channeling frequencies to be considered
frequencies
$
{number_of_channeling_frequencies}

Channeling: (cos,sin) amplitude per frequency (row) and mw (column)
$
{amplitude_per_frequency}

Write internal files generated by forward model to file?
$
{write_internal_files}

****************
Derivatives:

Calculate VMR derivatives(T/F)?
Calculate p-broadening scale derivative(T/F)?
Calculate line intensity derivative(T/F)?
How many species?
$
{calculate_vmr_derivaties}
{calculate_p_scale_derivative}
{calculate_intensity_derivative}
{number_of_species}

For each selected derivative species:
pointer to VMR forward calculation selection
$
{pointer_to_vmr_forward_calculation}

Calculate T derivatives(T/F)?
$
{calculate_t_derivatives}

Calculate LOS derivative(T/F)?
$
{calculate_los_derivative}

Calculate solar scale derivative(T/F)?
$
{calculate_solar_scale_derivative}

Calculate ILS derivatives(T/F)?
$
{calculate_ils_derivatives}

Calculate shift derivatives(T/F)?
$
{calculate_shift_derivatives}

Calculate offset derivative(T/F)?
$
{calculate_offset_derivative}

Calculate baseline derivatives(T/F)?
$
{calculate_baseline_derivatives}

Calculate channeling derivatives(T/F)?
$
{calculate_channeling_derivatives}
""".format(
        lat=lat,
        lon=lon,
        semi_fov=semi_fov,
        apparent_angle=apparent_angle,
        julian_date_time=julian_date_time,
        solar_zenith_angle=solar_zenith_angle,
        azimuth_angle=azimuth_angle,
        number_of_microwindows=number_of_microwindows,
        microwindows=microwindows,
        mw_bounds_compatible=mw_bounds_compatible,
        spectrum=spectrum,
        number_of_atm_levels=number_of_atm_levels,
        atm_levels=atm_levels,
        path_to_pt_prf=path_to_pt_prf,
        h2o_among_selected_species=h2o_among_selected_species,
        h2o=h2o,
        lbl_species_info_file=lbl_species_info_file,
        number_of_lbl_species=number_of_lbl_species,
        number_of_xsc_species=number_of_xsc_species,
        hitran_lbl=hitran_lbl,
        clipping_of_line_contribution=clipping_of_line_contribution,
        internal_lbl_grid_resolution=internal_lbl_grid_resolution,
        radiative_transfer_grid_resolution=radiative_transfer_grid_resolution,
        include_rim_contributions=include_rim_contributions,
        hitran_xsc=hitran_xsc,
        trace_species=trace_species,
        prf_files=prf_files,
        prf_ref_files=prf_ref_files,
        ground_temperature=ground_temperature,
        eps_table_dimension=eps_table_dimension,
        eps_table=eps_table,
        continuum_opt_thickness=continuum_opt_thickness,
        atm_self_emission=atm_self_emission,
        doppler_shift=doppler_shift,
        doppler_wind_file=doppler_wind_file,
        opd_max=opd_max,
        apodisation=apodisation,
        convolution_accuracy=convolution_accuracy,
        internal_semiFOV=internal_semiFOV,
        apolin=apolin,
        phaslin=phaslin,
        apo_phase=apo_phase,
        with_noise=with_noise,
        noise=noise,
        radiance_offset_per_mw=radiance_offset_per_mw,
        shift_per_mw=shift_per_mw,
        empirical_continuum_points_per_mw=empirical_continuum_points_per_mw,
        continuum_values=continuum_values,
        number_of_channeling_frequencies=number_of_channeling_frequencies,
        amplitude_per_frequency=amplitude_per_frequency,
        write_internal_files=write_internal_files,
        calculate_vmr_derivaties=calculate_vmr_derivaties,
        calculate_p_scale_derivative=calculate_p_scale_derivative,
        calculate_intensity_derivative=calculate_intensity_derivative,
        number_of_species=number_of_species,
        pointer_to_vmr_forward_calculation=pointer_to_vmr_forward_calculation,
        calculate_t_derivatives=calculate_t_derivatives,
        calculate_los_derivative=calculate_los_derivative,
        calculate_solar_scale_derivative=calculate_solar_scale_derivative,
        calculate_ils_derivatives=calculate_ils_derivatives,
        calculate_shift_derivatives=calculate_shift_derivatives,
        calculate_offset_derivative=calculate_offset_derivative,
        calculate_baseline_derivatives=calculate_baseline_derivatives,
        calculate_channeling_derivatives=calculate_channeling_derivatives)
