"""Preprocessing module"""
import os
import re
import math
import shutil
import time
# import psutil
import logging
import subprocess
from operator import is_not
from functools import partial

from datetime import datetime, timedelta
from concurrent.futures import ProcessPoolExecutor

import netCDF4
import julian

import numpy as np
import pandas as pd
import xarray as xr

# from scandir import scandir
from scipy.interpolate import interp1d

from dateutil import tz

from proffi_processing import config

from .utils import create_L1CL2_netcdf
from .prffwd_inp import (prffwd_inp, hitran_lbl_default, hno3_atm_a,
                         n2o_atm_a, ch4_atm_a, co2_atm_a)

logger = logging.getLogger('proffi-processing')

heights_of_interest = {'default':
                       np.array([
                           0.00, 0.39, 0.83, 1.30, 1.82, 2.37, 2.95,
                           3.57, 4.22, 4.90, 5.62, 6.38, 7.18, 8.01, 8.88,
                           9.78, 10.92, 12.00, 13.66, 15.96, 18.31, 22.10,
                           26.22, 30.69, 36.34, 42.37, 48.55, 55.60])
                       * 1000.,
                       'KA':
                       np.array([
                           0.000, 0.490, 0.910, 1.370, 1.870, 2.400,
                           2.980, 3.590, 4.230, 4.920, 5.630, 6.390,
                           7.190, 8.010, 8.880, 9.780, 10.920, 12.000,
                           13.660, 15.960, 18.310, 22.100, 26.220, 30.690,
                           36.340, 42.370, 48.550, 55.600])
                       * 1000.,
                       'KI':
                       np.array([
                           0.000, 0.420, 0.850, 1.320, 1.830, 2.370,
                           2.940, 3.570, 4.220, 4.900, 5.620, 6.380,
                           7.180, 8.010, 8.880, 9.780, 10.920, 12.000,
                           13.660, 15.960, 18.310, 22.100, 26.220, 30.690,
                           36.340, 42.370, 48.550, 55.600])
                       * 1000.}


iasi_wavenumbers = np.arange(645.0, 2760.25, 0.25)
# emissivity_wavenumbers = np.arange(1180.0, 1420.0, 15.0)
emissivity_wavenumbers = np.linspace(1185.0, 1405.0, 20)

iremis = None

def _compress(array):
    if isinstance(array, np.ma.MaskedArray):
        return np.ma.masked_less(array, 0).compressed()
    return array


def _find_nearest(array, value):
    idx = (np.abs(array-value)).argmin()
    return idx, array[idx]


def _valid_recordings(nc_l1, nc_l2):
    # print("get valid recordings")
    l2_lat = nc_l2['lat']
    l2_lon = nc_l2['lon']
    l1_lat = nc_l1['lat']

    l2_idx, _ = _find_nearest(l2_lat[:, 0], l1_lat[0][0])

    num_along_track = nc_l2.dimensions['along_track'].size
    num_across_track = nc_l2.dimensions['across_track'].size
    num_total = num_along_track * num_across_track

    _filter = np.ones((num_along_track, num_across_track)) == 1
    # valid_indices = set([(i, j) for i in range(num_along_track) \
    #    for j in range(num_across_track)])

    if 'latitude' in config:
        lat_upper = 90.0
        lat_lower = -90.0
        if 'upper_bound' in config['latitude']:
            lat_upper = config.getfloat('latitude', 'upper_bound')
        if 'lower_bound' in config['latitude']:
            lat_lower = config.getfloat('latitude', 'lower_bound')

        logger.debug("filter for lat {}".format((lat_lower, lat_upper)))
        _filter &= ((l2_lat[...] >= lat_lower) & (l2_lat[...] <= lat_upper))
        logger.debug("{} valid l2 indices within bounds".format(_filter.sum()))

    if 'longitude' in config:
        lon_upper = 180.0
        lon_lower = -180.0
        if 'upper_bound' in config['longitude']:
            lon_upper = config.getfloat('longitude', 'upper_bound')
        if 'lower_bound' in config['longitude']:
            lon_lower = config.getfloat('longitude', 'lower_bound')

        logger.debug("filter for lon {}".format((lon_lower, lon_upper)))
        _filter &= ((l2_lon[...] >= lon_lower) & (l2_lon[...] <= lon_upper))
        logger.debug("{} valid l2 indices within bounds".format(_filter.sum()))

    if 'filter_or' in config:
        _filter_or = np.ones((num_along_track, num_across_track)) != 1
        for key in config['filter_or']:
            value = int(config.get('filter_or', key, raw=True))
            key = re.sub(r'_[0-9]*$', '', key)
            if key in nc_l2.variables:
                logger.debug("filter for {} == {}".format(key, value))
                if len(nc_l2[key][...].shape) == 3:
                    _filter_or |= ((nc_l2[key][:, :, 0] == value))
                    logger.debug("{} valid l2 indices after filter".format(
                        _filter_or.sum()))
                else:
                    _filter_or |= ((nc_l2[key][...] == value))
                    logger.debug("{} valid l2 indices after filter".format(
                        _filter_or.sum()))
        if _filter_or.any():
            _filter &= _filter_or
            logger.debug("{} valid l2 indices after filter_or".format(
                _filter.sum()))

    if 'filter_and' in config:
        _filter_and = np.ones((num_along_track, num_across_track)) == 1
        for key in config['filter_and']:
            value = int(config.get('filter_and', key, raw=True))
            key = re.sub(r'_[0-9]*$', '', key)
            if key in nc_l2.variables:
                if key == 'fractional_cloud_cover':
                    logger.debug("filter for {} >= {}".format(key, value))
                    if len(nc_l2[key][...].shape) == 3:
                        _filter_and &= ((nc_l2[key][:, :, 0] >= value))
                        logger.debug("{} valid l2 indices after filter".format(
                            _filter_and.sum()))
                    else:
                        _filter_and &= ((nc_l2[key][...] >= value))
                        logger.debug("{} valid l2 indices after filter".format(
                            _filter_and.sum()))
                else:
                    logger.debug("filter for {} == {}".format(key, value))
                    if len(nc_l2[key][...].shape) == 3:
                        _filter_and &= ((nc_l2[key][:, :, 0] == value))
                        logger.debug("{} valid l2 indices after filter".format(
                            _filter_and.sum()))
                    else:
                        _filter_and &= ((nc_l2[key][...] == value))
                        logger.debug("{} valid l2 indices after filter".format(
                            _filter_and.sum()))

        _filter &= _filter_and
        logger.debug("{} valid l2 indices after filter_and".format(
            _filter.sum()))

    valid_indices = np.transpose(_filter.nonzero())
    num_valid = len(valid_indices)
    logger.debug("valid l2 indices {} out of {} ({} %)".format(
        num_valid, num_total, num_valid/num_total * 100))

    l2_indices = [(i, j) for (i, j) in valid_indices if i >= l2_idx]
    l1_indices = [(i - l2_idx, j) for (i, j) in l2_indices
                  if (i - l2_idx) < nc_l1.dimensions['along_track'].size]

    logger.debug("valid l1 indices {}".format(len(l1_indices)))
    return l1_indices, l2_indices


def _get_surface_height_opt(lat, lon):
    data_directory = os.path.abspath(config.get('gtopo30', 'data_directory'))
    if lat < -60.0:
        lat_str = str(60)
        lons = np.asarray(list(range(-180, 180, 60)))
        we = 'e' if lon > 60 else 'w'
    else:
        lats = np.asarray(list(range(-60, 140, 50)))
        lons = np.asarray(list(range(-180, 180, 40)))
        lat_idx = np.where(lats <= lat)
        lat_idx = lat_idx[0][-1] + 1
        lat_str = str(np.abs(lats[lat_idx]))
        we = 'e' if lon > 20 else 'w'

    lon_idx = np.where(lons <= lon)
    lon_idx = lon_idx[0][-1]
    lon_str = str(np.abs(lons[lon_idx])).zfill(3)

    ns = 'n' if lat > -10 else 's'

    file_name = 'gt30' + we + lon_str + ns + lat_str + '.nc'
    path = os.path.join(data_directory, file_name)
    # logger.debug("read gtopo30 {} for {}".format(path, (lat,lon)))
    height = 0.0
    try:
        ds = xr.open_dataset(path)
        height = float(ds.__xarray_dataarray_variable__.sel(y=lat, x=lon,
                                                            method='nearest'))
        # height = height if height > 0.0 else 0.0
        height = height if height > -9999 else 0.0
    except Exception as e:
        logger.error("ERROR for lat:{} lon:{}".format(lat, lon))
    return height


def _get_surface_height(lat, lon):
    data_directory = os.path.abspath(config.get('gtopo30', 'data_directory'))
    if lat < -60.0:
        lat_str = str(60)
        lons = np.asarray(list(range(-180, 180, 60)))
        we = 'e' if lon > 60 else 'w'
    else:
        lats = np.asarray(list(range(-60, 140, 50)))
        lons = np.asarray(list(range(-180, 180, 40)))
        lat_idx = np.where(lats <= lat)
        lat_idx = lat_idx[0][-1] + 1
        lat_str = str(np.abs(lats[lat_idx]))
        we = 'e' if lon > 20 else 'w'

    lon_idx = np.where(lons <= lon)
    lon_idx = lon_idx[0][-1]
    lon_str = str(np.abs(lons[lon_idx])).zfill(3)

    ns = 'n' if lat > -10 else 's'

    file_name = 'gt30' + we + lon_str + ns + lat_str + '.tif'
    path = os.path.join(data_directory, file_name)
    # logger.debug("read gtopo30 {} for {}".format(path, (lat,lon)))
    height = 0.0
    try:
        p = subprocess.Popen(
            ['gdallocationinfo', '-valonly', '-wgs84',
             path, str(lon), str(lat)], stdout=subprocess.PIPE)
        value, _ = p.communicate()
        height = float(value.strip())
        height = height if height > 0.0 else 0.0
    except Exception as e:
        logger.error("ERROR for lat:{} lon:{}".format(lat, lon))
    return height


def _get_surface_height_srtm(lat, lon):
    if lon > -29.9895834:
        if lat > -0.0104168:
            srtm = config.get('srtm', 'north_east')
        else:
            srtm = config.get('srtm', 'south_east')
    else:
        srtm = config.get('srtm', 'west')
    try:
        p = subprocess.Popen(
            ['gdallocationinfo', '-valonly', '-wgs84',
             srtm, str(lon), str(lat)], stdout=subprocess.PIPE)
        value, _ = p.communicate()
        value = value.strip()
        height = float(value)
        return height if height > 0.0 else 0.0
    except Exception as e:
        return 0.0


def _generate_altitude_profile(lat, lon, srf_pres, srf_height, atm_pres,
                               atm_temp, atm_hum):
    # molar constants
    molar_mass_h2o = 18  # gmol-1
    molar_mass_dry_air = 28.97  # gmol-1
    mole_ratio_h2o_dry_air = molar_mass_h2o / molar_mass_dry_air
    # gas constant dry air
    R = 287.058
    # gravitational constant at latitude
    g_lat = (9.780327 * (1 + 0.0053024 * np.sin(np.deg2rad(lat))**2
                         - 0.0000058 * np.sin(2 * np.deg2rad(lat))**2))
    # earth radius
    r_earth = 6371. * 1000.

    if isinstance(atm_pres, np.ma.MaskedArray):
        atm_pres = np.ma.masked_less(atm_pres, 0).filled(np.nan)

    df = pd.DataFrame({
        'pres': np.append(atm_pres[atm_pres < srf_pres], srf_pres)})

    if isinstance(atm_temp, np.ma.MaskedArray):
        # srf_temp = atm_temp[~atm_temp.mask][-1]
        atm_temp = np.ma.masked_less(atm_temp, 0)
        srf_temp = atm_temp.compressed()[-1] if atm_temp.compressed().any()\
            else np.nan
        # atm_temp = atm_temp.compressed()
        df['temp'] = np.append(atm_temp[atm_pres < srf_pres]
                               .filled(srf_temp), srf_temp)
    else:
        df['temp'] = np.append(atm_temp[atm_pres < srf_pres], atm_temp[-1])

    if isinstance(atm_hum, np.ma.MaskedArray):
        # srf_hum = atm_hum[~atm_hum.mask][-1]
        atm_hum = np.ma.masked_less(atm_hum, 0)
        srf_hum = atm_hum.compressed()[-1] if atm_hum.compressed().any()\
            else np.nan
        # atm_hum = atm_hum.compressed()
        df['hum'] = np.append(atm_hum[atm_pres < srf_pres]
                              .filled(srf_hum), srf_hum)
        # df['hum'] = np.interp(df['pres'].values, df['pres'].values[:-1],\
        #        atm_hum[atm_pres<srf_pres].filled(np.nan))
    else:
        df['hum'] = np.append(atm_hum[atm_pres < srf_pres], atm_hum[-1])
        # df['hum'] = np.interp(df['pres'].values, df['pres'].values[:-1],\
        #        atm_hum[atm_pres<srf_pres])

    df['hum'] = (df['hum'] / (1 - df['hum']))
    df['pres_delta'] = (df['pres'].shift(-1) - df['pres']).shift(1)
    df['pres_mean'] = df['pres'].rolling(window=2).mean()
    df['temp_virt'] = df['temp'] * (1.0 + 0.6078 * df['hum'].fillna(0.0))
    df['temp_mean'] = df['temp_virt'].rolling(window=2).mean()
    df['delta'] = R * df['pres_delta']/df['pres_mean'] * df['temp_mean']/g_lat

    # height approximated by hydrostatic equilibrium
    #
    # height_i+1 = height_i + (C_i * (r_earth / (r_earth + height_i))**-2)
    # C_i = (pres_delta_i/pres_mean_i) * R * (temp_mean_i / g_lat)

    delta = df['delta'].values[::-1]
    alt = np.array([srf_height])

    for i, d in enumerate(delta):
        alt = np.append(alt, alt[i] + (d * (r_earth/(r_earth + alt[i]))**-2))

    df['alt'] = alt[::-1][1:]

    heights = heights_of_interest['default']
    if 'inv_heights' in config:
        if 'heights' in config['inv_heights']:
            key = config.get('inv_heights', 'heights')
            heights = heights_of_interest.get(key,
                                              heights_of_interest['default'])

    hoi = heights[heights > srf_height + 10]
    hoi = np.insert(hoi, 0, srf_height)

    df_inter = pd.DataFrame({'hoi': hoi})
    df_inter['temp'] = np.interp(hoi,
                                 df['alt'].values[::-1],
                                 df['temp'].values[::-1])
    df_inter['pres'] = np.exp(np.interp(hoi,
                                        df['alt'].values[::-1],
                                        np.log(df['pres'].values[::-1])))
    df_inter['hum'] = np.exp(np.interp(hoi,
                                       df['alt'].values[::-1],
                                       np.log(df['hum'].values[::-1])))

    # convert to mole ratio ppm
    df_inter['hum'] = 1e6 * (df_inter['hum'] / mole_ratio_h2o_dry_air)

    df.fillna(value=-9999.99, inplace=True)
    df_inter.fillna(value=-9999.99, inplace=True)
    return df, df_inter


def _get_surface_emissivity(freq, emi, wn):
    if emi.mask.any():
        emissivity = np.ma.array(data=[-1] * 20, mask=[True] * 20)
    else:
        cof_ems_path = os.path.abspath(config.get('iasi', 'cof_ems'))
        cof_ems = netCDF4.Dataset(cof_ems_path)
        eigenvector = cof_ems['COF_EMS/eigenvector'][...]
        mean = cof_ems['COF_EMS/mean'][...]

        size = mean.size

        emi_mean = np.interp(wn, freq[:size], mean)
        emi_eig = np.array([
            np.interp(wn, freq[:size], eigenvector[i])
            for i in range(5)]).T

        emissivity = (eigenvector.data.T @ np.linalg.inv(emi_eig.T @ emi_eig) @
                      emi_eig.T) @ (emi - emi_mean) + mean

        emissivity = np.interp(emissivity_wavenumbers, freq[:size], emissivity)
        emissivity = np.ma.masked_less(emissivity, 0.0)
    return emissivity, emissivity_wavenumbers


def _get_surface_emissivity_iremis(date, lat, lon):
    # logger.debug("{} closest grid point index to {}".format(\
    #    (lon_idx, lat_idx), (lon, lat)))
    # logger.debug("IREMIS file {}".format(iremis['file']))
    # logger.debug("measurement date {}".format(date))

    emi = np.array([np.nan] * 10)
    emi_wn = np.array([np.nan] * 10)
    emi_flag = 0

    if iremis is not None:
        location_lat = np.linspace(89.975, -89.975, num=3600, dtype=np.float32)
        location_lon = np.linspace(-179.975, 179.975, num=7200,
                                   dtype=np.float32)
        lon_idx = (np.abs(location_lon - lon)).argmin()
        lat_idx = (np.abs(location_lat - lat)).argmin()

        emi = np.array([iremis.emis10[lon_idx, lat_idx],
                        iremis.emis9[lon_idx, lat_idx],
                        iremis.emis8[lon_idx, lat_idx],
                        iremis.emis7[lon_idx, lat_idx],
                        iremis.emis6[lon_idx, lat_idx],
                        iremis.emis5[lon_idx, lat_idx],
                        iremis.emis4[lon_idx, lat_idx],
                        iremis.emis3[lon_idx, lat_idx],
                        iremis.emis2[lon_idx, lat_idx],
                        iremis.emis1[lon_idx, lat_idx]])

        emi_wn = iremis.wavenumber.values[::-1]
        emi_flag = float(iremis.emis_flag[lon_idx, lat_idx].values)

    emi = np.ma.masked_less(emi, 0.0)
    return emi, emi_wn, emi_flag


def _get_sea_surface_emissivity(zenith_angle):
    emissivity = np.array([])
    wavenumbers = np.array([769., 800., 833., 870., 909., 952., 1000., 1042.,
                            1087., 1136., 1190., 1250., 2439., 2500., 2564.,
                            2632., 2703., 2778., 2857.])

    angles = np.array([0., 10., 20., 30., 40., 50., 60.])
    emi = np.array([
        [.97672, .97664, .97616, .97432, .96866, .95324, .91478],
        [.98286, .98280, .98242, .98094, .97624, .96296, .92827],
        [.98892, .98888, .98862, .98758, .98420, .97413, .94589],
        [.99193, .99190, .99171, .99096, .98847, .98086, .95838],
        [.99251, .99248, .99232, .99165, .98947, .98277, .96270],
        [.99132, .99129, .99111, .99040, .98807, .98104, .96035],
        [.98910, .98907, .98886, .98803, .98537, .97747, .95490],
        [.98752, .98748, .98725, .98634, .98345, .97500, .95123],
        [.98625, .98621, .98597, .98500, .98195, .97308, .94844],
        [.98509, .98505, .98479, .98378, .98057, .97135, .94598],
        [.98399, .98395, .98367, .98261, .97928, .96975, .94373],
        [.98305, .98300, .98272, .98162, .97818, .96839, .94186],
        [.97758, .97752, .97718, .97590, .97193, .96091, .93198],
        [.97704, .97698, .97664, .97533, .97132, .96019, .93107],
        [.97638, .97632, .97597, .97465, .97058, .95933, .92997],
        [.97561, .97555, .97519, .97385, .96971, .95832, .92869],
        [.97449, .97443, .97406, .97268, .96846, .95687, .92688],
        [.97323, .97317, .97280, .97138, .96707, .95527, .92490],
        [.97149, .97143, .97104, .96958, .96514, .95307, .92222]])

    for e in emi:
        emissivity = np.append(emissivity, np.interp(zenith_angle, angles, e))

    # emissivity = np.ma.array(emissivity, mask=np.greater(emissivity, 1.0),\
    #         fill_value=-9999.99)

    # emissivity = np.interp(emissivity_wavenumbers, wavenumbers, emissivity)

    return emissivity, wavenumbers, -1


def _get_cloud_emissivity():
    # wavenumbers = np.array([769., 800., 833., 870., 909., 952., 1000., 1042.,
    #                        1087., 1136., 1190., 1250., 2439., 2500., 2564.,
    #                        2632., 2703., 2778., 2857.])

    emissivity = np.array([1.0] * len(emissivity_wavenumbers))

    return emissivity, emissivity_wavenumbers, -2


def _get_sea_ice_emissivity():
    wavenumbers = np.arange(650, 2650, 100)
    emissivity = np.array([0.94891, 0.9393, 0.95938, 0.99422, 0.98774, 0.98428,
                           0.9813, 0.98081, 0.9804, 0.98026, 0.98189, 0.98383,
                           0.98233, 0.98074, 0.97929, 0.97826, 0.9789, 0.97848,
                           0.97722, 0.97572])

    return emissivity, wavenumbers, 5


def _get_netcdf_measurement(idx, nc):
    """Gets a dictionary representation of a single measurement

    This function is used to get a measurment for processing from either the
    initially created combined L1+L2 IASI netCDF file or a already processed
    result netCDF file.

    Parameters
    ----------
    idx : int
        The index representing the event number in the input netCDF file
    nc : str
        The combined L1+L2 IASI or result netCDF file location

    Returns
    -------
    dict
        a dictionary representing a measurement
    """

    measurement = dict()

    measurement['event'] = idx
    measurement['Date'] = nc['Date'][idx]
    measurement['Time'] = nc['Time'][idx]
    measurement['lat'] = nc['lat'][idx]
    measurement['lon'] = nc['lon'][idx]
    measurement['sat_angle'] = nc['sat_angle'][idx]

    measurement['along_track'] = nc['along_track'][idx] \
        if 'along_track' in nc.variables else np.nan
    measurement['across_track'] = nc['across_track'][idx] \
        if 'across_track' in nc.variables else np.nan
    measurement['srf_flag'] = nc['srf_flag'][idx] \
        if 'srf_flag' in nc.variables else np.nan
    measurement['sfc_emi_flag'] = nc['sfc_emi_flag'][idx] \
        if 'sfc_emi_flag' in nc.variables else np.nan
    measurement['cld_cov'] = nc['cld_cov'][idx] \
        if 'cld_cov' in nc.variables else np.nan
    measurement['cld_ph'] = nc['cld_ph'][idx] \
        if 'cld_ph' in nc.variables else np.nan
    measurement['cld_tp'] = nc['cld_tp'][idx] \
        if 'cld_tp' in nc.variables else np.nan
    measurement['cld_tt'] = nc['cld_tt'][idx] \
        if 'cld_tt' in nc.variables else np.nan
    measurement['instrument'] = nc['instrument'][idx] \
        if 'instrument' in nc.variables else np.nan
    measurement['flg_iasicld'] = nc['flg_iasicld'][idx] \
        if 'flg_iasicld' in nc.variables else np.nan
    measurement['flg_cdlfrm'] = nc['flg_cdlfrm'][idx] \
        if 'flg_cdlfrm' in nc.variables else np.nan
    measurement['flg_numit'] = nc['flg_numit'][idx] \
        if 'flg_numit' in nc.variables else np.nan
    measurement['flg_dustcld'] = nc['flg_dustcld'][idx] \
        if 'flg_dustcld' in nc.variables else np.nan
    measurement['flg_retcheck'] = nc['flg_retcheck'][idx] \
        if 'flg_retcheck' in nc.variables else np.nan
    measurement['flg_itconv'] = nc['flg_itconv'][idx] \
        if 'flg_itconv' in nc.variables else np.nan
    measurement['flg_resid'] = nc['flg_resid'][idx] \
        if 'flg_resid' in nc.variables else np.nan
    measurement['flg_cldnes'] = nc['flg_cldnes'][idx] \
        if 'flg_cldnes' in nc.variables else np.nan
    measurement['flg_initia'] = nc['flg_initia'][idx] \
        if 'flg_initia' in nc.variables else np.nan
    measurement['L2_processor_major_version'] = \
        nc['L2_processor_major_version'][idx] \
        if 'L2_processor_major_version' in nc.variables else np.nan
    measurement['L2_product_minor_version'] = \
        nc['L2_product_minor_version'][idx] \
        if 'L2_product_minor_version' in nc.variables else np.nan

    # generate measurements from results netcdf
    if 'atm_H2O_EUML2' in nc.variables:
        measurement['state_Tskin_a'] = _compress(nc['state_Tskin_a'][idx])
        measurement['sfc_emi'] = nc['sfc_emi'][idx]
        measurement['sfc_emi_wn'] = nc['sfc_emi_wn'][...]
        measurement['IASI_spectrum_freq'] = nc['IASI_spectrum_freq'][...]
        measurement['IASI_spectrum_meas'] = nc['IASI_spectrum_meas'][idx]
        measurement['atm_nol'] = nc['atm_nol'][idx]
        measurement['atm_altitude'] = _compress(nc['atm_altitude'][idx])
        measurement['atm_pressure'] = _compress(nc['atm_pressure'][idx])
        measurement['state_Tatm_a'] = _compress(nc['state_Tatm_a'][idx])
        measurement['state_H2Oatm_a'] =\
            _compress(np.exp(nc['atm_H2O_EUML2'][idx]))
        measurement['state_HNO3atm_a'] = _compress(nc['state_HNO3atm_a'][idx])
        measurement['state_N2Oatm_a'] =\
            _compress(np.exp(nc['state_GHGatm_a'][idx][0]))
        measurement['state_CH4atm_a'] =\
            _compress(np.exp(nc['state_GHGatm_a'][idx][1]))
        measurement['state_CO2atm_a'] =\
            co2_atm_a[co2_atm_a.size-int(nc['atm_nol'][idx]):]

    # generate measurements from L1C+L2 combined netcdf
    else:
        measurement['IASI_spectrum_freq'] = nc['L1C_freq'][...]
        measurement['IASI_spectrum_meas'] = nc['L1C_radiances'][idx]

        cloud = (measurement['flg_cldnes'] == 4)

        if cloud:
            measurement['state_Tskin_a'] = measurement['cld_tt']
        else:
            sfc_temp = nc['L2_sfc_Tskin'][idx]
            if isinstance(sfc_temp, np.ma.core.MaskedArray):
                sfc_temp = np.ma.masked_less(sfc_temp, 0)
                measurement['state_Tskin_a'] = sfc_temp.compressed()[0]\
                    if sfc_temp.compressed().any() else np.nan
            else:
                measurement['state_Tskin_a'] =\
                    np.ravel(np.array([sfc_temp]))[0]

        # cloud emissivity
        if measurement['sfc_emi_flag'] == -2:
            # logger.debug('use cloud emissivity')
            sfc_emi_wn = nc['sfc_emi_wn'][0].compressed()
            sfc_emi = nc['sfc_emi'][idx].compressed()

            sfc_emi = np.interp(emissivity_wavenumbers, sfc_emi_wn, sfc_emi)
            measurement['sfc_emi_wn'] = emissivity_wavenumbers
        # MASUDA emissivity
        elif measurement['sfc_emi_flag'] == -1:
            # logger.debug('use MASUDA emissivity')
            sfc_emi_wn = nc['sfc_emi_wn'][2].compressed()
            sfc_emi = nc['sfc_emi'][idx].compressed()

            sfc_emi = np.interp(emissivity_wavenumbers, sfc_emi_wn, sfc_emi)
            measurement['sfc_emi_wn'] = emissivity_wavenumbers
        # ASTER emissivity
        elif measurement['sfc_emi_flag'] == 5:
            # logger.debug('use ASTER emissivity')
            sfc_emi_wn = nc['sfc_emi_wn'][1].compressed()
            sfc_emi = nc['sfc_emi'][idx].compressed()

            sfc_emi = np.interp(emissivity_wavenumbers, sfc_emi_wn, sfc_emi)
            measurement['sfc_emi_wn'] = emissivity_wavenumbers
        else:
            if measurement['L2_processor_major_version'] == 6:
                # logger.debug('IASI version 6, use L2 emissivity')
                sfc_emi = nc['L2_sfc_emi'][idx]
                sfc_emi_wn = nc['L2_sfc_emi_wn'][...]

                sfc_emi, sfc_emi_wn =\
                    _get_surface_emissivity(measurement['IASI_spectrum_freq'],
                                            sfc_emi, sfc_emi_wn)
                measurement['sfc_emi_flag'] = 6
            else:
                # logger.debug('old IASI version, use IREMIS emissivity')
                sfc_emi = nc['sfc_emi'][idx].compressed()
                sfc_emi_wn = nc['sfc_emi_wn'][0].compressed()

                sfc_emi, sfc_emi_wn =\
                    _get_surface_emissivity(measurement['IASI_spectrum_freq'],
                                            sfc_emi, sfc_emi_wn)

            measurement['sfc_emi_wn'] = sfc_emi_wn

        if isinstance(sfc_emi, np.ma.core.MaskedArray):
            if not sfc_emi.compressed().any()\
              or (sfc_emi.compressed() < 0.0).any():
                logger.warning("no emissivity for event {}".format(idx))
                return None
            else:
                measurement['sfc_emi'] = sfc_emi.compressed()
        else:
            measurement['sfc_emi'] = sfc_emi

        srf_pres = nc['L2_sfc_pressure'][idx]
        if isinstance(srf_pres, np.ma.core.MaskedConstant):
            srf_pres = np.nan

        srf_height = nc['sfc_altitude'][idx]

        df, df_inter = _generate_altitude_profile(
            measurement['lat'], measurement['lon'], srf_pres, srf_height,
            nc['L2_atm_pressure'][...] * 1e2, nc['L2_atm_T'][idx],
            nc['L2_atm_hum'][idx] / 1e3)

        if cloud:
            cld_tp = measurement['cld_tp']
            df_cloud = pd.concat([
                df_inter[df_inter['pres'] > cld_tp],
                pd.DataFrame({'pres': cld_tp,
                              'hoi': np.interp(
                                  np.log(cld_tp),
                                  np.log(df_inter['pres'].values[::-1]),
                                  df_inter['hoi'].values[::-1]),
                              'temp': np.interp(
                                  np.log(cld_tp),
                                  np.log(df_inter['pres'].values[::-1]),
                                  df_inter['temp'].values[::-1]),
                              'hum': np.interp(
                                  np.log(cld_tp),
                                  np.log(df_inter['pres'].values[::-1]),
                                  df_inter['hum'].values[::-1])},
                             index=[len(df_inter[df_inter['pres'] > cld_tp])]),
                df_inter[df_inter['pres'] < cld_tp]]).reset_index(drop=True)
            df_inter = df_cloud[np.where(np.isclose(df_cloud['pres'],
                                                    cld_tp))[0][0]:]

            # correction for grid level
            if (df_inter.shift(-1)-df_inter).iloc[0].hoi < 10.0:
                df_inter = df_inter[1:]

        measurement['atm_nol'] = len(df_inter)
        measurement['atm_altitude'] = df_inter['hoi'].values
        measurement['atm_pressure'] = df_inter['pres'].values
        measurement['state_Tatm_a'] = df_inter['temp'].values
        measurement['state_H2Oatm_a'] = df_inter['hum'].values
        measurement['state_HNO3atm_a'] =\
            hno3_atm_a[hno3_atm_a.size-len(df_inter):]
        measurement['state_N2Oatm_a'] =\
            n2o_atm_a[n2o_atm_a.size-len(df_inter):]
        measurement['state_CH4atm_a'] =\
            ch4_atm_a[ch4_atm_a.size-len(df_inter):]
        measurement['state_CO2atm_a'] =\
            co2_atm_a[co2_atm_a.size-len(df_inter):]

        measurement['vprofile'] = df
        measurement['vprofile_comp'] = df_inter

    return measurement


def _get_iasi_measurement(idx, nc_l1, nc_l2, l1_idx, l2_idx):
    """Gets a dictionary representation of a single measurement

    This function is used to get a measurement from original IASI L1 and L2
    netCDF files to be written to the combined L1+L2 netCDF file.

    Parameters
    ----------
    idx : int
        The index representing the event number in the input netCDF file
    nc_l1 : str
        The L1 IASI netCDF file location
    nc_l2: str
        The L2 IASI netCDF file location
    l1_idx : (int, int)
        The tuple L1 index for a valid IASI measurement
    l2_idx : (int, int)
        The tuple L2 index for a valid IASI measurement

    Returns
    -------
    dict
        a dictionary representing a measurement
    """

    # print(l1_idx, l2_idx)

    # check for L1 spectrum
    spectrum = nc_l1['gs_1c_spect'][l1_idx]
    if isinstance(spectrum, np.ma.core.MaskedArray):
        if not spectrum.data.any():
            logger.warning("no spectrum for l1 {} l2 {}".format(
                l1_idx, l2_idx))
            return None

    # check for cloud == 4
    if 'flag_cldnes' in nc_l2.variables:
        cloud = (nc_l2['flag_cldnes'][l2_idx] == 4)
    else:
        cloud = False

    # check for surface pressure
    srf_pres = nc_l2['surface_pressure'][l2_idx]
    if isinstance(srf_pres, np.ma.core.MaskedConstant):
        logger.warning("no surface pressure for l1 {} l2 {}".format(
            l1_idx, l2_idx))
        return None

    # check for surface temperature
    srf_temp = nc_l2['surface_temperature'][l2_idx]
    if (isinstance(srf_temp, np.ma.core.MaskedArray)
            or isinstance(srf_temp, np.ma.core.MaskedConstant))\
            and not cloud:

        if not srf_temp.compressed().any():
            logger.warning("no surface temperature for l1 {} l2 {}".format(
                l1_idx, l2_idx))
            return None

    # check for atmospheric temperature
    atm_temp = nc_l2['atmospheric_temperature'][l2_idx]
    if isinstance(atm_temp, np.ma.core.MaskedArray) and not cloud:
        if not atm_temp.compressed().any():
            logger.warning("no atmospheric temperature for l1 {} l2 {}"
                           .format(l1_idx, l2_idx))
            return None

    # check for resid
    # flg_resid = nc_l2['flg_resid'][l2_idx]
    # if flg_resid != 1:
    #     logger.warning("flg_resid for l1 {} l2 {} = {}".format(\
    #         l1_idx, l2_idx, flg_resid))
    #     return None
    # check for itconv for version 6
    if 'flag_itconv' in nc_l2.variables and not cloud:
        flg_itconv = float(nc_l2['flag_itconv'][l2_idx])
        if flg_itconv != 5 and flg_itconv != 3:
            logger.warning("flg_itconv for l1 {} l2 {} = {}".format(
                l1_idx, l2_idx, flg_itconv))
            return None

    measurement = dict()
    measurement['event'] = idx

    if 'flg_resid' in nc_l2.variables:
        measurement['flg_resid'] = nc_l2['flg_resid'][l2_idx]
    if 'flg_iasicld' in nc_l2.variables:
        measurement['flg_iasicld'] = nc_l2['flg_iasicld'][l2_idx]
    if 'flag_cldnes' in nc_l2.variables:
        measurement['flg_cldnes'] = nc_l2['flag_cldnes'][l2_idx]
    if 'flg_itconv' in nc_l2.variables:
        measurement['flg_itconv'] = nc_l2['flg_itconv'][l2_idx]
    elif 'flag_itconv' in nc_l2.variables:
        measurement['flg_itconv'] = nc_l2['flag_itconv'][l2_idx]
    if 'flag_dustcld' in nc_l2.variables:
        measurement['flg_dustcld'] = nc_l2['flag_dustcld'][l2_idx]
    if 'flag_numit' in nc_l2.variables:
        measurement['flg_numit'] = nc_l2['flag_numit'][l2_idx]
    if 'flag_retcheck' in nc_l2.variables:
        measurement['flg_retcheck'] = nc_l2['flag_retcheck'][l2_idx]
    if 'flg_landsea' in nc_l2.variables:
        measurement['srf_flag'] = nc_l2['flg_landsea'][l2_idx]
    elif 'flag_landsea' in nc_l2.variables:
        measurement['srf_flag'] = nc_l2['flag_landsea'][l2_idx]
    if 'flg_cdlfrm' in nc_l2.variables:
        measurement['flg_cdlfrm'] = nc_l2['flg_cdlfrm'][l2_idx]
    elif 'flag_cdlfrm' in nc_l2.variables:
        measurement['flg_cdlfrm'] = nc_l2['flag_cdlfrm'][l2_idx]
    if 'flag_initia' in nc_l2.variables:
        measurement['flg_initia'] = nc_l2['flag_initia'][l2_idx]

    if nc_l1['measurement_date'].shape[1] == 120:
        seconds = float(nc_l1['measurement_date'][l1_idx])
    else:
        seconds = float(nc_l1['measurement_date'][l1_idx[0], int(l1_idx[1]/4)])

    date2000 = datetime(2000, 1, 1, 0, 0, 0)
    date = date2000 + timedelta(seconds=seconds)
    julian_date_time = julian.to_jd(date)
    date = julian.from_jd(float(julian_date_time))
    measurement['Date'] = float(date.strftime("%Y%m%d"))
    measurement['Time'] = float(date.strftime("%H%M%S"))
    measurement['lat'] = nc_l1['lat'][l1_idx]
    measurement['lon'] = nc_l1['lon'][l1_idx]
    date = date.replace(tzinfo=tz.tzutc())
    measurement['UT_hours'] =\
        (date.hour + (date.minute / 60.) + date.second / 3600.)
    measurement['LT_hours'] = measurement['UT_hours'] + (measurement['lon']/15)
    if measurement['LT_hours'] < 0.0:
        measurement['LT_hours'] += 24.0
    if measurement['LT_hours'] >= 24.0:
        measurement['LT_hours'] -= 24.0

    measurement['sat_angle'] = nc_l1['pixel_zenith_angle'][l1_idx]
    measurement['along_track'] = l2_idx[0]
    measurement['across_track'] = l2_idx[1]
    measurement['IASI_spectrum_freq'] = np.arange(645.0, 2820.0, 0.250)
    measurement['IASI_spectrum_meas'] = nc_l1['gs_1c_spect'][l1_idx]

    cld_tp = nc_l2['cloud_top_pressure'][l2_idx]
    if isinstance(cld_tp, np.ma.MaskedArray):
        measurement['cld_tp'] = cld_tp.compressed()[0]\
            if cld_tp.compressed().any() else np.nan
    else:
        measurement['cld_tp'] = cld_tp[0]
    cld_tt = nc_l2['cloud_top_temperature'][l2_idx]
    if isinstance(cld_tt, np.ma.MaskedArray):
        measurement['cld_tt'] = cld_tt.compressed()[0]\
            if cld_tt.compressed().any() else np.nan
    else:
        measurement['cld_tt'] = cld_tt[0]
    measurement['cld_ph'] = nc_l2['cloud_phase'][l2_idx][0]
    cld_cov = nc_l2['fractional_cloud_cover'][l2_idx][0]
    if isinstance(cld_cov, np.ma.core.MaskedConstant):
        measurement['cld_cov'] = np.nan
    else:
        measurement['cld_cov'] = cld_cov

    measurement['instrument'] = {'M01': 2, 'M02': 1}.get(nc_l1.platform)
    measurement['L2_sfc_Tskin'] = nc_l2['surface_temperature'][l2_idx]
    measurement['L2_sfc_emi_wn'] =\
        nc_l2['surface_emissivity_wavelengths'][:]**-1 * 1e4
    measurement['L2_sfc_emi'] = nc_l2['surface_emissivity'][l2_idx]

    if cloud:
        measurement['sfc_emi'], measurement['sfc_emi_wn'],\
            measurement['sfc_emi_flag'] =\
            _get_cloud_emissivity()
    elif (measurement['srf_flag'] == 0) or (measurement['srf_flag'] == 3)\
            or (measurement['srf_flag'] == 4):
        measurement['sfc_emi'], measurement['sfc_emi_wn'],\
            measurement['sfc_emi_flag'] =\
            _get_sea_surface_emissivity(measurement['sat_angle'])
    elif measurement['srf_flag'] == 5:
        measurement['sfc_emi'], measurement['sfc_emi_wn'],\
            measurement['sfc_emi_flag'] =\
            _get_sea_ice_emissivity()
    else:
        measurement['sfc_emi'], measurement['sfc_emi_wn'],\
            measurement['sfc_emi_flag'] =\
            _get_surface_emissivity_iremis(measurement['Date'],
                                           measurement['lat'],
                                           measurement['lon'])

    # check emissivity flag
    if measurement['sfc_emi_flag'] == 0:
        logger.warning("sfc_emi_flag for l1 {} l2 {} = {}".format(
            l1_idx, l2_idx, measurement['sfc_emi_flag']))
    #    return None

    srf_height = _get_surface_height_opt(measurement['lat'],
                                         measurement['lon'])

    if cloud:
        df, df_inter = _generate_altitude_profile(
            measurement['lat'], measurement['lon'],
            srf_pres, srf_height,
            nc_l2['pressure_levels_temp'][:],
            nc_l2['fg_atmospheric_temperature'][l2_idx],
            nc_l2['fg_atmospheric_water_vapor'][l2_idx])

        cld_tp = measurement['cld_tp']
        df_cloud = pd.concat([
            df_inter[df_inter['pres'] > cld_tp],
            pd.DataFrame({'pres': cld_tp,
                          'hoi': np.interp(
                              np.log(cld_tp),
                              np.log(df_inter['pres'].values[::-1]),
                              df_inter['hoi'].values[::-1]),
                          'temp': np.interp(
                              np.log(cld_tp),
                              np.log(df_inter['pres'].values[::-1]),
                              df_inter['temp'].values[::-1]),
                          'hum': np.interp(
                              np.log(cld_tp),
                              np.log(df_inter['pres'].values[::-1]),
                              df_inter['hum'].values[::-1])},
                         index=[len(df_inter[df_inter['pres'] > cld_tp])]),
            df_inter[df_inter['pres'] < cld_tp]]).reset_index(drop=True)
        df_inter = df_cloud[np.where(np.isclose(df_cloud['pres'],
                                                cld_tp))[0][0]:]

        # correction for grid level
        if (df_inter.shift(-1)-df_inter).iloc[0].hoi < 10.0:
            df_inter = df_inter[1:]
    else:
        df, df_inter = _generate_altitude_profile(
            measurement['lat'], measurement['lon'],
            srf_pres, srf_height,
            nc_l2['pressure_levels_temp'][:],
            nc_l2['atmospheric_temperature'][l2_idx],
            nc_l2['atmospheric_water_vapor'][l2_idx])

    measurement['atm_nol'] = len(df_inter)
    # measurement['atm_altitude'] = df['alt'].values[::-1][:len(df_inter)]
    measurement['atm_altitude'] = df_inter['hoi'].values
    measurement['atm_pressure'] = df_inter['pres'].values
    measurement['state_Tatm_a'] = df_inter['temp'].values
    measurement['state_H2Oatm_a'] = df_inter['hum'].values
    measurement['state_HNO3atm_a'] = hno3_atm_a[hno3_atm_a.size-len(df_inter):]
    measurement['state_N2Oatm_a'] = n2o_atm_a[n2o_atm_a.size-len(df_inter):]
    measurement['state_CH4atm_a'] = ch4_atm_a[ch4_atm_a.size-len(df_inter):]
    measurement['state_CO2atm_a'] = co2_atm_a[co2_atm_a.size-len(df_inter):]

    measurement['vprofile'] = df
    measurement['vprofile_comp'] = df_inter

    measurement['L2_atm_pressure'] = nc_l2['pressure_levels_temp'][:] / 100
    if cloud:
        measurement['L2_atm_T'] = nc_l2['fg_atmospheric_temperature'][l2_idx]
    else:
        measurement['L2_atm_T'] = nc_l2['atmospheric_temperature'][l2_idx]
    if cloud:
        measurement['L2_atm_hum'] =\
            nc_l2['fg_atmospheric_water_vapor'][l2_idx] * 1000
    else:
        measurement['L2_atm_hum'] =\
            nc_l2['atmospheric_water_vapor'][l2_idx] * 1000
    measurement['L2_atm_altitude'] = df['alt'].values[::-1] / 1000
    measurement['sfc_altitude'] = srf_height
    measurement['L2_sfc_pressure'] = srf_pres
    measurement['processor_major_version'] = nc_l2.processor_major_version
    measurement['product_minor_version'] = nc_l2.product_minor_version

    return measurement


def _write_netcdf_measurements(measurements, netcdf_file):
    """Writes a list of dictionary representations of measurements to netCDF

    This function is used to write measurements from original IASI L1 and L2
    netCDF files to the combined L1+L2 netCDF file.

    Parameters
    ----------
    measurements : list
        The dictionary representations of measurements
    netcdf_file : str
        The netCDF file location to write to

    Returns
    -------
    dict
        a dictionary representing a measurement
    """

    logger.debug("writing {} measurements to {}".format(str(len(measurements)),
                                                        netcdf_file))
    try:
        nc = netCDF4.Dataset(netcdf_file, 'a')
    except OSError:
        nc = netCDF4.Dataset(netcdf_file, 'w', format='NETCDF4')
        create_L1CL2_netcdf(nc)

    offset = nc.dimensions['event'].size
    for idx, measurement in enumerate(measurements):
        i = idx + offset
        nc['Date'][i] = measurement['Date']
        nc['Time'][i] = measurement['Time']
        nc['UT_hours'][i] = measurement['UT_hours']
        nc['LT_hours'][i] = measurement['LT_hours']
        nc['lon'][i] = measurement['lon']
        nc['lat'][i] = measurement['lat']
        nc['srf_flag'][i] = measurement['srf_flag']
        nc['instrument'][i] = measurement['instrument']
        nc['sat_angle'][i] = measurement['sat_angle']
        nc['along_track'][i] = measurement['along_track']
        nc['across_track'][i] = measurement['across_track']
        nc['L1C_freq'][:] = measurement['IASI_spectrum_freq']
        nc['L1C_radiances'][i] = measurement['IASI_spectrum_meas'].data
        nc['L2_atm_pressure'][:len(measurement['L2_atm_pressure'])] = \
            measurement['L2_atm_pressure']
        nc['L2_atm_T'][i, :len(measurement['L2_atm_T'])] = \
            measurement['L2_atm_T']
        nc['L2_atm_hum'][i, :len(measurement['L2_atm_hum'])] = \
            measurement['L2_atm_hum']
        nc['L2_atm_altitude'][i, :len(measurement['L2_atm_altitude'])] =\
            measurement['L2_atm_altitude'][::-1]
        nc['sfc_altitude'][i] = measurement['sfc_altitude']
        if measurement['flg_cldnes'] == 4:
            nc['sfc_emi'].description =\
                "surface emisivity (assumed 1.0 for clouds)"
            nc['sfc_emi_wn'][0, :len(measurement['sfc_emi_wn'])] =\
                measurement['sfc_emi_wn']
            nc['sfc_emi_wn'].description =\
                ("wavenumber scale for surface emissivity "
                 "data [cm-1] (vector {CLOUD,ASTER,MASUDA})")
        elif (measurement['srf_flag'] == 0) or (measurement['srf_flag'] == 3)\
                or (measurement['srf_flag'] == 4):
            nc['sfc_emi_wn'][2, :len(measurement['sfc_emi_wn'])] =\
                measurement['sfc_emi_wn']
        elif measurement['srf_flag'] == 5:
            nc['sfc_emi_wn'][1, :len(measurement['sfc_emi_wn'])] =\
                measurement['sfc_emi_wn']
        else:
            nc['sfc_emi_wn'][0, :len(measurement['sfc_emi_wn'])] =\
                measurement['sfc_emi_wn']
        nc['sfc_emi'][i, :len(measurement['sfc_emi'])] =\
            measurement['sfc_emi']
        nc['sfc_emi_flag'][i] = measurement['sfc_emi_flag']
        if isinstance(measurement['L2_sfc_pressure'],
                      np.ma.core.MaskedConstant):
            logger.warning("no L2_sfc_pressure value for index " + str(i))
            # nc['L2_sfc_pressure'][i] = -9999.99
        else:
            nc['L2_sfc_pressure'][i] = measurement['L2_sfc_pressure']
        if isinstance(measurement['L2_sfc_Tskin'], np.ma.core.MaskedConstant):
            logger.warning("no L2_sfc_Tskin value for index " + str(i))
            # nc['L2_sfc_Tskin'][i] = -9999.99
        elif isinstance(measurement['L2_sfc_Tskin'], np.ma.core.MaskedArray):
            logger.warning("no L2_sfc_Tskin value for index " + str(i))
            # nc['L2_sfc_Tskin'][i] = measurement['L2_sfc_Tskin'].filled(
            #                         -9999.99)
        else:
            nc['L2_sfc_Tskin'][i] = measurement['L2_sfc_Tskin']

        nc['cld_tp'][i] = measurement['cld_tp']
        nc['cld_tt'][i] = measurement['cld_tt']
        nc['cld_cov'][i] = measurement['cld_cov']
        nc['cld_ph'][i] = measurement['cld_ph']

        nc['L2_sfc_emi_wn'][:] = measurement['L2_sfc_emi_wn']
        nc['L2_sfc_emi'][i] = measurement['L2_sfc_emi']
        nc['L2_processor_major_version'][i] =\
            measurement['processor_major_version']
        nc['L2_product_minor_version'][i] =\
            measurement['product_minor_version']

        if 'flg_cdlfrm' in measurement:
            nc['flg_cdlfrm'][i] = measurement['flg_cdlfrm']
        if 'flg_numit' in measurement:
            nc['flg_numit'][i] = measurement['flg_numit']
        if 'flg_initia' in measurement:
            nc['flg_initia'][i] = measurement['flg_initia']
        if 'flg_dustcld' in measurement:
            nc['flg_dustcld'][i] = measurement['flg_dustcld']
        if 'flg_retcheck' in measurement:
            nc['flg_retcheck'][i] = measurement['flg_retcheck']
        if 'flg_itconv' in measurement:
            nc['flg_itconv'][i] = measurement['flg_itconv']
        if 'flg_resid' in measurement:
            nc['flg_resid'][i] = measurement['flg_resid']
        if 'flg_iasicld' in measurement:
            nc['flg_iasicld'][i] = measurement['flg_iasicld']
        if 'flg_cldnes' in measurement:
            nc['flg_cldnes'][i] = measurement['flg_cldnes']

    nc.close()


def _copy_proffi_dir(source_dir, target_dir):
    shutil.copytree(source_dir, target_dir)


def _write_spectrum_measurements(freq, spec, out_file='spectrum.dpt'):
    spec_dpt = ''
    try:
        if isinstance(spec, np.ma.MaskedArray):
            spec_dpt = '\n'.join('{:.3f} {:.3f}'.format(f, s) for f, s in
                                 zip(freq, spec.filled()))
        else:
            spec_dpt = '\n'.join('{:.3f} {:.3f}'.format(f, s) for f, s in
                                 zip(freq, spec))
    except Exception as e:
        logger.debug("no spectrum {}".format(out_file))

    with open(out_file, 'w') as f:
        f.write(spec_dpt)


def _write_pressure_temperature_profile(altitudes, pressures, temperatures,
                                        out_file='pt.prf'):
    num_lvl = len(altitudes)

    alt = '\n'.join('{:.5f}'.format(a) for a in altitudes / 1000.)
    pres = '\n'.join('{:.5f}'.format(p) for p in pressures / 100.)
    temp = '\n'.join('{:.3f}'.format(t) for t in temperatures)

    pt_prf = ("number of levels\n$\n{}\n\naltitudes [km]\n$\n{}\n\n"
              "pressure [hPa]\n$\n{}\n\ntemperature [K]\n$\n{}\n\n").format(
                  num_lvl, alt, pres, temp)
    with open(out_file, 'w') as f:
        f.write(pt_prf)


def _write_profile(altitudes, ppmv, name, out_file):
    num_lvl = len(altitudes)

    alt = '\n'.join('{:.5f}'.format(a) for a in altitudes / 1000.)
    prf = '\n'.join('{:e}'.format(p) for p in ppmv)

    ppmv_prf = ("number of levels\n$\n{}\n\naltitudes [km]\n$\n{}\n\n"
                "{} [ppmv]\n$\n{}\n\n").format(num_lvl, alt, name, prf)
    with open(out_file, 'w') as f:
        f.write(ppmv_prf)


def _write_prffwd_inp_file(m, out_file='PRFFWD.INP'):
    lat_rad = np.deg2rad(m['lat'])
    lon_rad = np.deg2rad(m['lon'])

    date = str(int(m['Date']))
    time = '{:06}'.format(int(m['Time']))
    d = datetime.strptime(date + time, '%Y%m%d%H%M%S')
    julian_date_time = julian.to_jd(d)

    solar_zenith_angle = np.deg2rad(m['sat_angle'])
    azimuth_angle = 0.0

    path_to_spectrum_dpt = config.get('prffwd', 'spectrum_dpt')
    spectrum = '0\n\'{}\''.format(path_to_spectrum_dpt)

    number_of_atm_levels = int(m['atm_nol'])
    atm_altitude = m['atm_altitude']
    altitude_groups = [atm_altitude[i:i+5]
                       for i in range(0, len(atm_altitude), 5)]
    atm_levels = ''
    for group in altitude_groups:
        atm_levels += ' '.join(str(h / 1000.) for h in group) + '\n'

    path_to_pt_prf = config.get('prffwd', 'pt_prf')
    path_to_h2o_prf = config.get('prffwd', 'h2o_prf')

    hitran_lbl = hitran_lbl_default(config)
    prf_files = '\n'.join([config.get('prffwd', 'h2o_prf'),
                           config.get('prffwd', 'h2o_prf'),
                           config.get('prffwd', 'co2_prf'),
                           config.get('prffwd', 'n2o_prf'),
                           config.get('prffwd', 'ch4_prf'),
                           config.get('prffwd', 'hno3_prf')])

    ground_temperature = m['state_Tskin_a']

    eps_table_dimension = len(m['sfc_emi_wn'])
    eps_table = '\n'.join('{:.3f} {:.3f}'.format(w, e) for w, e in
                          zip(m['sfc_emi_wn'], m['sfc_emi']))

    prffwd = prffwd_inp(lat=lat_rad, lon=lon_rad,
                        julian_date_time=julian_date_time,
                        solar_zenith_angle=solar_zenith_angle,
                        azimuth_angle=azimuth_angle,
                        spectrum=spectrum,
                        number_of_atm_levels=number_of_atm_levels,
                        atm_levels=atm_levels, path_to_pt_prf=path_to_pt_prf,
                        h2o=path_to_h2o_prf,
                        hitran_lbl=hitran_lbl,
                        prf_files=prf_files, prf_ref_files=prf_files,
                        ground_temperature=ground_temperature,
                        eps_table_dimension=eps_table_dimension,
                        eps_table=eps_table)

    with open(out_file, 'w') as f:
        f.write(prffwd)


def _toYearFraction(date):
    def sinceEpoch(date):
        return time.mktime(date.timetuple())
    s = sinceEpoch

    year = date.year
    startOfThisYear = datetime(year=year, month=1, day=1)
    startOfNextYear = datetime(year=year+1, month=1, day=1)

    yearElapsed = s(date) - s(startOfThisYear)
    yearDuration = s(startOfNextYear) - s(startOfThisYear)
    fraction = yearElapsed/yearDuration

    return date.year + fraction


def _regparm_apriori_h2o_hdo(date, lat_retr, altitudes, delD_data, param_nc,
                             tropz_param):
    h_delD = delD_data[:, 1]
    delD = delD_data[:, 2]

    d = datetime.strptime(date, '%Y%m%d')
    t_retr = _toYearFraction(d) - 2000
    ty_retr = t_retr - np.floor(t_retr)
    z_retr = altitudes / 1000.

    lat = tropz_param['lat'][...]
    c0 = tropz_param['c0'][...]
    a1 = tropz_param['a1'][...]
    a2 = tropz_param['a2'][...]
    p1 = tropz_param['p1'][...]
    p2 = tropz_param['p2'][...]

    f_seasonal = a1 * (np.cos(p1) * np.sin(2*np.pi*ty_retr) + np.sin(p1) *
                       np.cos(2*np.pi*ty_retr)) +\
        a2 * (np.cos(p2) * np.sin(2*2*np.pi*ty_retr) + np.sin(p2) *
              np.cos(2*2*np.pi*ty_retr))
    f_base = c0
    z_trop = f_seasonal + f_base

    z_strat = np.array([50.])
    f = interp1d(lat.data, z_trop.data, kind='cubic',
                 bounds_error=False,
                 fill_value=(z_trop[0], z_trop[-1]))

    z_strat = np.insert(z_strat, 0, f(lat_retr.data))
    delD_strat = np.array([0.0, -350.])

    lat = param_nc['lat'][...]
    z = param_nc['z'][...]
    c0 = param_nc['c0'][...]
    c1 = param_nc['c1'][...]
    c2 = param_nc['c2'][...]
    a1 = param_nc['a1'][...]
    a2 = param_nc['a2'][...]
    p1 = param_nc['p1'][...]
    p2 = param_nc['p2'][...]

    f_seasonal = a1 * (np.cos(p1) * np.sin(2*np.pi*ty_retr) + np.sin(p1) *
                       np.cos(2*np.pi*ty_retr)) +\
        a2 * (np.cos(p2) * np.sin(2*2*np.pi*ty_retr) + np.sin(p2) *
              np.cos(2*2*np.pi*ty_retr))

    f_longterm = c0 + c1 * t_retr + c2 * t_retr**2

    gas = f_seasonal + f_longterm

    gas_i = []
    for i, l in enumerate(lat):
        f = interp1d(l.data, gas[i].data, kind='cubic',
                     bounds_error=False,
                     fill_value=(gas[i, 0], gas[i, -1]))
        gas_i.append(f(lat_retr.data))
    gas_i = np.array(gas_i).flatten()

    f = interp1d(z.data, gas_i, fill_value='extrapolate')
    gas_h2o_ii = np.exp(np.array([f(z_r) for z_r in z_retr]))

    gas_delD_ii = []
    for i, z_r in enumerate(z_retr):
        if z_r <= z_strat[0]:
            f = interp1d(h_delD.data, delD.data, fill_value='extrapolate')
            gas_delD_ii.append(f(np.log(gas_h2o_ii[i])/np.log(10)))
            delD_strat[0] = gas_delD_ii[i]
        if z_r > z_strat[0] and z_r <= z_strat[-1]:
            f = interp1d(z_strat, delD_strat, fill_value='extrapolate')
            gas_delD_ii.append(f(z_r))
        if z_r > z_strat[-1]:
            gas_delD_ii.append(delD_strat[-1])

    gas_delD_ii = np.array(gas_delD_ii)
    gas_hdo_ii = gas_h2o_ii*((gas_delD_ii/1000) + 1)

    return gas_h2o_ii, gas_hdo_ii, z_strat[0] * 1000.


def _regparm_apriori(date, lat_retr, altitudes, ml_corr=None, param_nc=None):
    d = datetime.strptime(date, '%Y%m%d')

    t_retr = _toYearFraction(d) - 2000
    ty_retr = t_retr - np.floor(t_retr)

    z_retr = altitudes / 1000

    lat = param_nc['lat'][...]
    z = param_nc['z'][...]
    c0 = param_nc['c0'][...]
    c1 = param_nc['c1'][...]
    c2 = param_nc['c2'][...]
    a1 = param_nc['a1'][...]
    a2 = param_nc['a2'][...]
    p1 = param_nc['p1'][...]
    p2 = param_nc['p2'][...]

    f_corr = np.array([0.0] * z.shape[0])
    if ml_corr is not None:
        t_MLcorr = ml_corr[:, 0] - 2000
        MLcorr = ml_corr[:, 1]
        f = interp1d(t_MLcorr, MLcorr, fill_value='extrapolate')
        f_corr = np.array([f(t_retr)] * z.shape[0])

    f_seasonal = a1 * (np.cos(p1) * np.sin(2*np.pi*ty_retr) + np.sin(p1) *
                       np.cos(2*np.pi*ty_retr)) +\
        a2 * (np.cos(p2) * np.sin(2*2*np.pi*ty_retr) + np.sin(p2) *
              np.cos(2*2*np.pi*ty_retr))

    f_longterm = c0 + c1 * t_retr + c2 * t_retr**2

    gas = f_seasonal + f_longterm + f_corr[:, np.newaxis]

    gas_i = []
    for i, l in enumerate(lat):
        f = interp1d(l.data, gas[i].data, kind='cubic',
                     bounds_error=False,
                     fill_value=(gas[i, 0], gas[i, -1]))
        gas_i.append(f(lat_retr.data))
    gas_i = np.array(gas_i)

    f = interp1d(z.data, gas_i, fill_value='extrapolate')
    gas_ii = np.exp(np.array([f(z_r) for z_r in z_retr]))

    return gas_ii


def _constraints(date, lat_retr, altitudes, tropz_param, inv_nc):
    d = datetime.strptime(date, '%Y%m%d')

    t_retr = _toYearFraction(d) - 2000
    ty_retr = t_retr - np.floor(t_retr)

    z_gnd = altitudes[0] / 1000

    lat = tropz_param['lat'][...]
    c0 = tropz_param['c0'][...]
    a1 = tropz_param['a1'][...]
    a2 = tropz_param['a2'][...]
    p1 = tropz_param['p1'][...]
    p2 = tropz_param['p2'][...]

    f_seasonal = a1 * (np.cos(p1) * np.sin(2*np.pi*ty_retr) + np.sin(p1) *
                       np.cos(2*np.pi*ty_retr)) +\
        a2 * (np.cos(p2) * np.sin(2*2*np.pi*ty_retr) + np.sin(p2) *
              np.cos(2*2*np.pi*ty_retr))

    f_base = c0

    z_trop = f_seasonal + f_base

    f = interp1d(lat.data, z_trop.data, fill_value='extrapolate')
    z_trop_i = f(lat_retr.data)

    nol = inv_nc['nol'][...]
    alt_trop = inv_nc['alttrop'][...]
    kovT = inv_nc['kovar'][...]
    kovar = inv_nc['kovar'][...]
    correl = inv_nc['correl'][...]
    GHG = inv_nc['GHG'][...]
    HNO3 = inv_nc['HNO3'][...]

    idx = np.argmin(np.abs(alt_trop.data - z_trop_i))

    KOVTinp = kovT[idx, :, :]
    KOVARinp = kovar[idx, :, :]
    CORRELinp = np.block([[correl[idx, :, :], -correl[idx, :, :]],
                          [-correl[idx, :, :], correl[idx, :, :]]])
    REGFACSinp_GHG = GHG[idx, :, :]
    REGFACSinp_HNO3 = HNO3[idx, :, :]

    return KOVTinp, KOVARinp, CORRELinp, REGFACSinp_GHG, REGFACSinp_HNO3


def _copy_inv_files(m, inv_path, out_dir, h2o='', dD_apriori=None, hno3='',
                    co2='', co2_mlcorr=None, ch4='', ch4_mlcorr=None,
                    n2o='', n2o_mlcorr=None, tropz=''):
    date = str(int(m['Date']))
    lat = m['lat']
    altitudes = m['atm_altitude']
    temperatures = m['state_Tatm_a']

    height = math.floor(altitudes[0]/10) * 10
    if height > 9000:
        height = 9000
    inv = os.path.join(inv_path, 'inv_{:04}.nc'.format(height))
    if not os.path.exists(inv):
        logger.error("missing inv file {}".format(inv))
        return

    inv_nc = netCDF4.Dataset(inv)
    tropz_param = netCDF4.Dataset(tropz)

    KOVTinp, KOVARinp, CORRELinp, REGFACSinp_GHG, REGFACSinp_HNO3 =\
        _constraints(date, lat, altitudes, tropz_param, inv_nc)

    inv_nc.close()

    np.savetxt(os.path.join(out_dir, 'INP_INV', 'kovT.inp'), KOVTinp,
               fmt='%.3e', newline='\n  ', header='$\n.false.', comments='')
    np.savetxt(os.path.join(out_dir, 'INP_INV', 'kovar.inp'), KOVARinp,
               fmt='%.3e', newline='\n  ', header='$\n.false.', comments='')
    np.savetxt(os.path.join(out_dir, 'INP_INV', 'correl.inp'), CORRELinp,
               fmt='%.3e', newline='\n  ', header='$\n1.0\n\n$', comments='')

    _, nol = REGFACSinp_GHG.shape
    regfacs9_str = "Eingabedatei mit Regularisierungsfaktoren\n\nH2O\n$\n" +\
        nol * "1.0E+00,1.0E+00,.true.\n" + "\nHDO\n$\n" +\
        nol * "1.0E+00,1.0E+00,.true.\n" + "\nCO2\n$\n" +\
        nol * "1.0E+00,1.0E+00,.true.\n" + "\nN2O\n$\n" +\
        ''.join('{:.1E},{:.1E},.true.\n'.format(i, j) for (i, j)
                in REGFACSinp_GHG.data.T) + "\nCH4\n$\n" +\
        ''.join('{:.1E},{:.1E},.true.\n'.format(i, j) for (i, j)
                in REGFACSinp_GHG.data.T) + "\nHNO3\n$\n" +\
        ''.join('{:.1E},{:.1E},.true.\n'.format(i, j) for (i, j)
                in REGFACSinp_HNO3.data.T) + "\nT\n$\n" +\
        nol * "1.0E+00,1.0E+00,.true.\n" + "\n"

    with open(os.path.join(out_dir, 'INP_INV', 'REGFACS9.INP'), 'w') as f:
        f.write(regfacs9_str)

    # H2O/HDO
    h2o_param = netCDF4.Dataset(h2o)
    h2o_a, hdo_a, z_trop = _regparm_apriori_h2o_hdo(date, lat, altitudes,
                                                    dD_apriori, h2o_param,
                                                    tropz_param)
    tropz_param.close()
    h2o_param.close()

    np.savez_compressed(os.path.join(out_dir, "z_trop"), z_trop=z_trop)

    # CO2
    co2_param = netCDF4.Dataset(co2)
    co2_a = _regparm_apriori(date, lat, altitudes, co2_mlcorr, co2_param)
    co2_param.close()

    # N2O
    n2o_param = netCDF4.Dataset(n2o)
    n2o_a = _regparm_apriori(date, lat, altitudes, n2o_mlcorr, n2o_param)
    n2o_param.close()

    # CH4
    ch4_param = netCDF4.Dataset(ch4)
    ch4_a = _regparm_apriori(date, lat, altitudes, ch4_mlcorr, ch4_param)
    ch4_param.close()

    # HNO3
    hno3_param = netCDF4.Dataset(hno3)
    hno3_a = _regparm_apriori(date, lat, altitudes, param_nc=hno3_param)
    hno3_param.close()

    regparm9_str = "Regularisierungsprofil H2O\n$\n" +\
        " " + np.array2string(h2o_a, max_line_width=55,
                              formatter={'float': lambda x: "%.3e" % x})[1:-1]\
        + "\n\n" +\
        "Regularisierungsprofil HDO\n$\n" +\
        " " + np.array2string(hdo_a, max_line_width=55,
                              formatter={'float': lambda x: "%.3e" % x})[1:-1]\
        + "\n\n" +\
        "Regularisierungsprofil CO2\n$\n" +\
        " " + np.array2string(co2_a, max_line_width=55,
                              formatter={'float': lambda x: "%.3e" % x})[1:-1]\
        + "\n\n" +\
        "Regularisierungsprofil N2O\n$\n" +\
        " " + np.array2string(n2o_a, max_line_width=55,
                              formatter={'float': lambda x: "%.3e" % x})[1:-1]\
        + "\n\n" +\
        "Regularisierungsprofil CH4\n$\n" +\
        " " + np.array2string(ch4_a, max_line_width=55,
                              formatter={'float': lambda x: "%.3e" % x})[1:-1]\
        + "\n\n" +\
        "Regularisierungsprofil HNO3\n$\n" +\
        " " + np.array2string(hno3_a, max_line_width=55,
                              formatter={'float': lambda x: "%.3e" % x})[1:-1]\
        + "\n\n" +\
        "Temperature\n$\n" +\
        " " + np.array2string(temperatures, max_line_width=55,
                              formatter={'float': lambda x: "%.3e" % x})[1:-1]\
        + "\n"

    with open(os.path.join(out_dir, 'INP_INV', 'REGPARM9.INP'), 'w') as f:
        f.write(regparm9_str)


def _init_git(path):
    p = subprocess.Popen(['git', 'init'],
                         cwd=path, stdout=subprocess.PIPE)
    _, _ = p.communicate()
    p = subprocess.Popen(['git', 'config', 'user.name', 'iasi'],
                         cwd=path, stdout=subprocess.PIPE)
    _, _ = p.communicate()
    p = subprocess.Popen(['git', 'config', 'user.email', 'iasi@kit.edu'],
                         cwd=path, stdout=subprocess.PIPE)
    _, _ = p.communicate()
    p = subprocess.Popen(['git', 'add', '.'],
                         cwd=path, stdout=subprocess.PIPE)
    _, _ = p.communicate()
    p = subprocess.Popen(['git', 'commit', '-m', '"add input files"'],
                         cwd=path, stdout=subprocess.PIPE)
    _, _ = p.communicate()
    p = subprocess.Popen(['git', 'init', '--bare'],
                         cwd=path, stdout=subprocess.PIPE)
    _, _ = p.communicate()
    p = subprocess.Popen(['touch', '.git/git-daemon-export-ok'],
                         cwd=path, stdout=subprocess.PIPE)
    _, _ = p.communicate()


def _write_measurement_configuration(m, out_dir):

    header = ('event,inv_heights,srf_flag,across_track,along_track,'
              'instrument,cld_cov,cld_ph,cld_tp,cld_tt,sfc_emi_flag,'
              'flg_cdlfrm,flg_numit,flg_dustcld,flg_retcheck,flg_itconv,'
              'flg_cldnes,flg_iasicld,flg_resid,flg_initia,'
              'processor_major_version,product_minor_version\n')

    data = str(m['event']) + ','

    altitudes = m['atm_altitude']
    inv_heights = 'inv_{:04}'.format(math.floor(altitudes[0]/10) * 10)

    data += ','.join(
        map(str, [inv_heights, m['srf_flag'], m['across_track'],
                  m['along_track'], m['instrument'], m['cld_cov'],
                  m['cld_ph'], m['cld_tp'], m['cld_tt'], m['sfc_emi_flag'],
                  m['flg_cdlfrm'], m['flg_numit'], m['flg_dustcld'],
                  m['flg_retcheck'], m['flg_itconv'], m['flg_cldnes'],
                  m['flg_iasicld'], m['flg_resid'], m['flg_initia'],
                  m['L2_processor_major_version'],
                  m['L2_product_minor_version']]))

    with open(os.path.join(out_dir, config.get('processing',
                                               'configuration')), 'w') as f:
        f.write(header)
        f.write(data)


def _pickle_data(m, out_dir):
    if 'vprofile' in m:
        m['vprofile'].to_pickle(os.path.join(out_dir, '_vprofile.pickle'))
    if 'vprofile_comp' in m:
        m['vprofile_comp'].to_pickle(os.path.join(out_dir, 'vprofile.pickle'))


def _generate_input_dirs(measurements, out_dir, processes=None):
    # num_events = len(measurements)
    # init_git = False  # config.getboolean('git','init')

    # num_processes = min(processes, psutil.cpu_count()
    if processes is None:
        logger.debug("using {} processes".format(os.cpu_count()))
    else:
        logger.debug("using {} processes".format(processes))

    with ProcessPoolExecutor(max_workers=processes) as e:
        # create directories
        jobs = [e.submit(_copy_proffi_dir,
                         config.get('processing', 'input_path'),
                         os.path.join(out_dir, str(m['event'])))
                for m in measurements]

        [job.result() for job in jobs]

        ch4_mlcorr = np.loadtxt(os.path.abspath(
            config.get('inv_heights', 'ch4_mlcorr')), skiprows=1)
        co2_mlcorr = np.loadtxt(os.path.abspath(
            config.get('inv_heights', 'co2_mlcorr')), skiprows=1)
        n2o_mlcorr = np.loadtxt(os.path.abspath(
            config.get('inv_heights', 'n2o_mlcorr')), skiprows=1)
        dD_apriori = np.loadtxt(os.path.abspath(
            config.get('inv_heights', 'dD_apriori')), skiprows=1)
        jobs = []
        for m in measurements:
            jobs.append(e.submit(_copy_inv_files, m,
                                 config.get('inv_heights', 'path'),
                                 os.path.join(out_dir, str(m['event'])),
                                 h2o=config.get('inv_heights', 'h2o_param'),
                                 dD_apriori=dD_apriori,
                                 hno3=config.get('inv_heights', 'hno3_param'),
                                 co2=config.get('inv_heights', 'co2_param'),
                                 co2_mlcorr=co2_mlcorr,
                                 ch4=config.get('inv_heights', 'ch4_param'),
                                 ch4_mlcorr=ch4_mlcorr,
                                 n2o=config.get('inv_heights', 'n2o_param'),
                                 n2o_mlcorr=n2o_mlcorr,
                                 tropz=config.get('inv_heights', 'tropz_param')
                                 ))

            jobs.append(e.submit(
                _write_spectrum_measurements, m['IASI_spectrum_freq'],
                m['IASI_spectrum_meas'],
                os.path.join(out_dir, str(m['event']),
                             config.get('prffwd', 'spectrum_dpt'))))

            jobs.append(e.submit(
                _write_pressure_temperature_profile, m['atm_altitude'],
                m['atm_pressure'], m['state_Tatm_a'],
                os.path.join(out_dir, str(m['event']),
                             config.get('prffwd', 'pt_prf'))))

            jobs.append(e.submit(
                _write_profile, m['atm_altitude'], m['state_HNO3atm_a'], 'HNO3',
                os.path.join(out_dir, str(m['event']),
                             config.get('prffwd', 'hno3_prf'))))

            jobs.append(e.submit(
                _write_profile, m['atm_altitude'], m['state_N2Oatm_a'], 'N2O',
                os.path.join(out_dir, str(m['event']),
                             config.get('prffwd', 'n2o_prf'))))

            jobs.append(e.submit(
                _write_profile, m['atm_altitude'], m['state_CH4atm_a'], 'CH4',
                os.path.join(out_dir, str(m['event']),
                             config.get('prffwd', 'ch4_prf'))))

            jobs.append(e.submit(
                _write_profile, m['atm_altitude'], m['state_H2Oatm_a'], 'H2O',
                os.path.join(out_dir, str(m['event']),
                             config.get('prffwd', 'h2o_prf'))))

            jobs.append(e.submit(
                _write_profile, m['atm_altitude'], m['state_CO2atm_a'], 'CO2',
                os.path.join(out_dir, str(m['event']),
                             config.get('prffwd', 'co2_prf'))))

            jobs.append(e.submit(
                _write_prffwd_inp_file, m,
                os.path.join(out_dir, str(m['event']),
                             config.get('prffwd', 'prffwd_inp'))))

            jobs.append(e.submit(
                _write_measurement_configuration, m,
                os.path.join(out_dir, str(m['event']))))

            jobs.append(e.submit(
                _pickle_data, m, os.path.join(out_dir, str(m['event']))))

        [job.result() for job in jobs]

        # if init_git:
        #     logger.debug('init git repositories')
        #     jobs = [e.submit(_init_git, d.path) for d in scandir(out_dir)
        #             if d.is_dir()]
        #     [job.result() for job in jobs]
        # else:
        #     logger.warning('git repositories not initiated')


def from_l1_l2_input_netcdf(l1, l2, netcdf_file):
    nc_l1 = netCDF4.Dataset(l1, 'r')
    nc_l2 = netCDF4.Dataset(l2, 'r')

    l1_mandatory = ['lat', 'lon', 'measurement_date', 'pixel_zenith_angle',
                    'gs_1c_spect']

    missing = set(l1_mandatory) - set(nc_l1.variables.keys())
    if missing:
        logger.error("missing variables {} in {}".format(missing, l1))
        nc_l1.close()
        nc_l2.close()
        return

    l2_mandatory = ['lat', 'lon', 'surface_pressure', 'surface_temperature',
                    'cloud_top_pressure', 'cloud_top_temperature',
                    'cloud_phase', 'fractional_cloud_cover',
                    'surface_emissivity_wavelengths', 'surface_emissivity',
                    'pressure_levels_temp', 'atmospheric_temperature',
                    'atmospheric_water_vapor']

    missing = set(l2_mandatory) - set(nc_l2.variables.keys())
    if missing:
        logger.error("missing variables {} in {}".format(missing, l2))
        nc_l1.close()
        nc_l2.close()
        return

    l1_idx, l2_idx = _valid_recordings(nc_l1, nc_l2)

    measurements = [_get_iasi_measurement(i, nc_l1, nc_l2,
                                          l1_idx[i], l2_idx[i])
                    for i in range(len(l1_idx))]

    nc_l1.close()
    nc_l2.close()

    measurements = list(filter(partial(is_not, None), measurements))

    _write_netcdf_measurements(measurements, netcdf_file)


def from_input_netcdf(netcdf_path, out_dir, processes=1):
    nc = netCDF4.Dataset(netcdf_path, 'r')

    num_events = nc.dimensions['event'].size

    measurements = [_get_netcdf_measurement(i, nc) for i in range(num_events)]

    nc.close()

    measurements = list(filter(partial(is_not, None), measurements))

    _generate_input_dirs(measurements, out_dir, processes=processes)

    # subprocess.Popen(['nohup', 'git', 'daemon', '--reuseaddr',\
    #        '--base-path={}'.format(os.path.abspath(out_dir)),\
    #        '{}'.format(os.path.abspath(out_dir))])
