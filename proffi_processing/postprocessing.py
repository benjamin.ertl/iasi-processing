import os
import sys
import traceback
# import psutil
import itertools
import logging

import netCDF4
import julian

import numpy as np
import pandas as pd

from scandir import scandir
from operator import is_not
from functools import partial
from dateutil import tz
from concurrent.futures import ProcessPoolExecutor

from proffi_processing import config
from .utils import create_result_netcdf

logger = logging.getLogger('proffi-processing')
noise = None

def _get_proffi_result(directory):
    prffwd_inp = os.path.join(directory, config.get('prffwd', 'prffwd_inp'))
    invspeca_dat = os.path.join(directory, 'OUT_INV', 'INVSPECA.DAT')
    actrms_dat = os.path.join(directory, 'OUT_INV', 'ACTRMS.DAT')
    pt_out_dat = os.path.join(directory, 'OUT_INV', 'PT_OUT.DAT')
    auxparms_dat = os.path.join(directory, 'OUT_INV', 'AUXPARMS.DAT')
    profs_dat = os.path.join(directory, 'OUT_INV', 'PROFS.DAT')
    kern_raw_dat = os.path.join(directory, 'OUT_INV', 'KERN_RAW.DAT')
    kern_ful_dat = os.path.join(directory, 'OUT_INV', 'KERN_FUL.DAT')
    gain_raw_dat = os.path.join(directory, 'OUT_INV', 'GAIN_RAW.DAT')

    configuration = os.path.join(directory, config.get('processing',
                                 'configuration'))
    z_trop = os.path.join(directory, 'z_trop.npz')

    # check mandatory files
    mandatory_files = [prffwd_inp, invspeca_dat, actrms_dat, pt_out_dat,
                       auxparms_dat, profs_dat, kern_raw_dat, kern_ful_dat,
                       gain_raw_dat]
    for f in mandatory_files:
        if not os.path.exists(f):
            logger.warning('no {}'.format(f))
            return None

    proffi_result = dict()

    # read observation configuration
    if os.path.exists(configuration):
        conf = pd.read_csv(configuration, index_col=False)
        conf.replace('--', np.nan, inplace=True)
        proffi_result = conf.to_dict('index')[0]

    if os.path.exists(z_trop):
        loaded = np.load(z_trop)
        proffi_result['z_trop'] = loaded['z_trop']

    # read prffwd.inp
    prffwd = ''
    with open(prffwd_inp, 'r') as f:
        prffwd = f.read()
    prffwd = prffwd.split('$')

    # read lat, lon
    group1 = prffwd[1].strip().splitlines()
    lat = np.rad2deg(float(group1[0]))
    lon = np.rad2deg(float(group1[1]))

    proffi_result['lat'] = lat
    proffi_result['lon'] = lon

    # read date, sat angle and azimuth
    group2 = prffwd[2].strip().splitlines()
    jd, sat_angle, azimuth = group2[2].split()

    date = julian.from_jd(float(jd))
    proffi_result['Date'] = float(date.strftime("%Y%m%d"))
    proffi_result['Time'] = float(date.strftime("%H%M%S"))

    date = date.replace(tzinfo=tz.tzutc())

    proffi_result['UT_hours'] = (date.hour + (date.minute / 60.)
                                 + date.second / 3600.)
    proffi_result['LT_hours'] = proffi_result['UT_hours'] + (lon/15)
    if proffi_result['LT_hours'] < 0.0:
        proffi_result['LT_hours'] += 24.0
    if proffi_result['LT_hours'] >= 24.0:
        proffi_result['LT_hours'] -= 24.0

    sat_angle = np.rad2deg(float(sat_angle))
    proffi_result['sat_angle'] = sat_angle

    # read emissivity
    group16 = prffwd[16].strip().splitlines()
    emi_dim = int(group16[1])
    emi = list(map(float, itertools.chain.from_iterable([s.split(' ')
               for s in group16[2:emi_dim + 2]])))
    emi_wn = emi[::2]
    emi_val = emi[1::2]

    proffi_result['state_Tskin_a'] = float(group16[0])
    proffi_result['sfc_emi_wn'] = np.array(emi_wn)
    np_emi_val = np.full(20, -9999.99)
    np_emi_val[:emi_dim] = np.array(emi_val)
    # proffi_result['sfc_emi'] = np_emi_val.reshape(1, 20)
    proffi_result['sfc_emi'] = np_emi_val.reshape(20)

    # read invspeca.dat
    spectrum = pd.read_csv(invspeca_dat, header=None, delim_whitespace=True)
    proffi_result['IASI_spectrum_freq'] = np.array(spectrum[0])
    proffi_result['IASI_spectrum_meas'] =\
        np.array(spectrum[1]).reshape(1, len(spectrum[1]))
    proffi_result['IASI_spectrum_simu'] =\
        np.array(spectrum[2]).reshape(1, len(spectrum[1]))

    # read actrms.dat
    # rms = pd.read_csv(actrms_dat, header=None, delim_whitespace=True)
    # proffi_result['fit_quality'] = float(rms[-1:][1] / max(spectrum[1]))

    # read pt_out.dat
    pt = pd.read_csv(pt_out_dat, header=None, delim_whitespace=True)
    dim_grid_lvl = len(pt[0])
    proffi_result['atm_nol'] = dim_grid_lvl
    proffi_result['atm_altitude'] = np.array(pt[0]).reshape(1, len(pt[0]))
    proffi_result['atm_pressure'] = np.array(pt[1]).reshape(1, len(pt[0]))

    # read auxparms.dat
    auxparms = ''
    with open(auxparms_dat, 'r') as f:
        auxparms = f.read()
    auxparms = auxparms.split('$')

    group1 = auxparms[1].strip().splitlines()
    group5 = auxparms[5].strip().splitlines()
    try:
        proffi_result['iter'] = float(group1[0])
    except Exception as e:
        proffi_result['iter'] = float(group1[1])
    proffi_result['state_Tskin'] = float(group5[0])

    # fit quality
    proffi_result['fit_quality'] = np.zeros(5)
    residuals = spectrum[1] - spectrum[2]

    emissivity = np.interp(proffi_result['IASI_spectrum_freq'],
                           proffi_result['sfc_emi_wn'],
                           proffi_result['sfc_emi'])
    BBrad = emissivity * (1.191042e-5 *
                          np.power(proffi_result['IASI_spectrum_freq'], 3)) /\
        (np.exp(1.4387752 * proffi_result['IASI_spectrum_freq'] /
         proffi_result['state_Tskin'])-1) * 100
    transmisivity_residuals = residuals / BBrad
    res = residuals[8:-8].reset_index(drop=True)
    trans_res = transmisivity_residuals[:-16]
    trans_res_sm = transmisivity_residuals.rolling(17).mean().dropna()\
        .reset_index(drop=True)
    trans_res_res = transmisivity_residuals.shift(-8) - trans_res_sm

    # residual-to-signal_max
    proffi_result['fit_quality'][0] =\
        np.sqrt(np.mean((residuals)**2)) / max(spectrum[1])
    # residual
    proffi_result['fit_quality'][1] = np.std(res)
    # transmisivity residual
    proffi_result['fit_quality'][2] = np.std(trans_res)
    # systematic transmisivity residual
    proffi_result['fit_quality'][3] = np.std(trans_res_sm)
    # random transmissivity residual
    proffi_result['fit_quality'][4] = np.std(trans_res_res)

    # read profs.dat
    h2o_prf = pd.read_csv(profs_dat, skiprows=1,
                          nrows=dim_grid_lvl, header=None,
                          delim_whitespace=True)
    hdo_prf = pd.read_csv(profs_dat, skiprows=(1 + (1 * (dim_grid_lvl + 1))),
                          nrows=dim_grid_lvl, header=None,
                          delim_whitespace=True)
    # co2_prf = pd.read_csv(profs_dat, skiprows=(1 + (2 * (dim_grid_lvl + 1))),
    #                       nrows=dim_grid_lvl, header=None,
    #                       delim_whitespace=True)
    n2o_prf = pd.read_csv(profs_dat, skiprows=(1 + (3 * (dim_grid_lvl + 1))),
                          nrows=dim_grid_lvl, header=None,
                          delim_whitespace=True)
    ch4_prf = pd.read_csv(profs_dat, skiprows=(1 + (4 * (dim_grid_lvl + 1))),
                          nrows=dim_grid_lvl, header=None,
                          delim_whitespace=True)
    hno3_prf = pd.read_csv(profs_dat, skiprows=(1 + (5 * (dim_grid_lvl + 1))),
                           nrows=dim_grid_lvl, header=None,
                           delim_whitespace=True)
    temp_prf = pd.read_csv(profs_dat, skiprows=(1 + (6 * (dim_grid_lvl + 1))),
                           nrows=dim_grid_lvl, header=None,
                           delim_whitespace=True)

    proffi_result['atm_H2O_EUML2'] =\
        np.array(np.log(h2o_prf[2])).reshape(1, dim_grid_lvl)

    proffi_result['state_N2Oatm'] =\
        np.array(np.log(n2o_prf[3])).reshape(1, dim_grid_lvl)
    proffi_result['state_CH4atm'] =\
        np.array(np.log(ch4_prf[3])).reshape(1, dim_grid_lvl)

    proffi_result['state_N2Oatm_a'] =\
        np.array(np.log(n2o_prf[1])).reshape(1, dim_grid_lvl)
    proffi_result['state_CH4atm_a'] =\
        np.array(np.log(ch4_prf[1])).reshape(1, dim_grid_lvl)

    proffi_result['state_HNO3atm'] =\
        np.array(np.log(hno3_prf[3])).reshape(1, dim_grid_lvl)
    proffi_result['state_HNO3atm_a'] =\
        np.array(np.log(hno3_prf[1])).reshape(1, dim_grid_lvl)

    proffi_result['state_Tatm'] =\
        np.array(temp_prf[3]).reshape(1, dim_grid_lvl)
    proffi_result['state_Tatm_a'] =\
        np.array(temp_prf[1]).reshape(1, dim_grid_lvl)

    proffi_result['state_H2Oatm'] =\
        np.array(np.log(h2o_prf[3])).reshape(1, dim_grid_lvl)
    proffi_result['state_HDOatm'] =\
        np.array(np.log(hdo_prf[3])).reshape(1, dim_grid_lvl)

    proffi_result['state_H2Oatm_a'] =\
        np.array(np.log(h2o_prf[1])).reshape(1, dim_grid_lvl)
    proffi_result['state_HDOatm_a'] =\
        np.array(np.log(hdo_prf[1])).reshape(1, dim_grid_lvl)

    # read averaging kernels from kern_raw.dat
    k_raw = pd.read_csv(kern_raw_dat, header=None, delim_whitespace=True)

    wvatm_raw = k_raw.ix[:2*dim_grid_lvl-1, list(range(2*dim_grid_lvl))]

    tatm_wvatm_raw = k_raw.ix[:2*dim_grid_lvl-1,
                              list(range(5*dim_grid_lvl+1, 6*dim_grid_lvl+1))]

    tatm_wvatm_raw.reset_index(drop=True, inplace=True)
    tatm_wvatm_raw.columns = list(range(dim_grid_lvl))

    tskin_wvatm_raw = k_raw.ix[:2*dim_grid_lvl-1, k_raw.columns[-2]]

    tskin_wvatm_raw.reset_index(drop=True, inplace=True)
    tskin_wvatm_raw.columns = 0

    ghgatm_raw = k_raw.ix[2*dim_grid_lvl+1:4*dim_grid_lvl,
                          list(range(2*dim_grid_lvl+1,  4*dim_grid_lvl+1))]

    ghgatm_raw.reset_index(drop=True, inplace=True)
    ghgatm_raw.columns = list(range(2*dim_grid_lvl))

    tatm_ghgatm_raw = k_raw.ix[2*dim_grid_lvl+1:4*dim_grid_lvl,
                               list(range(5*dim_grid_lvl+1, 6*dim_grid_lvl+1))]

    tatm_ghgatm_raw.reset_index(drop=True, inplace=True)
    tatm_ghgatm_raw.columns = list(range(dim_grid_lvl))

    tskin_ghgatm_raw = k_raw.ix[2*dim_grid_lvl+1:4*dim_grid_lvl,
                                k_raw.columns[-2]]

    tskin_ghgatm_raw.reset_index(drop=True, inplace=True)
    tskin_ghgatm_raw.columns = 0

    hno3atm_raw = k_raw.ix[4*dim_grid_lvl+1:5*dim_grid_lvl,
                           list(range(4*dim_grid_lvl+1, 5*dim_grid_lvl+1))]

    tatm_hno3atm_raw = k_raw.ix[4*dim_grid_lvl+1:5*dim_grid_lvl,
                                list(range(
                                    5*dim_grid_lvl+1, 6*dim_grid_lvl+1))]

    tatm_hno3atm_raw.reset_index(drop=True, inplace=True)
    tatm_hno3atm_raw.columns = list(range(dim_grid_lvl))

    tskin_hno3atm_raw = k_raw.ix[4*dim_grid_lvl+1:5*dim_grid_lvl,
                                 k_raw.columns[-2]]

    tskin_hno3atm_raw.reset_index(drop=True, inplace=True)
    tskin_hno3atm_raw.columns = 0

    tatm_raw = k_raw.ix[5*dim_grid_lvl+1:6*dim_grid_lvl,
                        list(range(5*dim_grid_lvl+1, 6*dim_grid_lvl+1))]

    tatm_raw.reset_index(drop=True, inplace=True)
    tatm_raw.columns = list(range(dim_grid_lvl))

    tskin_tatm_raw = k_raw.ix[5*dim_grid_lvl+1:6*dim_grid_lvl,
                              k_raw.columns[-2]]

    tskin_tatm_raw.reset_index(drop=True, inplace=True)
    tskin_tatm_raw.columns = 0

    proffi_result['state_H2Oatm_avk'] = np.empty(
                                            [2, dim_grid_lvl, dim_grid_lvl])
    proffi_result['state_H2Oatm_avk'][0] =\
        np.array(wvatm_raw.ix[:dim_grid_lvl-1, list(range(dim_grid_lvl))]).T
    proffi_result['state_H2Oatm_avk'][1] =\
        np.array(wvatm_raw.ix[dim_grid_lvl:, list(range(dim_grid_lvl))]).T

    proffi_result['state_HDOatm_avk'] =\
        np.empty([2, dim_grid_lvl, dim_grid_lvl])
    proffi_result['state_HDOatm_avk'][0] =\
        np.array(wvatm_raw.ix[:dim_grid_lvl-1,
                 list(range(dim_grid_lvl, 2*dim_grid_lvl))]).T
    proffi_result['state_HDOatm_avk'][1] =\
        np.array(wvatm_raw.ix[dim_grid_lvl:,
                 list(range(dim_grid_lvl, 2*dim_grid_lvl))]).T

    proffi_result['state_Tatm2WVatm_xavk'] =\
        np.empty([2, dim_grid_lvl, dim_grid_lvl])
    proffi_result['state_Tatm2WVatm_xavk'][0] =\
        np.array(tatm_wvatm_raw.ix[:dim_grid_lvl-1,
                 list(range(dim_grid_lvl))]).T
    proffi_result['state_Tatm2WVatm_xavk'][1] =\
        np.array(tatm_wvatm_raw.ix[dim_grid_lvl:,
                 list(range(dim_grid_lvl))]).T

    proffi_result['state_Tskin2WVatm_xavk'] = np.empty([2, dim_grid_lvl])
    proffi_result['state_Tskin2WVatm_xavk'][0] =\
        np.array(tskin_wvatm_raw.ix[:dim_grid_lvl-1]).T
    proffi_result['state_Tskin2WVatm_xavk'][1] =\
        np.array(tskin_wvatm_raw.ix[dim_grid_lvl:]).T

    proffi_result['state_N2Oatm_avk'] =\
        np.empty([2, dim_grid_lvl, dim_grid_lvl])
    proffi_result['state_N2Oatm_avk'][0] =\
        np.array(ghgatm_raw.ix[:dim_grid_lvl-1,
                 list(range(dim_grid_lvl))]).T
    proffi_result['state_N2Oatm_avk'][1] =\
        np.array(ghgatm_raw.ix[dim_grid_lvl:, list(range(dim_grid_lvl))]).T

    proffi_result['state_CH4atm_avk'] =\
        np.empty([2, dim_grid_lvl, dim_grid_lvl])
    proffi_result['state_CH4atm_avk'][0] =\
        np.array(ghgatm_raw.ix[:dim_grid_lvl-1,
                 list(range(dim_grid_lvl, 2*dim_grid_lvl))]).T
    proffi_result['state_CH4atm_avk'][1] =\
        np.array(ghgatm_raw.ix[dim_grid_lvl:,
                 list(range(dim_grid_lvl, 2*dim_grid_lvl))]).T

    proffi_result['state_Tatm2GHGatm_xavk'] =\
        np.empty([2, dim_grid_lvl, dim_grid_lvl])
    proffi_result['state_Tatm2GHGatm_xavk'][0] =\
        np.array(tatm_ghgatm_raw.ix[:dim_grid_lvl-1,
                 list(range(dim_grid_lvl))]).T
    proffi_result['state_Tatm2GHGatm_xavk'][1] =\
        np.array(tatm_ghgatm_raw.ix[dim_grid_lvl:,
                 list(range(dim_grid_lvl))]).T

    proffi_result['state_Tskin2GHGatm_xavk'] =\
        np.empty([2, dim_grid_lvl])
    proffi_result['state_Tskin2GHGatm_xavk'][0] =\
        np.array(tskin_ghgatm_raw.ix[:dim_grid_lvl-1]).T
    proffi_result['state_Tskin2GHGatm_xavk'][1] =\
        np.array(tskin_ghgatm_raw.ix[dim_grid_lvl:]).T

    proffi_result['state_Tatm2HNO3atm_xavk'] = np.array(tatm_hno3atm_raw).T

    proffi_result['state_Tskin2HNO3atm_xavk'] = np.array(tskin_hno3atm_raw).T

    proffi_result['state_Tatm_avk'] = np.array(tatm_raw).T

    proffi_result['state_Tskin2Tatm_xavk'] = np.array(tskin_tatm_raw).T

    # read averaging kernels from kern_ful.dat
    # k_full = pd.read_csv(kern_ful_dat, header=None, delim_whitespace=True)

    # hno3atm_full = k_full.ix[5*dim_grid_lvl:6*dim_grid_lvl-1,
    #        list(range(5*dim_grid_lvl,6*dim_grid_lvl))]

    proffi_result['state_HNO3atm_avk'] = np.array(hno3atm_raw).T

    # read gain matrix from gain_raw.dat
    gain_raw = np.genfromtxt(gain_raw_dat)
    # wv_gain = gain_raw[:,:2*dim_grid_lvl]
    # proffi_result['state_H2Oatm_gain'] = wv_gain[:,:dim_grid_lvl].T
    # proffi_result['state_HDOatm_gain'] = wv_gain[:,dim_grid_lvl:].T
    # ghg_gain = gain_raw[:,2*dim_grid_lvl+1:4*dim_grid_lvl+1]
    # proffi_result['state_N2Oatm_gain'] = ghg_gain[:,:dim_grid_lvl].T
    # proffi_result['state_CH4atm_gain'] = ghg_gain[:,dim_grid_lvl:].T
    # hno3_gain = gain_raw[:,4*dim_grid_lvl+1:5*dim_grid_lvl+1]
    # proffi_result['state_HNO3atm_gain'] = hno3_gain.T
    # tatm_gain = gain_raw[:,5*dim_grid_lvl+1:6*dim_grid_lvl+1]
    # proffi_result['state_Tatm_gain'] = tatm_gain.T
    # tskin_gain = gain_raw[:,6*dim_grid_lvl+1]
    # proffi_result['state_Tskin_gain'] = tskin_gain.T

    gain = np.delete(gain_raw, 2*dim_grid_lvl, 1)[:, :-1]
    noise_mat = np.matmul(gain.T, np.matmul((noise * 1e4), gain))

    proffi_result['state_H2Oatm_n'] =\
        np.empty([2, dim_grid_lvl, dim_grid_lvl])
    proffi_result['state_H2Oatm_n'][0] =\
        noise_mat[:dim_grid_lvl, :dim_grid_lvl].T
    proffi_result['state_H2Oatm_n'][1] =\
        noise_mat[dim_grid_lvl:2*dim_grid_lvl, :dim_grid_lvl].T
    proffi_result['state_HDOatm_n'] =\
        np.empty([2, dim_grid_lvl, dim_grid_lvl])
    proffi_result['state_HDOatm_n'][0] =\
        noise_mat[:dim_grid_lvl, dim_grid_lvl:2*dim_grid_lvl].T
    proffi_result['state_HDOatm_n'][1] =\
        noise_mat[dim_grid_lvl:2*dim_grid_lvl, dim_grid_lvl:2*dim_grid_lvl].T
    proffi_result['state_N2Oatm_n'] =\
        np.empty([2, dim_grid_lvl, dim_grid_lvl])
    proffi_result['state_N2Oatm_n'][0] =\
        noise_mat[2*dim_grid_lvl:3*dim_grid_lvl,
                  2*dim_grid_lvl:3*dim_grid_lvl].T
    proffi_result['state_N2Oatm_n'][1] =\
        noise_mat[3*dim_grid_lvl:4*dim_grid_lvl,
                  2*dim_grid_lvl:3*dim_grid_lvl].T
    proffi_result['state_CH4atm_n'] =\
        np.empty([2, dim_grid_lvl, dim_grid_lvl])
    proffi_result['state_CH4atm_n'][0] =\
        noise_mat[2*dim_grid_lvl:3*dim_grid_lvl,
                  3*dim_grid_lvl:4*dim_grid_lvl].T
    proffi_result['state_CH4atm_n'][1] =\
        noise_mat[3*dim_grid_lvl:4*dim_grid_lvl,
                  3*dim_grid_lvl:4*dim_grid_lvl]
    proffi_result['state_HNO3atm_n'] =\
        noise_mat[4*dim_grid_lvl:5*dim_grid_lvl, 4*dim_grid_lvl:5*dim_grid_lvl]
    proffi_result['state_Tatm_n'] =\
        noise_mat[5*dim_grid_lvl:6*dim_grid_lvl, 5*dim_grid_lvl:6*dim_grid_lvl]
    proffi_result['state_Tskin_n'] = noise_mat[-1, -1]

    return proffi_result


def to_netcdf(input_dir, output_netcdf, processes=None):
    results = []

    # num_processes = min(processes, psutil.cpu_count())
    if processes is None:
        logger.debug("using {} processes".format(os.cpu_count()))
    else:
        logger.debug("using {} processes".format(processes))

    #noise = np.genfromtxt(config.get('iasi', 'noise'))

    with ProcessPoolExecutor(max_workers=processes) as e:
        jobs = [e.submit(_get_proffi_result, wd.path)
                for wd in scandir(input_dir) if os.path.isdir(wd.path)]

        results = []
        for i, job in enumerate(jobs):
            try:
                result = job.result()
                results.append(result)
            except Exception as e:
                print("ERROR for result %d" % i)
                print("-"*60)
                traceback.print_exc(file=sys.stdout)
                print("-"*60)

    results = list(filter(partial(is_not, None), results))
    if not results:
        return

    results = sorted(results, key=lambda d: d['event'])
    # num_events = int(results[-1]['event']) + 1

    s = output_netcdf
    s = os.path.join(os.path.dirname(s), os.path.basename(s).strip("L1C+L2_"))

    logger.debug("writing {} results to {}".format(str(len(results)), s))

    nc = netCDF4.Dataset(s, 'w', format="NETCDF4")
    create_result_netcdf(nc)

    for result in results:
        _write_result_to_netcdf(result, nc)

    nc.close()


def _write_result_to_netcdf(result, nc):
    # for current_row, result in enumerate(results):

    current_row = result['event']

    nc['L2_processor_major_version'][current_row] =\
        result['processor_major_version']
    nc['L2_product_minor_version'][current_row] =\
        result['product_minor_version']
    nc['along_track'][current_row] = result['along_track']
    nc['across_track'][current_row] = result['across_track']
    nc['srf_flag'][current_row] = result['srf_flag']

    if not np.isnan(result['cld_cov']):
        nc['cld_cov'][current_row] = result['cld_cov']
    if not np.isnan(result['cld_ph']):
        nc['cld_ph'][current_row] = result['cld_ph']
    if not np.isnan(result['cld_tp']):
        nc['cld_tp'][current_row] = result['cld_tp']
    if not np.isnan(result['cld_tt']):
        nc['cld_tt'][current_row] = result['cld_tt']
    if not np.isnan(result['flg_cdlfrm']):
        nc['flg_cdlfrm'][current_row] = result['flg_cdlfrm']
    if not np.isnan(result['flg_numit']):
        nc['flg_numit'][current_row] = result['flg_numit']
    if not np.isnan(result['flg_dustcld']):
        nc['flg_dustcld'][current_row] = result['flg_dustcld']
    if not np.isnan(result['flg_retcheck']):
        nc['flg_retcheck'][current_row] = result['flg_retcheck']
    if not np.isnan(result['flg_itconv']):
        nc['flg_itconv'][current_row] = result['flg_itconv']
    if not np.isnan(result['flg_resid']):
        nc['flg_resid'][current_row] = result['flg_resid']
    if not np.isnan(result['flg_iasicld']):
        nc['flg_iasicld'][current_row] = result['flg_iasicld']
    if not np.isnan(result['flg_cldnes']):
        nc['flg_cldnes'][current_row] = result['flg_cldnes']
    if not np.isnan(result['flg_initia']):
        nc['flg_initia'][current_row] = result['flg_initia']

    nc['lat'][current_row] = result['lat']
    nc['lon'][current_row] = result['lon']
    nc['Date'][current_row] = result['Date']
    nc['Time'][current_row] = result['Time']
    nc['UT_hours'][current_row] = result['UT_hours']
    nc['LT_hours'][current_row] = result['LT_hours']
    nc['sat_angle'][current_row] = result['sat_angle']
    nc['state_Tskin_a'][current_row] = result['state_Tskin_a']
    nc['tropopause_altitude'][current_row] = result['z_trop']

    nc['sfc_emi_flag'][current_row] = result['sfc_emi_flag']
    nc['sfc_emi'][current_row] = result['sfc_emi']
    nc['sfc_emi_wn'][:len(result['sfc_emi_wn'])] = result['sfc_emi_wn']

    # cloud
    if result['sfc_emi_flag'] == -2.0:
        nc['sfc_emi'].description =\
            "surface emisivity (assumed 1.0 for clouds)"
    # MASUDA
    # elif result['sfc_emi_flag'] == -1.0:
    #     nc['sfc_emi_wn'][1, :len(result['sfc_emi_wn'])] =\
    #         result['sfc_emi_wn']
    # IASI version 6
    # elif result['processor_major_version'] == 6.0:
    #     # nc['sfc_emi_flag'][current_row] = 6.0
    #     nc['sfc_emi_flag'].description =\
    #         ("Flag_-2 = -2 = Cloud\n"
    #          "Flag_-1 = -1 = MASUDA\n"
    #          "Flag_6 = 6 = IASIL2 surface emissivity")
    #
    #     nc['sfc_emi_wn'][0, :len(result['sfc_emi_wn'])] =\
    #         result['sfc_emi_wn']
    #     nc['sfc_emi_wn'].description =\
    #         ("wavenumber scale for surface emissivity "
    #          "data [cm-1] (vector {IASIL2,MASUDA})")
    # # IREMIS
    # else:
    #     nc['sfc_emi_wn'][0, :len(result['sfc_emi_wn'])] =\
    #         result['sfc_emi_wn']

    nc['IASI_spectrum_freq'][:] = result['IASI_spectrum_freq']
    nc['IASI_spectrum_meas'][current_row] = result['IASI_spectrum_meas']
    nc['IASI_spectrum_simu'][current_row] = result['IASI_spectrum_simu']
    nc['fit_quality'][current_row] = result['fit_quality']
    nc['atm_nol'][current_row] = result['atm_nol']
    nc['atm_altitude'][current_row, :result['atm_altitude'].size] =\
        result['atm_altitude']
    nc['atm_pressure'][current_row, :result['atm_pressure'].size] =\
        result['atm_pressure']
    nc['instrument'][current_row] = result['instrument']
    nc['iter'][current_row] = result['iter']
    nc['state_Tskin'][current_row] = result['state_Tskin']
    nc['atm_H2O_EUML2'][current_row, :result['atm_H2O_EUML2'].size] =\
        result['atm_H2O_EUML2']
    nc['state_GHGatm'][current_row, 0, :result['state_N2Oatm'].size] =\
        result['state_N2Oatm']
    nc['state_GHGatm'][current_row, 1, :result['state_CH4atm'].size] =\
        result['state_CH4atm']
    nc['state_GHGatm_a'][current_row, 0, :result['state_N2Oatm_a'].size] =\
        result['state_N2Oatm_a']
    nc['state_GHGatm_a'][current_row, 1, :result['state_CH4atm_a'].size] =\
        result['state_CH4atm_a']
    nc['state_HNO3atm'][current_row, :result['state_HNO3atm'].size] =\
        result['state_HNO3atm']
    nc['state_HNO3atm_a'][current_row, :result['state_HNO3atm_a'].size] =\
        result['state_HNO3atm_a']
    nc['state_Tatm'][current_row, :result['state_Tatm'].size] =\
        result['state_Tatm']
    nc['state_Tatm_a'][current_row, :result['state_Tatm_a'].size] =\
        result['state_Tatm_a']
    nc['state_WVatm'][current_row, 0, :result['state_H2Oatm'].size] =\
        result['state_H2Oatm']
    nc['state_WVatm'][current_row, 1, :result['state_HDOatm'].size] =\
        result['state_HDOatm']
    nc['state_WVatm_a'][current_row, 0, :result['state_H2Oatm_a'].size] =\
        result['state_H2Oatm_a']
    nc['state_WVatm_a'][current_row, 1, :result['state_HDOatm_a'].size] =\
        result['state_HDOatm_a']
    nc['state_WVatm_avk'][current_row, 0, 0,
                          :result['state_H2Oatm_avk'][0].shape[0],
                          :result['state_H2Oatm_avk'][0].shape[1]] =\
        result['state_H2Oatm_avk'][0]
    nc['state_WVatm_avk'][current_row, 0, 1,
                          :result['state_H2Oatm_avk'][1].shape[0],
                          :result['state_H2Oatm_avk'][1].shape[1]] =\
        result['state_H2Oatm_avk'][1]
    nc['state_WVatm_avk'][current_row, 1, 0,
                          :result['state_HDOatm_avk'][0].shape[0],
                          :result['state_HDOatm_avk'][0].shape[1]] =\
        result['state_HDOatm_avk'][0]
    nc['state_WVatm_avk'][current_row, 1, 1,
                          :result['state_HDOatm_avk'][1].shape[0],
                          :result['state_HDOatm_avk'][1].shape[1]] =\
        result['state_HDOatm_avk'][1]
    nc['state_Tatm2WVatm_xavk'][current_row, 0,
                                :result['state_Tatm2WVatm_xavk'][0]
                                .shape[0],
                                :result['state_Tatm2WVatm_xavk'][0]
                                .shape[1]] =\
        result['state_Tatm2WVatm_xavk'][0]
    nc['state_Tatm2WVatm_xavk'][current_row, 1,
                                :result['state_Tatm2WVatm_xavk'][1]
                                .shape[0],
                                :result['state_Tatm2WVatm_xavk'][1]
                                .shape[1]] =\
        result['state_Tatm2WVatm_xavk'][1]
    nc['state_Tskin2WVatm_xavk'][current_row, 0,
                                 :result['state_Tskin2WVatm_xavk'][0]
                                 .shape[0]] =\
        result['state_Tskin2WVatm_xavk'][0]
    nc['state_Tskin2WVatm_xavk'][current_row, 1,
                                 :result['state_Tskin2WVatm_xavk'][1]
                                 .shape[0]] =\
        result['state_Tskin2WVatm_xavk'][1]
    nc['state_GHGatm_avk'][current_row, 0, 0,
                           :result['state_N2Oatm_avk'][0].shape[0],
                           :result['state_N2Oatm_avk'][0].shape[1]] =\
        result['state_N2Oatm_avk'][0]
    nc['state_GHGatm_avk'][current_row, 0, 1,
                           :result['state_N2Oatm_avk'][1].shape[0],
                           :result['state_N2Oatm_avk'][1].shape[1]] =\
        result['state_N2Oatm_avk'][1]
    nc['state_GHGatm_avk'][current_row, 1, 0,
                           :result['state_CH4atm_avk'][0].shape[0],
                           :result['state_CH4atm_avk'][0].shape[1]] =\
        result['state_CH4atm_avk'][0]
    nc['state_GHGatm_avk'][current_row, 1, 1,
                           :result['state_CH4atm_avk'][1].shape[0],
                           :result['state_CH4atm_avk'][1].shape[1]] =\
        result['state_CH4atm_avk'][1]
    nc['state_Tatm2GHGatm_xavk'][current_row, 0,
                                 :result['state_Tatm2GHGatm_xavk'][0]
                                 .shape[0],
                                 :result['state_Tatm2GHGatm_xavk'][0]
                                 .shape[1]] =\
        result['state_Tatm2GHGatm_xavk'][0]
    nc['state_Tatm2GHGatm_xavk'][current_row, 1,
                                 :result['state_Tatm2GHGatm_xavk'][1]
                                 .shape[0],
                                 :result['state_Tatm2GHGatm_xavk'][1]
                                 .shape[1]] =\
        result['state_Tatm2GHGatm_xavk'][1]
    nc['state_Tskin2GHGatm_xavk'][current_row, 0,
                                  :result['state_Tskin2GHGatm_xavk'][0]
                                  .shape[0]] =\
        result['state_Tskin2GHGatm_xavk'][0]
    nc['state_Tskin2GHGatm_xavk'][current_row, 1,
                                  :result['state_Tskin2GHGatm_xavk'][1]
                                  .shape[0]] =\
        result['state_Tskin2GHGatm_xavk'][1]
    nc['state_HNO3atm_avk'][current_row,
                            :result['state_HNO3atm_avk'].shape[0],
                            :result['state_HNO3atm_avk'].shape[1]] =\
        result['state_HNO3atm_avk']
    nc['state_Tatm2HNO3atm_xavk'][current_row,
                                  :result['state_Tatm2HNO3atm_xavk']
                                  .shape[0],
                                  :result['state_Tatm2HNO3atm_xavk']
                                  .shape[1]] =\
        result['state_Tatm2HNO3atm_xavk']
    nc['state_Tskin2HNO3atm_xavk'][current_row,
                                   :result['state_Tskin2HNO3atm_xavk']
                                   .shape[0]] =\
        result['state_Tskin2HNO3atm_xavk']
    nc['state_Tatm_avk'][current_row,
                         :result['state_Tatm_avk'].shape[0],
                         :result['state_Tatm_avk'].shape[1]] =\
        result['state_Tatm_avk']
    nc['state_Tskin2Tatm_xavk'][current_row,
                                :result['state_Tskin2Tatm_xavk']
                                .shape[0]] =\
        result['state_Tskin2Tatm_xavk']
    nc['state_WVatm_n'][current_row, 0, 0,
                        :result['state_H2Oatm_n'][0].shape[0],
                        :result['state_H2Oatm_n'][0].shape[1]] =\
        result['state_H2Oatm_n'][0]
    nc['state_WVatm_n'][current_row, 0, 1,
                        :result['state_H2Oatm_n'][1].shape[0],
                        :result['state_H2Oatm_n'][1].shape[1]] =\
        result['state_H2Oatm_n'][1]
    nc['state_WVatm_n'][current_row, 1, 0,
                        :result['state_HDOatm_n'][0].shape[0],
                        :result['state_HDOatm_n'][0].shape[1]] =\
        result['state_HDOatm_n'][0]
    nc['state_WVatm_n'][current_row, 1, 1,
                        :result['state_HDOatm_n'][1].shape[0],
                        :result['state_HDOatm_n'][1].shape[1]] =\
        result['state_HDOatm_n'][1]
    nc['state_GHGatm_n'][current_row, 0, 0,
                         :result['state_N2Oatm_n'][0].shape[0],
                         :result['state_N2Oatm_n'][0].shape[1]] =\
        result['state_N2Oatm_n'][0]
    nc['state_GHGatm_n'][current_row, 0, 1,
                         :result['state_N2Oatm_n'][1].shape[0],
                         :result['state_N2Oatm_n'][1].shape[1]] =\
        result['state_N2Oatm_n'][1]
    nc['state_GHGatm_n'][current_row, 1, 0,
                         :result['state_CH4atm_n'][0].shape[0],
                         :result['state_CH4atm_n'][0].shape[1]] =\
        result['state_CH4atm_n'][0]
    nc['state_GHGatm_n'][current_row, 1, 1,
                         :result['state_CH4atm_n'][1].shape[0],
                         :result['state_CH4atm_n'][1].shape[1]] =\
        result['state_CH4atm_n'][1]
    nc['state_HNO3atm_n'][current_row,
                          :result['state_HNO3atm_n'].shape[0],
                          :result['state_HNO3atm_n'].shape[1]] =\
        result['state_HNO3atm_n']
    nc['state_Tatm_n'][current_row,
                       :result['state_Tatm_n'].shape[0],
                       :result['state_Tatm_n'].shape[1]] =\
        result['state_Tatm_n']
    nc['state_Tskin_n'][current_row] = result['state_Tskin_n']
