import netCDF4
import numpy as np

from glob import glob
from collections import defaultdict
from functools import partial

files = glob('IASI-*.nc')
locations = {"Ny-Alesund": (78.92, 11.93), "Kiruna": (67.84, 20.41),
             "St.Peterburgo": (59.88, 29.83), "Karlsruhe": (49.1, 8.9),
             "Rikubetsu": (43.5, 143.8), "Izana": (28.30, -16.48),
             "Maido": (-21.1, 55.4), "Wollongon": (-34.41, 150.88),
             "Lauder": (-45.038, 169.684), "ArrivalHeights": (-77.82, 166.65)}


def get_indices(lat, lon, center=(0,0), margin=4):
    lat_indx = np.where((lat > center[0] - margin/2) &
                        (lat < center[0] + margin/2))
    lon_indx = np.where((lon > center[1] - margin/2) &
                        (lon < center[1] + margin/2))
    return np.intersect1d(lat_indx, lon_indx)


def write_netcdf(location, files):
    nc = netCDF4.Dataset(location + ".nc", 'w', format="NETCDF4")

    nc.createDimension("event")
    nc.createDimension("atmospheric_grid_levels", 28)
    nc.createDimension("atmospheric_species", 2)
    nc.createDimension("emissivity_specgrid", 20)
    nc.createDimension("spectral_bins", 841)

    # along_track
    nc_along_track = nc.createVariable("along_track", "f8", ("event",))
    nc_along_track.description = "along track satellite observation index"
    # across_track
    nc_across_track = nc.createVariable("across_track", "f8", ("event",))
    nc_across_track.description = "across track satellite observation index"
    # srf_flag
    nc_srf_flag = nc.createVariable("srf_flag", "f8", ("event",))
    nc_srf_flag.description = ("0:water, 1:land low, 2:land high, "\
                               "3:land water low, 4:land water high")
    # cld_cov
    nc_cld_cov = nc.createVariable("cld_cov", "f8", ("event",))
    nc_cld_cov.description = "L2 cloud coverage [%]"
    # cld_ph
    nc_cld_ph = nc.createVariable("cld_ph", "f8", ("event",))
    nc_cld_ph.description = "L2 cloud phase (1 = liquid, 2 = ice, 3 = mixed)"
    # cld_tp
    nc_cld_tp = nc.createVariable("cld_tp", "f8", ("event",))
    nc_cld_tp.description = "L2 cloud top pressure [Pa]"
    # cld_tt
    nc_cld_tt = nc.createVariable("cld_tt", "f8", ("event",))
    nc_cld_tt.description = "L2 cloud top temperature [K]"
    # Date
    nc_date = nc.createVariable("Date", "f8", ("event",))
    nc_date.description = "UT Date [YYYYMMDD]"
    # IASI_spectrum_freq
    nc_iasi_spectrum_freq = nc.createVariable("IASI_spectrum_freq", "f8",\
            ("spectral_bins",))
    nc_iasi_spectrum_freq.description = "frequency of spectra [cm-1]"
    # IASI_spectrum_meas
    nc_iasi_spectrum_meas = nc.createVariable("IASI_spectrum_meas", "f8",\
            ("event", "spectral_bins",))
    nc_iasi_spectrum_meas.description =\
            "measured IASI spectra [nW/(sr*cm^2*cm-1)]"
    # IASI_spectrum_simu
    nc_iasi_spectrum_simu = nc.createVariable("IASI_spectrum_simu", "f8",\
            ("event", "spectral_bins",))
    nc_iasi_spectrum_simu.description =\
            "simulated IASI spectra [nW/(sr*cm^2*cm-1)]"
    # LT_hours
    nc_lt_hours = nc.createVariable("LT_hours", "f8", ("event",))
    nc_lt_hours.description = "decimal hours of local time"
    # Time
    nc_time = nc.createVariable("Time", "f8", ("event",))
    nc_time.description = "UT Time [hhmmss]"
    # UT_hours
    nc_ut_hours = nc.createVariable("UT_hours", "f8", ("event",))
    nc_ut_hours.description = "UT decimal hours"
    # atm_H2O_EUML2
    nc_atm_h2o_euml2 = nc.createVariable("atm_H2O_EUML2", "f8",\
            ("event", "atmospheric_grid_levels",), fill_value=-9999.99)
    nc_atm_h2o_euml2.description = ("EUMETSAT L2 H2O profiles ({ln[H2O]} "
            "[natural logarithms of mixing ratios in ppmv])")
    # atm_altitude
    nc_atm_altitude = nc.createVariable("atm_altitude", "f8",\
            ("event", "atmospheric_grid_levels",), fill_value=-9999.99)
    nc_atm_altitude.description = "atmospheric altitude levels [m a.s.l.]"
    # atm_nol
    nc_atm_nol = nc.createVariable("atm_nol", "f8", ("event",))
    nc_atm_nol.description = "number of actual atmospheric grid levels"
    # atm_pressure
    nc_atm_pressure = nc.createVariable("atm_pressure", "f8",\
            ("event", "atmospheric_grid_levels",), fill_value=-9999.99)
    nc_atm_pressure.description = "atmospheric pressure levels [Pa]"
    # fit_quality
    nc_fit_quality = nc.createVariable("fit_quality", "f8", ("event",))
    nc_fit_quality.description =\
            "spectral fit residual (root-mean-square of measurement)"
    # instrument
    nc_instrument = nc.createVariable("instrument", "f8", ("event",))
    nc_instrument.description = "1: IASI-A; 2: IASI-B; 3: IASI-C"
    # iter
    nc_iter = nc.createVariable("iter", "f8", ("event",))
    nc_iter.description =\
            "number of iterations needed for the retrieval process"
    # lat
    nc_lat = nc.createVariable("lat", "f8", ("event",))
    nc_lat.description = ("geographical latitude [deg], where north is "
            "positive and south negative")
    # lon
    nc_lon = nc.createVariable("lon", "f8", ("event",))
    nc_lon.description = ("geographical longitude [deg], where east is "
            "positive and west negative")
    # sat_angle
    nc_sat_angle = nc.createVariable("sat_angle", "f8", ("event",))
    nc_sat_angle.description = "satellite instrument viewing angle [deg]"
    # sfc_emi
    nc_sfc_emi = nc.createVariable("sfc_emi", "f8",\
            ("event", "emissivity_specgrid",), fill_value=-9999.99)
    nc_sfc_emi.description = "surface emisivity"
    # sfc_emi_wn
    nc_sfc_emi_wn = nc.createVariable("sfc_emi_wn", "f8",\
            ("emissivity_specgrid",), fill_value=-9999.99)
    nc_sfc_emi_wn.description =\
            "wavenumber scale for surface emissivity data [cm-1]"
    # state_GHGatm
    nc_state_ghgatm = nc.createVariable("state_GHGatm", "f8",\
            ("event", "atmospheric_species",\
            "atmospheric_grid_levels",), fill_value=-9999.99)
    nc_state_ghgatm.description = ("retrieved profiles of atmospheric "
            "greenhouse gas species (state vector {ln[N2O],ln[CH4]} "
            "[natural logarithms of mixing ratios in ppmv])")
    # state_GHGatm_a
    nc_state_ghgatm_a = nc.createVariable("state_GHGatm_a", "f8",\
            ("event", "atmospheric_species",\
            "atmospheric_grid_levels",), fill_value=-9999.99)
    nc_state_ghgatm_a.description = ("apriori profiles of atmospheric "
            "greenhouse gas species (state vector {ln[N2O],ln[CH4]} "
            "[natural logarithms of mixing ratios in ppmv])")
    # state_GHGatm_n
    nc_state_ghgatm_n = nc.createVariable("state_GHGatm_n", "f8",\
            ("event", "atmospheric_species", "atmospheric_species",\
            "atmospheric_grid_levels", "atmospheric_grid_levels",),\
            fill_value=-9999.99)
    nc_state_ghgatm_n.description = ("state vector noise matrix "
            "for greenhouse gases")
    # state_GHGatm_avk
    nc_state_ghgatm_avk = nc.createVariable("state_GHGatm_avk", "f8",\
            ("event", "atmospheric_species", "atmospheric_species",\
            "atmospheric_grid_levels", "atmospheric_grid_levels",),\
            fill_value=-9999.99)
    nc_state_ghgatm_avk.description = ("state vector {ln[N2O],ln[CH4]} "
            "averaging kernel matrix "
            "[for natural logarithmus of mixing ratios in ppmv]")
    # state_Tatm2GHGatm_xavk
    nc_state_tatm2ghgatm_xavk = nc.createVariable("state_Tatm2GHGatm_xavk",\
            "f8", ("event", "atmospheric_species",\
            "atmospheric_grid_levels", "atmospheric_grid_levels",),\
            fill_value=-9999.99)
    nc_state_tatm2ghgatm_xavk.description = ("cross averaging kernel matrix "
            "for natural logarithmus of temperature and greenhouse gases")
    # state_Tskin2GHGatm_xavk
    nc_state_tskin2ghgatm_xavk = nc.createVariable("state_Tskin2GHGatm_xavk",\
            "f8", ("event", "atmospheric_species",\
            "atmospheric_grid_levels",),
            fill_value=-9999.99)
    nc_state_tskin2ghgatm_xavk.description = ("cross averaging kernel matrix "
            "for skin temperature and greenhouse gas species")
    # state_HNO3atm
    nc_state_hno3atm = nc.createVariable("state_HNO3atm", "f8",\
            ("event", "atmospheric_grid_levels",), fill_value=-9999.99)
    nc_state_hno3atm.description = ("retrieved profiles of atmospheric "
            "greenhouse gas species (state vector {[HNO3]} "
            "[mixing ratios in ppmv])")
    # state_HNO3atm_a
    nc_state_hno3atm_a = nc.createVariable("state_HNO3atm_a", "f8",\
            ("event", "atmospheric_grid_levels",), fill_value=-9999.99)
    nc_state_hno3atm_a.description = ("apriori profiles of atmospheric "
            "greenhouse gas species (state vector {[HNO3]} "
            "[mixing ratios in ppmv])")
    # state_HNO3atm_n
    nc_state_hno3atm_n = nc.createVariable("state_HNO3atm_n", "f8",\
            ("event", "atmospheric_grid_levels", "atmospheric_grid_levels",),\
            fill_value=-9999.99)
    nc_state_hno3atm_n.description = ("state vector noise matrix for HNO3")
    # state_HNO3atm_avk
    nc_state_hno3atm_avk = nc.createVariable("state_HNO3atm_avk", "f8",\
            ("event", "atmospheric_grid_levels", "atmospheric_grid_levels",),\
            fill_value=-9999.99)
    nc_state_hno3atm_avk.description = ("state vector {[HNO3]} averaging "
            "kernel matrix [for mixing ratios in ppmv]")
    # state_Tatm2HNO3atm_xavk
    nc_state_tatm2hno3atm_xavk = nc.createVariable("state_Tatm2HNO3atm_xavk",\
            "f8", ("event",\
            "atmospheric_grid_levels", "atmospheric_grid_levels",),\
            fill_value=-9999.99)
    nc_state_tatm2hno3atm_xavk.description = ("cross averaging kernel matrix "
            "for natural logarithmus of temperature and HNO3")
    # state_Tskin2HNO3atm_xavk
    nc_state_tskin2hno3atm_xavk = nc.createVariable("state_Tskin2HNO3atm_xavk",
            "f8", ("event", "atmospheric_grid_levels",),\
            fill_value=-9999.99)
    nc_state_tskin2hno3atm_xavk.description = ("cross averaging kernel matrix "
            "for skin temperature and HNO3")
    # state_Tatm
    nc_state_tatm = nc.createVariable("state_Tatm", "f8",\
            ("event", "atmospheric_grid_levels",), fill_value=-9999.99)
    nc_state_tatm.description =\
            "retrieved profiles of atmospheric temperature [K]"
    # state_Tatm_a
    nc_state_tatm_a = nc.createVariable("state_Tatm_a", "f8",\
            ("event", "atmospheric_grid_levels",), fill_value=-9999.99)
    nc_state_tatm_a.description =\
            "apriori profiles of atmospheric temperature [K]"
    # state_Tatm_n
    nc_state_tatm_n = nc.createVariable("state_Tatm_n", "f8",\
            ("event", "atmospheric_grid_levels", "atmospheric_grid_levels",),\
            fill_value=-9999.99)
    nc_state_tatm_n.description = ("state vector noise matrix "
            "for atmospheric temperature")
    # state_Tskin
    nc_state_tskin = nc.createVariable("state_Tskin", "f8", ("event",))
    nc_state_tskin.description = "retrieved surface skin temperature [K]"
    # state_Tskin_a
    nc_state_tskin_a = nc.createVariable("state_Tskin_a", "f8", ("event",))
    nc_state_tskin_a.description = "apriori surface skin temperature [K]"
    # state_Tskin_n
    nc_state_tskin_n = nc.createVariable("state_Tskin_n", "f8", ("event",))
    nc_state_tskin_n.description = "noise for surface skin temperature"
    # state_WVatm
    nc_state_wvatm = nc.createVariable("state_WVatm", "f8",\
            ("event", "atmospheric_species", "atmospheric_grid_levels",),\
            fill_value=-9999.99)
    nc_state_wvatm.description = ("retrieved profiles of atmospheric "
            "water vapour species (state vector {ln[H2O],ln[HDO]} "
            "[natural logarithms of mixing ratios in ppmv])")
    # state_WVatm_a
    nc_state_wvatm_a = nc.createVariable("state_WVatm_a", "f8",\
            ("event", "atmospheric_species", "atmospheric_grid_levels",),\
            fill_value=-9999.99)
    nc_state_wvatm_a.description = ("apriori profiles of atmospheric "
            "water vapour species (state vector {ln[H2O],ln[HDO]} "
            "[natural logarithms of mixing ratios in ppmv])")
    # state_WVatm_n
    nc_state_wvatm_n = nc.createVariable("state_WVatm_n", "f8",\
            ("event", "atmospheric_species", "atmospheric_species",\
            "atmospheric_grid_levels", "atmospheric_grid_levels",),
            fill_value=-9999.99)
    nc_state_wvatm_n.description = ("state vector noise matrix "
            "for atmospheric water vapour species")
    # state_WVatm_avk
    nc_state_wvatm_avk = nc.createVariable("state_WVatm_avk", "f8",\
            ("event", "atmospheric_species", "atmospheric_species",\
            "atmospheric_grid_levels", "atmospheric_grid_levels",),\
            fill_value=-9999.99)
    nc_state_wvatm_avk.description = ("state vector {ln[H2O],ln[HDO]} "
            "averaging kernel matrix "
            "[for natural logarithms of mixing ratios in ppmv]")
    # state_Tatm2WVatm_xavk
    nc_state_tatm2wvatm_xavk = nc.createVariable("state_Tatm2WVatm_xavk",\
            "f8", ("event", "atmospheric_species",\
            "atmospheric_grid_levels", "atmospheric_grid_levels",),\
            fill_value=-9999.99)
    nc_state_tatm2wvatm_xavk.description = ("cross averaging kernel matrix "
            "for natural logarithmus of temperature and water vapor")
    # state_Tskin2WVatm_xavk
    nc_state_tskin2wvatm_xavk = nc.createVariable("state_Tskin2WVatm_xavk",\
            "f8", ("event", "atmospheric_species",\
            "atmospheric_grid_levels",),\
            fill_value=-9999.99)
    nc_state_tskin2wvatm_xavk.description = ("cross averaging kernel matrix "
            "for skin temperature and water vapor")
    # state_Tatm_avk
    nc_state_tatm_avk = nc.createVariable("state_Tatm_avk", "f8",\
            ("event",\
            "atmospheric_grid_levels", "atmospheric_grid_levels",),\
            fill_value=-9999.99)
    nc_state_tatm_avk.description = ("averaging kernel matrix for "
            "atmospheric temperature")
    # state_Tskin2Tatm_xavk
    nc_state_tskin2tatm_xavk = nc.createVariable("state_Tskin2Tatm_xavk",\
            "f8", ("event", "atmospheric_grid_levels",),\
            fill_value=-9999.99)
    nc_state_tskin2tatm_xavk.description = ("cross averaging kernel matrix "
            "for skin temperature and atmospheric temperature")

    current_row = 0

    for f in files:
        nc_src = netCDF4.Dataset(f)
        indx = get_indices(nc_src.variables['lat'][:],
                           nc_src.variables['lon'][:],
                           locations[location])

        print(f, indx)

        if indx.any():
            for i in indx:
                for variable in nc_src.variables:
                    #print(nc_src.variables[variable])
                    #print(nc.variables[variable])
                    print(i, current_row)
                    try:
                        v = nc_src.variables[variable][i]
                        #print(v.shape)
                        nc.variables[variable][current_row] = v
                    except:
                        print("exception on ", variable)
                current_row += 1

        nc.variables['IASI_spectrum_freq'] = nc_src.variables['IASI_spectrum_freq']
        nc.variables['sfc_emi_wn'] = nc_src.variables['sfc_emi_wn']
        nc_src.close()

    nc.close()
