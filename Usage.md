# PROFFI96 processing for IASI data
http://www.imk-asf.kit.edu/english/2750.php

https://www.eumetsat.int/website/home/Satellites/CurrentSatellites/Metop/MetopDesign/IASI/index.html

## Table of Contents

* [Requirements](#Requirements)
  * [System](#System)
  * [Python](#Python)
  * [HITRAN](#HITRAN)
  * [SRTM](#SRTM)
  * [inv_heights](#inv_heights)
* [Usage](#Usage)
  * [Configuration](#Configuration)
  * [Build proffi96](#Build proffi96)
  * [Get IASI data](#Get IASI data)
  * [Run the processor](#Run the processor)
* [Common errors](#Common errors)
* [TL;DR](#TL;DR)

## Requirements <a name="Requirements"></a>
### System <a name="System"></a>
This program has been tested on Debian- and Sun-based systems and requires Python 3.

https://www.python.org/

The program needs the external `gdallocationinfo` program to be installed. It is part of the `gdal-bin` package for most distributions or can be installed with the GDAL - Geospatial Data Abstraction Library.

http://www.gdal.org/

For Debian-based systems
```bash
sudo apt install gdal-bin
```

### Python <a name="Python"></a>
Following python libraries are required

* [numpy](http://www.numpy.org/)
* [pandas](http://pandas.pydata.org/)
* [netCDF4](https://unidata.github.io/netcdf4-python/)
* [julian](https://pypi.python.org/pypi/julian)
* [timezonefinder](https://pypi.python.org/pypi/timezonefinder)
* [aiohttp](https://pypi.python.org/pypi/aiohttp)
* [psutil](https://pypi.python.org/pypi/psutil/)

You might require the `build-essential` `python3` `python3-virtualenv` `python3-dev` packages.

```bash
sudo apt install build-essential python3 python3-virtualenv python3-dev
```

It is recommended to install the python libraries in a virtual environment, for example

```bash
python3 -m venv env
source env/bin/activate
pip install --upgrade numpy pandas netCDF4 julian timezonefinder aiohttp psutil
```

### HITRAN <a name="HITRAN"></a>

You need the following HITRAN *.par files from [HITRAN](http://hitran.org/)

* H2O - e.g. 01_HIT16.par
* CO2 - e.g. 02_HIT16.par
* N2O - e.g. 04_HIT16.par
* CH4 - e.g. 06_HIT16.par
* HNO3 - e.g. 12_HIT16.par

A recommended place is for example `iasi-processing/data/hitran/`

### SRTM <a name="SRTM"></a>

To retrieve the surface hight, the processor requires the SRTM version 4.1 resampled 250m *.tif files from [SRTM](http://gisweb.ciat.cgiar.org/TRMM/SRTM_Resampled_250m/)

!! These files need ~21GB space when unpacked !!

* [SRTM_NE_250m.tif](http://gisweb.ciat.cgiar.org/TRMM/SRTM_Resampled_250m/SRTM_NE_250m_TIF.rar)
* [SRTM_SE_250m.tif](http://gisweb.ciat.cgiar.org/TRMM/SRTM_Resampled_250m/SRTM_SE_250m_TIF.rar)
* [SRTM_W_250m.tif](http://gisweb.ciat.cgiar.org/TRMM/SRTM_Resampled_250m/SRTM_W_250m_TIF.rar)

You can use `unrar` to unpack the files

```bash
sudo apt install unrar
```

And unpack them with

```bash
unrar e SRTM_NE_250m_TIF.rar
```

A recommended place is for example `iasi-processing/data/srtm/`

### inv_heights <a name="inv_heights"></a>

You need specific input files for the retrieval according to the altitude grid.

Example files can be found here [v2017_oTf.zip](/uploads/56a3b42913aad6d3bed998c937a08a59/v2017_oTf.zip)

You can use `unzip` to unpack the files

```bash
sudo apt install unzip
```

And unpack them with

```bash
unzip v2017_oTf.zip
```

A recommended place is for example `iasi-processing/data/v2017_oTf/`

## Usage <a name="Usage"></a>

Get the source code

```bash
git clone https://git.scc.kit.edu/jz9384/iasi-processing.git
```

Change to your python virtual environment and run the program with the `--help` parameter

```bash
source env/bin/activate
cd iasi-processing/
python proffi-processing.py --help
```

You should see the following output

```bash
usage: proffi-processing.py [-h] [-c CONFIG] [--log LOG] [-o OUTPUT] [-l] [-r]
                            [--clean] [-p PROCESSES]
                            {netcdf,iasi,results} ...

PROFFI processing

(C) Karlsruhe Institute of Technology (KIT)
    Institute of Meteorology and Climate Research (IMK)
    Steinbuch Centre for Computing (SCC)

Run PROFFI data processor

positional arguments:
  {netcdf,iasi,results}
    netcdf              process netCDF input data
    iasi                process IASI L1 L2 data
    results             generate netcdf file from results

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG
                        configuration file (default: proffi-processing.cfg)
  --log LOG             log file (default: proffi-processing.log)
  -o OUTPUT, --output OUTPUT
                        output directory (default: out/)
  -l, --local           enalbe local processing
  -r, --remote          enable remote processing
  --clean               remove preprocessed data after processing
  -p PROCESSES, --processes PROCESSES
                        maximum number of processes
```

If you don't see the above output please check the error messages before you continue.

### Configuration <a name="Configuration"></a>

The main configuration is done in the [proffi-configuration.cfg](https://git.scc.kit.edu/jz9384/iasi-processing/blob/master/proffi-processing.cfg) file.

To run the processor change the following sections

```python
[HITRAN]
h2o = ABSOLUTE_PATH_TO_YOUR_HITRAN_H2O_PAR
hdo = ABSOLUTE_PATH_TO_YOUR_HITRAN_HDO_PAR
co2 = ABSOLUTE_PATH_TO_YOUR_HITRAN_CO2_PAR
n2o = ABSOLUTE_PATH_TO_YOUR_HITRAN_N2O_PAR
ch4 = ABSOLUTE_PATH_TO_YOUR_HITRAN_CH4_PAR
hno3 = ABSOLUTE_PATH_TO_YOUR_HITRAN_HNO3_PAR

[inv_heights]
path = PATH_TO_YOUR_INV_HEIGHTS

...

[processing]
input_path = PATH_TO_YOUR_TEMPLATE_INPUT
local_exe = NAME_OF_THE_EXECUTABLE

...

[srtm]
north_east = PATH_TO_YOUR_SRTM_NE_250M
south_east = PATH_TO_YOUR_SRTM_SE_250M
west = PATH_TO_YOUR_SRTM_W_250M

...

```

A default configuration when you stored the required data in the recommended directories would look like the following

```python
[HITRAN]
h2o = /home/user1/iasi-processing/data/hitran/01_HIT16.par
hdo = /home/user1/iasi-processing/data/hitran/01_HIT16.par
co2 = /home/user1/iasi-processing/data/hitran/02_HIT16.par
n2o = /home/user1/iasi-processing/data/hitran/04_HIT16.par
ch4 = /home/user1/iasi-processing/data/hitran/06_HIT16.par
hno3 = /home/user1/iasi-processing/data/hitran/12_HIT16.par

[inv_heights]
path = data/v2017_oTf/

...

[processing]
input_path = input
local_exe = proffi96

...

[srtm]
north_east = data/srtm/SRTM_NE_250m.tif
south_east = data/srtm/SRTM_SE_250m.tif
west = data/srtm/SRTM_W_250m.tif 

...

```

### Build proffi96 <a name="Build proffi96"></a>

To build the main proffi96 Fortran program, go to the [code](https://git.scc.kit.edu/jz9384/iasi-processing/blob/master/code) directory or the [code/new](https://git.scc.kit.edu/jz9384/iasi-processing/blob/master/code/new) directory, depending on the version you want to run. There you can build the program with the given [Makefile](https://git.scc.kit.edu/jz9384/iasi-processing/blob/master/code/new/Makefile) which will produce the `proffi96` executable by default.

You should then place the executable in the template [input](https://git.scc.kit.edu/jz9384/iasi-processing/blob/master/input) directory.

```bash
cd code/new
make
mv proffi96 ../../input
```

### Get IASI data <a name="Get IASI data"></a>

You can get the IASI data from [EUMETSAT](https://eoportal.eumetsat.int/userMgmt/login.faces) after registration.

To work with the processor you have to get the data in netCDF format.

### Run the processor <a name="Run the processor"></a>

To produce all the input directories for all valid observations in a specific output directory you can run the program as following, assuming your IASI Level 1 and Level 2 data is called IASI-L1.nc and IASI-L2.nc

```bash
python proffi-processing.py -o output_dir iasi IASI-L1.nc IASI-L2.nc
```

You can change the filter criterion for valid observations in the [proffi-configuration.cfg](https://git.scc.kit.edu/jz9384/iasi-processing/blob/master/proffi-processing.cfg) configuration file.

```bash
...

[iasi]
filter = flg_iasicld
filter_value = 0

...
```

To not only produce the input directories but also start the proffi96 processing you can run the program as following, with `-p` specifying the number of processors to use

```bash
python proffi-processing.py -o output_dir -l -p 4 iasi IASI-L1.nc IASI-L2.nc
```

## Common errors <a name="Common errors"></a>

Make sure that the template [input](https://git.scc.kit.edu/jz9384/iasi-processing/blob/master/input) directory looks similar to the following before you run the program

```bash
input/
├── INP_FWD
│   ├── species.inf
│   ├── Tclim.dat
│   └── voigttable.dat
├── INP_INV
│   ├── INTERCV9.INP
│   └── PROFFIT9.INP
├── OUT_FWD
├── OUT_INV
├── proffi96
└── wrk_fwd
```

Make sure that the template [input](https://git.scc.kit.edu/jz9384/iasi-processing/blob/master/input) directory and the local processing exe are correctly specified in the [proffi-configuration.cfg](https://git.scc.kit.edu/jz9384/iasi-processing/blob/master/proffi-processing.cfg) configuration file.

```bash
...

[processing]
input_path = input
local_exe = proffi96

...
```

Make sure that you have downloaded the [HITRAN](#HITRAN), [SRTM](#SRTM) and [inv_heights](#inv_heights) files and all files are correctly specified in the [proffi-configuration.cfg](https://git.scc.kit.edu/jz9384/iasi-processing/blob/master/proffi-processing.cfg) configuration file.

[see](#Configuration)

## TL;DR <a name="TL;DR"></a>
```bash
sudo apt install gdal-bin build-essential python3 python3-virtualenv python3-dev
git clone https://git.scc.kit.edu/jz9384/iasi-processing.git
cd iasi-processing/
python3 -m venv env
source env/bin/activate
pip install --upgrade numpy pandas netCDF4 julian timezonefinder aiohttp psutil
python proffi-processing.py --help
```
