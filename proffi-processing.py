import os
import sys
import glob
import shutil
import logging
import traceback
import netCDF4

import numpy as np

from datetime import datetime
from argparse import ArgumentParser, RawDescriptionHelpFormatter

from proffi_processing import preprocessing, processing, postprocessing, utils
from proffi_processing import config

# import warnings
# warnings.filterwarnings("ignore", message="numpy.dtype size changed")
# warnings.filterwarnings("ignore", message="numpy.ufunc size changed")

logging.captureWarnings(True)
logger = logging.getLogger('proffi-processing')


def _setup_logging(args):
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter(('%(asctime)s %(module)s %(lineno)d '
                                   '%(levelname)s  %(message)s'),
                                  datefmt='%m/%d/%Y %I:%M:%S %p')

    file_handler = logging.FileHandler(args.log, 'w')
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)

    logger.addHandler(file_handler)


def _generate_netcdf(args):
    config.read(args.config)
    input_glob = glob.glob(args.path)
    for path in input_glob:
        #  if not os.path.exists(path):
        #      print('[ERROR] could not open path {}'.format(path))
        #      continue

        logger.debug('generate netcdf from results {}'.format(path))
        results_path = os.path.normpath(path)
        postprocessing.noise = \
            np.genfromtxt(os.path.abspath(config.get('iasi', 'noise')))
        postprocessing.to_netcdf(results_path, os.path.join(args.output,
                                 os.path.basename(results_path)) + '.nc',
                                 processes=args.processes)


def _orbit(iasi_netcdf):
    orbit = None
    try:
        orbit = iasi_netcdf.split('_')[-4]
    except Exception as e:
        logger.error('could not parse orbit from {}'.format(iasi_netcdf))
    return orbit


def _combine_iasi(args):
    config.read(args.config)
    input_l1_glob = args.l1  # glob.glob(args.l1, recursive=True)
    input_l2_glob = args.l2  # glob.glob(args.l2, recursive=True)

    input_data = [(l1, l2) for l1 in input_l1_glob for l2 in input_l2_glob
                  if _orbit(l1) == _orbit(l2)]

    netcdf_file = os.path.join(args.output, args.file)
    os.makedirs(args.output, exist_ok=True)

    for l1, l2 in input_data:
        logger.debug("processing {}".format(l1))
        logger.debug("processing {}".format(l2))

        nc_l1 = netCDF4.Dataset(l1, 'r')
        date = datetime.strptime(nc_l1.start_sensing_data_time,
                                 '%Y%m%d%H%M%SZ').strftime('%Y%m%d')
        nc_l1.close()

        try:
            preprocessing. iremis = utils.load_iremis_data_opt(date,
                config.get('iremis', 'data_directory'))
        except Exception as ex:
            logger.error("iremis error {}".format(ex))

        preprocessing.from_l1_l2_input_netcdf(l1, l2, netcdf_file)


def _process_netcdf(args):
    config.read(args.config)
    # check mandatory files
    mandatory_files = [
        config.get('hitran', 'h2o'), config.get('hitran', 'hdo'),
        config.get('hitran', 'co2'), config.get('hitran', 'n2o'),
        config.get('hitran', 'ch4'), config.get('hitran', 'hno3'),
        config.get('inv_heights', 'path'),
        config.get('iasi', 'noise'), config.get('iasi', 'cof_ems'),
        config.get('processing', 'input_path'),
        os.path.join(config.get('processing', 'input_path'),
                     config.get('processing', 'local_exe'))
        ]
    for f in mandatory_files:
        if not os.path.exists(f):
            print('[ERROR] {} from {} not found'.format(f, args.config))
            raise FileNotFoundError(f)

    input_glob = glob.glob(args.input)  # , recursive=True)
    input_data = input_glob
    if not input_data:
        print('[ERROR] no input netCDF found')
        raise FileNotFoundError()
    for data in input_data:
        logger.debug("processing {}".format(data))
        netcdf_name = os.path.basename(data).split('.')[0]
        out_name = netcdf_name + datetime.now().strftime('_%Y%m%d%H%M%S')

        output_dir = os.path.join(args.output, out_name)
        os.makedirs(output_dir, exist_ok=True)

        preprocessing.from_input_netcdf(data, output_dir,
                                        processes=args.processes)

        if args.local:
            postprocessing.noise = \
                np.genfromtxt(os.path.abspath(config.get('iasi', 'noise')))
            processing.process_locally(output_dir, processes=args.processes)
            postprocessing.to_netcdf(output_dir, output_dir + '.nc',
                                     processes=args.processes)
        # if args.remote:
        #    processing.process_remotely(output_dir, config=conf)
        if args.clean:
            shutil.rmtree(output_dir, ignore_errors=True)


if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=RawDescriptionHelpFormatter,
                            description=("""PROFFI processing\n\n
(C) Karlsruhe Institute of Technology (KIT)\n
    Institute of Meteorology and Climate Research (IMK)\n
    Steinbuch Centre for Computing (SCC)\n\n
Run PROFFI data processor"""))
    subparsers = parser.add_subparsers()

    parser_netcdf = subparsers.add_parser('netcdf',
                                          help='process netCDF input data')
    parser_netcdf.add_argument('input', type=str,
                               help='input netCDF data path')
    parser_netcdf.set_defaults(func=_process_netcdf)

    parser_iasi = subparsers.add_parser('iasi',
                                        help='combine IASI L1 L2 data')
    parser_iasi.add_argument('-l1', nargs='+', type=str,
                             help='input netCDF level1 data')
    parser_iasi.add_argument('-l2', nargs='+', type=str,
                             help='input netCDF level2 data')
    parser_iasi.add_argument('--file', type=str,
                             help='output L1C+L2 netcdf file', required=True)
    parser_iasi.set_defaults(func=_combine_iasi)

    parser_results =\
        subparsers.add_parser('results',
                              help='generate netcdf file from results')
    parser_results.add_argument('path', type=str,
                                help='path to results')
    parser_results.set_defaults(func=_generate_netcdf)

    parser.add_argument('-c', '--config', type=str,
                        default='proffi-processing.cfg',
                        help=('configuration file '
                              '(default: proffi-processing.cfg)'))
    parser.add_argument('--log', type=str,
                        default='proffi-processing.log',
                        help='log file (default: proffi-processing.log)')
    parser.add_argument('-o', '--output', type=str,
                        default='out',
                        help='output directory (default: out/)')
    parser.add_argument('-l', '--local', action='store_true',
                        help='enalbe local processing')
    # parser.add_argument('-r', '--remote', action='store_true',\
    #    help='enable remote processing')
    parser.add_argument('--clean', action='store_true',
                        help='remove preprocessed data after processing')
    parser.add_argument('-p', '--processes', type=int,
                        default=None,
                        help='maximum number of processes')
    parser.set_defaults(func=lambda _: True)
    args = parser.parse_args()

    try:
        _setup_logging(args)
        args.func(args)
    except KeyboardInterrupt:
        print()
    except Exception as e:
        traceback.print_exc()
        sys.exit(1)

    sys.exit(0)
