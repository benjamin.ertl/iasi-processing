#!/bin/bash
#MSUB -l signal=15@60
#MSUB -N proffi_processing
set -eux

cleanup(){
	printf "[%s] %s\n" "$(date +'%D %T')" "cleanup before sigterm"
	exit 1
}

trap 'cleanup' 15

printf "[%s] %s\n" "$(date +'%D %T')" "start proffi processing"

# unload all modules
module purge

# load python module
module load devel/python/3.5.2

# load own modules in ~/privatemodules
module load use.own
# load gdal
module load gdal
# load python virtualenv
module load python-env

# print loaded modules
module list 2>&1

# get processing workspace
my_workspace=$(ws_find proffi)

# which input subdir to use
input=missing
# which configuration to use
#conf=tfe.cfg

# set directory variables
# proffi-processing directory
proffi_dir=${my_workspace}/iasi-processing
# input directory for netCDF files
input_dir=${my_workspace}/input/${input}
# output directory for result netCDF files
output_dir=${my_workspace}/output
# log directory for proffi-processing logs
log_dir=${my_workspace}/log
# run directory for job scripts
run_dir=${my_workspace}/run

# create directories
mkdir -pv ${input_dir}
mkdir -pv ${output_dir}
mkdir -pv ${log_dir}
mkdir -pv ${run_dir}

# generate and run job scripts

nc_file=L1C+L2_20121116to20130515_IASI-A_KIR_nc
nc_name=L1C+L2_20121116to20130515_IASI-A_KIR
config_file=kir.cfg
job_file=${run_dir}/job_${nc_name}.sh
log_file=${nc_name}.log

printf "[%s] %s\n" "$(date +'%D %T')" "generate job script ${job_file}"
cat <<- EOF > ${job_file}
#!/bin/bash

trap 'exit' ERR

cd ${proffi_dir}
python proffi-processing.py -p 28 --conf "${config_file}" --log "\${TMP}/${log_file}" -o "\${TMP}/${nc_name}" -l netcdf "${input_dir}/${nc_file}"
cp -pr \${TMP}/${log_file} ${log_dir}
cp -pr \${TMP}/${nc_name}/*.nc ${output_dir}
EOF
chmod +x ${job_file}
printf "[%s] %s\n" "$(date +'%D %T')" "start srun -N 1 -n 1 -c 28 ${job_file}"
srun -N 1 -n 1 -c 28 ${job_file} &

nc_file=L1C+L2_20130201to20130329_IASI-A_KAR_nc
nc_name=L1C+L2_20130201to20130329_IASI-A_KAR
config_file=kar.cfg
job_file=${run_dir}/job_${nc_name}.sh
log_file=${nc_name}.log

printf "[%s] %s\n" "$(date +'%D %T')" "generate job script ${job_file}"
cat <<- EOF > ${job_file}
#!/bin/bash

trap 'exit' ERR

cd ${proffi_dir}
python proffi-processing.py -p 28 --conf "${config_file}" --log "\${TMP}/${log_file}" -o "\${TMP}/${nc_name}" -l netcdf "${input_dir}/${nc_file}"
cp -pr \${TMP}/${log_file} ${log_dir}
cp -pr \${TMP}/${nc_name}/*.nc ${output_dir}
EOF
chmod +x ${job_file}
printf "[%s] %s\n" "$(date +'%D %T')" "start srun -N 1 -n 1 -c 28 ${job_file}"
srun -N 1 -n 1 -c 28 ${job_file} &

wait

printf "[%s] %s\n" "$(date +'%D %T')" "finished all srun jobs"
