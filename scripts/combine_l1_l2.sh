#!/bin/bash
set -eux

input_dir=/home/benjamin/KIT/IMK/iasi-processing/data/lsdf/global_20160627_20160720
l1_dir=${input_dir}/l1
l2_dir=${input_dir}/l2

arr_l1=(${l1_dir}/*.nc)

for ((i=0; i<${#arr_l1[@]}; i++)); do
#for ((i=0; i<3; i++)); do
    nc_name=${arr_l1[$i]##*/}
    nc_name=${nc_name%.nc}

    IFS='_' read -a nc <<< "$nc_name"

    date=${nc[4]}
    orbit=${nc[5]}
    prefix=${nc[1]}

    IFS=',' read -a sat <<< "$prefix"

    metop=${sat[2]%+*}

    l1=${arr_l1[$i]}
    l2=$(find ${l2_dir} -name *_${orbit}_*.nc)

    if [ -z "$l2" ]
    then
        printf "[%s] %s\n" "$(date +'%D %T')" "missing level 2 data for orbit ${orbit}"
    else
        output=data/lsdf/global_20160627_20160720/l1+l2/
        output_file=L1C+L2_${metop}_${date}_${orbit}.nc
        log=data/lsdf/global_20160627_20160720/l1+l2_log/${orbit}.log
        python proffi-processing.py --output ${output} --log ${log} iasi -l1 ${l1} -l2 ${l2} --file ${output_file}
    fi
done
