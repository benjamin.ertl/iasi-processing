#!/bin/bash
# generate HIPPO 1 - 5 data

python proffi-processing.py --log hippo1.log -o hippo1 iasi 'data/share/IASI_Benjamin/HIPPO/NEU_v180105/EUMETSAT/L1C/W_XX-EUMETSAT-Darmstadt,HYPERSPECT+SOUNDING,METOPA+IASI_C_EUMP_200901*' 'data/share/IASI_Benjamin/HIPPO/NEU_v180105/EUMETSAT/L2/W_XX-EUMETSAT-Darmstadt,HYPERSPECT+SOUNDING,METOPA+IASI_C_EUMP_200901*' L1C+L2_HIPPO1_20090109-20090130.nc
python -c 'from proffi_processing.utils import split_combined_iasi_netcdf; split_combined_iasi_netcdf("hippo1/L1C+L2_HIPPO1_20090109-20090130.nc")'
pymv hippo1/* data/share/IASI_Benjamin/HIPPO/hippo1/

python proffi-processing.py --log hippo2.log -o hippo2 iasi 'data/share/IASI_Benjamin/HIPPO/NEU_v180105/EUMETSAT/L1C/W_XX-EUMETSAT-Darmstadt,HYPERSPECT+SOUNDING,METOPA+IASI_C_EUMP_20091*' 'data/share/IASI_Benjamin/HIPPO/NEU_v180105/EUMETSAT/L2/W_XX-EUMETSAT-Darmstadt,HYPERSPECT+SOUNDING,METOPA+IASI_C_EUMP_20091*' L1C+L2_HIPPO2_20091031-20091123.nc
python -c 'from proffi_processing.utils import split_combined_iasi_netcdf; split_combined_iasi_netcdf("hippo2/L1C+L2_HIPPO2_20091031-20091123.nc")'
pymv hippo2/* data/share/IASI_Benjamin/HIPPO/hippo2/

python proffi-processing.py --log hippo3.log -o hippo3 iasi 'data/share/IASI_Benjamin/HIPPO/NEU_v180105/EUMETSAT/L1C/W_XX-EUMETSAT-Darmstadt,HYPERSPECT+SOUNDING,METOPA+IASI_C_EUMP_20100*' 'data/share/IASI_Benjamin/HIPPO/NEU_v180105/EUMETSAT/L2/W_XX-EUMETSAT-Darmstadt,HYPERSPECT+SOUNDING,METOPA+IASI_C_EUMP_20100*' L1C+L2_HIPPO3_20100324-20100417.nc
python -c 'from proffi_processing.utils import split_combined_iasi_netcdf; split_combined_iasi_netcdf("hippo3/L1C+L2_HIPPO3_20100324-20100417.nc")'
pymv hippo3/* data/share/IASI_Benjamin/HIPPO/hippo3/

python proffi-processing.py --log hippo4.log -o hippo4 iasi 'data/share/IASI_Benjamin/HIPPO/NEU_v180105/EUMETSAT/L1C/W_XX-EUMETSAT-Darmstadt,HYPERSPECT+SOUNDING,METOPA+IASI_C_EUMP_20110[6..7]*' 'data/share/IASI_Benjamin/HIPPO/NEU_v180105/EUMETSAT/L2/W_XX-EUMETSAT-Darmstadt,HYPERSPECT+SOUNDING,METOPA+IASI_C_EUMP_20110[6..7]*' L1C+L2_HIPPO4_20110614-201100711.nc
python -c 'from proffi_processing.utils import split_combined_iasi_netcdf; split_combined_iasi_netcdf("hippo4/L1C+L2_HIPPO4_20110614-201100711.nc")'
pymv hippo4/* data/share/IASI_Benjamin/HIPPO/hippo4/

python proffi-processing.py --log hippo5.log -o hippo5 iasi 'data/share/IASI_Benjamin/HIPPO/NEU_v180105/EUMETSAT/L1C/W_XX-EUMETSAT-Darmstadt,HYPERSPECT+SOUNDING,METOPA+IASI_C_EUMP_20110[8..9]*' 'data/share/IASI_Benjamin/HIPPO/NEU_v180105/EUMETSAT/L2/W_XX-EUMETSAT-Darmstadt,HYPERSPECT+SOUNDING,METOPA+IASI_C_EUMP_20110[8..9]*' L1C+L2_HIPPO5_20110816-20110909.nc
python -c 'from proffi_processing.utils import split_combined_iasi_netcdf; split_combined_iasi_netcdf("hippo5/L1C+L2_HIPPO5_20110816-20110909.nc")'
pymv hippo5/* data/share/IASI_Benjamin/HIPPO/hippo5/
