# PROFFI96 processing for IASI data
```bash
$ python proffi-processing.py --help
usage: proffi-processing.py [-h] [-c CONFIG] [--log LOG] [-o OUTPUT] [-l] [-r]
                            [--clean] [-p PROCESSES]
                            {netcdf,iasi,results} ...

PROFFI processing

(C) Karlsruhe Institute of Technology (KIT)
    Institute of Meteorology and Climate Research (IMK)
    Steinbuch Centre for Computing (SCC)

Run PROFFI data processor

positional arguments:
  {netcdf,iasi,results}
    netcdf              process netCDF input data
    iasi                process IASI L1 L2 data
    results             generate netcdf file from results

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG
                        configuration file (default: proffi-processing.cfg)
  --log LOG             log file (default: proffi-processing.log)
  -o OUTPUT, --output OUTPUT
                        output directory (default: out/)
  -l, --local           enalbe local processing
  -r, --remote          enable remote processing
  --clean               remove preprocessed data after processing
  -p PROCESSES, --processes PROCESSES
                        maximum number of processes
```
## Documentation

[Wiki](https://git.scc.kit.edu/jz9384/iasi-processing/wikis/home)

## Requirements

### System libraries (apt)
sudo apt install gdal-bin build-essential python3 python3-virtualenv python3-dev

### Python libraries (pip)
pip install --upgrade numpy pandas netCDF4 julian timezonefinder aiohttp psutil

